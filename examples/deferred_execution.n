# Copyright 2018 N-Lang Project Authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import "std/io.n"a;

func foo : () -> void
{
    defer{println($"DEFER INSIDE A FUNCTION WITH A RETURN STATEMENT"a);}
    return;
    println($"THIS WILL NEVER BE PRINTED!!!"a);
    defer{println($"THIS WILL ALSO NEVER BE PRINTED!!!"a);}
}

func bar : () -> s32
{
    defer{println($"DEFER INSIDE ANOTHER FUNCTION WITH A RETURN STATEMENT"a);}
    return 123s32;
    println($"THIS WILL NEVER BE PRINTED!!!"a);
    defer{println($"THIS WILL ALSO NEVER BE PRINTED!!!"a);}
}

func entry : () -> void
{
    defer
    {
        defer{println($"D"a);}
        defer{println($"C"a);}
        println($"B"a);
    }

    foo();
    bar();

    loop true
    {
        defer{println($"DEFER INSIDE A LOOP WITH A BREAK STATEMENT"a);}
        break;
        println($"THIS WILL NEVER BE PRINTED!!!"a);
        defer{println($"THIS WILL ALSO NEVER BE PRINTED!!!"a);}
    }

    loop i : mut u in [1u, 3u]
    {
        defer{println($"DEFER INSIDE A LOOP WITH A CONTINUE STATEMENT"a);}
        continue;
        println($"THIS WILL NEVER BE PRINTED!!!"a);
        defer{println($"THIS WILL ALSO NEVER BE PRINTED!!!"a);}
    }

    println($"How does the alphabet start again?"a);
    println($"A"a);
}
