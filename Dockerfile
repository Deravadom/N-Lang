# Copyright 2018 The N Language Authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

FROM ubuntu:latest

RUN apt-get update
RUN apt-get install -y clang
RUN apt-get install -y make
RUN apt-get install -y binutils
RUN apt-get install -y gcc
RUN apt-get install -y sudo
RUN apt-get install -y gdb
RUN apt-get install -y doxygen
RUN apt-get install -y valgrind
RUN apt-get install -y clang-format

COPY . /app/nc
WORKDIR /app/nc
RUN make
RUN make install

RUN useradd -ms /bin/bash user
RUN chown -R user:user /home/user
RUN chown -R user:user /home/user
USER user
VOLUME /home/user
WORKDIR /home/user

