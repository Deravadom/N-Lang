N_LANG_SPEC = 0.0

COMPILER_NAME     = nompile
COMPILER_VERSION  = 0.0.0

N_ROOT_DIR       = .
N_SRC_DIR        = $(N_ROOT_DIR)/src
N_TEST_DIR       = $(N_ROOT_DIR)/test
N_DEPS_DIR       = $(N_ROOT_DIR)/deps
N_BUILD_DIR      = $(N_ROOT_DIR)/build
N_BUILD_LIB_DIR  = $(N_BUILD_DIR)/lib
N_BUILD_OBJ_DIR  = $(N_BUILD_DIR)/obj
N_BUILD_BIN_DIR  = $(N_BUILD_DIR)/bin
N_BUILD_DOCS_DIR = $(N_BUILD_DIR)/docs

LIBN_DIR = $(N_ROOT_DIR)/libn

PREFIX                 = /usr/local
N_BIN_INSTALL_DIR      = $(PREFIX)/bin
N_INCLUDE_INSTALL_DIR  = $(PREFIX)/include/libn
N_DEFAULT_INCLUDE_PATH = $(N_INCLUDE_INSTALL_DIR)

UNAME_S = $(shell uname -s)

##########

# Compiler setup.
CC = clang
CFLAGS := -std=gnu11 \
	-Wall -Wextra -Wconversion \
	-Werror=implicit-function-declaration \
	-Werror=incompatible-pointer-types \
	-fwrapv \
	-I$(N_SRC_DIR) -I$(N_DEPS_DIR) \
	-DN_LANG_SPEC_STR=\"$(N_LANG_SPEC)\" \
	-DCOMPILER_NAME_STR=\"$(COMPILER_NAME)\" \
	-DCOMPILER_VERSION_STR=\"$(COMPILER_VERSION)\" \
	-DN_BIN_INSTALL_DIR=$(N_BIN_INSTALL_DIR) \
	-DN_INCLUDE_INSTALL_DIR=$(N_INCLUDE_INSTALL_DIR) \
	-DN_DEFAULT_INCLUDE_PATH=$(N_DEFAULT_INCLUDE_PATH)

ifdef PROF
	CC = gcc
	CFLAGS += $(CFLAGS) -g -pg
endif

DEBUG_CFLAGS   = -DDEBUG -O0 -g
RELEASE_CFLAGS = -DNDEBUG -O3 -Werror
GOLDEN_CFLAGS  = -DDEBUG -O0 -g

##########

COMPILER_BUILD_OBJ_DIR       = $(N_BUILD_OBJ_DIR)/compiler
COMPILER_COMPILED_OUTPUT_DIR = $(N_BUILD_DIR)/compiled

COMPILER_SRC_DIR        = $(N_SRC_DIR)/compiler
COMPILER_SRC_FILES      = $(wildcard $(COMPILER_SRC_DIR)/*.c)
COMPILER_SRC_OBJ_FILES_ = $(patsubst %.c,%.o,$(COMPILER_SRC_FILES))
COMPILER_SRC_OBJ_FILES  = $(subst $(N_SRC_DIR)/,$(N_BUILD_OBJ_DIR)/,$(COMPILER_SRC_OBJ_FILES_))
COMPILER_OUT_NAME       = $(COMPILER_NAME)
COMPILER_OUT_FILE       = $(N_BUILD_BIN_DIR)/$(COMPILER_OUT_NAME)
COMPILER_LINK_LIBS      = pthread
ifeq ($(UNAME_S),FreeBSD)
	COMPILER_LINK_LIBS += execinfo
endif
COMPILER_LINK_LIBS_PREFIXED = $(addprefix -l, $(COMPILER_LINK_LIBS))

COMPILER_TEST_DIR        = $(N_TEST_DIR)/compiler
COMPILER_TEST_FILES      = $(wildcard $(COMPILER_TEST_DIR)/*test.c)
COMPILER_TEST_OBJ_FILES_ = $(patsubst %.test.c,%.test.o,$(COMPILER_TEST_FILES))
COMPILER_TEST_OBJ_FILES  = $(subst $(N_TEST_DIR)/,$(N_BUILD_OBJ_DIR)/,$(COMPILER_TEST_OBJ_FILES_))
COMPILER_TEST_NAME       = compiler.tests
COMPILER_TEST_FILE       = $(N_BUILD_BIN_DIR)/$(COMPILER_TEST_NAME)
COMPILER_TEST_LINK_LIBS          = pthread
COMPILER_TEST_LINK_LIBS_PREFIXED = $(addprefix -l, $(COMPILER_TEST_LINK_LIBS))

PRETTY_PRINT_BIN = $(N_BUILD_BIN_DIR)/pretty_print

### MOST USED TARGETS ##########################################################

# The default make target should build everything necessary for a user to use
# the compiler and related tooling. Running the commands
#   $ make
#   $ make install
# should completely install everything the user needs to begin programming in N.
.PHONY: default
ifndef PROF
default: CFLAGS += $(RELEASE_CFLAGS)
endif
default: clean build_compiler

# Perform a build, and then run golden testing.
.PHONY: golden
golden: CFLAGS += $(GOLDEN_CFLAGS)
golden: clean build_compiler run_golden_tests

# Perform a full build using debug settings.
.PHONY: debug
debug: CFLAGS += $(DEBUG_CFLAGS)
debug: clean build_compiler build_compiler_test run_compiler_test golden docs

# Perform a full build using release settings.
.PHONY: release
release: CFLAGS += $(RELEASE_CFLAGS)
release: clean build_compiler build_compiler_test run_compiler_test golden docs

# Run the golden testing
.PHONY: golden
golden: CFLAGS += $(GOLDEN_FLAGS)
golden: clean build_compiler run_golden_tests

# Build and run all unit tests through Valgrind using debug settings.
.PHONY: memcheck_tests_debug
memcheck_tests_debug: CFLAGS += $(DEBUG_CFLAGS)
memcheck_tests_debug: build_compiler build_compiler_test run_memcheck_tests

# Build and run all unit tests through Valgrind using release settings.
.PHONY: memcheck_tests_release
memcheck_tests_release: CFLAGS += $(RELEASE_CFLAGS)
memcheck_tests_release: build_compiler build_compiler_test run_memcheck_tests

# Build and run all unit tests through Callgrind using debug settings.
.PHONY: callgrind_tests_debug
callgrind_tests_debug: CFLAGS += $(DEBUG_CFLAGS)
callgrind_tests_debug: run_callgrind_tests

# Install the compiler and tooling locally.
.PHONY: install
install: $(COMPILER_OUT_FILE)
	mkdir -p $(N_BIN_INSTALL_DIR)
	mkdir -p $(N_INCLUDE_INSTALL_DIR)
	cp $(COMPILER_OUT_FILE) $(N_BIN_INSTALL_DIR)
	cp -a $(LIBN_DIR)/. $(N_INCLUDE_INSTALL_DIR)

# Uninstall the compiler and tooling. Mirrored version of the `install`
# target.
.PHONY: uninstall
uninstall:
	rm $(N_BIN_INSTALL_DIR)/$(COMPILER_OUT_NAME)
	rm -r $(N_INCLUDE_INSTALL_DIR)

### MISC #######################################################################

.PHONY: clean
clean:
	@rm -f  a.c
	@rm -f ./*.out
	@rm -rf $(N_BUILD_DIR)

.PHONY: setup_build_dir
setup_build_dir:
	@mkdir -p $(N_BUILD_DIR)
	@mkdir -p $(N_BUILD_LIB_DIR)
	@mkdir -p $(N_BUILD_OBJ_DIR)
	@mkdir -p $(N_BUILD_BIN_DIR)

### COMPILER ###################################################################

.PHONY: build_compiler_dirs
build_compiler_dirs: setup_build_dir
	@mkdir -p $(COMPILER_BUILD_OBJ_DIR)
	@mkdir -p $(COMPILER_COMPILED_OUTPUT_DIR)

##########

$(COMPILER_BUILD_OBJ_DIR)/%.o: $(COMPILER_SRC_DIR)/%.c
	$(CC) -c -o $@ $(CFLAGS) $<

.PHONY: build_compiler
build_compiler: build_compiler_dirs $(COMPILER_SRC_OBJ_FILES)
	$(CC) -o $(COMPILER_OUT_FILE) $(CFLAGS) -rdynamic \
		$(COMPILER_SRC_OBJ_FILES) \
		-L$(N_BUILD_LIB_DIR) \
		$(COMPILER_LINK_LIBS_PREFIXED)

##########

$(COMPILER_BUILD_OBJ_DIR)/%.test.o: $(COMPILER_TEST_DIR)/%.test.c
	$(CC) -c -o $@ $(CFLAGS) $<

.PHONY: build_compiler_test
build_compiler_test: $(COMPILER_TEST_OBJ_FILES)
	echo $(COMPILER_NAME)/main.o
	$(CC) -o $(COMPILER_TEST_FILE) $(CFLAGS) -rdynamic \
		$(COMPILER_TEST_OBJ_FILES) \
		$(filter-out $(N_BUILD_OBJ_DIR)/compiler/main.o,$(COMPILER_SRC_OBJ_FILES)) \
		-L$(N_BUILD_LIB_DIR) \
		$(COMPILER_TEST_LINK_LIBS_PREFIXED)

.PHONY: run_compiler_test
run_compiler_test: build_compiler_test
	$(COMPILER_TEST_FILE)

### PRETTY PRINT ###############################################################
.PHONY: build_pretty_print_grammar
build_pretty_print_grammar: setup_build_dir
	@$(CC) -o $(PRETTY_PRINT_BIN) $(CFLAGS) \
		$(N_SRC_DIR)/pretty_print_grammar/pretty_print_grammar.c \
		$(N_SRC_DIR)/compiler/nutils.c \
		$(N_SRC_DIR)/compiler/token.c \
		$(N_SRC_DIR)/compiler/grammar.c

.PHONY: pretty_print_grammar
pretty_print_grammar: build_pretty_print_grammar
	@$(PRETTY_PRINT_BIN)

### VALGRIND ###################################################################

.PHONY: run_memcheck_tests
run_memcheck_tests: build_compiler_test
	valgrind --leak-check=yes --leak-check=full --show-leak-kinds=all --track-origins=yes $(COMPILER_TEST_FILE)

.PHONY: run_callgrind_tests
run_callgrind_tests: build_compiler_test
	valgrind -v --tool=callgrind --simulate-cache=yes $(COMPILER_TEST_FILE)

### GOLDEN TESTING #############################################################

run_golden_tests: build_compiler
	./test/golden.sh --all

### DOCUMENTATION ##############################################################

.PHONY: build_docs_dirs
build_docs_dirs: setup_build_dir
	@mkdir -p $(N_BUILD_DOCS_DIR)

.PHONY: docs
docs: build_docs_dirs
	@doxygen Doxyfile > /dev/null
