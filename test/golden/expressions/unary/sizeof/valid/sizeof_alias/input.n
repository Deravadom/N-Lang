struct foo
{
    let bar : u32;
    let baz : ascii;
}

alias bar = foo;

func entry : () -> u
{
    return sizeof(bar);
}
