introduce struct foo;
introduce func foo_get_bar : (this : ^ foo) -> u;
struct foo
{
    let bar : u;
    func get_bar = foo_get_bar;
}

func foo_get_bar : (this : ^ foo) -> u
{
    return this->bar;
}

func entry : () -> void
{
    let myfoo : foo;
    # TODO: this test was not invoked by the EMU tests because it produces
    # a C error instead of an N error.
    # myfoo.get_bar;
}
