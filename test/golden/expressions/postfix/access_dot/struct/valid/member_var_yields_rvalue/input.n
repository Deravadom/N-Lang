struct foo
{
    let bar : mut u;
}

func entry : () -> void
{
    let myfoo : foo;
    let myu : u = myfoo.bar;
}
