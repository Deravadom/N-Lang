introduce struct foo;
introduce func foo_get_bar : (this : ^ foo) -> u;

struct foo
{
    let bar : mut u;
    func get_bar = foo_get_bar;
}

func foo_get_bar : (this : ^ foo) -> u
{
    return this->bar;
}

func entry : () -> void
{
    let myfoo : foo;
    let myu : u = myfoo.get_bar();
}
