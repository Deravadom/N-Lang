struct foo
{
    let bar : mut u;
}

func entry : () -> void
{
    let myfoo : foo;
    let myu : u = 12u;
    myfoo.bar = myu;
}
