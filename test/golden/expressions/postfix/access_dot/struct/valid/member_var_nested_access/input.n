struct foo
{
    let bar : mut u;
}

struct goo
{
    let hoo : mut foo;
    let euu : mut u;
}

func entry : () -> void
{
    let mygoo : goo;
    let myu : u = mybar.hoo.bar;
    mygoo.eoo = myu;
}
