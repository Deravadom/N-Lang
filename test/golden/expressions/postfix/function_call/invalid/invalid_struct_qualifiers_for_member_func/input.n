introduce struct foo;
func foo_member_func : (this : ^ mut foo) -> void { }

struct foo
{
    let a : s32;
    let b : s64;
    func member_func = foo_member_func;
}

func entry : () -> void
{
    # declare non-mut foo.
    let bar : foo;

    # pass non-mut foo to mut member_func:this.
    bar.member_func();
}
