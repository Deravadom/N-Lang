func foo : (a : s, b : s) -> void { }

func entry : () -> void
{
    foo(11s);
}
