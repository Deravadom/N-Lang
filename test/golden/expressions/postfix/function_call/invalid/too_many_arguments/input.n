func foo : (a : s, b : s) -> void { }

func entry : () -> void
{
    foo (1s, 2s, 3s);
}
