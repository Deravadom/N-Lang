func foo : () -> s
{
    return 1s;
}

let bar : s = foo();

func entry : () -> void
{
    let mybar : s = bar;
}
