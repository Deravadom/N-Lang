struct foo
{
    let bar : mut u;
}

struct goo
{
    let hoo : mut ^ foo;
    let eoo : mut u;
}

func entry : () -> void
{
    let mygoo : ^mut goo;
    mygoo->eoo = mygoo->hoo->bar;
    mygoo->hoo->bar = mygoo->eoo;
}
