struct foo
{
    let bar : mut u;
}

func entry : () -> void
{
    let myfoo : ^mut foo;
    let myu : u = 123u;
    myfoo-> bar = myu;
}
