struct foo
{
    let bar : mut u;
}

func entry : () -> void
{
    let myfoo : mut foo;
    myfoo->bar;
}
