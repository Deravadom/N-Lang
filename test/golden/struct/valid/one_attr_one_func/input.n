introduce struct jawn;

introduce func do_jawn : (this : ^ jawn) -> void;

struct jawn
{
    let a : u;
    func do_jawn = do_jawn;
}
