introduce variant jawn;

introduce func do_jawn1 : (this : ^ jawn) -> void;
introduce func do_jawn2 : (this : ^ jawn, a : u) -> s;
introduce func do_jawn3 : (this : ^ jawn, b : ^ ascii) -> ^ ascii;

introduce func do_jawn1_tag_1 : (this : ^ jawn) -> void;
introduce func do_jawn2_tag_1 : (this : ^ jawn, a : s) -> u;
introduce func do_jawn3_tag_1 : (this : ^ jawn, b : ^ ascii) -> ^ ascii;

variant jawn {
    let a : bool;
    let b : s;
    let c : ^ ascii;

    func do_jawn1 = do_jawn1;
    func do_jawn2 = do_jawn2;
    func do_jawn3 = do_jawn3;

    tag_1
    {
        let d : bool;
        let e : u;
        let f : ^ ascii;

        func do_jawn1_tag_1 = do_jawn1_tag_1;
        func do_jawn2_tag_2 = do_jawn2_tag_1;
        func do_jawn3_tag_3 = do_jawn3_tag_1;
    }
}
