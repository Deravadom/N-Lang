introduce variant jawn;

introduce func do_jawn : (this : ^ jawn) -> void;
introduce func do_jawn_tag_1 : (this : ^ jawn) -> void;

variant jawn
{
    let a : u;
    func do_jawn = do_jawn;

    tag_1
    {
        let b : u;
        func do_jawn_tag_1 = do_jawn_tag_1;
    }
}
