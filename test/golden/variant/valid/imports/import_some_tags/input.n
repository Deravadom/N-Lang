import "some_tags.n"a;

let j : mut jawn;

func entry : () -> void
{
    j.a = $"jawn"a;
    j.print();
    j.tag_1.b = $"jawn b"a;
    j.tag_1.print();
}
