import "std/io.n"a;

export introduce variant jawn;

export introduce func jawn_print : (this : ^ jawn) -> void;
export introduce func jawn_tag_1_print : (this : ^ jawn) -> void;

export variant jawn
{
    let a : mut ^ ascii;
    func print = jawn_print;
    tag_1
    {
        let b : mut ^ ascii;
        func print = jawn_tag_1_print;
    }
}

export func jawn_print : (this : ^ jawn) -> void
{
    println(this->a);
}

export func jawn_tag_1_print : (this : ^ jawn) -> void
{
    println(this->tag_1.b);
}

