introduce variant jawn;

introduce func do_jawn : (this : ^ jawn) -> void;

variant jawn
{
    let a : u;
    func do_jawn = do_jawn;
}
