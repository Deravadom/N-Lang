
/* variant */ struct jawn
{
    n_bool  const (a);
    n_s  const (b);
    n_ascii  const *  const(c);
    n_u  (tag);

    /* tags */ union
    {
        /* tag */ struct
        {
            n_bool  const (d);
            n_u  const (e);
            n_ascii  const *  const(f);
        } tag_1a;
        /* tag */ struct
        {
            n_bool  const (d);
            n_u  const (e);
            n_ascii  const *  const(f);
        } tag_1b;
        /* tag */ struct
        {
            n_ascii  const (d);
            n_s  const (e);
            n_u64  const (f);
        } tag_2a;
        /* tag */ struct
        {
            n_ascii  const (d);
            n_s  const (e);
            n_u64  const (f);
        } tag_2b;
        /* tag */ struct
        {
            n_s32  const (d);
            n_u16  const (e);
            n_ascii  const *  const(f);
        } tag_3a;
        /* tag */ struct
        {
            n_s32  const (d);
            n_u16  const (e);
            n_ascii  const *  const(f);
        } tag_3b;
    };
};

