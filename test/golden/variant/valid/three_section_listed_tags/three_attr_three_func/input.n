introduce variant jawn;

introduce func do_jawn1 : (this : ^ jawn) -> void;
introduce func do_jawn2 : (this : ^ jawn, a : u) -> s;
introduce func do_jawn3 : (this : ^ jawn, b : ^ ascii) -> ^ ascii;

introduce func do_jawn1_tag_1 : (this : ^ jawn) -> void;
introduce func do_jawn2_tag_1 : (this : ^ jawn, a : s) -> u;
introduce func do_jawn3_tag_1 : (this : ^ jawn, b : ^ ascii) -> ^ ascii;

introduce func do_jawn1_tag_2 : (this : ^ jawn) -> void;
introduce func do_jawn2_tag_2 : (this : ^ jawn, a : u) -> bool;
introduce func do_jawn3_tag_2 : (this : ^ jawn, b : ^ ascii) -> s;

introduce func do_jawn1_tag_3 : (this : ^ jawn) -> void;
introduce func do_jawn2_tag_3 : (this : ^ jawn, a : s64) -> bool;
introduce func do_jawn3_tag_3 : (this : ^ jawn, b : bool) -> ^ ascii;

variant jawn {
    let a : bool;
    let b : s;
    let c : ^ ascii;

    func do_jawn1 = do_jawn1;
    func do_jawn2 = do_jawn2;
    func do_jawn3 = do_jawn3;

    tag_1a, tag_1b
    {
        let d : bool;
        let e : u;
        let f : ^ ascii;

        func do_jawn1_tag_1 = do_jawn1_tag_1;
        func do_jawn2_tag_1 = do_jawn2_tag_1;
        func do_jawn3_tag_1 = do_jawn3_tag_1;
    }

    tag_2a, tag_2b
    {
        let d : s32;
        let e : bool;
        let f : ascii;

        func do_jawn1_tag_2 = do_jawn1_tag_2;
        func do_jawn2_tag_2 = do_jawn2_tag_2;
        func do_jawn3_tag_2 = do_jawn3_tag_2;
    }

    tag_3a, tag_3b
    {
        let d : s8;
        let e : u16;
        let f : s32;

        func do_jawn1_tag_3 = do_jawn1_tag_3;
        func do_jawn2_tag_3 = do_jawn2_tag_3;
        func do_jawn3_tag_3 = do_jawn3_tag_3;
    }
}
