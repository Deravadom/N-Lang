variant jawn {
    let a : bool;
    let b : s;
    let c : ^ ascii;

    tag_1a, tag_1b
    {
        let d : bool;
        let e : u;
        let f : ^ ascii;
    }

    tag_2a, tag_2b
    {
        let d : ascii;
        let e : s;
        let f : u64;
    }

    tag_3a, tag_3b
    {
        let d : s32;
        let e : u16;
        let f : ^ ascii;
    }
}
