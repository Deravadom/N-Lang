
/* variant */ struct jawn
{
    n_u  const (a);
    n_u16  (tag);

    /* tags */ union
    {
        /* tag */ struct
        {
            n_s  const (b);
        } tag_1a;
        /* tag */ struct
        {
            n_s  const (b);
        } tag_1b;
        /* tag */ struct
        {
            n_u  const (c);
        } tag_2a;
        /* tag */ struct
        {
            n_u  const (c);
        } tag_2b;
        /* tag */ struct
        {
            n_s  const (d);
        } tag_3a;
        /* tag */ struct
        {
            n_s  const (d);
        } tag_3b;
    };
};

