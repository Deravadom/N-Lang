variant jawn1 : u8
{
    let a : u;

    tag_1 : 5u8
    {
        let b : s;
    }

    tag_2 : 7u8
    {
        let c : u;
    }

    tag_3 : 9u8
    {
        let d : s;
    }
}

variant jawn2 : u16
{
    let a : u;

    tag_1 : 5u16
    {
        let b : s;
    }

    tag_2 : 7u16
    {
        let c : u;
    }

    tag_3 : 9u16
    {
        let d : s;
    }
}

variant jawn3 : u32
{
    let a : u;

    tag_1 : 5u32
    {
        let b : s;
    }

    tag_2 : 7u32
    {
        let c : u;
    }

    tag_3 : 9u32
    {
        let d : s;
    }
}

variant jawn4 : u64
{
    let a : u;

    tag_1 : 5u64
    {
        let b : s;
    }

    tag_2 : 7u64
    {
        let c : u;
    }

    tag_3 : 9u64
    {
        let d : s;
    }
}

variant jawn5 : u
{
    let a : u;

    tag_1 : 5u
    {
        let b : s;
    }

    tag_2 : 7u
    {
        let c : u;
    }

    tag_3 : 9u
    {
        let d : s;
    }
}

variant jawn6 : s8
{
    let a : u;

    tag_1 : 5s8
    {
        let b : s;
    }

    tag_2 : 7s8
    {
        let c : u;
    }

    tag_3 : 9s8
    {
        let d : s;
    }
}

variant jawn7: s16
{
    let a : u;

    tag_1 : 5s16
    {
        let b : s;
    }

    tag_2 : 7s16
    {
        let c : u;
    }

    tag_3 : 9s16
    {
        let d : s;
    }
}

variant jawn8 : s32
{
    let a : u;

    tag_1 : 5s32
    {
        let b : s;
    }

    tag_2 : 7s32
    {
        let c : u;
    }

    tag_3 : 9s32
    {
        let d : s;
    }
}

variant jawn9 : s64
{
    let a : u;

    tag_1 : 5s64
    {
        let b : s;
    }

    tag_2 : 7s64
    {
        let c : u;
    }

    tag_3 : 9s64
    {
        let d : s;
    }
}

variant jawn10 : s
{
    let a : u;

    tag_1 : 5s
    {
        let b : s;
    }

    tag_2 : 7s
    {
        let c : u;
    }

    tag_3 : 9s
    {
        let d : s;
    }
}
