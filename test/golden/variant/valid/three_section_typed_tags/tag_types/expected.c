
/* variant */ struct jawn1
{
    n_u  const (a);
    n_u8  (tag);

    /* tags */ union
    {
        /* tag */ struct
        {
            n_s  const (b);
        } tag_1;
        /* tag */ struct
        {
            n_u  const (c);
        } tag_2;
        /* tag */ struct
        {
            n_s  const (d);
        } tag_3;
    };
};

/* variant */ struct jawn2
{
    n_u  const (a);
    n_u16  (tag);

    /* tags */ union
    {
        /* tag */ struct
        {
            n_s  const (b);
        } tag_1;
        /* tag */ struct
        {
            n_u  const (c);
        } tag_2;
        /* tag */ struct
        {
            n_s  const (d);
        } tag_3;
    };
};

/* variant */ struct jawn3
{
    n_u  const (a);
    n_u32  (tag);

    /* tags */ union
    {
        /* tag */ struct
        {
            n_s  const (b);
        } tag_1;
        /* tag */ struct
        {
            n_u  const (c);
        } tag_2;
        /* tag */ struct
        {
            n_s  const (d);
        } tag_3;
    };
};

/* variant */ struct jawn4
{
    n_u  const (a);
    n_u64  (tag);

    /* tags */ union
    {
        /* tag */ struct
        {
            n_s  const (b);
        } tag_1;
        /* tag */ struct
        {
            n_u  const (c);
        } tag_2;
        /* tag */ struct
        {
            n_s  const (d);
        } tag_3;
    };
};

/* variant */ struct jawn5
{
    n_u  const (a);
    n_u  (tag);

    /* tags */ union
    {
        /* tag */ struct
        {
            n_s  const (b);
        } tag_1;
        /* tag */ struct
        {
            n_u  const (c);
        } tag_2;
        /* tag */ struct
        {
            n_s  const (d);
        } tag_3;
    };
};

/* variant */ struct jawn6
{
    n_u  const (a);
    n_s8  (tag);

    /* tags */ union
    {
        /* tag */ struct
        {
            n_s  const (b);
        } tag_1;
        /* tag */ struct
        {
            n_u  const (c);
        } tag_2;
        /* tag */ struct
        {
            n_s  const (d);
        } tag_3;
    };
};

/* variant */ struct jawn7
{
    n_u  const (a);
    n_s16  (tag);

    /* tags */ union
    {
        /* tag */ struct
        {
            n_s  const (b);
        } tag_1;
        /* tag */ struct
        {
            n_u  const (c);
        } tag_2;
        /* tag */ struct
        {
            n_s  const (d);
        } tag_3;
    };
};

/* variant */ struct jawn8
{
    n_u  const (a);
    n_s32  (tag);

    /* tags */ union
    {
        /* tag */ struct
        {
            n_s  const (b);
        } tag_1;
        /* tag */ struct
        {
            n_u  const (c);
        } tag_2;
        /* tag */ struct
        {
            n_s  const (d);
        } tag_3;
    };
};

/* variant */ struct jawn9
{
    n_u  const (a);
    n_s64  (tag);

    /* tags */ union
    {
        /* tag */ struct
        {
            n_s  const (b);
        } tag_1;
        /* tag */ struct
        {
            n_u  const (c);
        } tag_2;
        /* tag */ struct
        {
            n_s  const (d);
        } tag_3;
    };
};

/* variant */ struct jawn10
{
    n_u  const (a);
    n_s  (tag);

    /* tags */ union
    {
        /* tag */ struct
        {
            n_s  const (b);
        } tag_1;
        /* tag */ struct
        {
            n_u  const (c);
        } tag_2;
        /* tag */ struct
        {
            n_s  const (d);
        } tag_3;
    };
};

