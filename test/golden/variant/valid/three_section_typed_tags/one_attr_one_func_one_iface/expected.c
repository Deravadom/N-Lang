
/* variant */ struct jawn;

static  void   do_jawn(/* variant */ struct jawn *  const(this));
static  void   do_jawn_tag_1(/* variant */ struct jawn *  const(this));
static  void   do_jawn_tag_2(/* variant */ struct jawn *  const(this));
static  void   do_jawn_tag_3(/* variant */ struct jawn *  const(this));
 n_ascii  const *  const jawn_print_tag_1(/* variant */ struct jawn *  const(this));
 n_ascii  const *  const jawn_print_tag_2(/* variant */ struct jawn *  const(this));
 n_ascii  const *  const jawn_print_tag_3(/* variant */ struct jawn *  const(this));
/* variant */ struct jawn
{
    n_u  const (a);
    n_s32  (tag);

    /* tags */ union
    {
        /* tag */ struct
        {
            n_u  const (b);
        } tag_1;
        /* tag */ struct
        {
            n_u8  const (c);
        } tag_2;
        /* tag */ struct
        {
            n_u16  const (b);
        } tag_3;
    };
};
n_ascii  const *  const(n_variant_jawn_iface_print)(/* variant */ struct jawn *  const(n_iface_param_0))
{
    switch(n_iface_param_0->tag)
    {
    case (/*literal*/(n_s32)(3ULL)):
        return jawn_print_tag_1(n_iface_param_0);
    case (/*literal*/(n_s32)(5ULL)):
        return jawn_print_tag_2(n_iface_param_0);
    case (/*literal*/(n_s32)(7ULL)):
        return jawn_print_tag_3(n_iface_param_0);
    default:
        return n_null;
    }
}

 n_ascii  const *  const jawn_print_tag_1(/* variant */ struct jawn *  const(this))
{
    n_ascii  const * (___ret_value);
    /*return[0]*/___ret_value = n_null;
    /*return[1]*/goto ___scope_end_1;

    ___scope_end_1:;
    return ___ret_value;
}

 n_ascii  const *  const jawn_print_tag_2(/* variant */ struct jawn *  const(this))
{
    n_ascii  const * (___ret_value);
    /*return[0]*/___ret_value = n_null;
    /*return[1]*/goto ___scope_end_2;

    ___scope_end_2:;
    return ___ret_value;
}

 n_ascii  const *  const jawn_print_tag_3(/* variant */ struct jawn *  const(this))
{
    n_ascii  const * (___ret_value);
    /*return[0]*/___ret_value = n_null;
    /*return[1]*/goto ___scope_end_3;

    ___scope_end_3:;
    return ___ret_value;
}

