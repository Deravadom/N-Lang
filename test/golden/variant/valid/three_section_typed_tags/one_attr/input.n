variant jawn : u16
{
    let a : u;

    tag_1 : 5u16
    {
        let b : s;
    }

    tag_2 : 7u16
    {
        let c : u;
    }

    tag_3 : 9u16
    {
        let d : s;
    }
}
