variant jawn {
    let a : bool;
    let b : s;
    let c : ^ ascii;

    tag_1
    {
        let d : bool;
        let e : u;
        let f : ^ ascii;
    }

    tag_2
    {
        let d : ascii;
        let e : s;
        let f : u64;
    }

    tag_3
    {
        let d : s32;
        let e : u16;
        let f : ^ ascii;
    }
}
