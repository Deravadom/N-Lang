variant foo
{
    let a : mut ^ ascii;

    tag_1
    {
        let b : mut ^ ascii;
    }

    tag_2
    {
        let c : mut ^ ascii;
    }
}

func testfunc : () -> void
{
    foo;
}
