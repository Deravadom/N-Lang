func foo : () -> void
{
    loop true
    {
        defer
        {
            break;
        }
    }
}
