/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <EMU/EMUtest.h>
#include <compiler/nutils.h>

////////////////////////////////////////////////////////////////////////////////
//      NARRAY                                                                //
////////////////////////////////////////////////////////////////////////////////
EMU_TEST(narr_alloc_and_narr_free)
{
    int* narrA = narr_alloc(0, sizeof(int));

    EMU_ASSERT_NOT_NULL(narrA);
    EMU_ASSERT_EQ_UINT(narr_length(narrA), 0);
    EMU_ASSERT_GE_UINT(narr_capacity(narrA), narr_length(narrA));
    EMU_ASSERT_EQ_UINT(narr_sizeof_elem(narrA), sizeof(int));

    short* narrB = narr_alloc(13, sizeof(short));

    EMU_ASSERT_NOT_NULL(narrB);
    EMU_ASSERT_EQ_UINT(narr_length(narrB), 13);
    EMU_ASSERT_GE_UINT(narr_capacity(narrB), narr_length(narrB));
    EMU_ASSERT_EQ_UINT(narr_sizeof_elem(narrB), sizeof(short));

    narr_free(narrA);
    narr_free(narrB);
    EMU_END_TEST();
}

EMU_TEST(narr_reserve__does_nothing_when_reserving_zero_elements)
{
    // Zero length array.
    int* narr_zero = narr_alloc(0, sizeof(int));
    size_t len = narr_length(narr_zero);
    size_t cap = narr_capacity(narr_zero);
    size_t elsz = narr_sizeof_elem(narr_zero);

    narr_zero = narr_reserve(narr_zero, 0);

    EMU_ASSERT_EQ_UINT(narr_length(narr_zero), len);
    EMU_ASSERT_EQ_UINT(narr_capacity(narr_zero), cap);
    EMU_ASSERT_EQ_UINT(narr_sizeof_elem(narr_zero), elsz);

    // Small array.
    int* narr_small = narr_alloc(10, sizeof(int));
    len = narr_length(narr_small);
    cap = narr_capacity(narr_small);
    elsz = narr_sizeof_elem(narr_small);

    narr_small = narr_reserve(narr_small, 0);

    EMU_ASSERT_EQ_UINT(narr_length(narr_small), len);
    EMU_ASSERT_EQ_UINT(narr_capacity(narr_small), cap);
    EMU_ASSERT_EQ_UINT(narr_sizeof_elem(narr_small), elsz);

    // Large array.
    int* narr_large = narr_alloc(2000, sizeof(int));
    len = narr_length(narr_large);
    cap = narr_capacity(narr_large);
    elsz = narr_sizeof_elem(narr_large);

    narr_large = narr_reserve(narr_large, 0);

    EMU_ASSERT_EQ_UINT(narr_length(narr_large), len);
    EMU_ASSERT_EQ_UINT(narr_capacity(narr_large), cap);
    EMU_ASSERT_EQ_UINT(narr_sizeof_elem(narr_large), elsz);

    narr_free(narr_zero);
    narr_free(narr_small);
    narr_free(narr_large);
    EMU_END_TEST();
}

EMU_TEST(narr_reserve__reserving_a_large_amount_should_change_capacity)
{
    int* narr = narr_alloc(0, sizeof(int));
    size_t const cap = narr_capacity(narr);
    narr = narr_reserve(narr, 5000);
    EMU_EXPECT_GT_UINT(narr_capacity(narr), cap);
    narr_free(narr);
    EMU_END_TEST();
}

EMU_TEST(narr_reserve__ensures_capacity_is_always_greater_or_equal_to_len)
{
    int* narr = narr_alloc(0, sizeof(int));

    narr = narr_reserve(narr, 0);
    EMU_EXPECT_GE(narr_capacity(narr), narr_length(narr));

    narr = narr_reserve(narr, 10);
    EMU_EXPECT_GE(narr_capacity(narr), narr_length(narr));

    narr = narr_reserve(narr, 100);
    EMU_EXPECT_GE(narr_capacity(narr), narr_length(narr));

    narr = narr_reserve(narr, 1000);
    EMU_EXPECT_GE(narr_capacity(narr), narr_length(narr));

    narr = narr_reserve(narr, 5000);
    EMU_EXPECT_GE(narr_capacity(narr), narr_length(narr));

    narr_free(narr);
    EMU_END_TEST();
}

EMU_GROUP(narr_reserve)
{
    EMU_ADD(narr_reserve__ensures_capacity_is_always_greater_or_equal_to_len);
    EMU_ADD(narr_reserve__does_nothing_when_reserving_zero_elements);
    EMU_ADD(narr_reserve__reserving_a_large_amount_should_change_capacity);
    EMU_END_GROUP();
}

EMU_TEST(narr_resize__correctly_changes_length_and_capacity)
{
    int* narr = narr_alloc(0, sizeof(int));

    narr = narr_resize(narr, 10);
    EMU_ASSERT_EQ_UINT(narr_length(narr), 10);
    EMU_ASSERT_GE_UINT(narr_capacity(narr), narr_length(narr));

    narr = narr_resize(narr, 0);
    EMU_ASSERT_EQ_UINT(narr_length(narr), 0);
    EMU_ASSERT_GE_UINT(narr_capacity(narr), narr_length(narr));

    narr = narr_resize(narr, 4096);
    EMU_ASSERT_EQ_UINT(narr_length(narr), 4096);
    EMU_ASSERT_GE_UINT(narr_capacity(narr), narr_length(narr));

    narr = narr_resize(narr, narr_length(narr)+1);
    EMU_ASSERT_EQ_UINT(narr_length(narr), 4097);
    EMU_ASSERT_GE_UINT(narr_capacity(narr), narr_length(narr));

    narr = narr_resize(narr, 500);
    EMU_ASSERT_EQ_UINT(narr_length(narr), 500);
    EMU_ASSERT_GE_UINT(narr_capacity(narr), narr_length(narr));

    narr = narr_resize(narr, 0);
    EMU_ASSERT_EQ_UINT(narr_length(narr), 0);
    EMU_ASSERT_GE_UINT(narr_capacity(narr), narr_length(narr));

    narr_free(narr);
    EMU_END_TEST();
}

EMU_TEST(narr_resize__resize_with_same_length_should_not_change_data_members)
{
    int* narr = narr_alloc(100, sizeof(int));
    narr[0] = 33;
    narr[50] = 44;
    narr[99] = 55;
    size_t const len = narr_length(narr);
    size_t const cap = narr_capacity(narr);
    size_t const elsz = narr_sizeof_elem(narr);

    narr = narr_resize(narr, 100);

    EMU_ASSERT_EQ(narr[0], 33);
    EMU_ASSERT_EQ(narr[50], 44);
    EMU_ASSERT_EQ(narr[99], 55);
    EMU_ASSERT_EQ(narr_length(narr), len);
    EMU_ASSERT_EQ(narr_capacity(narr), cap);
    EMU_ASSERT_EQ(narr_sizeof_elem(narr), elsz);

    narr_free(narr);
    EMU_END_TEST();
}

EMU_TEST(narr_resize__downsizing_should_not_change_capacity)
{
    int* narr = narr_alloc(130, sizeof(int));
    size_t const cap = narr_capacity(narr);

    narr = narr_resize(narr, 10);

    EMU_ASSERT_EQ(narr_capacity(narr), cap);

    narr_free(narr);
    EMU_END_TEST();
}

EMU_TEST(narr_resize__upsizing_by_a_large_amount_should_change_capacity)
{
    int* narr = narr_alloc(50, sizeof(int));
    size_t const cap = narr_capacity(narr);

    narr = narr_resize(narr, 4096);

    EMU_ASSERT_GE(narr_capacity(narr), cap);

    narr_free(narr);
    EMU_END_TEST();
}

EMU_GROUP(narr_resize)
{
    EMU_ADD(narr_resize__correctly_changes_length_and_capacity);
    EMU_ADD(narr_resize__resize_with_same_length_should_not_change_data_members);
    EMU_ADD(narr_resize__downsizing_should_not_change_capacity);
    EMU_ADD(narr_resize__upsizing_by_a_large_amount_should_change_capacity);
    EMU_END_GROUP();
}

EMU_TEST(narr_push)
{
    int* narr = narr_alloc(0, sizeof(int));

    int val = 3;
    narr = narr_push(narr, &val);
    EMU_ASSERT_EQ_UINT(narr_length(narr), 1);
    EMU_ASSERT_EQ_INT(narr[narr_length(narr)-1], val);

    val = 100;
    narr = narr_push(narr, &val);
    EMU_ASSERT_EQ_UINT(narr_length(narr), 2);
    EMU_ASSERT_EQ_INT(narr[narr_length(narr)-1], val);

    narr_free(narr);
    EMU_END_TEST();
}

EMU_TEST(narr_pop)
{
    int* narr = narr_alloc(100, sizeof(int));

    narr_pop(narr);
    EMU_ASSERT_EQ_UINT(narr_length(narr), 99);
    narr_pop(narr);
    narr_pop(narr);
    EMU_ASSERT_EQ_UINT(narr_length(narr), 97);

    narr_free(narr);
    EMU_END_TEST();
}

EMU_TEST(narr_unshift)
{
    int* narr = narr_alloc(0, sizeof(int));

    int val = 40;
    narr = narr_unshift(narr, &val);
    EMU_ASSERT_EQ_UINT(narr_length(narr), 1);
    EMU_ASSERT_EQ_INT(narr[0], 40);

    val = 101;
    narr = narr_unshift(narr, &val);
    EMU_ASSERT_EQ_UINT(narr_length(narr), 2);
    EMU_ASSERT_EQ_INT(narr[0], 101);
    EMU_ASSERT_EQ_INT(narr[1], 40);

    narr_free(narr);
    EMU_END_TEST();
}

EMU_TEST(narr_shift)
{
    int* narr = narr_alloc(100, sizeof(int));

    narr_shift(narr);
    EMU_ASSERT_EQ_UINT(narr_length(narr), 99);
    narr_shift(narr);
    narr_shift(narr);
    EMU_ASSERT_EQ_UINT(narr_length(narr), 97);

    narr_free(narr);
    EMU_END_TEST();
}

EMU_TEST(narr_front)
{
    int* narr = narr_alloc(0, sizeof(int));

    for (size_t i = 0; i < 100; ++i)
    {
        int r = rand();
        narr = narr_push(narr, &r);
    }

    EMU_ASSERT_EQ(&narr[0], narr_front(narr));

    narr_free(narr);
    EMU_END_TEST();
}

EMU_TEST(narr_back)
{
    int* narr = narr_alloc(0, sizeof(int));

    for (size_t i = 0; i < 100; ++i)
    {
        int r = rand();
        narr = narr_push(narr, &r);
    }

    EMU_ASSERT_EQ(&narr[99], narr_back(narr));

    narr_free(narr);
    EMU_END_TEST();
}

EMU_GROUP(narray)
{
    EMU_ADD(narr_alloc_and_narr_free);
    EMU_ADD(narr_reserve);
    EMU_ADD(narr_resize);
    EMU_ADD(narr_push);
    EMU_ADD(narr_pop);
    EMU_ADD(narr_unshift);
    EMU_ADD(narr_shift);
    EMU_ADD(narr_front);
    EMU_ADD(narr_back);
    EMU_END_GROUP();
}

////////////////////////////////////////////////////////////////////////////////
//      STRING                                                                //
////////////////////////////////////////////////////////////////////////////////
EMU_TEST(cstr__cstr_cmp)
{
    EMU_EXPECT_EQ_INT(cstr_cmp("some str", "some str"), 0);
    EMU_EXPECT_NE_INT(cstr_cmp("some str", "SOME STR"), 0);
    EMU_EXPECT_GT_INT(cstr_cmp("some str", "another str"), 0);
    EMU_EXPECT_LT_INT(cstr_cmp("another str", "some str"), 0);
    EMU_END_TEST();
}

EMU_TEST(cstr__cstr_n_cmp)
{
    EMU_EXPECT_EQ_INT(cstr_n_cmp("some str", "some str", strlen("some str")), 0);
    EMU_EXPECT_EQ_INT(cstr_n_cmp("someFOOBAR", "some str", strlen("some")), 0);

    EMU_EXPECT_NE_INT(cstr_n_cmp("some str", "SOME STR", strlen("some str")), 0);
    EMU_EXPECT_NE_INT(cstr_n_cmp("someFOOBAR", "SOME str", strlen("some")), 0);

    EMU_EXPECT_GT_INT(cstr_n_cmp("some str", "another str", strlen("another str")), 0);
    EMU_EXPECT_LT_INT(cstr_n_cmp("another str", "some str", strlen("another str")), 0);

    EMU_EXPECT_GT_INT(cstr_n_cmp("some str", "\0", strlen("some str")), 0);
    EMU_EXPECT_LT_INT(cstr_n_cmp("\0", "some str", strlen("some str")), 0);

    EMU_END_TEST();
}

EMU_TEST(cstr__cstr_eq)
{
    EMU_EXPECT_TRUE(cstr_eq("some str", "some str"));
    EMU_EXPECT_FALSE(cstr_eq("some str", "SOME STR"));
    EMU_EXPECT_FALSE(cstr_eq("some str", "another str"));
    EMU_END_TEST();
}

EMU_TEST(cstr__cstr_ne)
{
    EMU_EXPECT_FALSE(cstr_ne("some str", "some str"));
    EMU_EXPECT_TRUE(cstr_ne("some str", "SOME STR"));
    EMU_EXPECT_TRUE(cstr_ne("some str", "another str"));
    EMU_END_TEST();
}

EMU_TEST(cstr__cstr_cmp_case)
{
    EMU_EXPECT_EQ_INT(cstr_cmp_case("some str", "some str"), 0);
    EMU_EXPECT_EQ_INT(cstr_cmp_case("some str", "SOME STR"), 0);
    EMU_EXPECT_GT_INT(cstr_cmp_case("some str", "another str"), 0);
    EMU_EXPECT_GT_INT(cstr_cmp_case("some str", "ANOTHER STR"), 0);
    EMU_EXPECT_LT_INT(cstr_cmp_case("another str", "some str"), 0);
    EMU_EXPECT_LT_INT(cstr_cmp_case("another str", "SOME STR"), 0);
    EMU_END_TEST();
}

EMU_TEST(cstr__cstr_n_cmp_case)
{
    EMU_EXPECT_EQ_INT(cstr_n_cmp_case("some str", "some str", strlen("some str")), 0);
    EMU_EXPECT_EQ_INT(cstr_n_cmp_case("some str", "SOME STR", strlen("some str")), 0);
    EMU_EXPECT_EQ_INT(cstr_n_cmp_case("some strFOOBAR", "some strBAZ", strlen("some str")), 0);
    EMU_EXPECT_EQ_INT(cstr_n_cmp_case("some strFOOBAR", "SOME STRbaz", strlen("some str")), 0);

    EMU_EXPECT_GT_INT(cstr_n_cmp_case("some str", "another str", strlen("another str")), 0);
    EMU_EXPECT_GT_INT(cstr_n_cmp_case("some str", "ANOTHER STR", strlen("another str")), 0);
    EMU_EXPECT_GT_INT(cstr_n_cmp_case("some strFOOBAR", "another strBAZ", strlen("some str")), 0);
    EMU_EXPECT_GT_INT(cstr_n_cmp_case("some strFOOBAR", "ANOTHER STRbaz", strlen("some str")), 0);

    EMU_EXPECT_LT_INT(cstr_n_cmp_case("another str", "some str", strlen("another str")), 0);
    EMU_EXPECT_LT_INT(cstr_n_cmp_case("another str", "SOME STR", strlen("another str")), 0);
    EMU_EXPECT_LT_INT(cstr_n_cmp_case("another strFOOBAR", "some strBAZ", strlen("some str")), 0);
    EMU_EXPECT_LT_INT(cstr_n_cmp_case("another strFOOBAR", "SOME STRbaz", strlen("some str")), 0);

    EMU_EXPECT_GT_INT(cstr_n_cmp_case("some str", "\0", strlen("some str")), 0);
    EMU_EXPECT_LT_INT(cstr_n_cmp_case("\0", "some str", strlen("some str")), 0);

    EMU_END_TEST();
}

EMU_TEST(cstr__cstr_eq_case)
{
    EMU_EXPECT_TRUE(cstr_eq_case("some str", "some str"));
    EMU_EXPECT_TRUE(cstr_eq_case("some str", "SOME STR"));
    EMU_EXPECT_FALSE(cstr_eq_case("some str", "another str"));
    EMU_END_TEST();
}

EMU_TEST(cstr__cstr_ne_case)
{
    EMU_EXPECT_FALSE(cstr_ne_case("some str", "some str"));
    EMU_EXPECT_FALSE(cstr_ne_case("some str", "SOME STR"));
    EMU_EXPECT_TRUE(cstr_ne_case("some str", "another str"));
    EMU_END_TEST();
}

EMU_TEST(cstr__cstr_len)
{
    EMU_EXPECT_EQ(cstr_len(""), strlen(""));
    EMU_EXPECT_EQ(cstr_len("some str"), strlen("some str"));
    EMU_EXPECT_EQ(cstr_len("foobar"), strlen("foobar"));
    EMU_END_TEST();
}

EMU_TEST(cstr__cstr_len_ge)
{
    EMU_EXPECT_TRUE(cstr_len_ge("", 0));
    EMU_EXPECT_TRUE(cstr_len_ge("a", 0));
    EMU_EXPECT_TRUE(cstr_len_ge("a", 1));
    EMU_EXPECT_FALSE(cstr_len_ge("a", 2));
    EMU_EXPECT_FALSE(cstr_len_ge("a", 100));
    EMU_EXPECT_TRUE(cstr_len_ge("some str", strlen("some str")));
    EMU_EXPECT_TRUE(cstr_len_ge("some str", strlen("some str")-1));
    EMU_EXPECT_FALSE(cstr_len_ge("some str", strlen("some str")+1));
    EMU_END_TEST();
}

EMU_TEST(cstr__cstr_cat)
{
    char buff[128] = {0};
    EMU_ASSERT_EQ(strlen(buff), 0);

    cstr_cat(buff, "some str");
    EMU_ASSERT_STREQ(buff, "some str");

    cstr_cat(buff, "another str");
    EMU_ASSERT_STREQ(buff, "some stranother str");

    EMU_END_TEST();
}

EMU_TEST(cstr__cstr_n_cat)
{
    char buff[128] = {0};
    EMU_ASSERT_EQ(strlen(buff), 0);

    cstr_n_cat(buff, "some str", cstr_len("some str"));
    EMU_ASSERT_STREQ(buff, "some str");

    cstr_n_cat(buff, "another str", cstr_len("another"));
    EMU_ASSERT_STREQ(buff, "some stranother");

    EMU_END_TEST();
}

EMU_TEST(cstr__cstr_cpy)
{
    char buff[128] = {0};
    EMU_ASSERT_EQ(strlen(buff), 0);

    cstr_cpy(buff, "some str");
    EMU_ASSERT_STREQ(buff, "some str");

    cstr_cpy(buff, "another str");
    EMU_ASSERT_STREQ(buff, "another str");

    EMU_END_TEST();
}

EMU_TEST(cstr__cstr_n_cpy)
{
    char buff[128] = {0};
    EMU_ASSERT_EQ(strlen(buff), 0);

    cstr_n_cpy(buff, "some str", strlen("some str"));
    EMU_ASSERT_STREQ(buff, "some str");

    cstr_n_cpy(buff, "another str", strlen("another"));
    buff[strlen("another")] = '\0';
    EMU_ASSERT_STREQ(buff, "another");

    EMU_END_TEST();
}

EMU_GROUP(cstr)
{
    EMU_ADD(cstr__cstr_cmp);
    EMU_ADD(cstr__cstr_n_cmp);
    EMU_ADD(cstr__cstr_eq);
    EMU_ADD(cstr__cstr_ne);
    EMU_ADD(cstr__cstr_cmp_case);
    EMU_ADD(cstr__cstr_n_cmp_case);
    EMU_ADD(cstr__cstr_eq_case);
    EMU_ADD(cstr__cstr_ne_case);
    EMU_ADD(cstr__cstr_len);
    EMU_ADD(cstr__cstr_len_ge);
    EMU_ADD(cstr__cstr_cat);
    EMU_ADD(cstr__cstr_n_cat);
    EMU_ADD(cstr__cstr_cpy);
    EMU_ADD(cstr__cstr_n_cpy);
    EMU_END_GROUP();
}

EMU_TEST(nstr__nstr_init_and_nstr_fini)
{
    nstr_t nstr;
    nstr_init(&nstr);

    EMU_ASSERT_NOT_NULL(nstr.data);
    EMU_ASSERT_EQ_UINT(0, nstr.len);
    EMU_ASSERT_EQ_UINT(0, strlen(nstr.data));
    EMU_ASSERT_EQ_CHAR('\0', nstr.data[0]);

    nstr_fini(&nstr);
    EMU_END_TEST();
}

EMU_TEST(nstr__nstr_init_cstr)
{
    nstr_t nstrA;
    nstr_init_cstr(&nstrA, "");

    EMU_ASSERT_NOT_NULL(nstrA.data);
    EMU_ASSERT_EQ_UINT(nstrA.len, 0);
    EMU_ASSERT_STREQ(nstrA.data, "");

    nstr_t nstrB;
    nstr_init_cstr(&nstrB, "some str");

    EMU_ASSERT_NOT_NULL(nstrB.data);
    EMU_ASSERT_EQ_UINT(nstrB.len, cstr_len("some str"));
    EMU_ASSERT_STREQ(nstrB.data, "some str");

    nstr_t nstrC;
    nstr_init_cstr(&nstrC, "a string that is much longer");

    EMU_ASSERT_NOT_NULL(nstrC.data);
    EMU_ASSERT_EQ_UINT(nstrC.len, cstr_len("a string that is much longer"));
    EMU_ASSERT_STREQ(nstrC.data, "a string that is much longer");

    nstr_fini(&nstrA);
    nstr_fini(&nstrB);
    nstr_fini(&nstrC);
    EMU_END_TEST();
}

EMU_TEST(nstr__nstr_init_nstr)
{
    nstr_t nstrA;
    nstr_t otherA;
    nstr_init_cstr(&otherA, "");
    nstr_init_nstr(&nstrA, &otherA);

    EMU_ASSERT_NOT_NULL(nstrA.data);
    EMU_ASSERT_EQ_UINT(nstrA.len, 0);
    EMU_ASSERT_STREQ(nstrA.data, "");

    nstr_t nstrB;
    nstr_t otherB;
    nstr_init_cstr(&otherB, "some str");
    nstr_init_nstr(&nstrB, &otherB);

    EMU_ASSERT_NOT_NULL(nstrB.data);
    EMU_ASSERT_EQ_UINT(nstrB.len, cstr_len("some str"));
    EMU_ASSERT_STREQ(nstrB.data, "some str");

    nstr_t nstrC;
    nstr_t otherC;
    nstr_init_cstr(&otherC, "a string that is much longer");
    nstr_init_nstr(&nstrC, &otherC);

    EMU_ASSERT_NOT_NULL(nstrC.data);
    EMU_ASSERT_EQ_UINT(nstrC.len, cstr_len("a string that is much longer"));
    EMU_ASSERT_STREQ(nstrC.data, "a string that is much longer");

    nstr_fini(&nstrA);
    nstr_fini(&otherA);
    nstr_fini(&nstrB);
    nstr_fini(&otherB);
    nstr_fini(&nstrC);
    nstr_fini(&otherC);
    EMU_END_TEST();
}

EMU_TEST(nstr__nstr_init_fmt)
{
    nstr_t nstrA;
    nstr_init_fmt(&nstrA, "");

    EMU_ASSERT_NOT_NULL(nstrA.data);
    EMU_ASSERT_EQ_UINT(nstrA.len, cstr_len(""));
    EMU_ASSERT_STREQ(nstrA.data, "");

    nstr_t nstrB;
    nstr_init_fmt(&nstrB, "%d %s", 10, "foo");

    EMU_ASSERT_NOT_NULL(nstrB.data);
    EMU_ASSERT_EQ_UINT(nstrB.len, cstr_len("10 foo"));
    EMU_ASSERT_STREQ(nstrB.data, "10 foo");

    nstr_t nstrC;
    nstr_init_fmt(&nstrC, "a longer formatted string %u %s", 8, "bar");

    EMU_ASSERT_NOT_NULL(nstrC.data);
    EMU_ASSERT_EQ_UINT(nstrC.len, cstr_len("a longer formatted string 8 bar"));
    EMU_ASSERT_STREQ(nstrC.data, "a longer formatted string 8 bar");

    nstr_fini(&nstrA);
    nstr_fini(&nstrB);
    nstr_fini(&nstrC);
    EMU_END_TEST();
}

EMU_TEST(nstr__nstr_defer_fini)
{
    nstr_t nstr_defer_fini nstr;
    // If nstr_fini is never called on nstr, then a memory leak will show when
    // the memcheck tests are run.
    nstr_init_cstr(&nstr, "The quick brown fox jumps over the lazy dog.");
    EMU_END_TEST();
}

EMU_TEST(nstr__nstr_len)
{
    nstr_t nstrA;
    nstr_init_cstr(&nstrA, "");
    EMU_ASSERT_EQ_UINT(nstrA.len, 0);

    nstr_t nstrB;
    nstr_init_cstr(&nstrB, "some str");
    EMU_ASSERT_EQ_UINT(nstrB.len, cstr_len("some str"));

    nstr_t nstrC;
    nstr_init_cstr(&nstrC, "a string that is longer");
    EMU_ASSERT_EQ_UINT(nstrC.len, cstr_len("a string that is longer"));

    nstr_fini(&nstrA);
    nstr_fini(&nstrB);
    nstr_fini(&nstrC);
    EMU_END_TEST();
}

EMU_TEST(nstr__nstr_capacity)
{
    nstr_t nstrA;
    nstr_init_cstr(&nstrA, "");
    EMU_ASSERT_GE_UINT(nstrA.capacity, 1);

    nstr_t nstrB;
    nstr_init_cstr(&nstrB, "123456789012345");
    EMU_ASSERT_GE_UINT(
        nstrB.capacity,
        cstr_len("123456789012345")+NULL_TERMINATOR_LENGTH
    );

    nstr_t nstrC;
    nstr_init_cstr(&nstrC, "a string that is much longer");
    EMU_ASSERT_GE_UINT(
        nstrC.capacity,
        cstr_len("a string that is much longer")+NULL_TERMINATOR_LENGTH
    );

    nstr_fini(&nstrA);
    nstr_fini(&nstrB);
    nstr_fini(&nstrC);
    EMU_END_TEST();
}

EMU_TEST(nstr__nstr_resize)
{
    nstr_t nstr;
    nstr_init(&nstr);

    // Expand, but still within a small string.
    nstr_resize(&nstr, 2);
    EMU_ASSERT_EQ_UINT(nstr.len, 2);
    // Underlying string wasn't changed, so the cstr_len should still be 0;
    EMU_ASSERT_STREQ(nstr.data, "");

    // Expand to heap allocated string..
    nstr_resize(&nstr, 100);
    EMU_ASSERT_EQ_UINT(nstr.len, 100);
    // Underlying string wasn't changed, so the cstr_len should still be 0;
    EMU_ASSERT_STREQ(nstr.data, "");

    // Change data and string. Appended null terminator should prevent overflow.
    nstr.data[0] = 'A';
    nstr.data[1] = 'B';
    nstr.data[2] = 'C';
    nstr.data[3] = 'D';
    nstr.data[4] = 'E';
    nstr_resize(&nstr, 3);
    EMU_ASSERT_EQ_UINT(nstr.len, 3);
    EMU_ASSERT_STREQ(nstr.data, "ABC");

    nstr_fini(&nstr);
    EMU_END_TEST();
}

EMU_TEST(nstr__nstr_reserve)
{
    nstr_t nstr;
    nstr_init_cstr(&nstr, "small str");

    nstr_reserve(&nstr, 1000);
    EMU_ASSERT_STREQ(nstr.data, "small str");
    EMU_ASSERT_GE(nstr.capacity, cstr_len("small str") + 1000);

    nstr_fini(&nstr);
    EMU_END_TEST();
}

EMU_TEST(nstr__nstr_assign_cstr)
{
    nstr_t nstr;
    nstr_init_cstr(&nstr, "foo");

    nstr_assign_cstr(&nstr, "bar");
    EMU_ASSERT_STREQ(nstr.data, "bar");

    // small str -> big str
    nstr_assign_cstr(&nstr, "everybody go bananas");
    EMU_ASSERT_STREQ(nstr.data, "everybody go bananas");

    // big str -> small str
    nstr_assign_cstr(&nstr, "baz");
    EMU_ASSERT_STREQ(nstr.data, "baz");

    nstr_fini(&nstr);
    EMU_END_TEST();
}

EMU_TEST(nstr__nstr_assign_nstr)
{
    nstr_t nstr;
    nstr_t bar;
    nstr_t bananas;
    nstr_t baz;
    nstr_init_cstr(&nstr, "foo");
    nstr_init_cstr(&bar, "bar");
    nstr_init_cstr(&bananas, "I wanna go bananas");
    nstr_init_cstr(&baz, "baz");

    nstr_assign_nstr(&nstr, &bar);
    EMU_ASSERT_STREQ(nstr.data, "bar");

    // small str -> big str
    nstr_assign_nstr(&nstr, &bananas);
    EMU_ASSERT_STREQ(nstr.data, "I wanna go bananas");

    // big str -> small str
    nstr_assign_nstr(&nstr, &baz);
    EMU_ASSERT_STREQ(nstr.data, "baz");

    nstr_fini(&nstr);
    nstr_fini(&bar);
    nstr_fini(&bananas);
    nstr_fini(&baz);
    EMU_END_TEST();
}

EMU_TEST(nstr__nstr_assign_fmt)
{
    nstr_t nstr;
    nstr_init(&nstr);

    nstr_assign_fmt(&nstr, "%d %s", 10, "foo");
    EMU_ASSERT_STREQ(nstr.data, "10 foo");

    nstr_assign_fmt(&nstr, "a longer formatted string %u %s", 8, "bar");
    EMU_ASSERT_STREQ(nstr.data, "a longer formatted string 8 bar");

    nstr_fini(&nstr);
    EMU_END_TEST();
}

EMU_TEST(nstr__nstr_clear)
{
    nstr_t nstr;
    nstr_init_cstr(&nstr, "apples");
    nstr_clear(&nstr);
    EMU_ASSERT_EQ(nstr.len, 0);
    EMU_ASSERT_STREQ(nstr.data, "");

    nstr_assign_cstr(&nstr, "everybody go bananas for nstrings!");
    nstr_clear(&nstr);
    EMU_ASSERT_EQ(nstr.len, 0);
    EMU_ASSERT_STREQ(nstr.data, "");

    nstr_fini(&nstr);
    EMU_END_TEST();
}

EMU_TEST(nstr__nstr_cmp)
{
    nstr_t A;
    nstr_t B;
    nstr_t C;
    nstr_init_cstr(&A, "some str");
    nstr_init_cstr(&B, "SOME STR");
    nstr_init_cstr(&C, "another str");

    EMU_EXPECT_EQ_INT(nstr_cmp(&A, &A), 0);
    EMU_EXPECT_NE_INT(nstr_cmp(&A, &B), 0);
    EMU_EXPECT_GT_INT(nstr_cmp(&A, &C), 0);
    EMU_EXPECT_LT_INT(nstr_cmp(&C, &A), 0);

    nstr_fini(&A);
    nstr_fini(&B);
    nstr_fini(&C);
    EMU_END_TEST();
}

EMU_TEST(nstr__nstr_eq)
{
    nstr_t A;
    nstr_t B;
    nstr_t C;
    nstr_init_cstr(&A, "some str");
    nstr_init_cstr(&B, "SOME STR");
    nstr_init_cstr(&C, "another str");

    EMU_EXPECT_TRUE(nstr_eq(&A, &A));
    EMU_EXPECT_FALSE(nstr_eq(&A, &B));
    EMU_EXPECT_FALSE(nstr_eq(&A, &C));

    nstr_fini(&A);
    nstr_fini(&B);
    nstr_fini(&C);
    EMU_END_TEST();
}

EMU_TEST(nstr__nstr_ne)
{
    nstr_t A;
    nstr_t B;
    nstr_t C;
    nstr_init_cstr(&A, "some str");
    nstr_init_cstr(&B, "SOME STR");
    nstr_init_cstr(&C, "another str");

    EMU_EXPECT_FALSE(nstr_ne(&A, &A));
    EMU_EXPECT_TRUE(nstr_ne(&A, &B));
    EMU_EXPECT_TRUE(nstr_ne(&A, &C));

    nstr_fini(&A);
    nstr_fini(&B);
    nstr_fini(&C);
    EMU_END_TEST();
}

EMU_TEST(nstr__nstr_cmp_case)
{
    nstr_t A;
    nstr_t B;
    nstr_t C;
    nstr_t D;
    nstr_init_cstr(&A, "some str");
    nstr_init_cstr(&B, "SOME STR");
    nstr_init_cstr(&C, "another str");
    nstr_init_cstr(&D, "ANOTHER STR");

    EMU_EXPECT_EQ_INT(nstr_cmp_case(&A, &A), 0);
    EMU_EXPECT_EQ_INT(nstr_cmp_case(&A, &B), 0);
    EMU_EXPECT_GT_INT(nstr_cmp_case(&A, &C), 0);
    EMU_EXPECT_GT_INT(nstr_cmp_case(&A, &D), 0);
    EMU_EXPECT_LT_INT(nstr_cmp_case(&C, &A), 0);
    EMU_EXPECT_LT_INT(nstr_cmp_case(&C, &B), 0);

    nstr_fini(&A);
    nstr_fini(&B);
    nstr_fini(&C);
    nstr_fini(&D);
    EMU_END_TEST();
}

EMU_TEST(nstr__nstr_eq_case)
{
    nstr_t A;
    nstr_t B;
    nstr_t C;
    nstr_init_cstr(&A, "some str");
    nstr_init_cstr(&B, "SOME STR");
    nstr_init_cstr(&C, "another str");

    EMU_EXPECT_TRUE(nstr_eq_case(&A, &A));
    EMU_EXPECT_TRUE(nstr_eq_case(&A, &B));
    EMU_EXPECT_FALSE(nstr_eq_case(&A, &C));

    nstr_fini(&A);
    nstr_fini(&B);
    nstr_fini(&C);
    EMU_END_TEST();
}

EMU_TEST(nstr__nstr_ne_case)
{
    nstr_t A;
    nstr_t B;
    nstr_t C;
    nstr_init_cstr(&A, "some str");
    nstr_init_cstr(&B, "SOME STR");
    nstr_init_cstr(&C, "another str");

    EMU_EXPECT_FALSE(nstr_ne_case(&A, &A));
    EMU_EXPECT_FALSE(nstr_ne_case(&A, &B));
    EMU_EXPECT_TRUE(nstr_ne_case(&A, &C));

    nstr_fini(&A);
    nstr_fini(&B);
    nstr_fini(&C);
    EMU_END_TEST();
}

EMU_TEST(nstr__nstr_cat_cstr)
{
    nstr_t dest;
    nstr_init(&dest);

    nstr_cat_cstr(&dest, "some str");
    EMU_ASSERT_STREQ(dest.data, "some str");

    nstr_cat_cstr(&dest, "another str");
    EMU_ASSERT_STREQ(dest.data, "some stranother str");

    nstr_fini(&dest);
    EMU_END_TEST();
}

EMU_TEST(nstr__nstr_cat_nstr)
{
    nstr_t dest;
    nstr_t src;
    nstr_init(&dest);
    nstr_init_cstr(&src, "the quick brown fox");

    nstr_cat_nstr(&dest, &src);
    EMU_ASSERT_STREQ(dest.data, "the quick brown fox");

    nstr_cat_nstr(&dest, &src);
    EMU_ASSERT_STREQ(dest.data, "the quick brown foxthe quick brown fox");

    nstr_fini(&dest);
    nstr_fini(&src);
    EMU_END_TEST();
}

EMU_TEST(nstr__nstr_cat_fmt)
{
    nstr_t dest;
    nstr_init(&dest);

    nstr_cat_fmt(&dest, "%d %s", 10, "foo");
    EMU_ASSERT_STREQ(dest.data, "10 foo");

    nstr_cat_fmt(&dest, "a longer formatted string %u %s", 8, "bar");
    EMU_ASSERT_STREQ(dest.data, "10 fooa longer formatted string 8 bar");

    nstr_fini(&dest);
    EMU_END_TEST();
}

EMU_GROUP(nstr)
{
    EMU_ADD(nstr__nstr_init_and_nstr_fini);
    EMU_ADD(nstr__nstr_init_cstr);
    EMU_ADD(nstr__nstr_init_nstr);
    EMU_ADD(nstr__nstr_init_fmt);
    EMU_ADD(nstr__nstr_defer_fini);
    EMU_ADD(nstr__nstr_len);
    EMU_ADD(nstr__nstr_capacity);
    EMU_ADD(nstr__nstr_resize);
    EMU_ADD(nstr__nstr_reserve);
    EMU_ADD(nstr__nstr_assign_cstr);
    EMU_ADD(nstr__nstr_assign_nstr);
    EMU_ADD(nstr__nstr_assign_fmt);
    EMU_ADD(nstr__nstr_clear);
    EMU_ADD(nstr__nstr_cmp);
    EMU_ADD(nstr__nstr_eq);
    EMU_ADD(nstr__nstr_ne);
    EMU_ADD(nstr__nstr_cmp_case);
    EMU_ADD(nstr__nstr_eq_case);
    EMU_ADD(nstr__nstr_ne_case);
    EMU_ADD(nstr__nstr_cat_cstr);
    EMU_ADD(nstr__nstr_cat_nstr);
    EMU_ADD(nstr__nstr_cat_fmt);
    EMU_END_GROUP();
}

EMU_GROUP(string)
{
    EMU_ADD(cstr);
    EMU_ADD(nstr);
    EMU_END_GROUP();
}

////////////////////////////////////////////////////////////////////////////////
//      IO                                                                    //
////////////////////////////////////////////////////////////////////////////////
// These log tests mostly just make sure we can write without crashing. Ouput is
// written to a log file.
EMU_TEST(nlogf)
{
    FILE* log_fp = fopen("build/test.log", "w+");
    EMU_ASSERT_NOT_NULL(log_fp);
    log_stream = &log_fp; // Set log_stream for testing.

    log_config = LOG_DEBUG | LOG_METRIC | LOG_ERROR | LOG_WARNING | LOG_ERROR | LOG_FATAL;
    int foo = 0;
    nlogf(LOG_DEBUG,   "This is a message created with LOG_DEBUG foo = %d", foo++);
    nlogf(LOG_METRIC,  "This is a message created with LOG_METRIC foo = %d", foo++);
    nlogf(LOG_INFO,    "This is a message created with LOG_INFO foo = %d", foo++);
    nlogf(LOG_WARNING, "This is a message created with LOG_WARNING foo = %d", foo++);
    nlogf(LOG_ERROR,   "This is a message created with LOG_ERROR foo = %d", foo++);
    nlogf(LOG_FATAL,   "This is a message created with LOG_FATAL foo = %d", foo++);
    nlogf(9999, "Message logged with an unknown log level"); // unknown garbage log level

    log_config = LOG_DEBUG | LOG_ERROR | LOG_FATAL;
    int bar = 0;
    nlogf(LOG_DEBUG,   "This is a message created with LOG_DEBUG bar = %d", bar++);
    nlogf(LOG_METRIC,  "This is a message created with LOG_METRIC bar = %d", bar++);
    nlogf(LOG_INFO,    "This is a message created with LOG_INFO bar = %d", bar++);
    nlogf(LOG_WARNING, "This is a message created with LOG_WARNING bar = %d", bar++);
    nlogf(LOG_ERROR,   "This is a message created with LOG_ERROR bar = %d", bar++);
    nlogf(LOG_FATAL,   "This is a message created with LOG_FATAL bar = %d", bar++);
    nlogf(9999, "Message logged with an unknown log level"); // unknown garbage log level

    fclose(log_fp);
    log_config = DEFAULT_LOG_CONFIG; // Restore log configuration.
    log_stream = DEFAULT_LOG_STREAM; // Restore log stream.
    EMU_END_TEST();
}

EMU_TEST(nlogf_flc)
{
    FILE* log_fp = fopen("build/test.log", "a");
    EMU_ASSERT_NOT_NULL(log_fp);
    log_stream = &log_fp; // Set log_stream for testing.

    char const* test_src = "test_src_file.c";
    size_t test_line = 42;
    size_t test_col = 101;

    log_config = LOG_DEBUG | LOG_ERROR | LOG_WARNING | LOG_ERROR | LOG_FATAL;
    int foo = 0;
    nlogf_flc(LOG_DEBUG,   test_src, test_line, test_col, "This is a message created with LOG_DEBUG foo = %d", foo++);
    nlogf_flc(LOG_INFO,    test_src, test_line, test_col, "This is a message created with LOG_INFO foo = %d", foo++);
    nlogf_flc(LOG_WARNING, test_src, test_line, test_col, "This is a message created with LOG_WARNING foo = %d", foo++);
    nlogf_flc(LOG_ERROR,   test_src, test_line, test_col, "This is a message created with LOG_ERROR foo = %d", foo++);
    nlogf_flc(LOG_FATAL,   test_src, test_line, test_col, "This is a message created with LOG_FATAL foo = %d", foo++);
    nlogf_flc(9999, test_src, test_line, test_col, "Message logged with an unknown log level"); // unknown garbage log level

    log_config = LOG_DEBUG | LOG_ERROR | LOG_FATAL;
    int bar = 0;
    nlogf_flc(LOG_DEBUG,   test_src, test_line, test_col, "This is a message created with LOG_DEBUG bar = %d", bar++);
    nlogf_flc(LOG_INFO,    test_src, test_line, test_col, "This is a message created with LOG_INFO bar = %d", bar++);
    nlogf_flc(LOG_WARNING, test_src, test_line, test_col, "This is a message created with LOG_WARNING bar = %d", bar++);
    nlogf_flc(LOG_ERROR,   test_src, test_line, test_col, "This is a message created with LOG_ERROR bar = %d", bar++);
    nlogf_flc(LOG_FATAL,   test_src, test_line, test_col, "This is a message created with LOG_FATAL bar = %d", bar++);
    nlogf_flc(9999, test_src, test_line, test_col, "Message logged with an unknown log level"); // unknown garbage log level

    fclose(log_fp);
    log_config = DEFAULT_LOG_CONFIG; // Restore log configuration.
    log_stream = DEFAULT_LOG_STREAM; // Restore log stream.
    EMU_END_TEST();
}

#define file_0_char           "./test/compiler/resources/file_0_char"
#define file_8_char           "./test/compiler/resources/file_8_char"
#define file_18_char          "./test/compiler/resources/file_18_char"
#define file_DOS_line_endings "./test/compiler/resources/file_DOS_line_endings" // 8 chars
#define existing_file         "./test/compiler/resources/existing_file"
#define nonexisting_file      "not_an_actual_file"

EMU_TEST(file_exist)
{

    EMU_EXPECT_TRUE(file_exist(existing_file));
    EMU_EXPECT_FALSE(file_exist(nonexisting_file));
    EMU_END_TEST();
}

EMU_TEST(file_read_into_buff)
{
    size_t const small_sz = 3;
    size_t const large_sz = 128;
    char buff_small[small_sz];
    char buff_large[large_sz];

    EMU_ASSERT_EQ_INT(file_read_into_buff(file_0_char, buff_small, small_sz), 0);
    EMU_EXPECT_STREQ(buff_small, "");
    EMU_ASSERT_EQ_INT(file_read_into_buff(file_0_char, buff_large, large_sz), 0);
    EMU_EXPECT_STREQ(buff_large, "");

    EMU_EXPECT_EQ_INT(file_read_into_buff(file_8_char, buff_small, small_sz), -1);
    EMU_ASSERT_EQ_INT(file_read_into_buff(file_8_char, buff_large, large_sz), 8);
    EMU_EXPECT_STREQ(buff_large, "12345678");

    EMU_ASSERT_EQ_INT(file_read_into_buff(file_DOS_line_endings, buff_large, large_sz), 8);
    EMU_ASSERT_EQ(strcmp(buff_large, "123456\r\n"), 0);
    EMU_EXPECT_STREQ(buff_large, "123456\r\n");

    EMU_EXPECT_EQ_INT(file_read_into_buff(nonexisting_file, buff_small, small_sz), -1);
    EMU_EXPECT_EQ_INT(file_read_into_buff(nonexisting_file, buff_large, large_sz), -1);
    EMU_END_TEST();
}

EMU_TEST(file_read_into_nstr)
{
    {
        nstr_t nstr_defer_fini nstr;
        nstr_init(&nstr);
        EMU_ASSERT_EQ_INT(file_read_into_nstr(file_0_char, &nstr), 0);
        EMU_EXPECT_STREQ(nstr.data, "");
    }
    {
        nstr_t nstr_defer_fini nstr;
        nstr_init(&nstr);
        EMU_ASSERT_EQ_INT(file_read_into_nstr(file_8_char, &nstr), 8);
        EMU_EXPECT_STREQ(nstr.data, "12345678");
    }
    {
        nstr_t nstr_defer_fini nstr;
        nstr_init(&nstr);
        EMU_ASSERT_EQ_INT(file_read_into_nstr(file_DOS_line_endings, &nstr), 8);
        EMU_EXPECT_STREQ(nstr.data, "123456\r\n");
    }
    {
        nstr_t nstr_defer_fini nstr;
        nstr_init(&nstr);
        EMU_EXPECT_EQ_INT(file_read_into_nstr(nonexisting_file, &nstr), -1);
    }
    EMU_END_TEST();
}

EMU_TEST(file_size)
{

    EMU_EXPECT_EQ_INT(file_size(file_0_char), 0);
    EMU_EXPECT_EQ_INT(file_size(file_8_char), 8);
    EMU_EXPECT_EQ_INT(file_size(file_18_char), 18);

    // CR LF should not be converted to '\n' for file_size on windows so the
    // size should always be 8
    EMU_EXPECT_EQ_INT(file_size(file_DOS_line_endings), 8);

    EMU_EXPECT_EQ_INT(file_size(nonexisting_file), -1);
    EMU_END_TEST();
}

EMU_TEST(fmt_len)
{
    EMU_EXPECT_EQ_INT(fmt_len(""), 0);
    EMU_EXPECT_EQ_INT(fmt_len("foo"), 3);
    EMU_EXPECT_EQ_INT(fmt_len("foo%s", "bar"), 6);
    EMU_EXPECT_EQ_INT(
        fmt_len("Hello,%d%s%zu\n", 10, "world!", 999),
        strlen("Hello,") + 2 + strlen("world!") + 3 + 1
    );
    EMU_END_TEST();
}

EMU_GROUP(io)
{
    EMU_ADD(nlogf);
    EMU_ADD(nlogf_flc);
    EMU_ADD(file_exist);
    EMU_ADD(file_read_into_buff);
    EMU_ADD(file_read_into_nstr);
    EMU_ADD(file_size);
    EMU_ADD(fmt_len);
    EMU_END_GROUP();
}

////////////////////////////////////////////////////////////////////////////////

EMU_GROUP(nutils)
{
    EMU_ADD(narray);
    EMU_ADD(string);
    EMU_ADD(io);
    EMU_END_GROUP();
}
