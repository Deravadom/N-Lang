/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "compiler.testutils.h"

EMU_DECLARE(expressions__primary);
EMU_DECLARE(expressions__postfix);
EMU_DECLARE(expressions__unary);
EMU_DECLARE(expressions__binary);

EMU_TEST(expressions__without_semicolon__should_not_compile)
{
    COMPILE_TEST_PROG(COMPILE_FAILURE_SYNTAX_ANALYSIS, "                     \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    1s                                                                       \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_GROUP(compile__expressions)
{
    EMU_ADD(expressions__primary);
    EMU_ADD(expressions__postfix);
    EMU_ADD(expressions__unary);
    EMU_ADD(expressions__binary);
    EMU_ADD(expressions__without_semicolon__should_not_compile);
    EMU_END_GROUP();
}
