/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "compiler.testutils.h"

EMU_TEST(binary_cast__identity__implicit)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : mut u;                                                         \n\
    foo = 10u;                                                               \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(binary_cast__identity__explicit)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : mut u;                                                         \n\
    foo = 10u as u;                                                          \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_GROUP(binary_cast__identity)
{
    EMU_ADD(binary_cast__identity__implicit);
    EMU_ADD(binary_cast__identity__explicit);
    EMU_END_GROUP();
}

EMU_TEST(binary_cast__T_ptr_to_void_ptr__implicit)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let bar : ^u;                                                            \n\
    let foo : mut ^void = bar;                                               \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(binary_cast__T_ptr_to_void_ptr__explicit)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let bar : ^u;                                                            \n\
    let foo : mut ^void = bar as ^void;                                      \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_GROUP(binary_cast__T_ptr_to_void_ptr)
{
    EMU_ADD(binary_cast__T_ptr_to_void_ptr__implicit);
    EMU_ADD(binary_cast__T_ptr_to_void_ptr__explicit);
    EMU_END_GROUP();
}

EMU_TEST(binary_cast__void_ptr_to_T_ptr__implicit)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : mut ^u;                                                        \n\
    let bar : ^void;                                                         \n\
    foo = bar;                                                               \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(binary_cast__void_ptr_to_T_ptr__explicit)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : mut ^u;                                                        \n\
    let bar : ^void;                                                         \n\
    foo = bar as ^ u;                                                        \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_GROUP(binary_cast__void_ptr_to_T_ptr)
{
    EMU_ADD(binary_cast__void_ptr_to_T_ptr__implicit);
    EMU_ADD(binary_cast__void_ptr_to_T_ptr__explicit);
    EMU_END_GROUP();
}

EMU_TEST(binary_cast__integer_to_integer__implicit)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : mut u64;                                                       \n\
    let bar : s16;                                                           \n\
    foo = bar;                                                               \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(binary_cast__integer_to_integer__explicit)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : mut u64;                                                       \n\
    let bar : s16;                                                           \n\
    foo = bar as u64;                                                        \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_GROUP(binary_cast__integer_to_integer)
{
    EMU_ADD(binary_cast__integer_to_integer__implicit);
    EMU_ADD(binary_cast__integer_to_integer__explicit);
    EMU_END_GROUP();
}

EMU_TEST(binary_cast__ascii_to_integer__implicit)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : mut u64;                                                       \n\
    let bar : ascii;                                                         \n\
    foo = bar;                                                               \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(binary_cast__ascii_to_integer__explicit)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : mut u64;                                                       \n\
    let bar : ascii;                                                         \n\
    foo = bar as u64;                                                        \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_GROUP(binary_cast__ascii_to_integer)
{
    EMU_ADD(binary_cast__ascii_to_integer__implicit);
    EMU_ADD(binary_cast__ascii_to_integer__explicit);
    EMU_END_GROUP();
}

EMU_TEST(binary_cast__integer_to_ascii__implicit)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : mut ascii;                                                     \n\
    let bar : s16;                                                           \n\
    foo = bar;                                                               \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(binary_cast__integer_to_ascii__explicit)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : mut ascii;                                                     \n\
    let bar : s16;                                                           \n\
    foo = bar as ascii;                                                      \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_GROUP(binary_cast__integer_to_ascii)
{
    EMU_ADD(binary_cast__integer_to_ascii__implicit);
    EMU_ADD(binary_cast__integer_to_ascii__explicit);
    EMU_END_GROUP();
}

EMU_TEST(binary_cast__pointer_to_integer__implicit)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : mut u;                                                         \n\
    let bar : ^s32;                                                          \n\
    foo = bar;                                                               \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(binary_cast__pointer_to_integer__explicit)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : mut u;                                                         \n\
    let bar : ^s32;                                                          \n\
    foo = bar as u;                                                          \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_GROUP(binary_cast__pointer_to_integer)
{
    EMU_ADD(binary_cast__pointer_to_integer__implicit);
    EMU_ADD(binary_cast__pointer_to_integer__explicit);
    EMU_END_GROUP();
}

EMU_TEST(binary_cast__integer_to_pointer__implicit)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : mut ^s32;                                                      \n\
    let bar : u;                                                             \n\
    foo = bar;                                                               \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(binary_cast__integer_to_pointer__explicit)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : mut ^s32;                                                      \n\
    let bar : u;                                                             \n\
    foo = bar as ^s32;                                                       \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_GROUP(binary_cast__integer_to_pointer)
{
    EMU_ADD(binary_cast__integer_to_pointer__implicit);
    EMU_ADD(binary_cast__integer_to_pointer__explicit);
    EMU_END_GROUP();
}

EMU_TEST(binary_cast__pointer_to_pointer__implicit)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : mut ^s32;                                                      \n\
    let bar : ^u;                                                            \n\
    foo = bar;                                                               \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(binary_cast__pointer_to_pointer__explicit)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : mut ^s32;                                                      \n\
    let bar : ^u;                                                            \n\
    foo = bar as ^s32;                                                       \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_GROUP(binary_cast__pointer_to_pointer)
{
    EMU_ADD(binary_cast__pointer_to_pointer__implicit);
    EMU_ADD(binary_cast__pointer_to_pointer__explicit);
    EMU_END_GROUP();
}

EMU_TEST(binary_cast__function_return_value)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> s {return 1s;}                                              \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let bar : u = foo() as u;                                                \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_GROUP(binary_cast)
{
    EMU_ADD(binary_cast__identity);
    EMU_ADD(binary_cast__T_ptr_to_void_ptr);
    EMU_ADD(binary_cast__void_ptr_to_T_ptr);
    EMU_ADD(binary_cast__integer_to_integer);
    EMU_ADD(binary_cast__ascii_to_integer);
    EMU_ADD(binary_cast__integer_to_ascii);
    EMU_ADD(binary_cast__pointer_to_integer);
    EMU_ADD(binary_cast__integer_to_pointer);
    EMU_ADD(binary_cast__pointer_to_pointer);
    EMU_ADD(binary_cast__function_return_value);
    EMU_END_GROUP();
}
