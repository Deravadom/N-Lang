/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "compiler.testutils.h"

EMU_TEST(nested_scope_empty)
{
    COMPILE_TEST_VALID_PROG("                                                \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    isolate                                                                  \n\
    {                                                                        \n\
    }                                                                        \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(nested_scope_nonempty)
{
    COMPILE_TEST_VALID_PROG("                                                \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    isolate                                                                  \n\
    {                                                                        \n\
        let foo : u32;                                                       \n\
    }                                                                        \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(nested_scope_redeclare_identifier_from_higher_scope)
{
    COMPILE_TEST_VALID_PROG("                                                \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : bool;                                                          \n\
    isolate                                                                  \n\
    {                                                                        \n\
        let foo : s;                                                         \n\
        isolate                                                              \n\
        {                                                                    \n\
            let foo : u;                                                     \n\
        }                                                                    \n\
    }                                                                        \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(nested_scope_reference_identifier_from_higher_scope)
{
    COMPILE_TEST_VALID_PROG("                                                \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : s;                                                             \n\
    isolate                                                                  \n\
    {                                                                        \n\
        foo;                                                                 \n\
        isolate                                                              \n\
        {                                                                    \n\
            foo;                                                             \n\
        }                                                                    \n\
    }                                                                        \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_GROUP(nested_scope__valid)
{
    EMU_ADD(nested_scope_empty);
    EMU_ADD(nested_scope_nonempty);
    EMU_ADD(nested_scope_redeclare_identifier_from_higher_scope);
    EMU_ADD(nested_scope_reference_identifier_from_higher_scope);
    EMU_END_GROUP();
}

EMU_GROUP(compile__nested_scope)
{
    EMU_ADD(nested_scope__valid);
    EMU_END_GROUP();
}
