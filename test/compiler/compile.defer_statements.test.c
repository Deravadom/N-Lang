/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "compiler.testutils.h"

EMU_TEST(defer__execute_expression)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
let i : mut u = 0u;                                                          \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    defer{i =  i + 1u;}                                                      \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(defer__declare_variable)
{
    // Most of the time a defer statement wouldn't have a reason to allocate
    // variables on the stack.
    // However, long defer statements may have complex logic that requires
    // declaration of new variables, so these declarations should be allowed.
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    defer                                                                    \n\
    {                                                                        \n\
        let bar : mut u;                                                     \n\
    }                                                                        \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(defer__nested_defer)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let i : mut u = 0u;                                                      \n\
    defer                                                                    \n\
    {                                                                        \n\
        i = i + 2u;                                                          \n\
        defer                                                                \n\
        {                                                                    \n\
            i = i + 2u;                                                      \n\
            defer                                                            \n\
            {                                                                \n\
                i = i + 2u;                                                  \n\
                defer                                                        \n\
                {                                                            \n\
                    i = i + 1u;                                              \n\
                }                                                            \n\
                i = i + 1u;                                                  \n\
            }                                                                \n\
            i = i + 1u;                                                      \n\
        }                                                                    \n\
        i = i + 1u;                                                          \n\
    }                                                                        \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_GROUP(defer__valid)
{
    EMU_ADD(defer__execute_expression);
    EMU_ADD(defer__declare_variable);
    EMU_ADD(defer__nested_defer);
    EMU_END_GROUP();
}

EMU_TEST(defer__without_scope_block)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SYNTAX_ANALYSIS, "                   \n\
let i : mut u = 0u;                                                          \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    defer i += 1u;                                                           \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_GROUP(defer__invalid)
{
    EMU_ADD(defer__without_scope_block);
    EMU_END_GROUP();
}

EMU_GROUP(compile__defer_statements)
{
    EMU_ADD(defer__valid);
    EMU_ADD(defer__invalid);
    EMU_END_GROUP();
}
