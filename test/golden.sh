#! /bin/sh

# Copyright 2018 N-Lang Project Authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

print_helps()
{
    echo "Golden testing is based on the concept of saving the expected output"
    echo "of some code, and running regression tests, which get compared to"
    echo "the saved output."
    echo ""
    echo "The N Lang Team uses golden testing to regression test the nompile"
    echo "compiler. This script is responsible for running those tests."
    echo ""
    echo "The N Lang regression tests are saved in the folder: test/golden"
    echo "each test is stored in its own sub-directory and consists of up to"
    echo "six (6) files:"
    echo " * input.n       the N code defining the regression test."
    echo " * expected.c    the expected compiled output, excluding imports."
    echo " * expected.txt  the expected console output of the regression test."
    echo " * expected.ret  the expected return value of the compiled program."
    echo " * expected.nom  the expected output of the nompile compiler."
    echo " * config.sh     some configuration parameters for the test."
    echo ""
    echo "The config.sh file may have the following parameters:"
    echo " * input_file              The N source file to input"
    echo "       type: file  default: input.n"
    echo " * expected_file           Expected output of the N code"
    echo "       type: file  default: expected.txt"
    echo " * expected_c_file         Expected C equivelant of the N code"
    echo "       type: file  default: expected.c"
    echo " * expected_ret_file       Expected return value of the N code"
    echo "       type: file  default: expected.ret"
    echo " * expected_nom_file       expected output of nompile invocation"
    echo "       type: file  default: expected.nom"
    echo " * actual_c_file           Name of the intermediate/actual C file"
    echo "       type: file  default: actual.c"
    echo " * actual_out_file         Name of the intermediate executable file"
    echo "       type: file  default: actual.out"
    echo " * enable_diff            Enable the output diff test"
    echo "       type: bool  default: false"
    echo " * enable_c_diff          Enable the C diff test"
    echo "       type: bool  default: false"
    echo " * disable_nom_diff        Disable the nompile output diff test"
    echo "       type: bool  default: false"
    echo "       warning: the output diff test depends on this"
    echo " * consider_whitespace     Match whitespace while diffing output"
    echo "       type: bool  default: false"
    echo " * consider_c_whitespace   Match whitespace while diffing C files"
    echo "       type: bool  default: false"
    echo " * nompile_flags           The flags to pass to nompile"
    echo "       type: astr  default: <empty>"
    echo " * output_args             The arguments to pass to the N program"
    echo "       type: astr  default: <empty>"
    echo ""
    echo "USAGE: $0 [ OPTIONS ] (<test-name>|--all)"
    echo "OPTIONS:"
    echo "  --all            Run all the tests in the testing directory"
    echo "  --randomize      Randomize the order of all tests."
    echo "  --show-diff      Show the differences in text output"
    echo "  --show-c-diff    Show the differences in compiled C output"
    echo "  --show-nom-diff  Show the differences in the nompile error output"
    echo "  --save-output    Save the output/return as the new expected output"
    echo "  --save-c-out     Save the compiled C as the new expected C code"
    echo "  --save-nom-out   Save the nompile output as the new expected"
    echo "  --show-output    Show the output of the actual code"
    echo "  --show-c-out     Show the actual C code"
    echo "  --show-nom-out   Show the full output of the nompile invocation"
    echo "  --vg             Run the test on valgrind instead of diff"
    echo "  --vg-nom         Run the compiler on valgrind instead of diff"
    echo "  --db-nom         Launch a GDB session on nompile"
    echo "                     use DEBUG_FLAGS environment variable for flags"
    echo "  --verbose        Print verbose output"
    echo "  --help           Show the help and exit"
}

# TESTING CONSTANTS
TESTS_DIRECTORY="$( realpath test/golden )"
NOMPILE_CMD="$( realpath ./build/bin/nompile )"
WORK_DIRECTORY="$( realpath . )"

export TESTS_DIRECTORY
export NOMPILE_CMD
export WORK_DIRECTORY

# PROGRAM PARAMETERS
export TESTS_LIST=""
export ALL_TESTS=false
export RANDOMIZER="tee"
export SHOW_DIFF=false
export SHOW_C_DIFF=false
export SHOW_NOM_DIFF=false
export SAVE_OUTPUT=false
export SAVE_C_OUTPUT=false
export SAVE_NOM_OUTPUT=false
export SHOW_OUTPUT=false
export SHOW_C_OUTPUT=false
export SHOW_NOM_OUTPUT=false
export VALGRIND=false
export VALGRIND_NOM=false
export DEBUG_NOM=false
export VERBOSE_OUTPUT=false
export SHOW_HELP=false

verb()
{
    if "$VERBOSE_OUTPUT"
    then
        echo "$@"
    fi
}

alias RED="printf '\033[31m'"
alias REGULAR="printf '\033[0m'"
alias GREEN="printf '\033[32m'"

run_test()
{
    echo "TESTING: $1"
    cd "$TESTS_DIRECTORY/$1"
    verb "$( pwd )"

    # Sub Shell to run the tests (to prevent overridding of parameters)
    # 1. configure the test.
    # 2. run the C source file diff test.
    # 3. run the nompile and nompile diff test
    # 3. run the output diff test.
    (
        input_file="input.n"
        expected_c_file="expected.c"
        expected_file="expected.txt"
        expected_ret_file="expected.ret"
        expected_nom_file="expected.nom"
        actual_c_file="actual.c"
        actual_out_file="actual.out"
        actual_nom_file="actual.nom"
        enable_diff=false
        enable_c_diff=false
        disable_nom_diff=false
        consider_whitespace=false
        consider_c_whitespace=false
        nompile_flags=""
        output_args=""

        if [ -f ./config.sh ]
        then
            . ./config.sh
        fi

        num_fails=0

        if ! "$enable_c_diff"
        then
            echo "  C99 diff: SKIP"
        else
            nomp_cmd="$NOMPILE_CMD $nompile_flags $input_file -t c99"
            nomp_cmd="$nomp_cmd -o $actual_c_file"
            verb "$nomp_cmd"
            $nomp_cmd > /dev/null 2>&1

            c_diff_awk="BEGIN{ p=\"false\"; }"
            c_diff_awk="$c_diff_awk { if(p == \"true\") { print(\$0); } }"
            c_diff_awk="$c_diff_awk \"/* END IMPORTS */\" == \$0{ p=\"true\"; }"
            c_diff_cmd="diff"
            if ! "$consider_c_whitespace"
            then
               c_diff_cmd="$c_diff_cmd --ignore-all-space"
            fi

            c_diff_cmd="$c_diff_cmd $expected_c_file -"

            if "$SAVE_C_OUTPUT"
            then
                echo "Saving C File: $expected_c_file"
                awk "$c_diff_awk" < "$actual_c_file" > "$expected_c_file"
            fi

            if "$SHOW_C_OUTPUT"
            then
                echo "Showing C Output"
                awk "$c_diff_awk" < "$actual_c_file"
            fi

            verb "awk \"$c_diff_awk\" < \"$actual_c_file\" |  $c_diff_cmd"
            if [ ! -f "$expected_c_file" ]
            then
                printf "  C99 diff: " ; RED ; printf "SKIP" ; REGULAR
                echo ": missing expected C file"
                num_fails=1
            else
                if "$SHOW_C_DIFF"
                then
                    echo "Showing C Diff"
                    awk "$c_diff_awk" < "$actual_c_file" | $c_diff_cmd
                else
                    awk "$c_diff_awk" < "$actual_c_file" | $c_diff_cmd > /dev/null
                fi

                if [ "$?" -eq 0 ]
                then
                    printf "  C99 diff:" ; GREEN ; echo " PASS" ; REGULAR
                else
                    printf "  C99 diff:" ;  RED  ; echo " FAIL" ; REGULAR
                    num_fails=1
                fi
            fi

            if [ -f "$actual_c_file" ]
            then
                rm "$actual_c_file"
            fi
        fi

        if "$disable_nom_diff"
        then
            echo "  nom diff: SKIP"
        elif "$VALGRIND_NOM"
        then
            echo "Nompile Valgrind:"
            nomp_cmd="$NOMPILE_CMD $nompile_flags $input_file -t native"
            nomp_cmd="$nomp_cmd -o $actual_out_file"
            vg_cmd="valgrind --leak-check=yes --leak-check=full"
            vg_cmd="$vg_cmd --show-leak-kinds=all --track-origins=yes"
            verb "$vg_cmd $nomp_cmd"
            $vg_cmd $nomp_cmd
        elif "$DEBUG_NOM"
        then
            nomp_cmd="$NOMPILE_CMD $nompile_flags $input_file -t native"
            nomp_cmd="$nomp_cmd -o $actual_out_file"
            verb "gdb $DEBUG_FLAGS --args $nomp_cmd"
            gdb $DEBUG_FLAGS --args $nomp_cmd
        elif [ ! -f "$expected_nom_file" -a ! "$SAVE_NOM_OUTPUT" ]
        then
            printf "  nom diff: " ; RED ; printf "SKIP" ; REGULAR
            echo ": missing expected nom file"
            num_fails=1
        else
            nomp_cmd="$NOMPILE_CMD $nompile_flags $input_file -t native"
            nomp_cmd="$nomp_cmd -o $actual_out_file"
            verb "$nomp_cmd"
            $nomp_cmd > "$actual_nom_file" 2>&1

            if "$SAVE_NOM_OUTPUT"
            then
                echo "Saving nom file: $expected_nom_file"
                cp "$actual_nom_file" "$expected_nom_file"
            fi

            if "$SHOW_NOM_OUTPUT"
            then
                echo "Showing nom output"
                cat "$actual_nom_file"
            fi


            if [ ! -f "$expected_nom_file" ]
            then
                printf "  nom diff: " ; RED ; printf "SKIP" ; REGULAR
                echo ": missing expected nom file"
                num_fails=1
            else
                if "$SHOW_NOM_DIFF"
                then
                    diff expected.nom actual.nom
                else
                    diff expected.nom actual.nom > /dev/null
                fi

                if [ "$?" -eq "0" ]
                then
                    printf "  nom diff:" ; GREEN ; echo " PASS" ; REGULAR
                else
                    printf "  nom diff:" ;  RED  ; echo " FAIL" ; REGULAR
                    num_fails=1
                fi
            fi

            if [ -f "$actual_nom_file" ]
            then
                rm "$actual_nom_file"
            fi
        fi

        if ! "$enable_diff"
        then
            echo "  txt diff: SKIP"
            echo "  ret diff: SKIP"
        elif "$disable_nom_diff"
        then
            printf "  txt diff: " RED ; printf "SKIP" ; REGULAR
            echo ": disabled by disabling nompile diff"
            printf "  ret diff: " RED ; printf "SKIP" ; REGULAR
            echo ": disabled by disabling nompile diff"
            num_fails=1
        elif [ ! -f "$actual_out_file" ]
        then
            printf "  txt diff: " ; RED ; printf "SKIP" ; REGULAR
            echo ": missing compiled code"
            printf "  ret diff: " RED ; printf "SKIP" ; REGULAR
            echo ": missing compiled code"
            num_fails=1
        elif "$VALGRIND"
        then
            vg_cmd="valgrind --leak-check=yes --leak-check=full"
            vg_cmd="$vg_cmd --show-leak-kinds=all --track-origins=yes"
            verb "$vg_cmd ./$actual_out_file $output_args"
            $vg_cmd "./$actual_out_file" $output_args

        else
            diff_cmd="diff"
            if ! "$consider_whitespace"
            then
                diff_cmd="$diff_cmd --ignore-all-space"
            fi
            diff_cmd="$diff_cmd $expected_file -"

            if "$SAVE_OUTPUT"
            then
                echo "Saving txt File: $expected_file"
                echo "Saving ret File: $expected_ret_file"
                sh -c "./$actual_out_file $output_args" > $expected_file
                echo "$?" > "$expected_ret_file"
            fi

            if "$SHOW_OUTPUT"
            then
                echo "Showing Output"
                sh -c "./$actual_out_file $output_args"
                echo "Return Value: $?"
            fi

            verb "sh -c \"./$actual_out_file $output_args\" 2>&1 | $diff_cmd"

            if [ ! -f "$expected_file" ]
            then
                printf "  txt diff: " ; RED ; printf "SKIP" ; REGULAR
                echo ": missing expected file"
                printf "  ret diff: " RED ; printf "SKIP" ; REGULAR
                echo ": missing expected file"
                num_fails=1
            elif [ ! -f "$expected_ret_file" ]
            then
                printf "  txt diff: " ; RED ; printf "SKIP" ; REGULAR
                echo ": missing expected ret file"
                printf "  ret diff: " RED ; printf "SKIP" ; REGULAR
                echo ": missing expected ret file"
                num_fails=1
            else
                if "$SHOW_DIFF"
                then
                    echo Showing Diff
                    sh -c "./$actual_out_file $output_args" 2>&1 | $diff_cmd
                else
                    sh -c "./$actual_out_file $output_args" 2>&1 | $diff_cmd > \
                        /dev/null
                fi
                diff_ret="$?"
                sh -c "./$actual_out_file $output_args" > /dev/null 2>&1
                actual_ret="$?"

                if [ "$diff_ret" -eq 0 ]
                then
                    printf "  txt diff:" ; GREEN ; echo " PASS" ; REGULAR
                else
                    printf "  txt diff:" ;  RED  ; echo " FAIL" ; REGULAR
                    num_fails=1
                fi

                if [ "$actual_ret" -eq "$( cat $expected_ret_file )" ]
                then
                    printf "  ret diff:" ; GREEN ; echo " PASS" ; REGULAR
                else
                    printf "  ret diff:" ;  RED  ; printf " FAIL" ; REGULAR
                    printf " expected: " ; GREEN
                    printf "%s" "$( cat $expected_ret_file )" ; REGULAR
                    printf " actual: " ; RED ; printf "%s" "$actual_ret" ; REGULAR
                    echo
                    num_fails=1
                fi
            fi
        fi

        if [ -f "$actual_out_file" ]
        then
            rm "$actual_out_file"
        fi

        if [ -d "./usr" ]
        then
            rm -rf ./usr
        fi

        exit "$num_fails"
    )

    fail_count="$( expr "$fail_count" + "$?" )"

    cd "$WORK_DIRECTORY"
}

parse_arguments()
{
    parsing=true
    while "$parsing"
    do
        verb "Parsing: $1"
        if [ "$1" = "--all" ]
        then
            export ALL_TESTS=true
        elif [ "$1" = "--randomize" ]
        then
            export RANDOMIZER="shuf"
        elif [ "$1" = "--show-diff" ]
        then
            export SHOW_DIFF=true
        elif [ "$1" = "--show-c-diff" ]
        then
            export SHOW_C_DIFF=true
        elif [ "$1" = "--show-nom-diff" ]
        then
            export SHOW_NOM_DIFF=true
        elif [ "$1" = "--save-output" ]
        then
            export SAVE_OUTPUT=true
        elif [ "$1" = "--save-c-out" ]
        then
            export SAVE_C_OUTPUT=true
        elif [ "$1" = "--save-nom-out" ]
        then
            export SAVE_NOM_OUTPUT=true
        elif [ "$1" = "--show-output" ]
        then
            export SHOW_OUTPUT=true
        elif [ "$1" = "--show-c-out" ]
        then
            export SHOW_C_OUTPUT=true
        elif [ "$1" = "--show-nom-out" ]
        then
            export SHOW_NOM_OUTPUT=true
        elif [ "$1" = "--vg" ]
        then
            export VALGRIND=true
        elif [ "$1" = "--vg-nom" ]
        then
            export VALGRIND_NOM=true
        elif [ "$1" = "--db-nom" ]
        then
            export DEBUG_NOM=true
        elif [ "$1" = "--verbose" ]
        then
            export VERBOSE_OUTPUT=true
        elif [ "$1" = "--help" ]
        then
            export SHOW_HELP=true
        else
            export TESTS_LIST="$TESTS_LIST $1"
        fi

        verb "number of args to parse: $#"
        if [ "$#" -gt 0 ]
        then
            shift
        fi
        if [ "$#" -eq 0 ]
        then
            parsing=false
        fi
    done
}

recurse_dirs()
{
    for DIR in $( cd "$TESTS_DIRECTORY/$1" ; ls ; cd "$WORK_DIRECTORY" )
    do
        if [ -d "$TESTS_DIRECTORY/$1/$DIR" ]
        then
            recurse_dirs "$1/$DIR"
        fi
    done
    if [ $( cd "$TESTS_DIRECTORY/$1" ; \
        find . -maxdepth 1 -type f | wc -l ; \
        cd "$WORK_DIRECTORY" ) -gt 0 ]
    then
        echo "$1"
    fi
}

recurse_all_dirs()
{
    for DIR in $TESTS_LIST
    do
        if [ -d "$TESTS_DIRECTORY/$DIR" ]
        then
            recurse_dirs "$DIR"
        fi
    done
}


parse_arguments "$@"

if "$SHOW_HELP"
then
    print_helps
    exit
fi

if "$ALL_TESTS"
then
    TESTS_LIST=$( cd "$TESTS_DIRECTORY" ; ls ; cd "$WORK_DIRECTORY" )
fi

verb "TESTS_LIST=$TESTS_LIST"
verb "ALL_TESTS=$ALL_TESTS"
verb "RANDOMIZER=$RANDOMIZER"
verb "SHOW_DIFF=$SHOW_DIFF"
verb "SHOW_C_DIFF=$SHOW_C_DIFF"
verb "SHOW_NOM_DIFF=$SHOW_NOM_DIFF"
verb "SAVE_OUTPUT=$SAVE_OUTPUT"
verb "SAVE_C_OUTPUT=$SAVE_C_OUTPUT"
verb "SAVE_NOM_OUTPUT=$SAVE_NOM_OUTPUT"
verb "SHOW_OUTPUT=$SHOW_OUTPUT"
verb "SHOW_C_OUTPUT=$SHOW_C_OUTPUT"
verb "SHOW_NOM_OUTPUT=$SHOW_NOM_OUTPUT"
verb "VALGRIND=$VALGRIND"
verb "VALGRIND_NOM=$VALGRIND_NOM"
verb "VERBOSE_OUTPUT=$VERBOSE_OUTPUT"
verb "SHOW_HELP=$SHOW_HELP"

ACTUAL_TEST_LIST=$( recurse_all_dirs | $RANDOMIZER )

fail_count=0
test_count=0

verb "$ACTUAL_TEST_LIST"

for ACTUAL_TEST in $ACTUAL_TEST_LIST
do
    if [ -d "$TESTS_DIRECTORY/$ACTUAL_TEST" ]
    then
        run_test "$ACTUAL_TEST"
        test_count=$( expr "$test_count" + 1 )
    fi
done

echo "Test Count: $test_count"
echo "Fail Count: $fail_count"

exit "$fail_count"
