import "std/character.n"a;
import "std/integer.n"a;
import "std/memory.n"a;

# Return the number of ASCII characters from the start of `str` up to but not
# including the null terminator.
export introduce func astr_length : (str : ^ascii) -> u;

# Copy `n` characters from `src` into the buffer `dest`.
#
# NOTE: The behavior of `astr_copy` is different from that of libc's `strncpy`.
# `strncpy` copies at most `n` bytes, but will *not* null terminate the string
# if a null terminator was not found. `astr_copy` will copy at most `n` bytes
# *including* a null terminator that will always be placed at the end of the
# destination string. With this behavior, the destination buffer will always be
# usable as an astr after a call to this function.
export introduce func astr_copy : (dest : ^mut ascii, src : ^ascii, n : u) -> void;
export introduce func astr_cat : (dest : ^mut ascii, left : ^ascii, right :
^ascii) -> void;

export introduce func astr_cmp : (lhs : ^ascii, rhs : ^ascii) -> s;
export introduce func astr_eq : (lhs : ^ascii, rhs : ^ascii) -> bool;
export introduce func astr_ne : (lhs : ^ascii, rhs : ^ascii) -> bool;

export introduce func astr_cmp_icase : (lhs : ^ascii, rhs : ^ascii) -> s;
export introduce func astr_eq_icase : (lhs : ^ascii, rhs : ^ascii) -> bool;
export introduce func astr_ne_icase : (lhs : ^ascii, rhs : ^ascii) -> bool;

export introduce func astr_cmp_n : (lhs : ^ascii, rhs : ^ascii, n : u) -> s;
export introduce func astr_eq_n : (lhs : ^ascii, rhs : ^ascii, n : u) -> bool;
export introduce func astr_ne_n : (lhs : ^ascii, rhs : ^ascii, n : u) -> bool;

export introduce func astr_cmp_n_icase : (lhs : ^ascii, rhs : ^ascii, n : u) -> s;
export introduce func astr_eq_n_icase : (lhs : ^ascii, rhs : ^ascii, n : u) -> bool;
export introduce func astr_ne_n_icase : (lhs : ^ascii, rhs : ^ascii, n : u) -> bool;

# astr_to_* functions return the number of chars from the input string traversed
# (positive) on success or 0u on failure.
export introduce func astr_to_u8 : (str : ^ascii, to : ^mut u8, base : u8) -> u;
export introduce func astr_to_u16 : (str : ^ascii, to : ^mut u16, base : u8) -> u;
export introduce func astr_to_u32 : (str : ^ascii, to : ^mut u32, base : u8) -> u;
export introduce func astr_to_u64 : (str : ^ascii, to : ^mut u64, base : u8) -> u;
export introduce func astr_to_u : (str : ^ascii, to : ^mut u8, base : u8) -> u;
export introduce func astr_to_s8 : (str : ^ascii, to : ^mut s8, base : u8) -> u;
export introduce func astr_to_s16 : (str : ^ascii, to : ^mut s16, base : u8) -> u;
export introduce func astr_to_s32 : (str : ^ascii, to : ^mut s32, base : u8) -> u;
export introduce func astr_to_s64 : (str : ^ascii, to : ^mut s64, base : u8) -> u;
export introduce func astr_to_s : (str : ^ascii, to : ^mut s, base : u8) -> u;

################################################################################
###                                 INTERNAL                                 ###
################################################################################

export func astr_length : (str : ^ascii) -> u
{
    let walker : mut ^ascii = str;
    loop @walker != '\0'a
    {
        walker = walker +^ 1u;
    }
    return walker -^^ str;
}

export func astr_copy : (dest : ^mut ascii, src : ^ascii, n : u) -> void
{
    let dest_walker : mut ^mut ascii = dest;
    let src_walker : mut ^ascii = src;
    let asciis_remaining : mut u = n;
    loop asciis_remaining != 0u && @src_walker != '\0'a
    {
        @dest_walker = @src_walker;
        src_walker = src_walker +^ 1u;
        dest_walker = dest_walker +^ 1u;
        asciis_remaining = asciis_remaining - 1u;
    }
    @dest_walker = '\0'a;
}

export func astr_cat : (dest : ^mut ascii, left : ^ascii, right : ^ascii) ->
void
{
	let dest_walker : mut ^ mut ascii = dest;
	let left_walker : mut ^ascii = left;
	let right_walker : mut ^ascii = right;
	
	loop @left_walker != '\0'a
	{
		@dest_walker = @left_walker;
		left_walker = left_walker +^ 1u;
		dest_walker = dest_walker +^ 1u;
	}
	loop @right_walker != '\0'a
		@dest_walker = @right_walker;
		right_walker = right_walker +^ 1u;
		dest_walker = dest_walker +^ 1u;
	}
	@dest_walker = '\0'a;
}

export func astr_cmp : (lhs : ^ascii, rhs : ^ascii) -> s
{
    let lhs_walker : mut ^ascii = lhs;
    let rhs_walker : mut ^ascii = rhs;
    loop @lhs_walker != '\0'a && @lhs_walker == @rhs_walker
    {
        lhs_walker = lhs_walker +^ 1u;
        rhs_walker = rhs_walker +^ 1u;
    }
    return @lhs_walker as s - @rhs_walker as s;
}

export func astr_eq : (lhs : ^ascii, rhs : ^ascii) -> bool
{
    return astr_cmp(lhs, rhs) == 0s;
}

export func astr_ne : (lhs : ^ascii, rhs : ^ascii) -> bool
{
    return astr_cmp(lhs, rhs) != 0s;
}

export func astr_cmp_icase : (lhs : ^ascii, rhs : ^ascii) -> s
{
    let lhs_walker : mut ^ascii = lhs;
    let rhs_walker : mut ^ascii = rhs;
    loop @lhs_walker != '\0'a
        && ascii_to_lower(@lhs_walker) == ascii_to_lower(@rhs_walker)
    {
        lhs_walker = lhs_walker +^ 1u;
        rhs_walker = rhs_walker +^ 1u;
    }
    return ascii_to_lower(@lhs_walker) as s - ascii_to_lower(@rhs_walker) as s;
}

export func astr_eq_icase : (lhs : ^ascii, rhs : ^ascii) -> bool
{
    return astr_cmp_icase(lhs, rhs) == 0s;
}

export func astr_ne_icase : (lhs : ^ascii, rhs : ^ascii) -> bool
{
    return astr_cmp_icase(lhs, rhs) != 0s;
}

export func astr_cmp_n : (lhs : ^ascii, rhs : ^ascii, n : u) -> s
{
    let lhs_walker : mut ^ascii = lhs;
    let rhs_walker : mut ^ascii = rhs;
    let chars_remaining : mut u = n;
    loop chars_remaining != 0u
        && @lhs_walker != '\0'a && @lhs_walker == @rhs_walker
    {
        lhs_walker = lhs_walker +^ 1u;
        rhs_walker = rhs_walker +^ 1u;
        chars_remaining = chars_remaining - 1u;
    }
    return @lhs_walker as s - @rhs_walker as s;
}

export func astr_eq_n : (lhs : ^ascii, rhs : ^ascii, n : u) -> bool
{
    return astr_cmp_n(lhs, rhs, n) == 0s;
}

export func astr_ne_n : (lhs : ^ascii, rhs : ^ascii, n : u) -> bool
{
    return astr_cmp_n(lhs, rhs, n) != 0s;
}

export func astr_cmp_n_icase : (lhs : ^ascii, rhs : ^ascii, n : u) -> s
{
    let lhs_walker : mut ^ascii = lhs;
    let rhs_walker : mut ^ascii = rhs;
    let chars_remaining : mut u = n;
    loop chars_remaining != 0u && @lhs_walker != '\0'a
        && ascii_to_lower(@lhs_walker) == ascii_to_lower(@rhs_walker)
    {
        lhs_walker = lhs_walker +^ 1u;
        rhs_walker = rhs_walker +^ 1u;
        chars_remaining = chars_remaining - 1u;
    }
    return ascii_to_lower(@lhs_walker) as s - ascii_to_lower(@rhs_walker) as s;
}

export func astr_eq_n_icase : (lhs : ^ascii, rhs : ^ascii, n : u) -> bool
{
    return astr_cmp_n_icase(lhs, rhs, n) == 0s;
}

export func astr_ne_n_icase : (lhs : ^ascii, rhs : ^ascii, n : u) -> bool
{
    return astr_cmp_n_icase(lhs, rhs, n) != 0s;
}

export func astr_to_u8 : (str : ^ascii, to : ^mut u8, base : u8) -> u
{
    let u64_to : mut u64;
    let ret : u = astr_to_u64(str, ?u64_to, base);
    @to = u64_to as typeof(@to);
    return ret;
}

export func astr_to_u16 : (str : ^ascii, to : ^mut u16, base : u8) -> u
{
    let u64_to : mut u64;
    let ret : u = astr_to_u64(str, ?u64_to, base);
    @to = u64_to as typeof(@to);
    return ret;
}

export func astr_to_u32 : (str : ^ascii, to : ^mut u32, base : u8) -> u
{
    let u64_to : mut u64;
    let ret : u = astr_to_u64(str, ?u64_to, base);
    @to = u64_to as typeof(@to);
    return ret;
}

export func astr_to_u64 : (str : ^ascii, to : ^mut u64, base : u8) -> u
{
    let walker : mut ^ascii = str;
    let tmp : mut u64 = 0s as u64;

    let is_negative : bool = @walker == '-'a;
    if (is_negative){return 0u;}

    let digit : s = ascii_to_digit_b36(@walker);
    loop digit >= 0s && digit < base as typeof(digit)
    {
        tmp = tmp * base as typeof(tmp);
        tmp = tmp + digit as typeof(tmp);
    }

    if (walker == str)
    {
        # No digits were parsed.
        @to = 0s as u64;
        return 0u;
    }
    @to = tmp;
    return walker -^^ str;
}

export func astr_to_u : (str : ^ascii, to : ^mut u8, base : u8) -> u
{
    let u64_to : mut u64;
    let ret : u = astr_to_u64(str, ?u64_to, base);
    @to = u64_to as typeof(@to);
    return ret;
}

export func astr_to_s8 : (str : ^ascii, to : ^mut s8, base : u8) -> u
{
    let s64_to : mut s64;
    let ret : u = astr_to_s64(str, ?s64_to, base);
    @to = s64_to as typeof(@to);
    return ret;
}

export func astr_to_s16 : (str : ^ascii, to : ^mut s16, base : u8) -> u
{
    let s64_to : mut s64;
    let ret : u = astr_to_s64(str, ?s64_to, base);
    @to = s64_to as typeof(@to);
    return ret;
}

export func astr_to_s32 : (str : ^ascii, to : ^mut s32, base : u8) -> u
{
    let s64_to : mut s64;
    let ret : u = astr_to_s64(str, ?s64_to, base);
    @to = s64_to as typeof(@to);
    return ret;
}

export func astr_to_s64 : (str : ^ascii, to : ^mut s64, base : u8) -> u
{
    let walker : mut ^ascii = str;
    let tmp : mut s64 = 0s as s64;

    let is_negative : bool = @walker == '-'a;
    if (is_negative){walker = walker +^ 1u;}

    let digit : s = ascii_to_digit_b36(@walker);
    loop digit >= 0s && digit < base as typeof(digit)
    {
        tmp = tmp * base as typeof(tmp);
        tmp = tmp + digit as typeof(tmp);
    }

    if (is_negative){tmp = tmp * -1s as s64;}
    if ((walker == str) || ((walker == str +^ 1u) && is_negative))
    {
        # No digits were parsed.
        @to = 0s as s64;
        return 0u;
    }
    @to = tmp;
    return walker -^^ str;
}

export func astr_to_s : (str : ^ascii, to : ^mut s, base : u8) -> u
{
    let s64_to : mut s64;
    let ret : u = astr_to_s64(str, ?s64_to, base);
    @to = s64_to as typeof(@to);
    return ret;
}
