# Copyright 2018 N-Lang Project Authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import "std/astr.n"a;
import "std/panic.n"a;

export introduce struct astring;

export introduce func astring_init : (this : ^mut astring) -> void;
export introduce func astring_init_astr : (this : ^mut astring, other : ^ascii) -> void;
export introduce func astring_init_astring : (this : ^mut astring, other : ^astring) -> void;
export introduce func astring_fini : (this : ^mut astring) -> void;

export introduce func astring_assign_astr : (this : ^mut astring, other : ^ascii) -> void;
export introduce func astring_assign_astring : (this : ^mut astring, other : ^astring) -> void;

export introduce func astring_data : (this : ^astring) -> ^ascii;
export introduce func astring_length : (this : ^astring) -> u;
export introduce func astring_capacity : (this : ^astring) -> u;

export introduce func astring_is_empty : (this : ^astring) -> bool;

export introduce func astring_at : (this : ^astring, idx : u) -> ^mut ascii;
export introduce func astring_front : (this : ^astring) -> ^mut ascii;
export introduce func astring_back : (this : ^astring) -> ^mut ascii;
export introduce func astring_end : (this : ^astring) -> ^ascii;

export introduce func astring_resize : (this : ^mut astring, length : u) -> void;
export introduce func astring_reserve : (this : ^mut astring, nasciis : u) -> void;
export introduce func astring_clear : (this : ^mut astring) -> void;

export introduce func astring_prepend_ascii : (this : ^mut astring, ch : ascii) -> void;
export introduce func astring_prepend_astr : (this : ^mut astring, str : ^ascii) -> void;
export introduce func astring_prepend_astring : (this : ^mut astring, str : ^astring) -> void;

export introduce func astring_append_ascii : (this : ^mut astring, ch : ascii) -> void;
export introduce func astring_append_astr : (this : ^mut astring, str : ^ascii) -> void;
export introduce func astring_append_astring : (this : ^mut astring, str : ^astring) -> void;

export introduce func astring_transform_upper : (this : ^mut astring) -> void;
export introduce func astring_transform_lower : (this : ^mut astring) -> void;
export introduce func astring_trim_front : (this : ^mut astring) -> void;
export introduce func astring_trim_back : (this : ^mut astring) -> void;

export struct astring
{
    let _data : mut ^mut ascii;
    let _length : mut u;
    let _capacity : mut u;

    func init            = astring_init;
    func init_astr       = astring_init_astr;
    func init_astring    = astring_init_astring;
    func fini            = astring_fini;

    func assign_astr     = astring_assign_astr;
    func assign_astring  = astring_assign_astring;

    func data            = astring_data;
    func length          = astring_length;
    func capacity        = astring_capacity;

    func is_empty        = astring_is_empty;

    func at              = astring_at;
    func front           = astring_front;
    func back            = astring_back;
    func end             = astring_end;

    func resize          = astring_resize;
    func reserve         = astring_reserve;
    func clear           = astring_clear;

    func prepend_ascii   = astring_prepend_ascii;
    func prepend_astr    = astring_prepend_astr;
    func prepend_astring = astring_prepend_astring;

    func append_ascii    = astring_append_ascii;
    func append_astr     = astring_append_astr;
    func append_astring  = astring_append_astring;

    func transform_upper = astring_transform_upper;
    func transform_lower = astring_transform_lower;
    func trim_front      = astring_trim_front;
    func trim_back       = astring_trim_back;
}

################################################################################
###                                 INTERNAL                                 ###
################################################################################

let ASTRING_CAPACITY_MIN : u = 10u;
let ASTRING_CAPACITY_FACTOR : f64 = 2.0f64;

let ASTRING_DEFAULT_INIT_LENGTH : u = 0u;
let ASTRING_DEFAULT_INIT_CAPACITY : u = 10u;

func new_capacity : (length : u) -> u
{
    if length < ASTRING_CAPACITY_MIN
    {
        return ASTRING_CAPACITY_MIN;
    }
    return (length as f64 * ASTRING_CAPACITY_FACTOR) as u;
}

func data_alloc_size : (capcity : u) -> u
{
    return capcity + sizeof('\0'a);
}

export func astring_init : (this : ^mut astring) -> void
{
    let data_size : u = data_alloc_size(ASTRING_DEFAULT_INIT_CAPACITY);

    this->_data = allocate(data_alloc_size(ASTRING_DEFAULT_INIT_CAPACITY));
    this->_length = ASTRING_DEFAULT_INIT_LENGTH;
    this->_capacity = ASTRING_DEFAULT_INIT_CAPACITY;
    this->_data[0u] = '\0'a;
}

export func astring_init_astr : (this : ^mut astring, other : ^ascii) -> void
{
    let other_length : u = astr_length(other);
    let capacity : u = new_capacity(other_length);
    let data_size : u = data_alloc_size(capacity);

    this->_data = allocate(data_size);
    astr_copy(this->_data, other, data_size);

    this->_length = other_length;
    this->_capacity = capacity;
}

export func astring_init_astring : (this : ^mut astring, other : ^astring) -> void
{
    let other_length : u = other->_length;
    let capacity : u = new_capacity(other_length);
    let data_size : u = data_alloc_size(capacity);

    this->_data = allocate(data_size);
    astr_copy(this->_data, other->_data, data_size);

    this->_length = other_length;
    this->_capacity = capacity;
}

export func astring_fini : (this : ^mut astring) -> void
{
    deallocate(this->_data);
}

export func astring_assign_astr : (this : ^mut astring, other : ^ascii) -> void
{
    let other_length : u = astr_length(other);
    this->resize(other_length);
    astr_copy(this->_data, other, other_length + 1u);
}

export func astring_assign_astring : (this : ^mut astring, other : ^astring) -> void
{
    let other_length : u = other->_length;
    this->resize(other_length);
    astr_copy(this->_data, other->_data, other_length + 1u);
}

export func astring_data : (this : ^astring) -> ^ascii
{
    return this->_data;
}

export func astring_length : (this : ^astring) -> u
{
    return this->_length;
}

export func astring_capacity : (this : ^astring) -> u
{
    return this->_capacity;
}

export func astring_is_empty : (this : ^astring) -> bool
{
    return this->length() == 0u;
}

export func astring_at : (this : ^astring, idx : u) -> ^mut ascii
{
    if idx >= this->_length
    {
        panic(PANIC_INVALID_INDEX);
    }
    return ?this->_data[idx];
}

export func astring_front : (this : ^astring) -> ^mut ascii
{
    return this->at(0u);
}

export func astring_back : (this : ^astring) -> ^mut ascii
{
    return this->at(this->length() - 1u);
}

export func astring_end : (this : ^astring) -> ^ascii
{
    return this->at(this->length());
}

export func astring_resize : (this : ^mut astring, length : u) -> void
{
    if this->_length < length
    {
        astring_reserve(this, length - (this->_length));
    }
    this->_length = length;
    this->_data[this->_length] = '\0'a;
}

export func astring_reserve : (this : ^mut astring, nasciis : u) -> void
{
    let min_capacity : u = this->_length + nasciis + sizeof('\0'a);
    let curr_capacity : u = this->_capacity;

    if curr_capacity >= min_capacity{return;}

    let capacity : u = (min_capacity as f64 * ASTRING_CAPACITY_FACTOR) as u;
    let data_size : u = data_alloc_size(capacity);

    this->_data = reallocate(this->_data, data_size);
    this->_capacity = capacity;
}

export func astring_clear : (this : ^mut astring) -> void
{
    astring_resize(this, 0u);
}

export func astring_prepend_ascii : (this : ^mut astring, ch : ascii) -> void
{
    let old_len : u = this->length();

    this->resize(old_len + 1u);
    memory_copy_overlapping(this->_data +^ 1u, this->_data, old_len);
    @this->front() = ch;
}

export func astring_prepend_astr : (this : ^mut astring, str : ^ascii) -> void
{
    let old_len : u = this->length();
    let str_len : u = astr_length(str);

    this->resize(old_len + str_len);
    memory_copy_overlapping(this->_data +^ str_len, this->_data, old_len);
    memory_copy(this->_data, str, str_len);
}

export func astring_prepend_astring : (this : ^mut astring, str : ^astring) -> void
{
    let old_len : u = this->length();
    let str_len : u = str->length();

    this->resize(old_len + str_len);
    memory_copy_overlapping(this->_data +^ str_len, this->_data, old_len);
    memory_copy(this->_data, str->data(), str_len);
}

export func astring_append_ascii : (this : ^mut astring, ch : ascii) -> void
{
    this->resize(this->length() + 1u);
    @this->back() = ch;
}

export func astring_append_astr : (this : ^mut astring, str : ^ascii) -> void
{
    let old_len : u = this->length();
    let str_len : u = astr_length(str);

    this->resize(old_len + str_len);
    memory_copy(this->_data +^ old_len, str, str_len);
}

export func astring_append_astring : (this : ^mut astring, str : ^astring) -> void
{
    let old_len : u = this->length();
    let str_len : u = str->_length;

    this->resize(old_len + str_len);
    memory_copy(this->_data +^ old_len, str->data(), str_len);
}

export func astring_transform_upper : (this : ^mut astring) -> void
{
    let ch : mut ^mut ascii = this->front();
    let end : ^ascii = this->end();
    loop ch != end
    {
        @ch = ascii_to_upper(@ch);
        ch = ch +^ 1u;
    }
}

export func astring_transform_lower : (this : ^mut astring) -> void
{
    let ch : mut ^mut ascii = this->front();
    let end : ^ascii = this->end();
    loop ch != end
    {
        @ch = ascii_to_lower(@ch);
        ch = ch +^ 1u;
    }
}

export func astring_trim_front : (this : ^mut astring) -> void
{
    let front : ^ascii = this->front();
    if !ascii_is_space(@front){return;}

    let ch : mut ^ascii = front;
    loop ascii_is_space(@ch){ch = ch +^ 1u;}
    let num_asciis : u = ch -^^ front;

    let new_length : u = this->length() - num_asciis;
    memory_copy_overlapping(this->_data, this->_data +^ num_asciis, new_length);
    this->resize(new_length);
}

export func astring_trim_back : (this : ^mut astring) -> void
{
    let back : ^ascii = this->back();
    if !ascii_is_space(@back){return;}

    let ch : mut ^ascii = back;
    loop ascii_is_space(@ch){ch = ch -^ 1u;}
    let num_asciis : u = back -^^ ch;

    let new_length : u = this->length() - num_asciis;
    this->resize(new_length);
}
