# Copyright 2018 N-Lang Project Authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
#  limitations under the License.
import "cstd/math.n"a;
import "std/integer.n"a;
import "std/panic.n"a;

export introduce func s8_abs : (val : s8) -> u8;
export introduce func s16_abs : (val : s16) -> u16;
export introduce func s32_abs : (val : s32) -> u32;
export introduce func s64_abs : (val : s64) -> u64;
export introduce func s_abs : (val : s) -> u;
export introduce func f32_abs : (val : f32) -> f32;
export introduce func f64_abs : (val : f64) -> f64;

export introduce func f32_ceil : (val : f32) -> f32;
export introduce func f64_ceil : (val : f64) -> f64;

export introduce func f32_floor : (val : f32) -> f32;
export introduce func f64_floor : (val : f64) -> f64;

export introduce func f32_round : (val : f32) -> f32;
export introduce func f64_round : (val : f64) -> f64;

export introduce func f32_root2 : (val : f32) -> f32;
export introduce func f64_root2 : (val : f64) -> f64;

export introduce func f32_root3 : (val : f32) -> f32;
export introduce func f64_root3 : (val : f64) -> f64;

export introduce func f32_sin : (val : f32) -> f32;
export introduce func f64_sin : (val : f64) -> f64;

export introduce func f32_cos : (val : f32) -> f32;
export introduce func f64_cos : (val : f64) -> f64;

export introduce func f32_tan : (val : f32) -> f32;
export introduce func f64_tan : (val : f64) -> f64;

export introduce func f32_sinh : (val : f32) -> f32;
export introduce func f64_sinh : (val : f64) -> f64;

export introduce func f32_cosh : (val : f32) -> f32;
export introduce func f64_cosh : (val : f64) -> f64;

export introduce func f32_tanh : (val : f32) -> f32;
export introduce func f64_tanh : (val : f64) -> f64;

export introduce func f32_arcsin : (val : f32) -> f32;
export introduce func f64_arcsin : (val : f64) -> f64;

export introduce func f32_arccos : (val : f32) -> f32;
export introduce func f64_arccos : (val : f64) -> f64;

export introduce func f32_arctan : (val : f32) -> f32;
export introduce func f64_arctan : (val : f64) -> f64;

export introduce func f32_arcsinh : (val : f32) -> f32;
export introduce func f64_arcsinh : (val : f64) -> f64;

export introduce func f32_arccosh : (val : f32) -> f32;
export introduce func f64_arccosh : (val : f64) -> f64;

export introduce func f32_arctanh : (val : f32) -> f32;
export introduce func f64_arctanh : (val : f64) -> f64;

export introduce func f32_ln : (val : f32) -> f32;
export introduce func f64_ln : (val : f64) -> f64;

export introduce func f32_log : (val : f32, base : f32) -> f32;
export introduce func f64_log : (val : f64, base : f64) -> f64;

export introduce func f32_log2 : (val : f32) -> f32;
export introduce func f64_log2 : (val : f64) -> f64;

export introduce func f32_log10 : (val : f32) -> f32;
export introduce func f64_log10 : (val : f64) -> f64;

# Modulo : Takes the sign of the divisor (i.e. m).
export introduce func u8_mod : (x : u8, m : u8) -> u8;
export introduce func u16_mod : (x : u16, m : u16) -> u16;
export introduce func u32_mod : (x : u32, m : u32) -> u32;
export introduce func u64_mod : (x : u64, m : u64) -> u64;
export introduce func u_mod : (x : u, m : u) -> u;
export introduce func s8_mod : (x : s8, m : s8) -> s8;
export introduce func s16_mod : (x : s16, m : s16) -> s16;
export introduce func s32_mod : (x : s32, m : s32) -> s32;
export introduce func s64_mod : (x : s64, m : s64) -> s64;
export introduce func s_mod : (x : s, m : s) -> s;

# Integer and floating point power.
export introduce func u8_pow : (base : u8, p : u8) -> u8;
export introduce func u16_pow : (base : u16, p : u16) -> u16;
export introduce func u32_pow : (base : u32, p : u32) -> u32;
export introduce func u64_pow : (base : u64, p : u64) -> u64;
export introduce func u_pow : (base : u, p : u) -> u;
export introduce func s8_pow : (base : s8, p : s8) -> s8;
export introduce func s16_pow : (base : s16, p : s16) -> s16;
export introduce func s32_pow : (base : s32, p : s32) -> s32;
export introduce func s64_pow : (base : s64, p : s64) -> s64;
export introduce func s_pow : (base : s, p : s) -> s;
export introduce func f32_pow : (base : f32, p : f32) -> f32;
export introduce func f64_pow : (base : f64, p : f64) -> f64;

export introduce func f32_exp : (val : f32) -> f32;
export introduce func f64_exp : (val : f64) -> f64;

export introduce func u8_gcd : (x : u8, y : u8) -> u8;
export introduce func u16_gcd : (x : u16, y : u16) -> u16;
export introduce func u32_gcd : (x : u32, y : u32) -> u32;
export introduce func u64_gcd : (x : u64, y : u64) -> u64;
export introduce func u_gcd : (x : u, y : u) -> u;
export introduce func s8_gcd : (x : s8, y : s8) -> u8;
export introduce func s16_gcd : (x : s16, y : s16) -> u16;
export introduce func s32_gcd : (x : s32, y : s32) -> u32;
export introduce func s64_gcd : (x : s64, y : s64) -> u64;
export introduce func s_gcd : (x : s, y : s) -> u;

export introduce func u8_lcm : (x : u8, y : u8) -> u8;
export introduce func u16_lcm : (x : u16, y : u16) -> u16;
export introduce func u32_lcm : (x : u32, y : u32) -> u32;
export introduce func u64_lcm : (x : u64, y : u64) -> u64;
export introduce func u_lcm : (x : u, y : u) -> u;
export introduce func s8_lcm : (x : s8, y : s8) -> u8;
export introduce func s16_lcm : (x : s16, y : s16) -> u16;
export introduce func s32_lcm : (x : s32, y : s32) -> u32;
export introduce func s64_lcm : (x : s64, y : s64) -> u64;
export introduce func s_lcm : (x : s, y : s) -> u;

export introduce func u8_bicoef : (x : u8, y : u8) -> u8;
export introduce func u16_bicoef : (x : u16, y : u16) -> u16;
export introduce func u32_bicoef : (x : u32, y : u32) -> u32;
export introduce func u64_bicoef : (x : u64, y : u64) -> u64;
export introduce func u_bicoef : (x : u, y : u) -> u;

export introduce func u8_fact : (val : u8) -> u8;
export introduce func u16_fact : (val : u16) -> u16;
export introduce func u32_fact : (val : u32) -> u32;
export introduce func u64_fact : (val : u64) -> u64;
export introduce func u_fact : (val : u) -> u;

# Remainder : Takes the sign of the dividend (i.e. x).
export introduce func u8_rem : (x : u8, m : u8) -> u8;
export introduce func u16_rem : (x : u16, m : u16) -> u16;
export introduce func u32_rem : (x : u32, m : u32) -> u32;
export introduce func u64_rem : (x : u64, m : u64) -> u64;
export introduce func u_rem : (x : u, m : u) -> u;
export introduce func s8_rem : (x : s8, m : s8) -> s8;
export introduce func s16_rem : (x : s16, m : s16) -> s16;
export introduce func s32_rem : (x : s32, m : s32) -> s32;
export introduce func s64_rem : (x : s64, m : s64) -> s64;
export introduce func s_rem : (x : s, m : s) -> s;

# Global variable PI and EULER_NUMBER
let PI : f64 = 3.1415926f64;
let EULER_NUMBER : f64 = 2.7182818f64;

################################################################################
###                                 INTERNAL                                 ###
################################################################################

export func s8_abs : (val : s8) -> u8
{
    if val < 0s8{return -val as u8;}
    return val as u8;
}

export func s16_abs : (val : s16) -> u16
{
    if val < 0s16{return -val as u16;}
    return val as u16;
}

export func s32_abs : (val : s32) -> u32
{
    if val < 0s32{return -val as u32;}
    return val as u32;
}

export func s64_abs : (val : s64) -> u64
{
    if val < 0s64{return -val as u64;}
    return val as u64;
}

export func s_abs : (val : s) -> u
{
    if val < 0s{return -val as u;}
    return val as u;
}

export func f32_abs : (val : f32) -> f32
{
    if val < 0.0f32{return -val;}
    return val;
}

export func f64_abs : (val : f64) -> f64
{
    if val < 0.0f64{return -val;}
    return val;
}

export func f32_ceil : (val : f32) -> f32
{
    let x : mut s32 = val as s32;
    if x as f32 != val && x > 0s32{return x as f32 + 1.0f32;}
    return x as f32;
}

export func f64_ceil : (val : f64) -> f64
{
    let x : mut s64 = val as s64;
    if x as f64 != val && x > 0s64{return x as f64 + 1.0f64;}
    return x as f64;
}

export func f32_floor : (val : f32) -> f32
{
    let x : mut s32 = val as s32;
    if x as f32 != val && x < 0s32{return x as f32 - 1.0f32;}
    return x as f32;
}

export func f64_floor : (val : f64) -> f64
{
    let x : mut s64 = val as s64;
    if x as f64 != val && x < 0s64{return x as f64 - 1.0f64;}
    return x as f64;
}

export func f32_round : (val : f32) -> f32
{
    if f32_abs(val - (val as s32) as f32) > 0.5f32
    {
        if val > 0.0f32 {return f32_ceil(val);}
        return f32_floor(val);
    }
    if val > 0.0f32 {return f32_floor(val);}
    return f32_ceil(val);
}

export func f64_round : (val : f64) -> f64
{
    if f64_abs(val - (val as s64) as f64) > 0.5f64{
        if val > 0.0f64 {return f64_ceil(val);}
        return f64_floor(val);
    }
    if val > 0.0f64 {return f64_floor(val);}
    return f64_ceil(val);
}

export func f32_root2 : (val : f32) -> f32
{
    return f32_pow(val, 0.5f32);
}

export func f64_root2 : (val : f64) -> f64
{
    return f64_pow(val, 0.5f64);
}

export func f32_root3 : (val : f32) -> f32
{
    return f32_pow(val, 1.0f32/3.0f32);
}

export func f64_root3 : (val : f64) -> f64
{
    return f64_pow(val, 1.0f64/3.0f64);
}

export func f32_sin : (val : f32) -> f32
{
    return sinf(val) as f32;
}

export func f64_sin : (val : f64) -> f64
{
    return sin(val) as f64;
}

export func f32_cos : (val : f32) -> f32
{
    return cosf(val) as f32;
}

export func f64_cos : (val : f64) -> f64
{
    return cos(val) as f64;
}

export func f32_tan : (val : f32) -> f32
{
    return f32_sin(val)/f32_cos(val);
}

export func f64_tan : (val : f64) -> f64
{
    return f64_sin(val)/f64_cos(val);
}

export func f32_sinh : (val : f32) -> f32
{
    return 0.5f32 * (f32_exp(val) - f32_exp(-val));
}

export func f64_sinh : (val : f64) -> f64
{
    return 0.5f64 * (f64_exp(val) - f64_exp(-val));
}

export func f32_cosh : (val : f32) -> f32
{
    return 0.5f32 * (f32_exp(val) + f32_exp(-val));
}

export func f64_cosh : (val : f64) -> f64
{
    return 0.5f64 * (f64_exp(val) + f64_exp(-val));
}

export func f32_tanh : (val : f32) -> f32
{
    return f32_sinh(val)/f32_cosh(val);
}

export func f64_tanh : (val : f64) -> f64
{
    return f64_sinh(val)/f64_cosh(val);
}

let TRIGO_ERROR : ^ascii =
    $"The absolute value of input cannot exceed 1"a;

export func f32_arcsin : (val : f32) -> f32
{
    return asinf(val) as f32;
}

export func f64_arcsin : (val : f64) -> f64
{
    return asin(val) as f64;
}

export func f32_arccos : (val : f32) -> f32
{
    if val < -1.0f32 || val > 1.0f32{panic(TRIGO_ERROR);}
    return (PI as f32)/2.0f32 - f32_arcsin(val);
}

export func f64_arccos : (val : f64) -> f64
{
    if val < -1.0f64 || val > 1.0f64{panic(TRIGO_ERROR);}
    return (PI as f64)/2.0f64 - f64_arcsin(val);
}

export func f32_arctan : (val : f32) -> f32
{
    return atanf(val) as f32;
}

export func f64_arctan : (val : f64) -> f64
{
    return atan(val) as f64;
}

export func f32_arcsinh : (val : f32) -> f32
{
    return f32_ln(val + f32_root2(val*val + 1.0f32));
}

export func f64_arcsinh : (val : f64) -> f64
{
    return f64_ln(val + f64_root2(val*val + 1.0f64));
}

export func f32_arccosh : (val : f32) -> f32
{
    if val > -1.0f32 && val < 1.0f32{panic(TRIGO_ERROR);}
    return f32_ln(val + f32_root2(val*val - 1.0f32));
}

export func f64_arccosh : (val : f64) -> f64
{
    if val > -1.0f64 && val < 1.0f64{panic(TRIGO_ERROR);}
    return f64_ln(val + f64_root2(val*val - 1.0f64));
}

export func f32_arctanh : (val : f32) -> f32
{
    return 0.5f32 * f32_ln((1.0f32 + val)/(1.0f32 - val));
}

export func f64_arctanh : (val : f64) -> f64
{
    return 0.5f64 * f64_ln((1.0f64 + val)/(1.0f64 - val));
}

let LOG_ERROR : ^ascii =
    $"Attempted to take the logarithm of a non-positive number."a;

export func f32_ln : (val : f32) -> f32
{
    return logf(val) as f32;
}

export func f64_ln : (val : f64) -> f64
{
    return log(val) as f64;
}

export func f32_log : (val : f32, base : f32) -> f32
{
    return f32_ln(val)/f32_ln(base);
}

export func f64_log : (val : f64, base : f64) -> f64
{
    return f64_ln(val)/f64_ln(base);
}

export func f32_log2 : (val : f32) -> f32
{
    return f32_ln(val)/f32_ln(2.0f32);
}

export func f64_log2 : (val : f64) -> f64
{
    return f64_ln(val)/f64_ln(2.0f64);
}

export func f32_log10 : (val : f32) -> f32
{
    return f32_ln(val)/f32_ln(10.0f32);
}

export func f64_log10 : (val : f64) -> f64
{
    return f64_ln(val)/f64_ln(10.0f64);
}

export func u8_rem : (x : u8, m : u8) -> u8
{
    return x - (m * (x/m));
}

export func u16_rem : (x : u16, m : u16) -> u16
{
    return x - (m * (x/m));
}

export func u32_rem : (x : u32, m : u32) -> u32
{
    return x - (m * (x/m));
}

export func u64_rem : (x : u64, m : u64) -> u64
{
    return x - (m * (x/m));
}

export func u_rem : (x : u, m : u) -> u
{
    return x - (m * (x/m));
}

export func s8_rem : (x : s8, m : s8) -> s8
{
    return x - (m * (x/m));
}

export func s16_rem : (x : s16, m : s16) -> s16
{
    return x - (m * (x/m));
}

export func s32_rem : (x : s32, m : s32) -> s32
{
    return x - (m * (x/m));
}

export func s64_rem : (x : s64, m : s64) -> s64
{
    return x - (m * (x/m));
}

export func s_rem : (x : s, m : s) -> s
{
    return x - (m * (x/m));
}

export func u8_mod : (x : u8, m : u8) -> u8
{
    return x - m * (x/m) ;
}

export func u16_mod : (x : u16, m : u16) -> u16
{
    return x - m * (x/m) ;
}

export func u32_mod : (x : u32, m : u32) -> u32
{
    return x - m * (x/m) ;
}

export func u64_mod : (x : u64, m : u64) -> u64
{
    return x - m * (x/m) ;
}

export func u_mod : (x : u, m : u) -> u
{
    return x - m * (x/m) ;
}

export func s8_mod : (x : s8, m : s8) -> s8
{
    if x < 0s8
    {
        if x * m < 0s8{return x - m * ((x/m) - 1s8);}
        elif x * m > 0s8{return x - m * ((x/m) + 1s8);}
    } 
    return x - m * (x/m) ;
}

export func s16_mod : (x : s16, m : s16) -> s16
{
    if x < 0s16
    {
        if x * m < 0s16{return x - m * ((x/m) - 1s16);}
        elif x * m > 0s16{return x - m * ((x/m) + 1s16);}
    } 
    return x - m * (x/m) ;
}

export func s32_mod : (x : s32, m : s32) -> s32
{
    if x < 0s32
    {
        if x * m < 0s32{return x - m * ((x/m) - 1s32);}
        elif x * m > 0s32{return x - m * ((x/m) + 1s32);}
    } 
    return x - m * (x/m) ;
}

export func s64_mod : (x : s64, m : s64) -> s64
{
    if x < 0s64
    {
        if x * m < 0s64{return x - m * ((x/m) - 1s64);}
        elif x * m > 0s64{return x - m * ((x/m) + 1s64);}
    } 
    return x - m * (x/m) ;
}

export func s_mod : (x : s, m : s) -> s
{
    if x < 0s
    {
        if x * m < 0s{return x - m * ((x/m) - 1s);}
        elif x * m > 0s{return x - m * ((x/m) + 1s);}
    } 
    return x - m * (x/m) ;
}

export func u8_pow : (base : u8, p : u8) -> u8
{
    if p == 0u8{return 1u8;}
    let tmp : mut u8 = u8_pow(base, p/2u8);
    if u8_mod(p, 2u8) == 1u8 {return base * tmp * tmp;}
    return tmp * tmp;
}

export func u16_pow : (base : u16, p : u16) -> u16
{
    if p == 0u16{return 1u16;}
    let tmp : mut u16 = u16_pow(base, p/2u16);
    if u16_mod(p, 2u16) == 1u16 {return base * tmp * tmp;}
    return tmp * tmp;
}

export func u32_pow : (base : u32, p : u32) -> u32
{
    if p == 0u32{return 1u32;}
    let tmp : mut u32 = u32_pow(base, p/2u32);
    if u32_mod(p, 2u32) == 1u32 {return base * tmp * tmp;}
    return tmp * tmp;
}

export func u64_pow : (base : u64, p : u64) -> u64
{
    if p == 0u64{return 1u64;}
    let tmp : mut u64 = u64_pow(base, p/2u64);
    if u64_mod(p, 2u64) == 1u64 {return base * tmp * tmp;}
    return tmp * tmp;
}

export func u_pow : (base : u, p : u) -> u
{
    if p == 0u{return 1u;}
    let tmp : mut u = u_pow(base, p/2u);
    if u_mod(p, 2u) == 1u {return base * tmp * tmp;}
    return tmp * tmp;
}

export func s8_pow : (base : s8, p : s8) -> s8
{
    if p < 0s8{return 1s8/s8_pow(base, -p);}
    elif p == 0s8{return 1s8;}
    let tmp : mut s8 = s8_pow(base, p/2s8);
    if s8_mod(p, 2s8) == 1s8 {return base * tmp * tmp;}
    return tmp * tmp;
}

export func s16_pow : (base : s16, p : s16) -> s16
{
    if p < 0s16 { return 1s16/s16_pow(base, -p); }
    elif p == 0s16{ return 1s16; }
    let tmp : mut s16 = s16_pow(base, p/2s16);
    if s16_mod(p, 2s16) == 1s16 { return base * tmp * tmp; }
    return tmp * tmp;
}

export func s32_pow : (base : s32, p : s32) -> s32
{
    if p < 0s32{return 1s32/s32_pow(base, -p);}
    elif p == 0s32{return 1s32;}
    let tmp : mut s32 = s32_pow(base, p/2s32);
    if s32_mod(p, 2s32) == 1s32 {return base * tmp * tmp;}
    return tmp * tmp;
}

export func s64_pow : (base : s64, p : s64) -> s64
{
    if p < 0s64{return 1s64/s64_pow(base, -p);}
    elif p == 0s64{return 1s64;}
    let tmp : mut s64 = s64_pow(base, p/2s64);
    if s64_mod(p, 2s64) == 1s64 {return base * tmp * tmp;}
    return tmp * tmp;
}

export func s_pow : (base : s, p : s) -> s
{
    if p < 0s{return 1s/s_pow(base, -p);}
    elif p == 0s{return 1s;}
    let tmp : mut s = s_pow(base, p/2s);
    if s_mod(p, 2s) == 1s {return base * tmp * tmp;}
    return tmp * tmp;
}

export func f32_pow : (base : f32, p : f32) -> f32
{
    if p == 0.0f32{return 1.0f32;}
    return f32_exp(base * f32_ln(p));
}

export func f64_pow : (base : f64, p : f64) -> f64
{
    if p == 0.0f64{return 1.0f64;}
    return f64_exp(base * f64_ln(p));
}

export func f32_exp : (val : f32) -> f32
{
    return expf(val) as f32;
}

export func f64_exp : (val : f64) -> f64
{
    return exp(val) as f64;
}

export func u8_gcd : (x : u8, y : u8) -> u8
{
    let bigger: mut u8;
    let smaller: mut u8;
    if x > y
    {
        bigger = x;
        smaller = y;
    }
    else
    {
        bigger = y;
        smaller = x;
    }
    let rem: mut u8 = u8_rem(bigger, smaller);
    loop rem != 0u8
    {
        bigger = smaller;
        smaller = rem;
        rem = u8_rem(bigger, smaller);
    }
    return smaller;
}

export func u16_gcd : (x : u16, y : u16) -> u16
{
    let bigger: mut u16;
    let smaller: mut u16;
    if x > y
    {
        bigger = x;
        smaller = y;
    } 
    else
    {
        bigger = y;
        smaller = x;
    }
    let rem: mut u16 = u16_rem(bigger, smaller);
    loop rem != 0u16
    {
        bigger = smaller;
        smaller = rem;
        rem = u16_rem(bigger, smaller);
    }
    return smaller;
}

export func u32_gcd : (x : u32, y : u32) -> u32
{
    let bigger: mut u32;
    let smaller: mut u32;
    if x > y
    {
        bigger = x;
        smaller = y;
    }
    else
    {
        bigger = y;
        smaller = x;
    }
    let rem: mut u32 = u32_rem(bigger, smaller);
    loop rem != 0u32
    {
        bigger = smaller;
        smaller = rem;
        rem = u32_rem(bigger, smaller);
    }
    return smaller;
}

export func u64_gcd : (x : u64, y : u64) -> u64
{
    let bigger: mut u64;
    let smaller: mut u64;
    if x > y
    {
        bigger = x;
        smaller = y;
    }
    else
    {
        bigger = y;
        smaller = x;
    }
    let rem: mut u64 = u64_rem(bigger, smaller);
    loop rem != 0u64
    {
        bigger = smaller;
        smaller = rem;
        rem = u64_rem(bigger, smaller);
    }
    return smaller;
}

export func u_gcd : (x : u, y : u) -> u
{
    let bigger: mut u;
    let smaller: mut u;
    if x > y
    {
        bigger = x;
        smaller = y;
    }
    else
    {
        bigger = y;
        smaller = x;
    }
    let rem: mut u = u_rem(bigger, smaller);
    loop rem != 0u
    {
        bigger = smaller;
        smaller = rem;
        rem = u_rem(bigger, smaller);
    }
    return smaller;
}

export func s8_gcd : (x : s8, y : s8) -> u8
{
    return u8_gcd(s8_abs(x), s8_abs(y));
}

export func s16_gcd : (x : s16, y : s16) -> u16
{
    return u16_gcd(s16_abs(x), s16_abs(y));
}

export func s32_gcd : (x : s32, y : s32) -> u32
{
    return u32_gcd(s32_abs(x), s32_abs(y));
}

export func s64_gcd : (x : s64, y : s64) -> u64
{
    return u64_gcd(s64_abs(x), s64_abs(y));
}

export func s_gcd : (x : s, y : s) -> u
{
    return u_gcd(s_abs(x), s_abs(y));
}

export func u8_lcm : (x : u8, y : u8) -> u8
{
    return x * y / u8_gcd(x, y);
}

export func u16_lcm : (x : u16, y : u16) -> u16
{
    return x * y / u16_gcd(x, y);
}

export func u32_lcm : (x : u32, y : u32) -> u32
{
    return x * y / u32_gcd(x, y);
}

export func u64_lcm : (x : u64, y : u64) -> u64
{
    return x * y / u64_gcd(x, y);
}

export func u_lcm : (x : u, y : u) -> u
{
    return x * y / u_gcd(x, y);
}

export func s8_lcm : (x : s8, y : s8) -> u8
{
    return u8_lcm(s8_abs(x), s8_abs(y));
}

export func s16_lcm : (x : s16, y : s16) -> u16
{
    return u16_lcm(s16_abs(x), s16_abs(y));
}

export func s32_lcm : (x : s32, y : s32) -> u32
{
    return u32_lcm(s32_abs(x), s32_abs(y));
}

export func s64_lcm : (x : s64, y : s64) -> u64
{
    return u64_lcm(s64_abs(x), s64_abs(y));
}

export func s_lcm : (x : s, y : s) -> u
{
    return u_lcm(s_abs(x), s_abs(y));
}

let BIOCOEF_ERROR : ^ascii =
    $"The second input cannot be bigger than the first input"a;


export func u8_bicoef : (x : u8, y : u8) -> u8
{
    if y > x {panic(BIOCOEF_ERROR);}
    return u8_fact(x)/(u8_fact(y) * u8_fact(x-y));
}

export func u16_bicoef : (x : u16, y : u16) -> u16
{
    if y > x {panic(BIOCOEF_ERROR);}
    return u16_fact(x)/(u16_fact(y) * u16_fact(x-y));
}

export func u32_bicoef : (x : u32, y : u32) -> u32
{
    if y > x {panic(BIOCOEF_ERROR);}
    return u32_fact(x)/(u32_fact(y) * u32_fact(x-y));
}

export func u64_bicoef : (x : u64, y : u64) -> u64
{
    if y > x {panic(BIOCOEF_ERROR);}
    return u64_fact(x)/(u64_fact(y) * u64_fact(x-y));
}

export func u_bicoef : (x : u, y : u) -> u
{
    if y > x {panic(BIOCOEF_ERROR);}
    return u_fact(x)/(u_fact(y) * u_fact(x-y));
}

export func u8_fact : (val : u8) -> u8
{
    if val == 0u8{return 1u8;}
    let result : mut u8 = 1u8;
    loop n : mut u8 in [1u8, val]
    {
        result = result * n;
    }
    return result;
}

export func u16_fact : (val : u16) -> u16
{
    if val == 0u16{return 1u16;}
    let result : mut u16 = 1u16;
    loop n : mut u16 in [1u16, val]
    {
        result = result * n;
    }
    return result;
}

export func u32_fact : (val : u32) -> u32
{
    if val == 0u32{return 1u32;}
    let result : mut u32 = 1u32;
    loop n : mut u32 in [1u32, val]
    {
        result = result * n;
    }
    return result;
}

export func u64_fact : (val : u64) -> u64
{
    if val == 0u64{return 1u64;}
    let result : mut u64 = 1u64;
    loop n : mut u64 in [1u64, val]
    {
        result = result * n;
    }
    return result;
}

export func u_fact : (val : u) -> u
{
    if val == 0u{return 1u;}
    let result : mut u = 1u;
    loop n : mut u in [1u, val]
    {
        result = result * n;
    }
    return result;
}
