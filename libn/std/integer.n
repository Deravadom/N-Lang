# Copyright 2018 N-Lang Project Authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
export let U8_BITS : u = 8u;
export let U8_MIN_VAL : u8 = 0x00u8;
export let U8_MAX_VAL : u8 = 0xFFu8;

export let U16_MIN_VAL : u16 = 0x0000u16;
export let U16_MAX_VAL : u16 = 0xFFFFu16;

export let U32_MIN_VAL : u32 = 0x00000000u32;
export let U32_MAX_VAL : u32 = 0xFFFFFFFFu32;

export let U64_MIN_VAL : u64 = 0x0000000000000000u64;
export let U64_MAX_VAL : u64 = 0xFFFFFFFFFFFFFFFFu64;

export let U_MIN_VAL : u = 0x0000000000000000u64 as u;
export let U_MAX_VAL : u = 0xFFFFFFFFFFFFFFFFu64 as u;

export let S8_BITS : u = 8u;
export let S8_MIN_VAL : s8 = -0x80s8;
export let S8_MAX_VAL : s8 =  0x7Fs8;

export let S16_MIN_VAL : s16 = -0x8000s16;
export let S16_MAX_VAL : s16 =  0x7FFFs16;

export let S32_MIN_VAL : s32 = -0x80000000s32;
export let S32_MAX_VAL : s32 =  0x7FFFFFFFs32;

export let S64_MIN_VAL : s64 = -0x8000000000000000s64;
export let S64_MAX_VAL : s64 =  0x7FFFFFFFFFFFFFFFs64;

export let S_MIN_VAL : s = -0x8000000000000000s64 as s;
export let S_MAX_VAL : s =  0x7FFFFFFFFFFFFFFFs64 as s;
