# Copyright 2018 N-Lang Project Authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import "cstd/primitives.n"a;

export introduce struct FILE;

export introduce let stdin  : mut ^mut FILE;
export introduce let stdout : mut ^mut FILE;
export introduce let stderr : mut ^mut FILE;

export let EOF : sint = -1s as sint;

export introduce func fopen : (path : mut ^ascii, mode : mut ^ascii) -> ^mut FILE;
export introduce func freopen : (path : mut ^ascii, mode : mut ^ascii, stream : ^mut FILE) -> ^mut FILE;
export introduce func fclose : (stream : mut ^mut FILE) -> sint;

export introduce func fflush : (stream : mut ^mut FILE) -> sint;

export introduce func fseek : (stream : mut ^mut FILE, offset : slong, whence : sint) -> sint;
export introduce func ftell : (stream : mut ^mut FILE) -> slong;
export introduce func rewind : (stream : mut ^mut FILE) -> void;

export introduce func fread  : (buf : mut ^mut void, size : mut size_t, count : mut size_t, stream : mut ^mut FILE) -> size_t;
export introduce func fwrite : (buf : mut ^void, size : mut size_t, count : mut size_t, stream : mut ^mut FILE) -> size_t;

export introduce func getchar : () -> sint;
export introduce func putchar : (c : mut sint) -> sint;

export introduce func fgetc : (stream : mut ^mut FILE) -> sint;
export introduce func fputc : (c : mut sint, stream : ^mut FILE) -> sint;

export introduce func gets : (str : mut ^mut ascii) -> ^ascii;
export introduce func puts : (str : mut ^ascii) -> sint;

export introduce func fgets : (str : mut ^mut ascii, stream : mut ^mut FILE) -> ^mut ascii;
export introduce func fputs : (str : mut ^ascii, stream : mut ^mut FILE) -> sint;

export introduce func ungetc : (c : mut sint, stream : mut ^mut FILE) -> sint;

export introduce func remove : (path : mut ^ascii) -> sint;
export introduce func rename : (old_path : mut ^mut ascii, new_path : mut ^mut ascii) -> sint;
