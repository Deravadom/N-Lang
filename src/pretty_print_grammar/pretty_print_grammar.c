#include "../compiler/grammar.h"
#include "../compiler/token.h"

char const* nonterminal_to_cstr(enum parser_nonterminal);

int main(void)
{
    for(int i = 0; i < NUMBER_OF_GRAMMAR_RULES; ++i)
    {
        struct parser_rule* curr = &parser_grammar[i];

        printf("%s ::=", nonterminal_to_cstr(curr->nonterminal));

        if(curr->symbol_list[0].tag == END_TERMINAL)
        {
            printf(" <empty>\n");
        }
        else
        {
            int j = 0;
            while(curr->symbol_list[j].tag != END_TERMINAL)
            {
                if(curr->symbol_list[j].tag == NON_TERMINAL)
                {
                    printf(" <%s>",
                        nonterminal_to_cstr(curr->symbol_list[j].nonterminal));
                }
                else
                {
                    printf(" \"%s\"",
                        token_type_to_cstr(curr->symbol_list[j].terminal));
                }
                ++j;
            }
            printf("\n");
        }
    }
}

char const* nonterminal_to_cstr(enum parser_nonterminal nonterminal)
{
    switch(nonterminal)
    {
    case NONTERMINAL_TOP:
        return "top";
    case NONTERMINAL_PROGRAM:
        return "program";
    case NONTERMINAL_LISTGLOBALSTATEMENT:
        return "list-global-statement";
    case NONTERMINAL_GLOBALSTATEMENT:
        return "global-statement";
    case NONTERMINAL_LISTBLOCKSTATEMENT:
        return "list-block-statement";
    case NONTERMINAL_LISTBLOCKSTATEMENT_RECURSE:
        return "list-block-statement-recurse";
    case NONTERMINAL_BLOCKSTATEMENT:
        return "block-statement";
    case NONTERMINAL_DATATYPE:
        return "datatype";
    case NONTERMINAL_PRIMITIVEDATATYPE:
        return "primitive-datatype";
    case NONTERMINAL_LISTDATATYPE:
        return "list-datatype";
    case NONTERMINAL_NONEMPTY_LISTDATATYPE:
        return "nonempty-list-datatype";
    case NONTERMINAL_QUALIFIER_MUT:
        return "qualifier-mut";
    case NONTERMINAL_QUALIFIER_UNIQUE:
        return "qualifier-unique";
    case NONTERMINAL_QUALIFIER_VOLATILE:
        return "qualifier-volatile";
    case NONTERMINAL_EXPORT:
        return "export";
    case NONTERMINAL_ARGWITHTYPE:
        return "arg-with-type";
    case NONTERMINAL_LISTARGWITHTYPE:
        return "list-arg-with-type";
    case NONTERMINAL_NONEMPTY_LISTARGWITHTYPE:
        return "nonempty-list-arg-with-type";
    case NONTERMINAL_IFBRANCH:
        return "if-branch";
    case NONTERMINAL_LISTELIFBRANCH:
        return "list-elif-branch";
    case NONTERMINAL_ELSEBRANCH:
        return "else-branch";
    case NONTERMINAL_RANGEBOUNDSTART:
        return "range-bound-start";
    case NONTERMINAL_RANGEBOUNDEND:
        return "range-bound-end";
    case NONTERMINAL_FUNCTIONEXPRESSION:
        return "function-expression";
    case NONTERMINAL_FUNCTIONARGUMENTS:
        return "function-arguments";
    case NONTERMINAL_LIST_ARGUMENT:
        return "list-argument";
    case NONTERMINAL_LIST_LITERAL:
        return "list-literal";
    case NONTERMINAL_LISTSTRUCTMEMBER:
        return "list-struct-member";
    case NONTERMINAL_STRUCTMEMBER:
        return "struct-member";
    case NONTERMINAL_VARIANTMEMBER_LIST:
        return "list-variant-member";
    case NONTERMINAL_VARIANTMEMBER:
        return "variant-member";
    case NONTERMINAL_VARIANTMEMBER_TAGLIST:
        return "list-variant-tag";
    case NONTERMINAL_VARIANTMEMBER_TAG:
        return "variant-tag";
    case NONTERMINAL_VARIANTMEMBER_SECTIONMEMBER_LIST:
        return "list-variant-tag-section-member";
    case NONTERMINAL_VARIANTMEMBER_SECTIONMEMBER:
        return "variant-tag-section-member";
    case NONTERMINAL_FUNCTION_SIGNATURE:
        return "function-signature";
    case NONTERMINAL_EXPRESSION:
        return "expression";
    case NONTERMINAL_EXPRESSION1:
        return "expression-1";
    case NONTERMINAL_EXPRESSION2:
        return "expression-2";
    case NONTERMINAL_EXPRESSION3:
        return "expression-3";
    case NONTERMINAL_EXPRESSION4:
        return "expression-4";
    case NONTERMINAL_EXPRESSION5:
        return "expression-5";
    case NONTERMINAL_EXPRESSION6:
        return "expression-6";
    case NONTERMINAL_EXPRESSION7:
        return "expression-7";
    case NONTERMINAL_EXPRESSION8:
        return "expression-8";
    case NONTERMINAL_EXPRESSION9:
        return "expression-9";
    case NONTERMINAL_EXPRESSION10:
        return "expression-10";
    case NONTERMINAL_EXPRESSION11:
        return "expression-11";
    case NONTERMINAL_EXPRESSION_UNARY:
        return "expression-unary";
    case NONTERMINAL_EXPRESSION_POSTFIX:
        return "expression-postfix";
    case NONTERMINAL_EXPRESSION_LIST_POSTFIX:
        return "list-expression-postfix";
    case NONTERMINAL_EXPRESSION_LIST_POSTFIX_NONEMPTY:
        return "nonempty-list-expression-postfix";
    case NONTERMINAL_EXPRESSION_POSTFIX_NONEMPTY:
        return "nonempty-expression-postfix";
    case NONTERMINAL_EXPRESSION_LITERAL:
        return "expression-literal";
    case NONTERMINAL_EXPRORDT:
        return "expression-or-datatype";
    case NONTERMINAL_EXPRORDT_NT:
        return "expression-or-datatype-nt";
    case NUMBER_OF_NONTERMINALS:
        return "number-of-nonterminals";
    }
}

