/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once
#include "nutils.h"

struct module;

enum token_type
//////////////////////
// KEYWORDS
//////////////////////

{
  TOKEN_KEYWORD_ALIAS,
  TOKEN_KEYWORD_AS,
  TOKEN_KEYWORD_BOOL,
  TOKEN_KEYWORD_BREAK,
  TOKEN_KEYWORD_ASCII,
  TOKEN_KEYWORD_CONTINUE,
  TOKEN_KEYWORD_COUNTOF,
  TOKEN_KEYWORD_DEFER,
  TOKEN_KEYWORD_ELIF,
  TOKEN_KEYWORD_ELSE,
  TOKEN_KEYWORD_EXPORT,
  TOKEN_KEYWORD_F128,
  TOKEN_KEYWORD_F16,
  TOKEN_KEYWORD_F32,
  TOKEN_KEYWORD_F64,
  TOKEN_KEYWORD_FALSE,
  TOKEN_KEYWORD_FUNC,
  TOKEN_KEYWORD_IF,
  TOKEN_KEYWORD_IFACE,
  TOKEN_KEYWORD_IMPORT,
  TOKEN_KEYWORD_IN,
  TOKEN_KEYWORD_INTRODUCE,
  TOKEN_KEYWORD_ISOLATE,
  TOKEN_KEYWORD_LET,
  TOKEN_KEYWORD_LOOP,
  TOKEN_KEYWORD_MUT,
  TOKEN_KEYWORD_NULL,
  TOKEN_KEYWORD_RETURN,
  TOKEN_KEYWORD_S,
  TOKEN_KEYWORD_S16,
  TOKEN_KEYWORD_S32,
  TOKEN_KEYWORD_S64,
  TOKEN_KEYWORD_S8,
  TOKEN_KEYWORD_SIZEOF,
  TOKEN_KEYWORD_ASTRING,
  TOKEN_KEYWORD_STRUCT,
  TOKEN_KEYWORD_TRUE,
  TOKEN_KEYWORD_TYPEOF,
  TOKEN_KEYWORD_U,
  TOKEN_KEYWORD_U16,
  TOKEN_KEYWORD_U32,
  TOKEN_KEYWORD_U64,
  TOKEN_KEYWORD_U8,
  TOKEN_KEYWORD_UNIQUE,
  TOKEN_KEYWORD_VARIANT,
  TOKEN_KEYWORD_VOID,
  TOKEN_KEYWORD_VOLATILE,

  //////////////////////////
  // OPERATOR
  //////////////////////////

  // LENGTH 3
  TOKEN_OPERATOR_DASH_CARET_CARET,
  // LENGTH 2
  TOKEN_OPERATOR_AMPERSAND_AMPERSAND,
  TOKEN_OPERATOR_BANG_EQUAL,
  TOKEN_OPERATOR_DASH_CARET,
  TOKEN_OPERATOR_DASH_GREATERTHAN,
  TOKEN_OPERATOR_DOT_DOT,
  TOKEN_OPERATOR_EQUAL_EQUAL,
  TOKEN_OPERATOR_GREATERTHAN_EQUAL,
  TOKEN_OPERATOR_GREATERTHAN_GREATERTHAN,
  TOKEN_OPERATOR_LESSTHAN_EQUAL,
  TOKEN_OPERATOR_LESSTHAN_LESSTHAN,
  TOKEN_OPERATOR_PIPE_PIPE,
  TOKEN_OPERATOR_PLUS_CARET,
  // LENGTH 1
  TOKEN_OPERATOR_AMPERSAND,
  TOKEN_OPERATOR_ASTERISK,
  TOKEN_OPERATOR_AT,
  TOKEN_OPERATOR_BANG,
  TOKEN_OPERATOR_CARET,
  TOKEN_OPERATOR_COLON,
  TOKEN_OPERATOR_COMMA,
  TOKEN_OPERATOR_DOLLAR,
  TOKEN_OPERATOR_DOT,
  TOKEN_OPERATOR_EQUAL,
  TOKEN_OPERATOR_GREATERTHAN,
  TOKEN_OPERATOR_LEFTBRACE,
  TOKEN_OPERATOR_LEFTBRACKET,
  TOKEN_OPERATOR_LEFTPARENTHESIS,
  TOKEN_OPERATOR_LESSTHAN,
  TOKEN_OPERATOR_PIPE,
  TOKEN_OPERATOR_QUESTION,
  TOKEN_OPERATOR_RIGHTBRACE,
  TOKEN_OPERATOR_RIGHTBRACKET,
  TOKEN_OPERATOR_RIGHTPARENTHESIS,
  TOKEN_OPERATOR_SEMICOLON,
  TOKEN_OPERATOR_SLASH,
  TOKEN_OPERATOR_TILDE,
  // SPECIAL CASES
  TOKEN_OPERATOR_DASH,
  TOKEN_OPERATOR_PLUS,

  //////////////////////////
  // VALUES
  //////////////////////////

  TOKEN_IDENTIFIER,

  TOKEN_LITERAL_U8,
  TOKEN_LITERAL_U16,
  TOKEN_LITERAL_U32,
  TOKEN_LITERAL_U64,
  TOKEN_LITERAL_U,
  TOKEN_LITERAL_S8,
  TOKEN_LITERAL_S16,
  TOKEN_LITERAL_S32,
  TOKEN_LITERAL_S64,
  TOKEN_LITERAL_S,
  TOKEN_LITERAL_F16,
  TOKEN_LITERAL_F32,
  TOKEN_LITERAL_F64,
  TOKEN_LITERAL_F128,
  TOKEN_LITERAL_ASCII,
  TOKEN_LITERAL_ASTRING,

  TOKEN_EOF,

  //! Delimiter token for Earley Parser
  TOKEN_UNDEFINED
};

struct token
{
  //! Variant tag.
  enum token_type tag;

  //! Pointer to the module in the #module_cache whose source this token was
  //! parsed from.
  struct module* module;

  //! Pointer to the location in the source buffer where the token begins.
  char const* start;

  //! Number of characters in the token.
  size_t length;

  //! Line in the source buffer where the first character of this token was
  //! parsed from.
  //! @note
  //!     1-indexed from the start of the source.
  size_t line;

  //! Column in the source buffer where the first character of this token was
  //! parsed from.
  //! @note
  //!     1-indexed from the beginning of the line.
  size_t column;
};

//! Get a cstring representation of a token type.
//! @param ttype
//!     Target #token_type.
//! @return
//!     Corresponding cstring for the target token type. For fixed tokens this
//!     is the token's string itself. For non-fixed tokens this is a description
//!     of the token type.
//! @return
//!     `NULL` if no cstring mapping exists for the provided token type.
char const*
token_type_to_cstr(enum token_type ttype);

//! #nlogf_flc using the file, line, and colum from the provided #token pointer.
//! @param level
//!     Log level the message will be written at.
//! @param p_token
//!     Pointer to the token used for the file, line, and column of the log.
//! @param ...
//!     printf-style format string and arguments to the format specifiers.
//! @note
//!     A newline is automatically appended to the format string.
#define NLOGF_FLC_TOK(level, p_token, ...)                                     \
  ({                                                                           \
    struct token const* const _t = p_token;                                    \
    nlogf_flc(                                                                 \
      level, _t->module->src_path.data, _t->line, _t->column, __VA_ARGS__);    \
  })
