/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "nutils.h"

////////////////////////////////////////////////////////////////////////////////
//      PANIC                                                                 //
////////////////////////////////////////////////////////////////////////////////
//! @TODO Replace execinfo.h with a portable backtrace library available on all
//! POSIX platforms, or conditionally include execinfo.h and backtrace
//! functionality for builds using GLIBC.
#include <execinfo.h>

//! Maximum number of backtrace frames.
#define MAX_BACKTRACE 512

void npanic(char const* fmt, ...)
{
    va_list args;

    // Write the panic message to stderr.
    va_start(args, fmt);
    fprintf(stderr, "panic: ");
    vfprintf(stderr, fmt, args);
    fprintf(stderr, "\n");
    va_end(args);

    // Write a backtrace to stderr.
    void* backtrace_buf[MAX_BACKTRACE];
    int num_backtrace_frames = backtrace(backtrace_buf, MAX_BACKTRACE);
    char** symbols = backtrace_symbols(backtrace_buf, num_backtrace_frames);
    if (NULL == symbols)
    {
        fprintf(stderr, "A failure occurred in `backtrace_symbols`.\n");
        fprintf(stderr, "Printing backtrace using `backtrace_symbols_fd`.\n");
        backtrace_symbols_fd(
            backtrace_buf, num_backtrace_frames, STDERR_FILENO);
        goto do_exit;
    }
    for (int i = 0; i < num_backtrace_frames; ++i)
    {
        fprintf(stderr, "%s\n", symbols[i]);
    }

do_exit:
    // Although the UNIX convention is to leave stderr unbuffered, there is no
    // guarantee of such behavior in the C standard, so stderr is flushed as a
    // precaution.
    fflush(stderr);
    exit(EXIT_FAILURE);
}
////////////////////////////////////////////////////////////////////////////////
//      NARRAY                                                                //
////////////////////////////////////////////////////////////////////////////////
struct _narray
{
    size_t _length;
    size_t _capacity;
    size_t _elemsize;
    alignas(alignof(max_align_t)) char _data[];
};

#define _NARR_CAPACITY_MIN ((size_t)10)
#define _NARR_CAPACITY_FACTOR ((double)1.3)
#define _NARR_NEW_CAPACITY_FROM_LENGTH(len)                                    \
    ((len) < _NARR_CAPACITY_MIN                                                \
         ? _NARR_CAPACITY_MIN                                                  \
         : ((size_t)(((double)len) * _NARR_CAPACITY_FACTOR)))
#define _NARR_TOTAL_SIZE(capacity, elsize)                                     \
    (sizeof(struct _narray) + ((capacity) * (elsize)))

#define _NARR_P_HEAD_FROM_HANDLE(narr_hndl)                                    \
    (((char*)narr_hndl) - sizeof(struct _narray))
#define _NARR_P_LENGTH_FROM_HANDLE(narr_hndl)                                  \
    ((size_t*)(_NARR_P_HEAD_FROM_HANDLE(narr_hndl) + offsetof(struct _narray, _length)))
#define _NARR_P_CAPACITY_FROM_HANDLE(narr_hndl)                                \
    ((size_t*)(_NARR_P_HEAD_FROM_HANDLE(narr_hndl) + offsetof(struct _narray, _capacity)))
#define _NARR_P_ELEMSIZE_FROM_HANDLE(narr_hndl)                                \
    ((size_t*)(_NARR_P_HEAD_FROM_HANDLE(narr_hndl) + offsetof(struct _narray, _elemsize)))

void* narr_alloc(size_t numelem, size_t elemsize)
{
    size_t const capacity = _NARR_NEW_CAPACITY_FROM_LENGTH(numelem);
    struct _narray* const mem = nalloc(_NARR_TOTAL_SIZE(capacity, elemsize));
    mem->_length = numelem;
    mem->_capacity = capacity;
    mem->_elemsize = elemsize;
    return mem->_data;
}

void narr_free(void* narr)
{
    nfree(_NARR_P_HEAD_FROM_HANDLE(narr));
}

size_t narr_length(void const* narr)
{
    return *_NARR_P_LENGTH_FROM_HANDLE(narr);
}

size_t narr_capacity(void const* narr)
{
    return *_NARR_P_CAPACITY_FROM_HANDLE(narr);
}

size_t narr_sizeof_elem(void const* narr)
{
    return *_NARR_P_ELEMSIZE_FROM_HANDLE(narr);
}

void* narr_reserve(void* narr, size_t numelem)
{
    size_t const min_capacity = narr_length(narr) + numelem;
    size_t const curr_capacity = narr_capacity(narr);
    if (curr_capacity >= min_capacity)
    {
        // Narray with capacity min_capacity can already fit within the
        // currently allocated memory block.
        return narr;
    }
    // new_capacity is large enough to hold the required space plus extra in
    // case of additional expansion.
    size_t const new_capacity = _NARR_NEW_CAPACITY_FROM_LENGTH(min_capacity);
    struct _narray* const new_mem = nrealloc(
        _NARR_P_HEAD_FROM_HANDLE(narr),
        _NARR_TOTAL_SIZE(new_capacity, narr_sizeof_elem(narr)));
    new_mem->_capacity = new_capacity;
    return new_mem->_data;
}

void* narr_resize(void* narr, size_t len)
{
    if (narr_length(narr) < len)
    {
        narr = narr_reserve(narr, len - narr_length(narr));
    }
    *_NARR_P_LENGTH_FROM_HANDLE(narr) = len;
    return narr;
}

void* narr_push(void* narr, void const* ptr_to_val)
{
    narr = narr_reserve(narr, 1);
    memcpy(
        (char*)narr + narr_length(narr) * narr_sizeof_elem(narr),
        ptr_to_val,
        narr_sizeof_elem(narr));
    *_NARR_P_LENGTH_FROM_HANDLE(narr) += 1;
    return narr;
}

void narr_pop(void* narr)
{
    *_NARR_P_LENGTH_FROM_HANDLE(narr) -= 1;
}

void* narr_unshift(void* narr, void const* ptr_to_val)
{
    narr = narr_reserve(narr, 1);
    memmove(
        (char*)narr + narr_sizeof_elem(narr),
        (char*)narr,
        narr_length(narr) * narr_sizeof_elem(narr));
    memcpy(narr, ptr_to_val, narr_sizeof_elem(narr));
    *_NARR_P_LENGTH_FROM_HANDLE(narr) += 1;
    return narr;
}

void narr_shift(void* narr)
{
    memmove(
        (char*)narr,
        (char*)narr + narr_sizeof_elem(narr),
        (narr_length(narr) - 1) * narr_sizeof_elem(narr));
    *_NARR_P_LENGTH_FROM_HANDLE(narr) -= 1;
}

void* narr_front(void const* narr)
{
    if (0 == narr_length(narr))
    {
        return NULL;
    }
    return (char*)narr;
}

void* narr_back(void const* narr)
{
    if (0 == narr_length(narr))
    {
        return NULL;
    }
    return (char*)narr + (narr_length(narr) - 1) * narr_sizeof_elem(narr);
}

////////////////////////////////////////////////////////////////////////////////
//      STRING                                                                //
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////
//      CSTRING                       //
////////////////////////////////////////
int cstr_cmp(char const* lhs, char const* rhs)
{
    return strcmp(lhs, rhs);
}

bool cstr_eq(char const* lhs, char const* rhs)
{
    return cstr_cmp(lhs, rhs) == 0;
}

bool cstr_ne(char const* lhs, char const* rhs)
{
    return cstr_cmp(lhs, rhs) != 0;
}

int cstr_n_cmp(char const* lhs, char const* rhs, size_t n)
{
    return strncmp(lhs, rhs, n);
}

bool cstr_n_eq(char const* lhs, char const* rhs, size_t n)
{
    return cstr_n_cmp(lhs, rhs, n) == 0;
}

bool cstr_n_ne(char const* lhs, char const* rhs, size_t n)
{
    return cstr_n_cmp(lhs, rhs, n) != 0;
}

int cstr_cmp_case(char const* lhs, char const* rhs)
{
    while (tolower(*lhs) == tolower(*rhs) && *lhs != '\0')
    {
        ++lhs;
        ++rhs;
    }
    return tolower(*lhs) - tolower(*rhs);
}

bool cstr_eq_case(char const* lhs, char const* rhs)
{
    return cstr_cmp_case(lhs, rhs) == 0;
}

bool cstr_ne_case(char const* lhs, char const* rhs)
{
    return cstr_cmp_case(lhs, rhs) != 0;
}

int cstr_n_cmp_case(char const* lhs, char const* rhs, size_t n)
{
    if (n == 0)
    {
        return 0;
    }
    while (--n && tolower(*lhs) == tolower(*rhs) && *lhs != '\0')
    {
        ++lhs;
        ++rhs;
    }
    return tolower(*lhs) - tolower(*rhs);
}

bool cstr_n_eq_case(char const* lhs, char const* rhs, size_t n)
{
    return cstr_n_cmp_case(lhs, rhs, n) == 0;
}

bool cstr_n_ne_case(char const* lhs, char const* rhs, size_t n)
{
    return cstr_n_cmp_case(lhs, rhs, n) != 0;
}

size_t cstr_len(char const* str)
{
    return strlen(str);
}

bool cstr_len_ge(char const* str, size_t n)
{
    while (n--)
    {
        if (*str++ == '\0')
        {
            return false;
        }
    }
    return true;
}

void cstr_cat(char* dest, char const* src)
{
    strcat(dest, src);
}

void cstr_n_cat(char* dest, char const* src, size_t n)
{
    strncat(dest, src, n);
}

void cstr_cpy(char* dest, char const* src)
{
    strcpy(dest, src);
}

void cstr_n_cpy(char* dest, char const* src, size_t n)
{
    strncpy(dest, src, n);
}

////////////////////////////////////////
//      NSTRING                       //
////////////////////////////////////////
#define _NSTR_CAPACITY_MIN ((size_t)10)
#define _NSTR_CAPACITY_FACTOR ((double)1.3)
#define _NSTR_NEW_CAPACITY_FROM_LEN(len)                                       \
    ((len) < _NSTR_CAPACITY_MIN                                                \
         ? _NSTR_CAPACITY_MIN                                                  \
         : ((size_t)(((double)len) * _NSTR_CAPACITY_FACTOR)))
#define _NSTR_DATA_SIZE_FROM_CAPACITY(capacity)                                \
    (capacity + NULL_TERMINATOR_LENGTH)

#define _NSTR_DFLT_INIT_LEN (0)
#define _NSTR_DFLT_INIT_CAPACITY                                               \
    (_NSTR_NEW_CAPACITY_FROM_LEN(_NSTR_DFLT_INIT_LEN))

// Used by nstr_assign_fmt and nstr_cat_fmt.
static void nstr_cat_vfmt(nstr_t* dest, char const* fmt, va_list args);

void nstr_init(nstr_t* nstr)
{
    nstr->data =
        nalloc(_NSTR_DATA_SIZE_FROM_CAPACITY(_NSTR_DFLT_INIT_CAPACITY));
    nstr->len = _NSTR_DFLT_INIT_LEN;
    nstr->capacity = _NSTR_DFLT_INIT_CAPACITY;
    (nstr->data)[0] = '\0';
}

void nstr_init_cstr(nstr_t* nstr, char const* other)
{
    size_t const other_len = cstr_len(other);
    size_t const capacity = _NSTR_NEW_CAPACITY_FROM_LEN(other_len);

    nstr->data = nalloc(_NSTR_DATA_SIZE_FROM_CAPACITY(capacity));
    cstr_cpy(nstr->data, other);

    nstr->len = other_len;
    nstr->capacity = capacity;
}

void nstr_init_nstr(nstr_t* nstr, nstr_t const* other)
{
    size_t const other_len = other->len;
    size_t const capacity = _NSTR_NEW_CAPACITY_FROM_LEN(other_len);

    nstr->data = nalloc(_NSTR_DATA_SIZE_FROM_CAPACITY(capacity));
    memcpy(nstr->data, other->data, other_len + NULL_TERMINATOR_LENGTH);

    nstr->len = other_len;
    nstr->capacity = capacity;
}

void nstr_init_fmt(nstr_t* nstr, char const* fmt, ...)
{
    nstr_init(nstr); // Init to empty string.

    va_list args;
    va_start(args, fmt);
    nstr_cat_vfmt(nstr, fmt, args);
    va_end(args);
}

void nstr_fini(nstr_t* nstr)
{
    nfree(nstr->data);
}

void nstr_resize(nstr_t* nstr, size_t len)
{
    if (nstr->len < len)
    {
        nstr_reserve(nstr, len - (nstr->len));
    }
    nstr->len = len;
    // Always set the end byte to null to prevent cstr overflows.
    (nstr->data)[nstr->len] = '\0';
}

void nstr_reserve(nstr_t* nstr, size_t nchars)
{
    size_t const min_capacity = nstr->len + nchars + NULL_TERMINATOR_LENGTH;
    size_t const curr_capacity = nstr->capacity;
    if (curr_capacity >= min_capacity)
    {
        return;
    }

    size_t new_capacity =
        (size_t)((double)min_capacity * _NSTR_CAPACITY_FACTOR);
    nstr->data =
        nrealloc(nstr->data, _NSTR_DATA_SIZE_FROM_CAPACITY(new_capacity));
    nstr->capacity = new_capacity;
}

void nstr_clear(nstr_t* nstr)
{
    nstr_resize(nstr, 0);
}

void nstr_assign_cstr(nstr_t* nstr, char const* other)
{
    nstr->len = 0;
    nstr_cat_cstr(nstr, other);
}

void nstr_assign_nstr(nstr_t* nstr, nstr_t const* other)
{
    nstr->len = 0;
    nstr_cat_nstr(nstr, other);
}

void nstr_assign_fmt(nstr_t* nstr, char const* fmt, ...)
{
    nstr->len = 0;
    va_list args;
    va_start(args, fmt);
    nstr_cat_vfmt(nstr, fmt, args);
    va_end(args);
}

int nstr_cmp(nstr_t const* lhs, nstr_t const* rhs)
{
    return cstr_cmp(lhs->data, rhs->data);
}

bool nstr_eq(nstr_t const* lhs, nstr_t const* rhs)
{
    return nstr_cmp(lhs, rhs) == 0;
}

bool nstr_ne(nstr_t const* lhs, nstr_t const* rhs)
{
    return nstr_cmp(lhs, rhs) != 0;
}

int nstr_cmp_case(nstr_t const* lhs, nstr_t const* rhs)
{
    return cstr_cmp_case(lhs->data, rhs->data);
}

bool nstr_eq_case(nstr_t const* lhs, nstr_t const* rhs)
{
    return nstr_cmp_case(lhs, rhs) == 0;
}

bool nstr_ne_case(nstr_t const* lhs, nstr_t const* rhs)
{
    return nstr_cmp_case(lhs, rhs) != 0;
}

void nstr_cat_cstr(nstr_t* dest, char const* src)
{
    size_t const dest_len = dest->len;
    size_t const src_len = cstr_len(src);
    nstr_reserve(dest, src_len);
    memcpy(dest->data + dest_len, src, src_len + NULL_TERMINATOR_LENGTH);
    dest->len += src_len;
}

void nstr_cat_nstr(nstr_t* dest, nstr_t const* src)
{
    size_t const dest_len = dest->len;
    size_t const src_len = src->len;
    nstr_reserve(dest, src_len);
    memcpy(dest->data + dest_len, src->data, src_len + NULL_TERMINATOR_LENGTH);
    dest->len += src_len;
}

void nstr_cat_fmt(nstr_t* dest, char const* fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    nstr_cat_vfmt(dest, fmt, args);
    va_end(args);
}

static void nstr_cat_vfmt(nstr_t* dest, char const* fmt, va_list args)
{
    va_list copy;
    va_copy(copy, args);
    size_t const fmt_len = (size_t)vsnprintf(NULL, 0, fmt, copy);
    va_end(copy);

    // A call to vsprintf that uses the destination buffer for the format string
    // or as a format parameter causes undefined behavior. A temporary buffer
    // is written to for the vsprintf call to avoid the UB. After the write, the
    // temporary is copied to the destination string.
    char* tmp = alloca(fmt_len + NULL_TERMINATOR_LENGTH);
    vsprintf(tmp, fmt, args);

    size_t pre_resize_len = dest->len;
    nstr_resize(dest, pre_resize_len + fmt_len);
    memcpy(dest->data + pre_resize_len, tmp, fmt_len);
}

////////////////////////////////////////////////////////////////////////////////
//      DICTIONARY UTILITIES                                                  //
////////////////////////////////////////////////////////////////////////////////
uint32_t const dict_primes[DICT_PRIMES_LENGTH] = {
    /*  prime      // splits 2^n and 2^(n-1) for n == ? */
    /*  =============================================== */
    53,        // 6
    97,        // 7
    193,       // 8
    389,       // 9
    769,       // 10
    1543,      // 11
    3079,      // 12
    6151,      // 13
    12289,     // 14
    24593,     // 15
    49157,     // 16
    98317,     // 17
    196613,    // 18
    393241,    // 19
    786433,    // 20
    1572869,   // 21
    3145739,   // 22
    6291469,   // 23
    12582917,  // 24
    25165843,  // 25
    50331653,  // 26
    100663319, // 27
    201326611, // 28
    402653189, // 29
    805306457, // 30
    1610612741 // 31
};

double dict_slot_load_factor(uint32_t slots_in_use, uint32_t slots_total)
{
    return ((double)slots_in_use) / slots_total;
}

#define HASH_CHAR_SLICE_MAGIC_PRIME_START 7
#define HASH_CHAR_SLICE_MAGIC_PRIME_MULT 101
uint32_t hash_char_slice(char const* start, size_t length)
{
    uint32_t hash = HASH_CHAR_SLICE_MAGIC_PRIME_START;
    while (length--)
    {
        DIAGNOSTIC_PUSH
        DIAGNOSTIC_IGNORE("-Wsign-conversion")
        hash = (hash * HASH_CHAR_SLICE_MAGIC_PRIME_MULT) + (uint32_t)(*start++);
        DIAGNOSTIC_POP
    }
    return hash;
}

////////////////////////////////////////////////////////////////////////////////
//      IO                                                                    //
////////////////////////////////////////////////////////////////////////////////
int log_config = DEFAULT_LOG_CONFIG;
FILE* const* log_stream = DEFAULT_LOG_STREAM;

static pthread_mutex_t log_mutex = PTHREAD_MUTEX_INITIALIZER;

static void _log(char const* logstr, char const* fmt, va_list args)
{
    pthread_mutex_lock(&log_mutex);
    fprintf(*log_stream, "%s", logstr);
    vfprintf(*log_stream, fmt, args);
    fputc('\n', *log_stream);
    fflush(*log_stream);
    pthread_mutex_unlock(&log_mutex);
}

static void _log_flc(
    char const* path,
    size_t line,
    size_t col,
    char const* logstr,
    char const* fmt,
    va_list args)
{
    pthread_mutex_lock(&log_mutex);
    fprintf(*log_stream, "%s:%zu:%zu %s", path, line, col, logstr);
    vfprintf(*log_stream, fmt, args);
    fputc('\n', *log_stream);
    fflush(*log_stream);
    pthread_mutex_unlock(&log_mutex);
}

static void _log_pretty(
    char const* source_code_snippet,
    char const* path,
    size_t line,
    size_t col,
    char const* logstr,
    char const* fmt,
    va_list args)
{
    pthread_mutex_lock(&log_mutex);

    // Write the error file, line, and column along with the error message.
    fprintf(*log_stream, "%s:%zu:%zu %s", path, line, col, logstr);
    vfprintf(*log_stream, fmt, args);
    fputc('\n', *log_stream);

    // Write the rendition of the source code.
    fprintf(*log_stream, "%s", source_code_snippet);
    fputc('\n', *log_stream);

    fflush(*log_stream);
    pthread_mutex_unlock(&log_mutex);
}

#define STR_LOG_DEBUG "DEBUG: "
#define STR_LOG_METRIC "METRIC: "
#define STR_LOG_INFO "INFO: "
#define STR_LOG_WARNING "WARNING: "
#define STR_LOG_ERROR "ERROR: "
#define STR_LOG_FATAL "FATAL: "

#define STR_LOG_DEBUG_COLOR                                                    \
    ANSI_ESC_BOLD ANSI_ESC_COLOR_YELLOW STR_LOG_DEBUG ANSI_ESC_DEFAULT
#define STR_LOG_DEBUG_COLOR_IF_TTY                                             \
    (isatty(fileno(*log_stream)) ? STR_LOG_DEBUG_COLOR : STR_LOG_DEBUG)

#define STR_LOG_METRIC_COLOR                                                   \
    ANSI_ESC_BOLD ANSI_ESC_COLOR_CYAN STR_LOG_METRIC ANSI_ESC_DEFAULT
#define STR_LOG_METRIC_COLOR_IF_TTY                                            \
    (isatty(fileno(*log_stream)) ? STR_LOG_METRIC_COLOR : STR_LOG_METRIC)

#define STR_LOG_INFO_COLOR                                                     \
    ANSI_ESC_BOLD ANSI_ESC_COLOR_BLUE STR_LOG_INFO ANSI_ESC_DEFAULT
#define STR_LOG_INFO_COLOR_IF_TTY                                              \
    (isatty(fileno(*log_stream)) ? STR_LOG_INFO_COLOR : STR_LOG_INFO)

#define STR_LOG_WARNING_COLOR                                                  \
    ANSI_ESC_BOLD ANSI_ESC_COLOR_MAGENTA STR_LOG_WARNING ANSI_ESC_DEFAULT
#define STR_LOG_WARNING_COLOR_IF_TTY                                           \
    (isatty(fileno(*log_stream)) ? STR_LOG_WARNING_COLOR : STR_LOG_WARNING)

#define STR_LOG_ERROR_COLOR                                                    \
    ANSI_ESC_BOLD ANSI_ESC_COLOR_RED STR_LOG_ERROR ANSI_ESC_DEFAULT
#define STR_LOG_ERROR_COLOR_IF_TTY                                             \
    (isatty(fileno(*log_stream)) ? STR_LOG_ERROR_COLOR : STR_LOG_ERROR)

#define STR_LOG_FATAL_COLOR                                                    \
    ANSI_ESC_BOLD ANSI_ESC_COLOR_RED STR_LOG_FATAL ANSI_ESC_DEFAULT
#define STR_LOG_FATAL_COLOR_IF_TTY                                             \
    (isatty(fileno(*log_stream)) ? STR_LOG_FATAL_COLOR : STR_LOG_FATAL)

void nlogf(int level, char const* fmt, ...)
{
    int const level_with_config_mask = level & log_config;
    if (level_with_config_mask == 0x00) // Log level not enabled.
    {
        return;
    }

    va_list args;
    va_start(args, fmt);
    switch (level_with_config_mask)
    {
    case LOG_DEBUG:
        _log(STR_LOG_DEBUG_COLOR_IF_TTY, fmt, args);
        break;
    case LOG_METRIC:
        _log(STR_LOG_METRIC_COLOR_IF_TTY, fmt, args);
        break;
    case LOG_INFO:
        _log(STR_LOG_INFO_COLOR_IF_TTY, fmt, args);
        break;
    case LOG_WARNING:
        _log(STR_LOG_WARNING_COLOR_IF_TTY, fmt, args);
        break;
    case LOG_ERROR:
        _log(STR_LOG_ERROR_COLOR_IF_TTY, fmt, args);
        break;
    case LOG_FATAL:
        _log(STR_LOG_FATAL_COLOR_IF_TTY, fmt, args);
        break;
    }
    va_end(args);
}

void nlogf_flc(
    int level, char const* path, size_t line, size_t col, char const* fmt, ...)
{
    int const level_with_config_mask = level & log_config;
    if (level_with_config_mask == 0x00) // Log level not enabled.
    {
        return;
    }

    va_list args;
    va_start(args, fmt);
    switch (level_with_config_mask)
    {
    case LOG_DEBUG:
        _log_flc(path, line, col, STR_LOG_DEBUG_COLOR_IF_TTY, fmt, args);
        break;
    case LOG_METRIC:
        _log_flc(path, line, col, STR_LOG_METRIC_COLOR_IF_TTY, fmt, args);
        break;
    case LOG_INFO:
        _log_flc(path, line, col, STR_LOG_INFO_COLOR_IF_TTY, fmt, args);
        break;
    case LOG_WARNING:
        _log_flc(path, line, col, STR_LOG_WARNING_COLOR_IF_TTY, fmt, args);
        break;
    case LOG_ERROR:
        _log_flc(path, line, col, STR_LOG_ERROR_COLOR_IF_TTY, fmt, args);
        break;
    case LOG_FATAL:
        _log_flc(path, line, col, STR_LOG_FATAL_COLOR_IF_TTY, fmt, args);
        break;
    }
    va_end(args);
}

void nlogf_pretty(
    int level,
    nstr_t const path_src,
    char const* path,
    size_t line,
    size_t col,
    char const* fmt,
    ...)
{
    int const level_with_config_mask = level & log_config;
    if (level_with_config_mask == 0x00) // Log level not enabled.
    {
        return;
    }

    size_t current_line = 1;
    size_t path_src_first_idx = 0;

    // We want to print out the last three lines.
    // If the current line is 1, we are done.
    bool success = line == 1;
    // Loop through the lines of the source file until we reach the line two
    // above the provided line.
    for (; !success && path_src_first_idx < path_src.len; ++path_src_first_idx)
    {
        if (current_line >= line - 2)
        {
            success = true;
            break;
        }
        current_line += path_src.data[path_src_first_idx] == '\n';
    }
    nassert(success);

    DIAGNOSTIC_PUSH
    DIAGNOSTIC_IGNORE("-Wconversion")
    uint8_t one_minus = (uint8_t)(line - current_line) + 1;
    size_t path_src_last_idx = path_src_first_idx;
    for (; path_src_last_idx < path_src.len; ++path_src_last_idx)
    {
        one_minus -= path_src.data[path_src_last_idx] == '\n';
        if (one_minus == 0)
        {
            break;
        }
    }
    DIAGNOSTIC_POP

    // Build the source code snippet.
    nstr_t nstr_defer_fini source_code_nstr;
    nstr_init(&source_code_nstr);

    // Add the line of souce code.
    size_t const line_length = path_src_last_idx - path_src_first_idx;
    nstr_cat_fmt(
        &source_code_nstr,
        "%.*s",
        line_length,
        &path_src.data[path_src_first_idx]);

    // Add the caret arrow below the line of source code.
    nstr_cat_cstr(&source_code_nstr, "\n");
    bool const log_stream_isatty = isatty(fileno(*log_stream));
    if (log_stream_isatty)
    {
        nstr_cat_cstr(&source_code_nstr, ANSI_ESC_BOLD ANSI_ESC_COLOR_GREEN);
    }
    size_t column_count = col - 1;
    while (column_count--)
    {
        nstr_cat_cstr(&source_code_nstr, "-");
    }
    nstr_cat_cstr(&source_code_nstr, "^");
    if (log_stream_isatty)
    {
        nstr_cat_cstr(&source_code_nstr, ANSI_ESC_DEFAULT);
    }

    va_list args;
    va_start(args, fmt);
    switch (level_with_config_mask)
    {
    case LOG_DEBUG:
        _log_pretty(
            source_code_nstr.data,
            path,
            line,
            col,
            STR_LOG_DEBUG_COLOR_IF_TTY,
            fmt,
            args);
        break;
    case LOG_METRIC:
        _log_pretty(
            source_code_nstr.data,
            path,
            line,
            col,
            STR_LOG_METRIC_COLOR_IF_TTY,
            fmt,
            args);
        break;
    case LOG_INFO:
        _log_pretty(
            source_code_nstr.data,
            path,
            line,
            col,
            STR_LOG_INFO_COLOR_IF_TTY,
            fmt,
            args);
        break;
    case LOG_WARNING:
        _log_pretty(
            source_code_nstr.data,
            path,
            line,
            col,
            STR_LOG_WARNING_COLOR_IF_TTY,
            fmt,
            args);
        break;
    case LOG_ERROR:
        _log_pretty(
            source_code_nstr.data,
            path,
            line,
            col,
            STR_LOG_ERROR_COLOR_IF_TTY,
            fmt,
            args);
        break;
    case LOG_FATAL:
        _log_pretty(
            source_code_nstr.data,
            path,
            line,
            col,
            STR_LOG_FATAL_COLOR_IF_TTY,
            fmt,
            args);
        break;
    }
    va_end(args);
}

bool file_exist(char const* path)
{
    return access(path, F_OK) == 0;
}

ssize_t file_size(char const* path)
{
    struct stat st;
    if (stat(path, &st) != 0)
    {
        return -1;
    }
    return (ssize_t)st.st_size;
}

ssize_t file_read_into_buff(char const* path, char* buff, size_t bufsize)
{
    ssize_t fsize = file_size(path);
    if (fsize == -1 || ((size_t)fsize > bufsize - 1))
    {
        return -1;
    }
    FILE* fp = fopen(path, "rb");
    if (!fp)
    {
        return -1;
    }
    if (fread(buff, sizeof(char), (size_t)fsize, fp) != (size_t)fsize)
    {
        fclose(fp);
        return -1;
    }
    buff[fsize] = '\0';
    fclose(fp);
    return fsize;
}

ssize_t file_read_into_nstr(char const* path, nstr_t* nstr)
{
    ssize_t fsize = file_size(path);
    if (fsize == -1)
    {
        return -1;
    }
    FILE* fp = fopen(path, "rb");
    if (!fp)
    {
        return -1;
    }
    nstr_resize(nstr, (size_t)fsize);
    if (fread(nstr->data, sizeof(char), (size_t)fsize, fp) != (size_t)fsize)
    {
        return -1;
    }
    nstr->data[fsize] = '\0';
    fclose(fp);
    return fsize;
}

int remove_rf_dir_cb(
    const char* fpath, const struct stat* sb, int typeflag, struct FTW* ftwbuf)
{
    SUPPRESS_UNUSED(sb);
    SUPPRESS_UNUSED(typeflag);
    SUPPRESS_UNUSED(ftwbuf);

    int const remove_status = remove(fpath);
    // The POSIX nftw function specifies that the callback will return zero on
    // success and a non-zero value on failure.
    if (0 != remove_status)
    {
        perror(fpath);
    }
    return remove_status;
}

//! Max number of simultaneously open file descriptors that we will allow nftw
//! to hold open.
//! This is a *not* the limit on the number of subdirectories that can be
//! handled in total, just the number of subdirectories that can be handled by
//! nftw at the same time as it runs before needing to close some of its open
//! file descriptors.
#define NFTW_MAX_SIMULTANEOUSLY_OPEN_FILE_DESCRIPTORS 64

int remove_rf_dir(char const* dirpath)
{
    return nftw(
        dirpath,
        remove_rf_dir_cb,
        NFTW_MAX_SIMULTANEOUSLY_OPEN_FILE_DESCRIPTORS,
        FTW_DEPTH | FTW_PHYS);
}

ssize_t fmt_len(char const* fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    ssize_t fmtted_strlen = vsnprintf(NULL, 0, fmt, args);
    if (fmtted_strlen == -1)
    {
        va_end(args);
        return -1;
    }
    va_end(args);
    return fmtted_strlen;
}

////////////////////////////////////////////////////////////////////////////////
//      MEMORY                                                                //
////////////////////////////////////////////////////////////////////////////////
void* nalloc(size_t size)
{
    void* p = malloc(size);
    if (p == NULL && size != 0)
    {
        npanic("nalloc out of memory error (%zu bytes)", size);
    }
    return p;
}

void* nrealloc(void* ptr, size_t size)
{
    void* p = realloc(ptr, size);
    if (p == NULL && size != 0)
    {
        npanic("nrealloc out of memory error (%zu bytes)", size);
    }
    return p;
}

void nfree(void* ptr)
{
    free(ptr);
}

void memswap(void* p1, void* p2, size_t size)
{
    char tmp;
    char* a = p1;
    char* b = p2;
    for (size_t i = 0; i < size; ++i)
    {
        tmp = a[i];
        a[i] = b[i];
        b[i] = tmp;
    }
}

////////////////////////////////////////////////////////////////////////////////
//      METRICS                                                               //
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////
//      INIT FINI COUNTER             //
////////////////////////////////////////
void _verify_balanced_init_fini(
    char const* type_name, unsigned init_numcalls, unsigned fini_numcalls)
{
    if (init_numcalls != fini_numcalls)
    {
        nlogf(
            LOG_ERROR,
            "Unbalanced calls to init and fini functions for data type '%s'.\n"
            "    init = %u\n"
            "    fini = %u",
            type_name,
            init_numcalls,
            fini_numcalls);
    }
}

////////////////////////////////////////
//      HIGH PRECISION TIMING         //
////////////////////////////////////////
void ngettime(struct timespec* ts)
{
    if (UNLIKELY(-1 == clock_gettime(CLOCK_MONOTONIC, ts)))
    {
        npanic("clock_gettime failure");
    }
}

#define MS_PER_SEC 1000
#define NS_PER_MS 1000000
#define SEC_TO_MS(_sec) (((long long)(_sec)) * MS_PER_SEC)
#define NS_TO_MS(_ns) (((long long)(_ns)) / NS_PER_MS)
long long nelapsed_ms(struct timespec start, struct timespec stop)
{
    return (SEC_TO_MS(stop.tv_sec) + NS_TO_MS(stop.tv_nsec))
        - (SEC_TO_MS(start.tv_sec) + NS_TO_MS(start.tv_nsec));
}

////////////////////////////////////////////////////////////////////////////////
//      SIGNAL HANDLING                                                       //
////////////////////////////////////////////////////////////////////////////////
void generic_sighandler(int signum)
{
    // It is perfectly reasonable for these signals to be raised during normal
    // program execution. Since all of them are requesting program termination,
    // we'll just kill the program wherever it is. In the future it may be worth
    // looking into doing some basic cleanup of open file pointers and whatnot,
    // but for now having the host OS take care of resource cleanup on program
    // exit is more than adequate.
    if (signum == SIGTERM)
    {
        fprintf(stderr, "SIGTERM received\n");
        exit(EXIT_FAILURE);
    }
    if (signum == SIGINT)
    {
        fprintf(stderr, "keyboard interrupt\n");
        exit(EXIT_FAILURE);
    }
    if (signum == SIGQUIT)
    {
        fprintf(stderr, "keyboard quit\n");
        exit(EXIT_FAILURE);
    }

    // If SIGHUP is raised the user most likely closed their terminal window,
    // which is indeed abnormal program termination. Similarly a segfault should
    // be treated as abnormal program termination worthy of a panic.
    if (signum == SIGHUP)
    {
        npanic("terminal hangup");
    }
    if (signum == SIGSEGV)
    {
        npanic("segmentation fault");
    }

    // The rest of the signals are uncommon enough that handling them with a
    // generic panic is probably fine.
    npanic("unhandled signal (signum = %d)", signum);
}

void install_generic_sighandler(void)
{
    struct sigaction sa;
    memset(&sa, 0, sizeof(struct sigaction));
    sa.sa_handler = generic_sighandler;

    sigaction(SIGTERM, &sa, NULL);
    sigaction(SIGINT, &sa, NULL);
    sigaction(SIGQUIT, &sa, NULL);

    sigaction(SIGHUP, &sa, NULL);
    sigaction(SIGSEGV, &sa, NULL);

    sigaction(SIGILL, &sa, NULL);
    sigaction(SIGABRT, &sa, NULL);
    sigaction(SIGFPE, &sa, NULL);
    sigaction(SIGBUS, &sa, NULL);
    sigaction(SIGPIPE, &sa, NULL);
}
