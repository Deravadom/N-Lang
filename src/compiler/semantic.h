/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file semantic.h
 * @brief Semantic analysis for language constructs described by the NIR.
 *
 */

#pragma once
#include "compile.h"
#include "nir.h"

struct semantic_analysis_context
{
    //! Pointer to the module of compilation.
    struct module* module;

    //! Current #scope that statements are being processed within.
    struct scope* curr_scope;

    //! The function that @p curr_scope is a part of.
    //! #NULL if @p curr_scope is not within a function.
    //! @note
    //!     Not necessarily the immediate child #scope of the function itself in
    //!     the case of additional nested scopes within the function.
    struct func_sig* curr_func;
};

//! Semantic analysis functions for individual elements of the NIR.
//! All NIR structures passed into these functions are assumed to have gone
//! through all phases of compilation up to but not including semantic analysis.
//!{

stbool semantic_analysis_identifier(
    struct semantic_analysis_context* ctx, struct identifier* identifier);

stbool semantic_analysis_data_type(
    struct semantic_analysis_context* ctx, struct data_type* data_type);

stbool semantic_analysis_member_var(
    struct semantic_analysis_context* ctx, struct member_var* member_var);

stbool semantic_analysis_member_func(
    struct semantic_analysis_context* ctx, struct member_func* member_func);

stbool semantic_analysis_member_tag(
    struct semantic_analysis_context* ctx, struct member_tag* member_tag);

stbool semantic_analysis_member_iface(
    struct semantic_analysis_context* ctx, struct member_iface* member_iface);

stbool semantic_analysis_struct_def(
    struct semantic_analysis_context* ctx, struct struct_def* struct_def);

stbool semantic_analysis_variant_def(
    struct semantic_analysis_context* ctx, struct variant_def* variant_def);

stbool semantic_analysis_func_sig(
    struct semantic_analysis_context* ctx, struct func_sig* func_sig);

stbool semantic_analysis_literal(
    struct semantic_analysis_context* ctx, struct literal* literal);

stbool semantic_analysis_expr(
    struct semantic_analysis_context* ctx, struct expr* expr);

stbool semantic_analysis_variable_decl(
    struct semantic_analysis_context* ctx, struct variable_decl* variable_decl);

stbool semantic_analysis_function_decl(
    struct semantic_analysis_context* ctx, struct function_decl* function_decl);

stbool semantic_analysis_struct_decl(
    struct semantic_analysis_context* ctx, struct struct_decl* struct_decl);

stbool semantic_analysis_variant_decl(
    struct semantic_analysis_context* ctx, struct variant_decl* variant_decl);

stbool semantic_analysis_if_stmt(
    struct semantic_analysis_context* ctx, struct if_stmt* if_stmt);

stbool semantic_analysis_loop_stmt(
    struct semantic_analysis_context* ctx, struct loop_stmt* loop_stmt);

stbool semantic_analysis_alias_stmt(
    struct semantic_analysis_context* ctx, struct alias_stmt* alias_stmt);

stbool semantic_analysis_stmt(
    struct semantic_analysis_context* ctx, struct stmt* stmt);

stbool semantic_analysis_scope(
    struct semantic_analysis_context* ctx, struct scope* scope);

//!}

//! Perform the semantic analysis phase of the compiling process.
//! @param module
//!     Target module of compilation.
//! @return
//!     #STBOOL_SUCCESS on success.
//! @return
//!     #STBOOL_FAILURE on failure.
stbool dophase_semantic_analysis(struct module* module);
