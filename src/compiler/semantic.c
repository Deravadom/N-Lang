/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file semantic.c
 * @brief Semantic analysis for language constructs described by the NIR.
 *
 */

#include "semantic.h"

static void populate_scope_metadata(
    struct semantic_analysis_context* ctx, struct scope* scope);

#define FMT_EXPR_HAS_TYPE_X_EXPECTED_Y(expected_type_cstr)                     \
    "Expression has type '%s'. Expected " expected_type_cstr " type."

#define FMT_CANNOT_IMPLICITLY_CONVERT_RHS_TO_LHS                               \
    "Cannot implicitly convert right hand side data type '%s' to left hand "   \
    "side data type '%s'."

static void log_identifier_redeclaration(
    struct identifier const* redeclared_identifier,
    struct identifier const* original_identifier)
{
    struct token const* const redeclared_identifier_token =
        redeclared_identifier->srctok;
    struct token const* const original_identifier_token =
        original_identifier->srctok;
    NLOGF_FLC_TOK(
        LOG_ERROR,
        redeclared_identifier_token,
        "Redeclaration of identifier '%.*s' previously defined at %s:%zu:%zu.",
        (int)redeclared_identifier_token->length,
        redeclared_identifier_token->start,
        original_identifier_token->module->src_path.data,
        original_identifier_token->line,
        original_identifier_token->column);
}

stbool semantic_analysis_identifier(
    struct semantic_analysis_context* ctx, struct identifier* identifier)
{
    nassert(NULL != identifier->srctok);

    SUPPRESS_UNUSED(identifier);
    SUPPRESS_UNUSED(ctx);

    // Identifiers are basically plain old data, so there is never a time that
    // they will fail semantic analysis since nothing needs actual analyzing.
    // A status Boolean will still be returned just to keep this function of the
    // same form as all other functions within semantic analysis, it just so
    // happens that that Boolean is always the success condition.
    return STBOOL_SUCCESS;
}

//! Resolve a data type that was parsed from an identifier.
//! @param ctx
//!     Current semantic analysis context.
//! @param data_type
//!     Data type to be resolved.
//! @return
//!     #STBOOL_SUCCESS on success.
//! @return
//!     #STBOOL_FAILURE on failure.
static stbool resolve_data_type_identifier(
    struct semantic_analysis_context* ctx, struct data_type* data_type)
{
    nassert(NULL != data_type->srctok);
    nassert(NULL == data_type->inner);
    nassert(DT_UNRESOLVED_ID == data_type->tag);

    struct identifier const* const type_id = &data_type->unresolved.id;
    struct symbol* const type_symbol = symbol_table_find(
        ctx->module->global_scope.symbol_table,
        type_id->start,
        type_id->length);

    if (NULL == type_symbol)
    {
        NLOGF_FLC_TOK(
            LOG_ERROR,
            data_type->srctok,
            "No data type matches identifier '%.*s'.",
            (int)type_id->length,
            type_id->start);
        return STBOOL_FAILURE;
    }
    else if (SYMBOL_VARIABLE == type_symbol->tag)
    {
        NLOGF_FLC_TOK(
            LOG_ERROR,
            data_type->srctok,
            "Illegal use of variable '%.*s' as a data type.",
            (int)type_symbol->srctok->length,
            type_symbol->srctok->start);
        return STBOOL_FAILURE;
    }
    else if (SYMBOL_FUNCTION == type_symbol->tag)
    {
        NLOGF_FLC_TOK(
            LOG_ERROR,
            data_type->srctok,
            "Illegal use of function '%.*s' as a data type.",
            (int)type_symbol->srctok->length,
            type_symbol->srctok->start);
        return STBOOL_FAILURE;
    }
    else if (SYMBOL_STRUCT == type_symbol->tag ||
        SYMBOL_VARIANT == type_symbol->tag ||
        SYMBOL_ALIAS == type_symbol->tag)
    {
        nassert(NULL != type_symbol->dt);
        nassert(DT_UNSET != type_symbol->dt->tag);

        // Save the source token of this data type.
        // After resolving the data type we still want the data type to say it
        // was parsed from the token that it was resolved from.
        struct token const* const srctok_save = data_type->srctok;
        // Qualifiers on the data type as it exists before being transformed.
        // Saved here so it can be merged with qualifiers on the resolved type.
        struct qualifiers const existing_qualifiers = data_type->qualifiers;
        // Resolve the actual data type via assignment.
        data_type_assign(data_type, type_symbol->dt, true);
        // Merge qualifiers.
        qualifers_union(
            &data_type->qualifiers,
            &data_type->qualifiers,
            &existing_qualifiers);
        // Restore the source token of the data type.
        data_type->srctok = srctok_save;

        return STBOOL_SUCCESS;
    }
    NPANIC_UNEXPECTED_CTRL_FLOW();
}

//! Resolve a data type that was parsed from the `typeof` an expression.
//! @param ctx
//!     Current semantic analysis context.
//! @param data_type
//!     Data type to be resolved.
//! @return
//!     #STBOOL_SUCCESS on success.
//! @return
//!     #STBOOL_FAILURE on failure.
static stbool resolve_data_type_typeof_expr(
    struct semantic_analysis_context* ctx, struct data_type* data_type)
{
    nassert(NULL != data_type->srctok);
    nassert(NULL == data_type->inner);
    nassert(DT_UNRESOLVED_TYPEOF_EXPR == data_type->tag);

    // Perform semantic analysis on the typeof expression.
    // If the expression itself is messed up then obviously we can't parse its
    // type correctly so we'll just bail.
    // An error message describing the problem _should_ have been printed during
    // the semantic analysis of the expression, so there is no reason to print
    // another one here.
    if (!semantic_analysis_expr(ctx, data_type->unresolved.typeof_expr))
    {
        return STBOOL_FAILURE;
    }

    // Save the source token of this data type.
    // After resolving the data type we still want the data type to say it
    // was parsed from the token that it was resolved from.
    struct token const* const srctok_save = data_type->srctok;
    // Qualifiers on the data type as it exists before being transformed.
    // Saved here so it can be merged with qualifiers on the resolved type.
    struct qualifiers const existing_qualifiers = data_type->qualifiers;

    // Use a separate data type so the expression doesn't get finalized before
    // it can be used.
    struct data_type ATTR_DEFER_FINI(data_type_fini) tmp_data_type;
    data_type_init(&tmp_data_type);

    // Grab the type of the expression and stick it on the data type being
    // analyzed.
    data_type_assign(
        &tmp_data_type, &data_type->unresolved.typeof_expr->dt, true);
    data_type_reset(data_type);
    data_type_assign(data_type, &tmp_data_type, true);
    // Merge qualifiers.
    qualifers_union(
        &data_type->qualifiers, &data_type->qualifiers, &existing_qualifiers);
    // Restore the source token of the data type.
    data_type->srctok = srctok_save;

    return STBOOL_SUCCESS;
}

//! Resolve a data type that was parsed from the `typeof` a data type.
//! @param ctx
//!     Current semantic analysis context.
//! @param data_type
//!     Data type to be resolved.
//! @return
//!     #STBOOL_SUCCESS on success.
//! @return
//!     #STBOOL_FAILURE on failure.
static stbool resolve_data_type_typeof_data_type(
    struct semantic_analysis_context* ctx, struct data_type* data_type)
{
    nassert(NULL != data_type->srctok);
    nassert(NULL == data_type->inner);
    nassert(DT_UNRESOLVED_TYPEOF_DT == data_type->tag);

    // Perform semantic analysis on the typeof data type.
    // If the data type itself is messed up then we're screwed so we'll just
    // bail.
    // An error message describing the problem _should_ have been printed during
    // the semantic analysis of the data type, so there is no reason to print
    // another one here.
    if (!semantic_analysis_data_type(ctx, data_type->unresolved.typeof_dt))
    {
        return STBOOL_FAILURE;
    }

    // Save the source token of this data type.
    // After resolving the data type we still want the data type to say it
    // was parsed from the token that it was resolved from.
    struct token const* const srctok_save = data_type->srctok;
    // Qualifiers on the data type as it exists before being transformed.
    // Saved here so it can be merged with qualifiers on the resolved type.
    struct qualifiers const existing_qualifiers = data_type->qualifiers;

    // Use a separate data type so the expression doesn't get finalized before
    // it can be used.
    struct data_type ATTR_DEFER_FINI(data_type_fini) tmp_data_type;
    data_type_init(&tmp_data_type);

    // Grab the type of the typeofed data type and stick it on the data type
    // being analyzed.
    data_type_assign(&tmp_data_type, data_type->unresolved.typeof_dt, true);
    data_type_reset(data_type);
    data_type_assign(data_type, &tmp_data_type, true);
    // Merge qualifiers.
    qualifers_union(
        &data_type->qualifiers, &data_type->qualifiers, &existing_qualifiers);
    // Restore the source token of the data type.
    data_type->srctok = srctok_save;

    return STBOOL_SUCCESS;
}

#define DATA_TYPE_ALREADY_FULLY_FORMED 1
#define DATA_TYPE_FULLY_FORMED_FROM_IDENTIFIER 2
#define DATA_TYPE_FULLY_FORMED_FROM_TYPEOF 3
#define FAILED_TO_FULLY_FORM_DATA_TYPE 0
//! Resolve a data type that may have an unresolved component.
//! @param ctx
//!     Current semantic analysis context.
//! @param data_type
//!     Data type to be resolved.
//! @return
//!     #DATA_TYPE_ALREADY_FULLY_FORMED if the data type needed no additional
//!     work to be fully formed into a valid data type.
//! @return
//!     #DATA_TYPE_FULLY_FORMED_FROM_IDENTIFIER if the data type was formed from
//!     an identifier.
//! @return
//!     #DATA_TYPE_FULLY_FORMED_FROM_TYPEOF if `typeof` was used to form this
//!     data type.
//!     In this case, the `typeof`ed data type is known to be validated.
//! @return
//!     #FAILED_TO_FULLY_FORM_DATA_TYPE on failure.
static int fully_form_data_type(
    struct semantic_analysis_context* ctx, struct data_type* data_type)
{
    nassert(NULL != data_type->srctok);

    // Find the base type.
    struct data_type* base = data_type;
    while (!data_type_is_base(base))
    {
        base = base->inner;
    }

    // Check if the base type needs to be fully formed and if so take
    // appropriate action.
    if (DT_UNRESOLVED_ID == base->tag)
    {
        return resolve_data_type_identifier(ctx, base)
            ? DATA_TYPE_FULLY_FORMED_FROM_IDENTIFIER
            : FAILED_TO_FULLY_FORM_DATA_TYPE;
    }
    else if (DT_UNRESOLVED_TYPEOF_EXPR == base->tag)
    {
        return resolve_data_type_typeof_expr(ctx, data_type)
            ? DATA_TYPE_FULLY_FORMED_FROM_TYPEOF
            : FAILED_TO_FULLY_FORM_DATA_TYPE;
    }
    else if (DT_UNRESOLVED_TYPEOF_DT == data_type->tag)
    {
        return resolve_data_type_typeof_data_type(ctx, data_type)
            ? DATA_TYPE_FULLY_FORMED_FROM_TYPEOF
            : FAILED_TO_FULLY_FORM_DATA_TYPE;
    }

    return DATA_TYPE_ALREADY_FULLY_FORMED;
}

//! Check if a @p data_type has a function base type and if so, validate that
//! function signature.
//! @param ctx
//!     Current semantic analysis context.
//! @param data_type
//!     Data type to be validated.
//! @return
//!     #STBOOL_SUCCESS on success.
//! @return
//!     #STBOOL_FAILURE on failure.
static stbool validate_data_type_function_signature(
    struct semantic_analysis_context* ctx, struct data_type* data_type)
{
    nassert(NULL != data_type->srctok);

    if (NULL != data_type->inner
        && !validate_data_type_function_signature(ctx, data_type->inner))
    {
        return STBOOL_FAILURE;
    }

    if (data_type_is_function(data_type))
    {
        return semantic_analysis_func_sig(ctx, data_type->func_sig);
    }

    return STBOOL_SUCCESS;
}

//! Check all array modifiers of @p data_type are semantically valid.
//! @param ctx
//!     Current semantic analysis context.
//! @param data_type
//!     Data type to be validated.
//! @return
//!     #STBOOL_SUCCESS on success.
//! @return
//!     #STBOOL_FAILURE on failure.
static stbool validate_data_type_array_modifiers(
    struct semantic_analysis_context* ctx, struct data_type* data_type)
{
    nassert(NULL != data_type->srctok);

    stbool status = STBOOL_SUCCESS;

    if (NULL != data_type->inner
        && !validate_data_type_array_modifiers(ctx, data_type->inner))
    {
        status = STBOOL_FAILURE;
    }

    if (data_type_is_array(data_type))
    {
        if (0 == data_type->array.length)
        {
            NLOGF_FLC_TOK(
                LOG_ERROR,
                data_type->srctok,
                "Array may not have length zero.");
            status = STBOOL_FAILURE;
        }
        if (!qualifiers_is_default(&data_type->qualifiers))
        {
            NLOGF_FLC_TOK(
                LOG_ERROR,
                data_type->srctok,
                "Illegal use of qualifiers on an array type.");
            status = STBOOL_FAILURE;
        }
    }

    return status;
}

//! If the data type is composite it may be the case that the type has been
//! declared, but not defined.
//! If so, the data type cannot be used if knowledge of the data type's size
//! is required.
//! @param data_type
//!     Data type to be validated.
//! @return
//!     #STBOOL_SUCCESS on success.
//! @return
//!     #STBOOL_FAILURE on failure.
static stbool validate_data_type_sizeof_knowledge(struct data_type* data_type)
{
    nassert(NULL != data_type->srctok);

    bool requires_sizeof_knowledge = true;
    struct data_type const* cur_data_type = data_type;
    while (NULL != cur_data_type)
    {
        if (data_type_is_primitive(cur_data_type)
            || data_type_is_pointer(cur_data_type)
            || data_type_is_function(cur_data_type)
            || (data_type_is_struct(cur_data_type)
                && (NULL != cur_data_type->struct_ref->def))
            || (data_type_is_variant(cur_data_type)
                && (NULL != cur_data_type->variant_ref->def)))
        {
            requires_sizeof_knowledge = false;
            break;
        }
        cur_data_type = cur_data_type->inner;
    }
    if (requires_sizeof_knowledge)
    {
        NLOGF_FLC_TOK(
            LOG_ERROR,
            data_type->srctok,
            "Use of undefined composite type requires sizeof knowledge.");
        return STBOOL_FAILURE;
    }

    return STBOOL_SUCCESS;
}

stbool semantic_analysis_data_type(
    struct semantic_analysis_context* ctx, struct data_type* data_type)
{
    nassert(NULL != data_type->srctok);

    int const fully_form_status = fully_form_data_type(ctx, data_type);
    if (FAILED_TO_FULLY_FORM_DATA_TYPE == fully_form_status)
    {
        return STBOOL_FAILURE;
    }
    else if (DATA_TYPE_FULLY_FORMED_FROM_TYPEOF == fully_form_status)
    {
        // `typeof` was used to form this data type.
        // In this case, the `typeof`ed data type is known to be validated.
        return STBOOL_SUCCESS;
    }

    return validate_data_type_function_signature(ctx, data_type)
        && validate_data_type_array_modifiers(ctx, data_type)
        && validate_data_type_sizeof_knowledge(data_type);
}

stbool semantic_analysis_member_var(
    struct semantic_analysis_context* ctx, struct member_var* member_var)
{
    nassert(NULL != member_var->srctok);
    nassert(scope_is_global(ctx->curr_scope));

    return semantic_analysis_identifier(ctx, &member_var->id)
        && semantic_analysis_data_type(ctx, &member_var->dt);
}

stbool semantic_analysis_member_func(
    struct semantic_analysis_context* ctx, struct member_func* member_func)
{
    nassert(NULL != member_func->srctok);
    nassert(scope_is_global(ctx->curr_scope));

    struct identifier* const member_id = &member_func->id;
    struct identifier* const target_id = &member_func->target.id;

    if (!semantic_analysis_identifier(ctx, member_id)
        || !semantic_analysis_identifier(ctx, target_id))
    {
        return STBOOL_FAILURE;
    }

    struct symbol* target_symbol = symbol_table_find(
        ctx->curr_scope->symbol_table, target_id->start, target_id->length);
    if (NULL == target_symbol)
    {
        NLOGF_FLC_TOK(
            LOG_ERROR,
            member_func->srctok,
            "Member function '%.*s' references invalid target.",
            (int)target_id->length,
            target_id->start);
        return STBOOL_FAILURE;
    }
    if (SYMBOL_FUNCTION != target_symbol->tag)
    {
        NLOGF_FLC_TOK(
            LOG_ERROR,
            member_func->srctok,
            "Member function '%.*s' references a non-function target.",
            (int)target_id->length,
            target_id->start);
        return STBOOL_FAILURE;
    }

    data_type_assign(&member_func->dt, target_symbol->dt, true);

    return STBOOL_SUCCESS;
}

stbool semantic_analysis_member_tag(
    struct semantic_analysis_context* ctx, struct member_tag* member_tag)
{
    nassert(scope_is_global(ctx->curr_scope));

    return semantic_analysis_identifier(ctx, &member_tag->id) &&
        semantic_analysis_literal(ctx, &member_tag->value) &&
        semantic_analysis_struct_def(ctx, &member_tag->section);
}

stbool semantic_analysis_member_iface(
    struct semantic_analysis_context* ctx, struct member_iface* member_iface)
{
    nassert(scope_is_global(ctx->curr_scope));

    return semantic_analysis_identifier(ctx, &member_iface->id) &&
        semantic_analysis_func_sig(ctx, &member_iface->sig);
}

//! Validate that the provided struct definition contains at least one member
//! variable.
//! The behavior of a stucture without data members is implementation defined in
//! C, so to avoid implementation defined oddities empty structs are not allowed
//! in N.
//! @param struct_def
//!     Struct definition to be validated.
//! @return
//!     #STBOOL_SUCCESS on success.
//! @return
//!     #STBOOL_FAILURE on failure.
static inline stbool validate_struct_def_has_at_least_one_member_variable(
    struct struct_def* struct_def)
{
    nassert(NULL != struct_def->srctok);

    size_t const member_vars_length = narr_length(struct_def->member_vars);
    if (0 == member_vars_length)
    {
        NLOGF_FLC_TOK(
            LOG_ERROR, struct_def->srctok, "Struct has no member variables.");
        return STBOOL_FAILURE;
    }
    return STBOOL_SUCCESS;
}

//! Validate that the identifiers of members within the provided struct
//! definition are all unique.
//! @param struct_def
//!     Struct definition to be validated.
//! @return
//!     #STBOOL_SUCCESS on success.
//! @return
//!     #STBOOL_FAILURE on failure.
static inline stbool validate_struct_def_has_unique_members(
    struct struct_def* struct_def)
{
    nassert(NULL != struct_def->srctok);

    static char const* const FMT_DUPLICATE_STRUCT_ID =
        "Duplicate identifier '%.*s' in struct.";

    size_t const member_vars_length = narr_length(struct_def->member_vars);
    size_t const member_funcs_length = narr_length(struct_def->member_funcs);

    // Each member is compared against all other members that it has not yet
    // been compared against.
    for (size_t i = 0; i < member_vars_length; ++i)
    {
        // Compare against member variables.
        for (size_t j = i + 1; j < member_vars_length; ++j)
        {
            if (identifier_eq(
                    &struct_def->member_vars[i].id,
                    &struct_def->member_vars[j].id))
            {
                NLOGF_FLC_TOK(
                    LOG_ERROR,
                    struct_def->member_vars[j].srctok,
                    FMT_DUPLICATE_STRUCT_ID,
                    (int)(struct_def->member_vars[j].id.length),
                    struct_def->member_vars[j].id.start);
                return STBOOL_FAILURE;
            }
        }
        // Compare against member functions.
        for (size_t j = 0; j < member_funcs_length; ++j)
        {
            if (identifier_eq(
                    &struct_def->member_vars[i].id,
                    &struct_def->member_funcs[j].id))
            {
                NLOGF_FLC_TOK(
                    LOG_ERROR,
                    struct_def->member_vars[j].srctok,
                    FMT_DUPLICATE_STRUCT_ID,
                    (int)(struct_def->member_vars[j].id.length),
                    struct_def->member_vars[j].id.start);
                return STBOOL_FAILURE;
            }
        }
    }
    for (size_t i = 0; i < member_funcs_length; ++i)
    {
        // Compare against member functions.
        for (size_t j = i + 1; j < member_funcs_length; ++j)
        {
            if (identifier_eq(
                    &struct_def->member_funcs[i].id,
                    &struct_def->member_funcs[j].id))
            {
                NLOGF_FLC_TOK(
                    LOG_ERROR,
                    struct_def->member_funcs[j].srctok,
                    FMT_DUPLICATE_STRUCT_ID,
                    (int)(struct_def->member_funcs[j].id.length),
                    struct_def->member_funcs[j].id.start);
                return STBOOL_FAILURE;
            }
        }
    }

    return STBOOL_SUCCESS;
}

//! Perform semantic analysis over all member variables of the provided struct.
//! @param struct_def
//!     Struct definition being analyzed.
//! @return
//!     #STBOOL_SUCCESS on success.
//! @return
//!     #STBOOL_FAILURE on failure.
static inline stbool semantic_analysis_struct_def_member_variables(
    struct semantic_analysis_context* ctx, struct struct_def* struct_def)
{
    nassert(NULL != struct_def->srctok);

    stbool status = STBOOL_SUCCESS;

    size_t const member_vars_length = narr_length(struct_def->member_vars);
    for (size_t i = 0; i < member_vars_length; ++i)
    {
        if (!semantic_analysis_member_var(ctx, &struct_def->member_vars[i]))
        {
            status = STBOOL_FAILURE;
        }
    }

    return status;
}

//! Perform semantic analysis over all member functions of the provided struct.
//! @param struct_def
//!     Struct definition being analyzed.
//! @return
//!     #STBOOL_SUCCESS on success.
//! @return
//!     #STBOOL_FAILURE on failure.
static inline stbool semantic_analysis_struct_def_member_functions(
    struct semantic_analysis_context* ctx, struct struct_def* struct_def)
{
    nassert(NULL != struct_def->srctok);

    stbool status = STBOOL_SUCCESS;

    const size_t member_funcs_length = narr_length(struct_def->member_funcs);
    for (size_t i = 0; i < member_funcs_length; ++i)
    {
        if (!semantic_analysis_member_func(ctx, &struct_def->member_funcs[i]))
        {
            status = STBOOL_FAILURE;
        }
    }

    return status;
}

stbool semantic_analysis_struct_def(
    struct semantic_analysis_context* ctx, struct struct_def* struct_def)
{
    nassert(NULL != struct_def->srctok);

    return validate_struct_def_has_at_least_one_member_variable(struct_def)
        && validate_struct_def_has_unique_members(struct_def)
        && semantic_analysis_struct_def_member_variables(ctx, struct_def)
        && semantic_analysis_struct_def_member_functions(ctx, struct_def);
}

stbool semantic_analysis_variant_def(
    struct semantic_analysis_context* ctx, struct variant_def* variant)
{
    const size_t n_attrs  = narr_length(variant->member_vars);
    const size_t n_funcs  = narr_length(variant->member_funcs);
    const size_t n_ifaces = narr_length(variant->member_ifaces);

    // This loop takes duplicated tags and moves all members from the latter
    // tag's section, and moves it to the previous tag's section.
    size_t n_dup_tags = narr_length(variant->member_tags);
    for(size_t i = 0; i < n_dup_tags; ++i)
    {
        // search the remaining tags for duplicated ones
        for(size_t j = i + 1; j < n_dup_tags; ++j)
        {
            if(identifier_eq(&variant->member_tags[i].id,
                &variant->member_tags[j].id))
            {
                // the first duplicate which gets kept and added to.
                struct struct_def* keep  = &variant->member_tags[i].section;
                // the second duplicate which gets merged into the first, then
                // thrown away.
                struct struct_def* throw = &variant->member_tags[j].section;

                // only the 1st tag may set the numerical value.
                // later on, tags will get their correct value, based on
                // incrementing the previous tags value.
                if(variant->member_tags[j].value.tag != LITERAL_UNSET)
                {
                    NLOGF_FLC_TOK(LOG_DEBUG,
                        variant->member_tags[j].value.srctok,
                        "Redefinition of tag value");
                }

                // copy all the section member variables into the original tag.
                while(narr_length(throw->member_vars) > 0)
                {
                    keep->member_vars = narr_push( keep->member_vars,
                        &throw->member_vars[0]);
                    narr_shift(throw->member_vars);
                }

                // copy all the section member functions into the original tag.
                while(narr_length(throw->member_funcs) > 0)
                {
                    keep->member_funcs = narr_push(keep->member_funcs,
                        &throw->member_funcs[0]);
                    narr_shift(throw->member_funcs);
                }

                // finish the duplicate tag, and then remove it from the
                // list of tags.
                member_tag_fini(&variant->member_tags[j]);

                for(size_t k = j; k < n_dup_tags - 1; ++k)
                {
                    variant->member_tags[k] = variant->member_tags[k+1];
                }
                narr_pop(variant->member_tags);
                n_dup_tags--;
            }
        }
    }

    size_t const n_tags   = narr_length(variant->member_tags);

    // analyze all the common section member variables
    for(size_t i = 0; i < n_attrs; ++i)
    {
        if(!semantic_analysis_member_var(ctx, &variant->member_vars[i]))
        {
            return STBOOL_FAILURE;
        }
    }

    // analyze all the common section member functions
    for(size_t i = 0; i < n_funcs; ++i)
    {
        if(!semantic_analysis_member_func(ctx, &variant->member_funcs[i]))
        {
            return STBOOL_FAILURE;
        }
    }

    // analyze all the tags 
    for(size_t i = 0; i < n_tags; ++i)
    {
        if(!semantic_analysis_member_tag(ctx, &variant->member_tags[i]))
        {
            return STBOOL_FAILURE;
        }

        // also check that the tag's value has the same type as the variant
        // tag type, or that it is unset (will be set later on)
        struct data_type tag_dt =
            literal_get_data_type(&variant->member_tags[i].value);
        if(!(variant->tag_dt.tag == tag_dt.tag || tag_dt.tag == DT_UNSET))
        {
            NLOGF_FLC_TOK(LOG_ERROR, variant->member_tags[i].value.srctok,
                "Tag literal is different from variant tag type");
            data_type_fini(&tag_dt);
            return STBOOL_FAILURE;
        }
        data_type_fini(&tag_dt);
    }

    // analyze all the ifaces.
    for(size_t i = 0; i < n_ifaces; ++i)
    {
        if(!semantic_analysis_member_iface(ctx, &variant->member_ifaces[i]))
        {
            return STBOOL_FAILURE;
        }
    }

    static char const * const FMT_VARIANT_DUP_ID =
        "Duplicate identifier \"%.*s\" in variant.";

    // check duplicate identifiers with variables against everything.
    for(size_t i = 0; i < n_attrs; ++i)
    {
        // duplicates where both are variables
        for(size_t j = i + 1; j < n_attrs; ++j)
        {
            if(identifier_eq(&variant->member_vars[i].id,
                &variant->member_vars[j].id))
            {
                NLOGF_FLC_TOK(LOG_ERROR,
                    variant->member_vars[j].srctok,
                    FMT_VARIANT_DUP_ID,
                    (int)(variant->member_vars[j].id.length),
                    variant->member_vars[j].id.start);
                return STBOOL_FAILURE;
            }
        }

        //duplicates where one is a variable and one is a function
        for(size_t j = 0; j < n_funcs; ++j)
        {
            if(identifier_eq(&variant->member_vars[i].id,
                &variant->member_funcs[j].id))
            {
                NLOGF_FLC_TOK(LOG_ERROR,
                    variant->member_funcs[j].srctok,
                    FMT_VARIANT_DUP_ID,
                    (int)(variant->member_funcs[j].id.length),
                    variant->member_funcs[j].id.start);
                return STBOOL_FAILURE;
            }
        }

        // duplicates where one is a variable and one is an iface
        for(size_t j = 0; j < n_ifaces; ++j)
        {
            if(identifier_eq(&variant->member_vars[i].id,
                &variant->member_ifaces[j].id))
            {
                NLOGF_FLC_TOK(LOG_ERROR,
                    variant->member_ifaces[j].srctok,
                    FMT_VARIANT_DUP_ID,
                    (int)(variant->member_ifaces[j].id.length),
                    variant->member_ifaces[j].id.start);
                return STBOOL_FAILURE;
            }
        }

        // duplicates where one is a variable and one is a tag.
        for(size_t j = 0; j < n_tags; ++j)
        {
            if(identifier_eq(&variant->member_vars[i].id,
                &variant->member_tags[j].id))
            {
                NLOGF_FLC_TOK(LOG_ERROR,
                    variant->member_tags[j].srctok,
                    FMT_VARIANT_DUP_ID,
                    (int)(variant->member_tags[j].id.length),
                    variant->member_tags[j].id.start);
                return STBOOL_FAILURE;
            }
        }
    }

    // check for duplicate functions.
    for(size_t i = 0; i < n_funcs; ++i)
    {
        // duplicates are both functions.
        for(size_t j = i + 1; j < n_funcs; ++j)
        {
            if(identifier_eq(&variant->member_funcs[i].id,
                &variant->member_funcs[j].id))
            {
                NLOGF_FLC_TOK(LOG_ERROR,
                    variant->member_funcs[j].srctok,
                    FMT_VARIANT_DUP_ID,
                    (int)(variant->member_funcs[j].id.length),
                    variant->member_funcs[j].id.start);
                return STBOOL_FAILURE;
            }
        }

        // one is a function and the other an iface.
        for(size_t j = 0; j < n_ifaces; ++j)
        {
            if(identifier_eq(&variant->member_funcs[i].id,
                &variant->member_ifaces[j].id))
            {
                NLOGF_FLC_TOK(LOG_ERROR,
                    variant->member_ifaces[j].srctok,
                    FMT_VARIANT_DUP_ID,
                    (int)(variant->member_ifaces[j].id.length),
                    variant->member_ifaces[j].id.start);
                return STBOOL_FAILURE;
            }
        }

        // one is a function and the other is a tag.
        for(size_t j = 0; j < n_tags; ++j)
        {
            if(identifier_eq(&variant->member_funcs[i].id,
                &variant->member_tags[j].id))
            {
                NLOGF_FLC_TOK(LOG_ERROR,
                    variant->member_tags[j].srctok,
                    FMT_VARIANT_DUP_ID,
                    (int)(variant->member_tags[j].id.length),
                    variant->member_tags[j].id.start);
                return STBOOL_FAILURE;
            }
        }
    }

    // check for duplicate ifaces.
    for(size_t i = 0; i < n_ifaces; ++i)
    {
        // duplicate ifaces.
        for(size_t j = i + 1; j < n_ifaces; ++j)
        {
            if(identifier_eq(&variant->member_ifaces[i].id,
                &variant->member_ifaces[j].id))
            {
                NLOGF_FLC_TOK(LOG_ERROR,
                    variant->member_ifaces[j].srctok,
                    FMT_VARIANT_DUP_ID,
                    (int)(variant->member_ifaces[j].id.length),
                    variant->member_ifaces[j].id.start);
                return STBOOL_FAILURE;
            }
        }

        // duplicate where one is an iface and the other a tag.
        for(size_t j = 0; j < n_tags; ++j)
        {
            if(identifier_eq(&variant->member_ifaces[i].id,
                &variant->member_tags[j].id))
            {
                NLOGF_FLC_TOK(LOG_ERROR,
                    variant->member_tags[j].srctok,
                    FMT_VARIANT_DUP_ID,
                    (int)(variant->member_tags[j].id.length),
                    variant->member_tags[j].id.start);
                return STBOOL_FAILURE;
            }
        }
    }

    // note. duplicate tags were merged earlier, so we aren't gonna check here.

    // here we're assigning the tag type if it was unset.
    if(variant->tag_dt.tag == DT_UNSET)
    {
        //TODO: better tag_dt choosing algorithm.
        variant->tag_dt.tag = DT_U16;
    }

    // the tag type must be an integer type
    if(variant->tag_dt.tag == DT_VOID || variant->tag_dt.tag > DT_S)
    {
        NLOGF_FLC_TOK(LOG_ERROR, variant->tag_dt.srctok,
            "Variant tag type must be of integer type.");
    }

    semantic_analysis_data_type(ctx, &variant->tag_dt);

    // now we assign incrementing tag numbers to un-assigned tags.
    switch(variant->tag_dt.tag)
    {
    case DT_U8:
    {
        n_u8 next_tag = 0;
        for(size_t i = 0; i < n_tags; ++i)
        {
            if(variant->member_tags[i].value.tag == LITERAL_UNSET)
            {
                variant->member_tags[i].value.tag = LITERAL_U8;
                variant->member_tags[i].value.u8 = next_tag;
            }
            else
            {
                next_tag = variant->member_tags[i].value.u8;
            }
            ++next_tag;

            // tag numbers may not be duplicated.
            for(size_t j = 0; j < i; ++j)
            {
                if(variant->member_tags[i].value.u8 ==
                    variant->member_tags[j].value.u8)
                {
                    NLOGF_FLC_TOK(LOG_ERROR,
                        variant->member_tags[i].value.srctok,
                        "Reusing previously used tag value: %hhuu8.",
                        variant->member_tags[i].value.u8);
                    return STBOOL_FAILURE;
                }
            }
        }
        break;
    }
    case DT_U16:
    {
        n_u16 next_tag = 0;
        for(size_t i = 0; i < n_tags; ++i)
        {
            if(variant->member_tags[i].value.tag == LITERAL_UNSET)
            {
                variant->member_tags[i].value.tag = LITERAL_U16;
                variant->member_tags[i].value.u16 = next_tag;
            }
            else
            {
                next_tag = variant->member_tags[i].value.u16;
            }
            ++next_tag;

            // tag numbers may not be duplicated.
            for(size_t j = 0; j < i; ++j)
            {
                if(variant->member_tags[i].value.u16 ==
                    variant->member_tags[j].value.u16)
                {
                    NLOGF_FLC_TOK(LOG_ERROR,
                        variant->member_tags[i].value.srctok,
                        "Reusing previously used tag value: %huu16.",
                        variant->member_tags[i].value.u16);
                    return STBOOL_FAILURE;
                }
            }
        }
        break;
    }
    case DT_U32:
    {
        n_u32 next_tag = 0;
        for(size_t i = 0; i < n_tags; ++i)
        {
            if(variant->member_tags[i].value.tag == LITERAL_UNSET)
            {
                variant->member_tags[i].value.tag = LITERAL_U32;
                variant->member_tags[i].value.u32 = next_tag;
            }
            else
            {
                next_tag = variant->member_tags[i].value.u32;
            }
            ++next_tag;

            // tag numbers may not be duplicated.
            for(size_t j = 0; j < i; ++j)
            {
                if(variant->member_tags[i].value.u32 ==
                    variant->member_tags[j].value.u32)
                {
                    NLOGF_FLC_TOK(LOG_ERROR,
                        variant->member_tags[i].value.srctok,
                        "Reusing previously used tag value: %uu32.",
                        variant->member_tags[i].value.u32);
                    return STBOOL_FAILURE;
                }
            }
        }
        break;
    }
    case DT_U64:
    {
        n_u64 next_tag = 0;
        for(size_t i = 0; i < n_tags; ++i)
        {
            if(variant->member_tags[i].value.tag == LITERAL_UNSET)
            {
                variant->member_tags[i].value.tag = LITERAL_U64;
                variant->member_tags[i].value.u64 = next_tag;
            }
            else
            {
                next_tag = variant->member_tags[i].value.u64;
            }
            ++next_tag;

            // tag numbers may not be duplicated.
            for(size_t j = 0; j < i; ++j)
            {
                if(variant->member_tags[i].value.u64 ==
                    variant->member_tags[j].value.u64)
                {
                    NLOGF_FLC_TOK(LOG_ERROR,
                        variant->member_tags[i].value.srctok,
                        "Reusing previously used tag value: %lluu64.",
                        (unsigned long long)variant->member_tags[i].value.u64);
                    return STBOOL_FAILURE;
                }
            }
        }
        break;
    }
    case DT_U:
    {
        n_u next_tag = 0;
        for(size_t i = 0; i < n_tags; ++i)
        {
            if(variant->member_tags[i].value.tag == LITERAL_UNSET)
            {
                variant->member_tags[i].value.tag = LITERAL_U;
                variant->member_tags[i].value.u = next_tag;
            }
            else
            {
                next_tag = variant->member_tags[i].value.u;
            }
            ++next_tag;

            // tag numbers may not be duplicated.
            for(size_t j = 0; j < i; ++j)
            {
                if(variant->member_tags[i].value.u ==
                    variant->member_tags[j].value.u)
                {
                    NLOGF_FLC_TOK(LOG_ERROR,
                        variant->member_tags[i].value.srctok,
                        "Reusing previously used tag value: %lluu.",
                        (unsigned long long)variant->member_tags[i].value.u);
                    return STBOOL_FAILURE;
                }
            }
        }
        break;
    }
    case DT_S8:
    {
        n_s8 next_tag = 0;
        for(size_t i = 0; i < n_tags; ++i)
        {
            if(variant->member_tags[i].value.tag == LITERAL_UNSET)
            {
                variant->member_tags[i].value.tag = LITERAL_S8;
                variant->member_tags[i].value.s8 = next_tag;
            }
            else
            {
                next_tag = variant->member_tags[i].value.s8;
            }
            ++next_tag;

            // tag numbers may not be duplicated.
            for(size_t j = 0; j < i; ++j)
            {
                if(variant->member_tags[i].value.s8 ==
                    variant->member_tags[j].value.s8)
                {
                    NLOGF_FLC_TOK(LOG_ERROR,
                        variant->member_tags[i].value.srctok,
                        "Reusing previously used tag value: %hhis8.",
                        variant->member_tags[i].value.s8);
                    return STBOOL_FAILURE;
                }
            }
        }
        break;
    }
    case DT_S16:
    {
        n_s16 next_tag = 0;
        for(size_t i = 0; i < n_tags; ++i)
        {
            if(variant->member_tags[i].value.tag == LITERAL_UNSET)
            {
                variant->member_tags[i].value.tag = LITERAL_S16;
                variant->member_tags[i].value.s16 = next_tag;
            }
            else
            {
                next_tag = variant->member_tags[i].value.s16;
            }
            ++next_tag;

            // tag numbers may not be duplicated.
            for(size_t j = 0; j < i; ++j)
            {
                if(variant->member_tags[i].value.s16 ==
                    variant->member_tags[j].value.s16)
                {
                    NLOGF_FLC_TOK(LOG_ERROR,
                        variant->member_tags[i].value.srctok,
                        "Reusing previously used tag value: %his16.",
                        variant->member_tags[i].value.s16);
                    return STBOOL_FAILURE;
                }
            }
        }
        break;
    }
    case DT_S32:
    {
        n_s32 next_tag = 0;
        for(size_t i = 0; i < n_tags; ++i)
        {
            if(variant->member_tags[i].value.tag == LITERAL_UNSET)
            {
                variant->member_tags[i].value.tag = LITERAL_S32;
                variant->member_tags[i].value.s32 = next_tag;
            }
            else
            {
                next_tag = variant->member_tags[i].value.s32;
            }
            ++next_tag;

            // tag numbers may not be duplicated.
            for(size_t j = 0; j < i; ++j)
            {
                if(variant->member_tags[i].value.s32 ==
                    variant->member_tags[j].value.s32)
                {
                    NLOGF_FLC_TOK(LOG_ERROR,
                        variant->member_tags[i].value.srctok,
                        "Reusing previously used tag value: %is32.",
                        variant->member_tags[i].value.s32);
                    return STBOOL_FAILURE;
                }
            }
        }
        break;
    }
    case DT_S64:
    {
        n_s64 next_tag = 0;
        for(size_t i = 0; i < n_tags; ++i)
        {
            if(variant->member_tags[i].value.tag == LITERAL_UNSET)
            {
                variant->member_tags[i].value.tag = LITERAL_S64;
                variant->member_tags[i].value.s64 = next_tag;
            }
            else
            {
                next_tag = variant->member_tags[i].value.s64;
            }
            ++next_tag;

            // tag numbers may not be duplicated.
            for(size_t j = 0; j < i; ++j)
            {
                if(variant->member_tags[i].value.s64 ==
                    variant->member_tags[j].value.s64)
                {
                    NLOGF_FLC_TOK(LOG_ERROR,
                        variant->member_tags[i].value.srctok,
                        "Reusing previously used tag value: %llis64.",
                        (long long)variant->member_tags[i].value.s64);
                    return STBOOL_FAILURE;
                }
            }
        }
        break;
    }
    case DT_S:
    {
        n_s next_tag = 0;
        for(size_t i = 0; i < n_tags; ++i)
        {
            if(variant->member_tags[i].value.tag == LITERAL_UNSET)
            {
                variant->member_tags[i].value.tag = LITERAL_S;
                variant->member_tags[i].value.s = next_tag;
            }
            else
            {
                next_tag = variant->member_tags[i].value.s;
            }
            ++next_tag;

            // tag numbers may not be duplicated.
            for(size_t j = 0; j < i; ++j)
            {
                if(variant->member_tags[i].value.s ==
                    variant->member_tags[j].value.s)
                {
                    NLOGF_FLC_TOK(LOG_ERROR,
                        variant->member_tags[i].value.srctok,
                        "Reusing previously used tag value: %llis.",
                        (long long)variant->member_tags[i].value.s);
                    return STBOOL_FAILURE;
                }
            }
        }
        break;
    }
    default:
    {
        NPANIC_DFLT_CASE();
    }
    }

    return STBOOL_SUCCESS;
}

stbool semantic_analysis_func_sig(
    struct semantic_analysis_context* ctx, struct func_sig* func_sig)
{
    stbool status = STBOOL_SUCCESS;

    // check the return type
    if (!semantic_analysis_data_type(ctx, &func_sig->return_dt))
    {
        status = STBOOL_FAILURE;
    }

    // and also check each of the param types.
    size_t const params_length = narr_length(func_sig->param_dts);
    for (size_t i = 0; i < params_length; ++i)
    {
        if (!semantic_analysis_data_type(ctx, &func_sig->param_dts[i]))
        {
            status = STBOOL_FAILURE;
        }
    }

    return status;
}

//! Perform semantic analysis on the provided array literal.
//! @param literal
//!     Array literal being analyzed.
//! @return
//!     #STBOOL_SUCCESS on success.
//! @return
//!     #STBOOL_FAILURE on failure.
static inline stbool semantic_analysis_literal_array(struct literal* literal)
{
    nassert(NULL != literal->srctok);
    nassert(LITERAL_ARRAY == literal->tag);

    size_t const array_length = narr_length(literal->array);

    if (0 == array_length)
    {
        NLOGF_FLC_TOK(
            LOG_ERROR,
            literal->srctok,
            "Array literal requires a non-zero number of elements.");
        return STBOOL_FAILURE;
    }

    if (LITERAL_ARRAY_FILL == literal->array[0].tag)
    {
        NLOGF_FLC_TOK(
            LOG_ERROR,
            literal->srctok,
            "Array literal requires at least one non-fill element.");
        return STBOOL_FAILURE;
    }

    // Verify that all members of the array share the same data type.
    struct data_type ATTR_DEFER_FINI(data_type_fini) first_dt =
        literal_get_data_type(&literal->array[0]);
    for (size_t i = 1; i < array_length; ++i)
    {
        if (LITERAL_ARRAY_FILL == literal->array[i].tag)
        {
            nassert(i == array_length - 1);
            if (i >= literal->array[i].array_fill_length)
            {
                NLOGF_FLC_TOK(
                    LOG_ERROR,
                    literal->array[i].srctok,
                    "Invalid array literal fill length.");
                return STBOOL_FAILURE;
            }
            break;
        }
        struct data_type ATTR_DEFER_FINI(data_type_fini) other_dt =
            literal_get_data_type(&literal->array[i]);
        if (!data_type_can_implicitly_convert(&other_dt, &first_dt))
        {
            DECLARE_DT_NSTR(first_dt_nstr, &first_dt);
            DECLARE_DT_NSTR(other_dt_nstr, &first_dt);
            NLOGF_FLC_TOK(
                LOG_ERROR,
                literal->array[i].srctok,
                "Mismatched data types '%s' and  '%s' in array literal.",
                first_dt_nstr.data,
                other_dt_nstr.data);
            return STBOOL_FAILURE;
        }
    }

    return STBOOL_SUCCESS;
}

stbool semantic_analysis_literal(
    struct semantic_analysis_context* ctx, struct literal* literal)
{
    nassert(NULL != literal->srctok);

    SUPPRESS_UNUSED(ctx);

    // At the time of writing this comment, the only literals that need any sort
    // of semantic analysis are array literals.
    // Currently literals are all parsed directly from a single token, except
    // for arrays, which are parsed as a collection of individual literals
    // within the array bracket syntax.
    // So if a literal is not an array, then it is already good to go, and a
    // success condition can be returned immediately.
    if (LITERAL_ARRAY == literal->tag)
    {
        return semantic_analysis_literal_array(literal);
    }

    return STBOOL_SUCCESS;
}

//! Perform semantic analysis on the identifier primary expression @p expr.
//! Identifier primary expressions are used in a number of contexts:
//! 1. Identifier representing a variable
//!     In an expression such as `foo + 3u`, `sizeof(foo)`, or `!foo(1u)` the
//!     identifier `foo` may represent a variable declared as `u`, a variable
//!     of some data type who's size may be taken, or a variable declared as
//!     `(u) -> bool` respectively.
//!     In short, if the variable was declared via `let foo : TYPE = ...` then
//!     the primary expression `foo` will resolve to that declared variable.
//! 2. Identifier representing a function
//!     In the case of an expression like `!foo(1u)`, the identifier `foo` may
//!     have been declared as a function via `func foo : () -> bool`.
//!     Just as with a identifier primary expression `foo` will resolve to the
//!     declared function.
//! 3. Identifier representing a type
//!     In the case of an expression like `sizeof(foo)`, it may be the case that
//!     `foo` corresponds to a declared struct, alias, etc, in which case the
//!     identifier `foo` does have semantic meaning as an expression, only that
//!     as an expression that resolves to the data type of `foo` rather than
//!     the value of `foo`.
//! Given that data type identifiers as expressions only has semantic meaning in
//! `sizeof` and `typeof` expressions and for variant or enum tag literals the
//!  parameter @p is_allowed_to_be_data_type is provided my the calling
//! function for these cases.
static inline stbool semantic_analysis_expr_primary_identifier(
    struct semantic_analysis_context* ctx,
    struct expr* expr,
    bool is_allowed_to_be_data_type)
{
    nassert(NULL != expr->srctok);
    nassert(EXPR_PRIMARY_IDENTIFIER == expr->tag);

    if (!semantic_analysis_identifier(ctx, &expr->id))
    {
        return STBOOL_FAILURE;
    }

    // Travel up the chain of scopes with symbol tables accessible to this
    // expression.
    // The first identifier found in one of these scopes is the identifier
    // that this identifier expression evaluates off of.
    // If the identifier is not found in any scope accessible to this
    // expression then the expression references an undeclared identifier
    // resulting in an error.
    struct scope* scope = ctx->curr_scope;
    struct symbol* symbol = NULL;
    do
    {
        struct identifier* id = &expr->id;
        symbol = symbol_table_find(scope->symbol_table, id->start, id->length);
        scope = (struct scope*)scope->parent;
    } while (NULL == symbol && scope != GLOBAL_SCOPE_PARENT_PTR);
    if (NULL == symbol)
    {
        NLOGF_FLC_TOK(
            LOG_ERROR,
            expr->srctok,
            "Use of undeclared identifier '%.*s' in an expression.",
            (int)expr->id.length,
            expr->id.start);
        return STBOOL_FAILURE;
    }

    // Assign the datatype of the expression.
    switch (symbol->tag)
    {
    case SYMBOL_VARIABLE:
    {
        data_type_assign(&expr->dt, symbol->dt, true);
        expr->valcat = VALCAT_LVALUE;
        // ISO/IEC 9899:TC3 - Section 6.6
        // Explicitly mentions:
        // + Arithmetic constant expressions.
        // + Null pointer constant.
        // + An address constant.
        // + An address constant for an object type plus or minus an integer
        // constant expression.
        // The standard does not mention const qualified variables as constant
        // expressions, apart from section 6.6.10:
        // _An implementation may accept other forms of of constant
        // expressions._
        // For translation into C99 or LLVM, we cannot rely on a const qualified
        // to be considered a constant expression.
        // Given observations using Clang and GCC show identifiers *not* being
        // considered constant expressions at the global scope, although the
        // address of such an identifier *is* still considered a constant
        // expression.
        //! @todo
        //!     Change to #true once we add constant expression evaluation of
        //!     non-mut variables.
        expr->is_constexpr = false;
    }
    break;

    case SYMBOL_FUNCTION:
    {
        data_type_assign(&expr->dt, symbol->dt, true);
        expr->valcat = VALCAT_RVALUE;
        // Declared functions in N translate to function pointers types with
        // compile time constant values in C satisfying the definition of an
        // address constant (see ISO/IEC 9899:TC3 - Section 6.6).
        expr->is_constexpr = true;
    }
    break;

    case SYMBOL_ENUM:    /* intentional fallthrough */
    case SYMBOL_UNION:   /* intentional fallthrough */
    case SYMBOL_STRUCT:  /* intentional fallthrough */
    case SYMBOL_VARIANT: /* intentional fallthrough */
    case SYMBOL_ALIAS:
    {
        if (!is_allowed_to_be_data_type)
        {
            nlogf_pretty(
                LOG_ERROR,
                expr->srctok->module->src,
                expr->srctok->module->src_path.data,
                expr->srctok->line,
                expr->srctok->column,
                "Illegal use of data type identifier '%.*s' in expression.",
                (int)expr->id.length,
                expr->id.start);
            return STBOOL_FAILURE;
        }
        // variants and enums may be expressions even though they are types,
        // because they can evaluate to tag literals.
        // Also structs and unions, in addition to enums and variants, may be
        // expressions which are part of `sizeof` and `typeof` expressions.
        data_type_assign(&expr->dt, symbol->dt, true);
        expr->valcat = VALCAT_NONE;
    }
    break;

    default:
        NPANIC_DFLT_CASE();
    }

    return STBOOL_SUCCESS;
}

static inline stbool semantic_analysis_expr_primary_literal(
    struct semantic_analysis_context* ctx, struct expr* expr)
{
    nassert(NULL != expr->srctok);
    nassert(EXPR_PRIMARY_LITERAL == expr->tag);

    if (!semantic_analysis_literal(ctx, expr->literal))
    {
        return STBOOL_FAILURE;
    }

    struct data_type ATTR_DEFER_FINI(data_type_fini) literal_data_type =
        literal_get_data_type(expr->literal);
    data_type_assign(&expr->dt, &literal_data_type, false);
    expr->valcat = VALCAT_RVALUE;
    expr->is_constexpr = true;

    return STBOOL_SUCCESS;
}

static inline stbool semantic_analysis_expr_primary_paren(
    struct semantic_analysis_context* ctx, struct expr* expr)
{
    nassert(NULL != expr->srctok);
    nassert(EXPR_PRIMARY_PAREN == expr->tag);

    if (!semantic_analysis_expr(ctx, expr->paren))
    {
        return STBOOL_FAILURE;
    }

    data_type_assign(&expr->dt, &expr->paren->dt, true);
    expr->valcat = expr->paren->valcat;
    expr->is_constexpr = expr->paren->is_constexpr;

    return STBOOL_SUCCESS;
}

static stbool semantic_analysis_expr_primary(
    struct semantic_analysis_context* ctx, struct expr* expr)
{
    nassert(NULL != expr->srctok);
    nassert(expr_is_primary(expr));

    switch (expr->tag)
    {
    case EXPR_PRIMARY_IDENTIFIER:
        return semantic_analysis_expr_primary_identifier(ctx, expr, false);
    case EXPR_PRIMARY_LITERAL:
        return semantic_analysis_expr_primary_literal(ctx, expr);
    case EXPR_PRIMARY_PAREN:
        return semantic_analysis_expr_primary_paren(ctx, expr);
    default:
        NPANIC_DFLT_CASE();
    }
}

static inline stbool semantic_analysis_expr_postfix_part_function_call(
    struct semantic_analysis_context* ctx, struct expr* expr)
{
    nassert(NULL != expr->srctok);
    nassert(EXPR_POSTFIX_FUNCTION_CALL == expr->tag);

    if (!semantic_analysis_expr(ctx, expr->postfix.lhs))
    {
        return STBOOL_FAILURE;
    }

    if (!data_type_is_function(&expr->postfix.lhs->dt))
    {
        DECLARE_DT_NSTR(lhs_data_type_nstr, &expr->postfix.lhs->dt);
        NLOGF_FLC_TOK(
            LOG_ERROR,
            expr->postfix.lhs->srctok,
            FMT_EXPR_HAS_TYPE_X_EXPECTED_Y("function"),
            lhs_data_type_nstr.data);
        return STBOOL_FAILURE;
    }

    struct func_sig const* const lhs_func_sig = expr->postfix.lhs->dt.func_sig;

    bool const is_dot_member_func_call =
        (EXPR_POSTFIX_ACCESS_DOT == expr->postfix.lhs->tag)
        && expr->postfix.lhs->postfix.access.is_member_func;
    bool const is_arrow_member_func_call =
        (EXPR_POSTFIX_ACCESS_ARROW == expr->postfix.lhs->tag)
        && expr->postfix.lhs->postfix.access.is_member_func;
    bool const is_member_func_call =
        is_dot_member_func_call || is_arrow_member_func_call;
    if (is_member_func_call)
    {
        struct expr* member_func_expr;
        struct expr* instance_expr;
        // If the expression is an invocation of a member function, the member
        // function call must be translated into a function call using the
        // defined function targeted by the member function and add the extra
        // 'this' pointer.
        //! @todo
        //!     In the future move this portion of 'analysis' into code
        //!     generation.
        //!     Right now the adding of the 'this' pointer isn't necessary for
        //!     any portion of semantic analysis, and the actual generation of
        //!     the code could easily perform this same transform.
        member_func_expr = expr->postfix.lhs;
        instance_expr = member_func_expr->postfix.lhs;
        nassert(member_func_expr->postfix.access.is_member_func);

        // If the previous identifier is a variant tag, then we need to
        // change it to the variant itself. because one cannot invoke a
        // member function directly on the tag, only on the variant owning the
        // tag.
        if(instance_expr->dt.tag == DT_VARIANT)
        {
            ssize_t const instance_tag_idx =
                variant_def_member_tag_idx(instance_expr->dt.variant_ref->def,
                    &instance_expr->postfix.access.id);
            if(MEMBER_TAG_NOT_FOUND != instance_tag_idx)
            {
                instance_expr = instance_expr->postfix.lhs;
            }
        }

        if (!expr_is_lvalue(instance_expr) && is_dot_member_func_call)
        {
            NLOGF_FLC_TOK(
                LOG_ERROR,
                expr->srctok,
                "Dot invocation of a member function is only allowed on lvalue "
                "structs and variants.");
            return STBOOL_FAILURE;
        }

        struct expr this_arg;
        expr_init(&this_arg);
        expr_assign(&this_arg, instance_expr, false);
        expr->postfix.args = narr_unshift(expr->postfix.args, &this_arg);
    }

    size_t const num_params = narr_length(lhs_func_sig->param_dts);
    size_t const num_args = narr_length(expr->postfix.args);
    if (num_args != num_params)
    {
        NLOGF_FLC_TOK(
            LOG_ERROR,
            expr->srctok,
            "Mismatched number of function arguments (expected %zu, received "
            "%zu).",
            num_params,
            num_args);
        return STBOOL_FAILURE;
    }

    stbool args_status = STBOOL_SUCCESS;
    for (size_t i = 0; i < num_args; ++i)
    {
        struct expr* const cur_arg = &expr->postfix.args[i];
        struct data_type* const cur_arg_dt = &cur_arg->dt;
        struct data_type* const func_param_dt = &lhs_func_sig->param_dts[i];

        bool const is_possibly_this_arg = i == 0;
        bool const is_this_arg_dot =
            is_possibly_this_arg && is_dot_member_func_call;
        bool const is_this_arg_arrow =
            is_possibly_this_arg && is_arrow_member_func_call;
        bool const is_this_arg = is_this_arg_dot || is_this_arg_arrow;

        // The 'this' argument should have already been
        // processed/assigned above so it should not be reprocessed.
        //! @todo
        //!     Remove this shit when we remove the `this` pointer stuff above.
        if (!is_this_arg && !semantic_analysis_expr(ctx, cur_arg))
        {
            args_status = STBOOL_FAILURE;
            continue;
        }

        if (is_this_arg_dot)
        {
            // The 'this' argument provided to the member function must be a
            // pointer, but a dot member function call indicates that the 'this'
            // argument needs to have its address taken.
            // We could build an address-of expression here by hand, but to make
            // sure future changes to the expr struct don't mess up this portion
            // of the code, the code generation phase will perform an address-of
            // operation by performing the same checks that were just done
            // above.
            // This may end up being a mistake, so if it turns out there is yet
            // another data mismatch bug having to do with member function calls
            // in the future, this is probably the issue.
            //      - Past Victor
            //! @todo
            //!     Remove this shit when we remove the `this` pointer stuff
            //!     above.
            struct data_type* const new_mem = nalloc(sizeof(struct data_type));
            data_type_init(new_mem);
            new_mem->tag = DT_POINTER;
            data_type_push_modifier(cur_arg_dt, new_mem);
        }
        if (!data_type_can_implicitly_convert(cur_arg_dt, func_param_dt))
        {
            nassert(DT_UNSET != cur_arg->dt.tag);
            nassert(DT_UNSET != func_param_dt->tag);

            DECLARE_DT_NSTR(arg_data_type_nstr, &cur_arg->dt);
            DECLARE_DT_NSTR(param_data_type_nstr, func_param_dt);
            NLOGF_FLC_TOK(
                LOG_ERROR,
                expr->postfix.args[i].srctok,
                "Cannot implicitly convert argument %zu type '%s' to parameter "
                "type '%s'.",
                i + 1,
                arg_data_type_nstr.data,
                param_data_type_nstr.data);
            args_status = STBOOL_FAILURE;
        }
    }
    if (STBOOL_SUCCESS != args_status)
    {
        return STBOOL_FAILURE;
    }

    nassert(DT_FUNCTION == expr->postfix.lhs->dt.tag);
    data_type_assign(&expr->dt, &lhs_func_sig->return_dt, true);
    expr->valcat = VALCAT_RVALUE;
    expr->is_constexpr = false;

    return STBOOL_SUCCESS;
}

static inline stbool semantic_analysis_expr_postfix_part_subscript(
    struct semantic_analysis_context* ctx, struct expr* expr)
{
    nassert(NULL != expr->srctok);
    nassert(EXPR_POSTFIX_SUBSCRIPT == expr->tag);

    if (!semantic_analysis_expr(ctx, expr->postfix.lhs))
    {
        return STBOOL_FAILURE;
    }

    if (!data_type_is_pointer(&expr->postfix.lhs->dt)
        && !data_type_is_array(&expr->postfix.lhs->dt))
    {
        DECLARE_DT_NSTR(lhs_dt_nstr, &expr->postfix.lhs->dt);
        NLOGF_FLC_TOK(
            LOG_ERROR,
            expr->postfix.lhs->srctok,
            FMT_EXPR_HAS_TYPE_X_EXPECTED_Y("pointer or array"),
            lhs_dt_nstr.data);
        return STBOOL_FAILURE;
    }

    if (!semantic_analysis_expr(ctx, expr->postfix.subscript))
    {
        return STBOOL_FAILURE;
    }

    if (!data_type_is_integer_unsigned(&expr->postfix.subscript->dt))
    {
        DECLARE_DT_NSTR(subscript_dt_nstr, &expr->postfix.subscript->dt);
        NLOGF_FLC_TOK(
            LOG_ERROR,
            expr->postfix.subscript->srctok,
            "Illegal use of data type '%s'. "
            "Array subscript must be an unsigned integer type.",
            subscript_dt_nstr.data);
        return STBOOL_FAILURE;
    }

    data_type_assign(&expr->dt, &expr->postfix.lhs->dt, true);
    data_type_pop_modifier(&expr->dt);
    expr->valcat = VALCAT_LVALUE;
    expr->is_constexpr = false;

    return STBOOL_SUCCESS;
}

static char const* const FMT_MEMBER_NOT_IN_TYPE =
    "No member named '%.*s' in type '%.*s'.";

static inline stbool semantic_analysis_expr_postfix_part_access_dot(
    struct semantic_analysis_context* ctx, struct expr* expr)
{
    nassert(NULL != expr->srctok);
    nassert(EXPR_POSTFIX_ACCESS_DOT == expr->tag);

    // The LHS is allowed to be a type name if it is the first in a dot/arrow
    // expression (EXPR_PRIMARY_IDENTIFIER), and if it is a variant or enum
    // type. Otherwise it may not be a typename which is the default.
    if(EXPR_PRIMARY_IDENTIFIER == expr->postfix.lhs->tag)
    {
        if(!semantic_analysis_expr_primary_identifier(ctx, expr->postfix.lhs,
            true))
        {
            return STBOOL_FAILURE;
        }
    }
    else if (!semantic_analysis_expr(ctx, expr->postfix.lhs))
    {
        return STBOOL_FAILURE;
    }

    struct data_type* const lhs_dt = &expr->postfix.lhs->dt;
    bool const is_struct = data_type_is_struct(lhs_dt);
    bool const is_variant = data_type_is_variant(lhs_dt) &&
        !((expr->postfix.lhs->tag == EXPR_POSTFIX_ACCESS_DOT ||
           expr->postfix.lhs->tag == EXPR_POSTFIX_ACCESS_ARROW) &&
        expr->postfix.lhs->postfix.access.is_member_tag);
    // A tag cannot be the first thing in a dots/arrows expression, so we must
    // make sure that the previous thing is a dots and arrows expression to
    // test if it is a member_tag
    bool const is_tag = data_type_is_variant(lhs_dt) &&
        (expr->postfix.lhs->tag == EXPR_POSTFIX_ACCESS_DOT ||
         expr->postfix.lhs->tag == EXPR_POSTFIX_ACCESS_ARROW) &&
        expr->postfix.lhs->postfix.access.is_member_tag;
    bool const is_typename =
        EXPR_PRIMARY_IDENTIFIER == expr->postfix.lhs->tag && is_variant
        && identifier_eq(&lhs_dt->variant_ref->id, &expr->postfix.lhs->id);

    // We have to check that typename rules are being followed.
    // Only variant and enum tag literals may use a typename as a dot/arrow
    // expression.
    if (is_struct && !is_typename)
    // Regular struct access
    {
        nassert(NULL != lhs_dt->struct_ref->def);
        struct struct_def* const struct_def = lhs_dt->struct_ref->def;
        struct identifier* const id = &expr->postfix.access.id;

        // search for either a member variable or function
        ssize_t const member_var_idx = struct_def_member_var_idx(struct_def, id);
        ssize_t const member_func_idx = struct_def_member_func_idx(struct_def, id);
        if (MEMBER_VAR_NOT_FOUND != member_var_idx)
        {
            data_type_assign(
                &expr->dt, &struct_def->member_vars[member_var_idx].dt, true);
            expr->valcat = VALCAT_LVALUE;
            expr->is_constexpr = false;
            expr->postfix.access.is_member_func = false;
            expr->postfix.access.is_member_tag = false;
        }
        else if (MEMBER_FUNC_NOT_FOUND != member_func_idx)
        {
            struct member_func* const member_func =
                &struct_def->member_funcs[member_func_idx];
            data_type_assign(&expr->dt, &member_func->dt, true);
            expr->valcat = VALCAT_NONE;
            expr->is_constexpr = false;
            expr->postfix.access.is_member_func = true;
            expr->postfix.access.is_member_tag = false;
        }
        else
        {
            NLOGF_FLC_TOK(
                LOG_ERROR,
                expr->postfix.lhs->srctok,
                FMT_MEMBER_NOT_IN_TYPE,
                (int)id->length,
                id->start,
                (int)lhs_dt->struct_ref->id.length,
                lhs_dt->struct_ref->id.start);
            return STBOOL_FAILURE;
        }
    }
    else if(is_tag && !is_typename)
    // accessing the attributes of a varaint's tag
    {
        ssize_t const tag_idx =
            variant_def_member_tag_idx(lhs_dt->variant_ref->def,
            &expr->postfix.lhs->postfix.access.id);
        nassert(tag_idx != MEMBER_TAG_NOT_FOUND);
        struct struct_def* const struct_def =
            &lhs_dt->variant_ref->def->member_tags[tag_idx].section;
        struct identifier* const id = &expr->postfix.access.id;

        // search for a member varaible or function.
        ssize_t const member_var_idx = struct_def_member_var_idx(struct_def, id);
        ssize_t const member_func_idx = struct_def_member_func_idx(struct_def, id);
        if (MEMBER_VAR_NOT_FOUND != member_var_idx)
        {
            data_type_assign(
                &expr->dt, &struct_def->member_vars[member_var_idx].dt, true);
            expr->valcat = VALCAT_LVALUE;
            expr->is_constexpr = false;
            expr->postfix.access.is_member_func = false;
            expr->postfix.access.is_member_tag = false;
        }
        else if (MEMBER_FUNC_NOT_FOUND != member_func_idx)
        {
            struct member_func* const member_func =
                &struct_def->member_funcs[member_func_idx];
            data_type_assign(&expr->dt, &member_func->dt, true);
            expr->valcat = VALCAT_NONE;
            expr->is_constexpr = false;
            expr->postfix.access.is_member_func = true;
            expr->postfix.access.is_member_tag = false;
        }
        else
        {
            NLOGF_FLC_TOK(
                LOG_ERROR,
                expr->postfix.lhs->srctok,
                FMT_MEMBER_NOT_IN_TYPE,
                (int)id->length,
                id->start,
                (int)lhs_dt->struct_ref->id.length,
                lhs_dt->struct_ref->id.start);
            return STBOOL_FAILURE;
        }
    }
    else if(is_variant && is_typename)
    // static reference to a tag literal of a variant.
    {
        nassert(NULL != lhs_dt->variant_ref->def);
        struct variant_def* const variant = lhs_dt->variant_ref->def;
        struct identifier* const id = &expr->postfix.access.id;

        ssize_t const member_tag_idx = variant_def_member_tag_idx(variant, id);
        if(MEMBER_TAG_NOT_FOUND == member_tag_idx)
        {
            NLOGF_FLC_TOK(LOG_ERROR, expr->srctok,
                "Variant \'%.*s\' does not have tag \'%.*s\'.",
                (int)lhs_dt->variant_ref->id.length,
                lhs_dt->variant_ref->id.start,
                (int)id->length,
                id->start);
            return STBOOL_FAILURE;
        }
        else
        {
            expr_fini(expr->postfix.lhs);
            nfree(expr->postfix.lhs);
            identifier_fini(id);

            // switch the `varaint_name.tag_name` expression to the literal
            // value of the tag.

            // TODO (VictorSCushman): better method of inserting values than
            // switching out to literal expressions.
            expr->tag = EXPR_PRIMARY_LITERAL;
            expr->literal = nalloc(sizeof(struct literal));
            literal_init(expr->literal);
            literal_assign(expr->literal,
                &variant->member_tags[member_tag_idx].value, true);
            struct data_type literal_dt = literal_get_data_type(expr->literal);
            data_type_assign(&expr->dt, &literal_dt, true);
            data_type_fini(&literal_dt);
            expr->valcat = VALCAT_RVALUE;
        }
    }
    else if(is_variant)
    // access of a variant member
    {
        nassert(NULL != lhs_dt->variant_ref->def);
        struct variant_def* const variant = lhs_dt->variant_ref->def;
        struct identifier* const id = &expr->postfix.access.id;

        // check if it is the automagical `.tag` attribute.
        struct identifier id_tag;
        identifier_init(&id_tag);
        id_tag.start = "tag";
        id_tag.length = strlen("tag");
        if(identifier_eq(&id_tag, id))
        {
            data_type_assign(&expr->dt, &variant->tag_dt, true);
            expr->valcat = VALCAT_LVALUE;
            expr->is_constexpr = false;
            expr->postfix.access.is_member_func = false;
            expr->postfix.access.is_member_tag = false;
            identifier_fini(&id_tag);
            return STBOOL_SUCCESS;
        }
        identifier_fini(&id_tag);

        //search the member variables, functions, ifaces, and tags.
        ssize_t const member_var_idx =
            variant_def_member_var_idx(variant, id);
        ssize_t const member_func_idx =
            variant_def_member_func_idx(variant, id);
        ssize_t const member_iface_idx =
            variant_def_member_iface_idx(variant, id);
        ssize_t const member_tag_idx =
            variant_def_member_tag_idx(variant, id);
        if(MEMBER_VAR_NOT_FOUND != member_var_idx)
        {
            // found a member_var
            data_type_assign(
                &expr->dt, &variant->member_vars[member_var_idx].dt, true);
            expr->valcat = VALCAT_LVALUE;
            expr->is_constexpr = false;
            expr->postfix.access.is_member_func = false;
            expr->postfix.access.is_member_tag = false;
        }
        else if(MEMBER_FUNC_NOT_FOUND != member_func_idx)
        {
            // found a member_func
            struct member_func* const member_func = 
                &variant->member_funcs[member_func_idx];
            data_type_assign(&expr->dt, &member_func->dt, true);
            expr->valcat = VALCAT_NONE;
            expr->is_constexpr = false;
            expr->postfix.access.is_member_func = true;
            expr->postfix.access.is_member_tag = false;
        }
        else if(MEMBER_IFACE_NOT_FOUND != member_iface_idx)
        {
            // found a member_iface
            struct member_iface* const iface =
                &variant->member_ifaces[member_iface_idx];
            expr->dt.tag = DT_FUNCTION;
            expr->dt.func_sig = nalloc(sizeof(struct func_sig));
            func_sig_init(expr->dt.func_sig);
            func_sig_assign(expr->dt.func_sig, &iface->sig, true);
            expr->valcat = VALCAT_NONE;
            expr->is_constexpr = false;
            expr->postfix.access.is_member_func = true;
            expr->postfix.access.is_member_tag = false;
        }
        else if(MEMBER_TAG_NOT_FOUND != member_tag_idx)
        {
            // found a member-tag
            expr->dt.tag = DT_VARIANT;
            expr->dt.variant_ref = lhs_dt->variant_ref;
            expr->valcat = VALCAT_LVALUE;
            expr->is_constexpr = false;
            expr->postfix.access.is_member_func = false;
            expr->postfix.access.is_member_tag = true;
        }
        else
        {
            NLOGF_FLC_TOK(LOG_ERROR,
                expr->postfix.lhs->srctok,
                FMT_MEMBER_NOT_IN_TYPE,
                (int)id->length,
                id->start,
                (int)lhs_dt->variant_ref->id.length,
                lhs_dt->variant_ref->id.start);
            return STBOOL_FAILURE;
        }
    }
    else if(is_typename)
    {
        NLOGF_FLC_TOK(LOG_ERROR,
            expr->postfix.lhs->srctok,
            // TODO: better error message.
            "Cannot take a tag literal value from a non- enum or variant.");
        return STBOOL_FAILURE;
    }
    else
    {
        DECLARE_DT_NSTR(lhs_dt_nstr, lhs_dt);
        NLOGF_FLC_TOK(
            LOG_ERROR,
            expr->postfix.lhs->srctok,
            FMT_EXPR_HAS_TYPE_X_EXPECTED_Y("struct or variant"),
            lhs_dt_nstr.data);
        return STBOOL_FAILURE;
    }

    return STBOOL_SUCCESS;
}

static inline stbool semantic_analysis_expr_postfix_part_access_arrow(
    struct semantic_analysis_context* ctx, struct expr* expr)
{
    nassert(NULL != expr->srctok);
    nassert(EXPR_POSTFIX_ACCESS_ARROW == expr->tag);

    if (!semantic_analysis_expr(ctx, expr->postfix.lhs))
    {
        return STBOOL_FAILURE;
    }


    struct data_type* const ptr_dt = &expr->postfix.lhs->dt;
    bool const dt_is_ptr = data_type_is_pointer(ptr_dt);
    if (!dt_is_ptr)
    {
        DECLARE_DT_NSTR(ptr_dt_nstr, ptr_dt);
        NLOGF_FLC_TOK(
            LOG_ERROR,
            expr->postfix.lhs->srctok,
            FMT_EXPR_HAS_TYPE_X_EXPECTED_Y("pointer"),
            ptr_dt_nstr.data);
        return STBOOL_FAILURE;
    }
    nassert(NULL != ptr_dt->inner);

    struct data_type* const inner_dt = ptr_dt->inner;
    bool const dt_is_ptr_to_struct = data_type_is_struct(inner_dt);
    bool const dt_is_ptr_to_variant = data_type_is_variant(inner_dt) &&
        !((expr->postfix.lhs->tag == EXPR_POSTFIX_ACCESS_DOT ||
           expr->postfix.lhs->tag == EXPR_POSTFIX_ACCESS_ARROW) &&
        expr->postfix.lhs->postfix.access.is_member_tag);
    bool const dt_is_ptr_to_tag = data_type_is_variant(inner_dt) &&
        (expr->postfix.lhs->tag == EXPR_POSTFIX_ACCESS_DOT ||
         expr->postfix.lhs->tag == EXPR_POSTFIX_ACCESS_ARROW) &&
        expr->postfix.lhs->postfix.access.is_member_tag;

    if (dt_is_ptr_to_struct) // LHS is a struct pointer.
    {
        struct struct_def* const struct_def = inner_dt->struct_ref->def;
        struct identifier* const id = &expr->postfix.access.id;

        // search the struct for a member variable or a member function.
        ssize_t const member_var_idx = struct_def_member_var_idx(struct_def, id);
        ssize_t const member_func_idx = struct_def_member_func_idx(struct_def, id);
        if (MEMBER_VAR_NOT_FOUND != member_var_idx)
        {
            data_type_assign(
                &expr->dt, &struct_def->member_vars[member_var_idx].dt, true);
            expr->valcat = VALCAT_LVALUE;
            expr->is_constexpr = false;
            expr->postfix.access.is_member_func = false;
            expr->postfix.access.is_member_tag = false;
        }
        else if (MEMBER_FUNC_NOT_FOUND != member_func_idx)
        {
            struct member_func* const member_func =
                &struct_def->member_funcs[member_func_idx];
            data_type_assign(&expr->dt, &member_func->dt, true);
            expr->valcat = VALCAT_NONE;
            expr->is_constexpr = false;
            expr->postfix.access.is_member_func = true;
            expr->postfix.access.is_member_tag = false;
        }
        else
        {
            NLOGF_FLC_TOK(
                LOG_ERROR,
                expr->postfix.lhs->srctok,
                FMT_MEMBER_NOT_IN_TYPE,
                (int)id->length,
                id->start,
                (int)inner_dt->struct_ref->id.length,
                inner_dt->struct_ref->id.start);
            return STBOOL_FAILURE;
        }
    }
    else if(dt_is_ptr_to_variant) // LHS is a variant.
    {
        struct variant_def* const variant = inner_dt->variant_ref->def;
        struct identifier* const id = &expr->postfix.access.id;

        // check if it is the automagical tag attribute.
        struct identifier id_tag;
        identifier_init(&id_tag);
        id_tag.start = "tag";
        id_tag.length = strlen("tag");
        if(identifier_eq(&id_tag, id))
        {
            data_type_assign(&expr->dt, &variant->tag_dt, true);
            expr->valcat = VALCAT_LVALUE;
            expr->is_constexpr = false;
            expr->postfix.access.is_member_func = false;
            expr->postfix.access.is_member_tag = false;
            identifier_fini(&id_tag);
            return STBOOL_SUCCESS;
        }
        identifier_fini(&id_tag);

        // search the variant for a member variable, function, iface or tag.
        ssize_t const member_var_idx = variant_def_member_var_idx(variant, id);
        ssize_t const member_func_idx = variant_def_member_func_idx(variant, id);
        ssize_t const member_iface_idx = variant_def_member_iface_idx(variant, id);
        ssize_t const member_tag_idx = variant_def_member_tag_idx(variant, id);
        if(MEMBER_VAR_NOT_FOUND != member_var_idx)
        {
            // found a member_var
            data_type_assign(&expr->dt,
                    &variant->member_vars[member_var_idx].dt, true);
            expr->valcat = VALCAT_LVALUE;
            expr->is_constexpr = false;
            expr->postfix.access.is_member_func = false;
            expr->postfix.access.is_member_tag = false;
        }
        else if(MEMBER_FUNC_NOT_FOUND != member_func_idx)
        {
            // found a member_func
            struct member_func* const member_func =
                &variant->member_funcs[member_func_idx];
            data_type_assign(&expr->dt, &member_func->dt, true);
            expr->valcat = VALCAT_NONE;
            expr->postfix.access.is_member_func = true;
            expr->postfix.access.is_member_tag = false;
        }
        else if(MEMBER_IFACE_NOT_FOUND != member_iface_idx)
        {
            // found a member_iface
            struct member_iface* const iface =
                &variant->member_ifaces[member_iface_idx];
            expr->dt.tag = DT_FUNCTION;
            expr->dt.func_sig = nalloc(sizeof(struct func_sig));
            func_sig_init(expr->dt.func_sig);
            func_sig_assign(expr->dt.func_sig, &iface->sig, true);
            expr->valcat = VALCAT_NONE;
            expr->postfix.access.is_member_func = true;
            expr->postfix.access.is_member_tag = false;
        }
        else if(MEMBER_TAG_NOT_FOUND != member_tag_idx)
        {
            // found a member_tag.
            expr->dt.tag = DT_VARIANT;
            expr->dt.variant_ref = inner_dt->variant_ref;
            expr->valcat = VALCAT_LVALUE;
            expr->postfix.access.is_member_func = false;
            expr->postfix.access.is_member_tag = true;
        }
        else
        {
            NLOGF_FLC_TOK(LOG_ERROR, expr->postfix.lhs->srctok,
                FMT_MEMBER_NOT_IN_TYPE,
                (int) id->length,
                id->start,
                (int) inner_dt->variant_ref->id.length,
                inner_dt->variant_ref->id.start);
            return STBOOL_FAILURE;
        }
    }
    else if(dt_is_ptr_to_tag) // LHS is a varaint's tag.
    {
        ssize_t const tag_idx =
            variant_def_member_tag_idx(inner_dt->variant_ref->def,
            &expr->postfix.lhs->postfix.access.id);
        nassert(MEMBER_TAG_NOT_FOUND != tag_idx);
        struct struct_def* const struct_def =
            &inner_dt->variant_ref->def->member_tags[tag_idx].section;
        struct identifier* const id = &expr->postfix.access.id;

        // search the tag's section for a member variable or function.
        ssize_t const member_var_idx = struct_def_member_var_idx(struct_def, id);
        ssize_t const member_func_idx = struct_def_member_func_idx(struct_def, id);
        if (MEMBER_VAR_NOT_FOUND != member_var_idx)
        {
            // found a member_var
            data_type_assign(
                &expr->dt, &struct_def->member_vars[member_var_idx].dt, true);
            expr->valcat = VALCAT_LVALUE;
            expr->is_constexpr = false;
            expr->postfix.access.is_member_func = false;
            expr->postfix.access.is_member_tag = false;
        }
        else if (MEMBER_FUNC_NOT_FOUND != member_func_idx)
        {
            // found a member_func
            struct member_func* const member_func =
                &struct_def->member_funcs[member_func_idx];
            data_type_assign(&expr->dt, &member_func->dt, true);
            expr->valcat = VALCAT_NONE;
            expr->is_constexpr = false;
            expr->postfix.access.is_member_func = true;
            expr->postfix.access.is_member_tag = false;
        }
        else
        {
            NLOGF_FLC_TOK(
                LOG_ERROR,
                expr->postfix.lhs->srctok,
                FMT_MEMBER_NOT_IN_TYPE,
                (int)id->length,
                id->start,
                (int)inner_dt->struct_ref->id.length,
                inner_dt->struct_ref->id.start);
            return STBOOL_FAILURE;
        }
    }
    else
    {
        DECLARE_DT_NSTR(ptr_dt_nstr, ptr_dt);
        NLOGF_FLC_TOK(
            LOG_ERROR,
            expr->postfix.lhs->srctok,
            FMT_EXPR_HAS_TYPE_X_EXPECTED_Y("pointer to struct or variant"),
            ptr_dt_nstr.data);
        return STBOOL_FAILURE;
    }

    return STBOOL_SUCCESS;
}

static stbool semantic_analysis_expr_postfix(
    struct semantic_analysis_context* ctx, struct expr* expr)
{
    nassert(NULL != expr->srctok);
    nassert(expr_is_postfix(expr));

    switch (expr->tag)
    {
    case EXPR_POSTFIX_FUNCTION_CALL:
        return semantic_analysis_expr_postfix_part_function_call(ctx, expr);
    case EXPR_POSTFIX_SUBSCRIPT:
        return semantic_analysis_expr_postfix_part_subscript(ctx, expr);
    case EXPR_POSTFIX_ACCESS_DOT:
        return semantic_analysis_expr_postfix_part_access_dot(ctx, expr);
    case EXPR_POSTFIX_ACCESS_ARROW:
        return semantic_analysis_expr_postfix_part_access_arrow(ctx, expr);
    default:
        NPANIC_DFLT_CASE();
    }
}

static char const* const FMT_UOP_COUNTOF_MUST_ONLY_BE_APPLIED_TO_ARRAY_TYPES =
    "Unary 'countof' must only be applied to array types.";
static char const* const FMT_UOP_CANNOT_TAKE_SIZEOF_VOID_TYPE =
    "Cannot take the sizeof a void type.";

static stbool semantic_analysis_expr_unary(
    struct semantic_analysis_context* ctx, struct expr* expr)
{
    nassert(NULL != expr->srctok);
    nassert(expr_is_unary(expr));

    // Perform semantic analysis on the operand of the unary operation.
    switch (expr->tag)
    {
    case EXPR_UNARY_ADDR_OF:     /* intentional fallthrough */
    case EXPR_UNARY_DEREF:       /* intentional fallthrough */
    case EXPR_UNARY_DECAY:       /* intentional fallthrough */
    case EXPR_UNARY_MINUS:       /* intentional fallthrough */
    case EXPR_UNARY_BITWISE_NOT: /* intentional fallthrough */
    case EXPR_UNARY_LOGICAL_NOT: /* intentional fallthrough */
    {
        if (!semantic_analysis_expr(ctx, expr->unary.rhs_expr))
        {
            return STBOOL_FAILURE;
        }
    }
    break;

    case EXPR_UNARY_COUNTOF_EXPR: /* intentional fallthrough */
    case EXPR_UNARY_SIZEOF_EXPR:
    {
        if (EXPR_PRIMARY_IDENTIFIER == expr->unary.rhs_expr->tag)
        {
            if (!semantic_analysis_expr_primary_identifier(
                    ctx, expr->unary.rhs_expr, true))
            {
                return STBOOL_FAILURE;
            }
        }
        else
        {
            if (!semantic_analysis_expr(ctx, expr->unary.rhs_expr))
            {
                return STBOOL_FAILURE;
            }
        }
    }
    break;

    case EXPR_UNARY_COUNTOF_DT: /* intentional fallthrough */
    case EXPR_UNARY_SIZEOF_DT:
    {
        if (!semantic_analysis_data_type(ctx, expr->unary.rhs_dt))
        {
            return STBOOL_FAILURE;
        }
    }
    break;

    default:
        NPANIC_DFLT_CASE();
    }

    // Perform operation specific semantic analysis on the unary expression as a
    // whole.
    switch (expr->tag)
    {
    case EXPR_UNARY_ADDR_OF:
    {
        if (!expr_is_lvalue(expr->unary.rhs_expr))
        {
            NLOGF_FLC_TOK(
                LOG_ERROR,
                expr->srctok,
                "Address-of operator '%s' is not applied to an lvalue object.",
                token_type_to_cstr(TOKEN_OPERATOR_QUESTION));
            return STBOOL_FAILURE;
        }

        data_type_assign(&expr->dt, &expr->unary.rhs_expr->dt, true);
        struct data_type* const new_mem = nalloc(sizeof(struct data_type));
        data_type_init(new_mem);
        new_mem->tag = DT_POINTER;
        data_type_push_modifier(&expr->dt, new_mem);

        expr->valcat = VALCAT_RVALUE;
        // The address of a global variable will not change since globals are
        // fixed in memory.
        // If the lvalue is a global then its address is constexpr.
        expr->is_constexpr = scope_is_global(ctx->curr_scope);
    }
    break;

    case EXPR_UNARY_DEREF:
    {
        if (!data_type_is_pointer(&expr->unary.rhs_expr->dt))
        {
            NLOGF_FLC_TOK(
                LOG_ERROR,
                expr->srctok,
                "Dereference operator '%s' is not applied applied to a pointer "
                "type.",
                token_type_to_cstr(TOKEN_OPERATOR_AT));
            return STBOOL_FAILURE;
        }

        data_type_assign(&expr->dt, &expr->postfix.lhs->dt, true);
        data_type_pop_modifier(&expr->dt);

        expr->valcat = VALCAT_LVALUE;
        expr->is_constexpr = false;
    }
    break;

    case EXPR_UNARY_DECAY:
    {
        if (!data_type_is_array(&expr->unary.rhs_expr->dt))
        {
            NLOGF_FLC_TOK(
                LOG_ERROR,
                expr->srctok,
                "Decay operator '%s' is not applied to an array type.",
                token_type_to_cstr(TOKEN_OPERATOR_DOLLAR));
            return STBOOL_FAILURE;
        }

        data_type_assign(&expr->dt, &expr->unary.rhs_expr->dt, true);
        expr->dt.tag = DT_POINTER;

        expr->valcat = VALCAT_RVALUE;
        // The address of a global variable will not change since globals are
        // fixed in memory.
        // If the lvalue is a global then the address of it's first element is
        // constexpr.
        expr->is_constexpr = scope_is_global(ctx->curr_scope);
    }
    break;

    case EXPR_UNARY_MINUS:
    {
        struct data_type const* const rhs_dt = &expr->unary.rhs_expr->dt;
        if (!(data_type_is_integer_signed(rhs_dt)
              || data_type_is_float(rhs_dt)))
        {
            NLOGF_FLC_TOK(
                LOG_ERROR,
                expr->srctok,
                "Unary minus '%s' must only be applied to signed integer and "
                "floating point types.",
                token_type_to_cstr(TOKEN_OPERATOR_DASH));
            return STBOOL_FAILURE;
        }
        data_type_assign(&expr->dt, &expr->unary.rhs_expr->dt, true);

        expr->valcat = VALCAT_RVALUE;
        // This expression is a constant arithmetic expression if its operand is
        // a constant arithmetic expressions.
        expr->is_constexpr = expr->unary.rhs_expr->is_constexpr;
    }
    break;

    case EXPR_UNARY_BITWISE_NOT:
    {
        if (!data_type_is_integer(&expr->unary.rhs_expr->dt))
        {
            NLOGF_FLC_TOK(
                LOG_ERROR,
                expr->srctok,
                "Unary bitwise-not '%s' must only be applied to integer types.",
                token_type_to_cstr(TOKEN_OPERATOR_TILDE));
            return STBOOL_FAILURE;
        }
        data_type_assign(&expr->dt, &expr->unary.rhs_expr->dt, true);

        expr->valcat = VALCAT_RVALUE;
        // This expression is a constant arithmetic expression if its operand is
        // a constant arithmetic expressions.
        expr->is_constexpr = expr->unary.rhs_expr->is_constexpr;
    }
    break;

    case EXPR_UNARY_LOGICAL_NOT:
    {
        if (!data_type_is_bool(&expr->unary.rhs_expr->dt))
        {
            NLOGF_FLC_TOK(
                LOG_ERROR,
                expr->srctok,
                "Unary logical not '%s' must only be applied to the bool type.",
                token_type_to_cstr(TOKEN_OPERATOR_BANG));
            return STBOOL_FAILURE;
        }
        data_type_assign(&expr->dt, &expr->unary.rhs_expr->dt, true);

        expr->valcat = VALCAT_RVALUE;
        // This expression is a constant Boolean expression if its operand is
        // a constant Boolean expressions.
        expr->is_constexpr = expr->unary.rhs_expr->is_constexpr;
    }
    break;

    case EXPR_UNARY_COUNTOF_EXPR:
    {
        if (!data_type_is_array(&expr->unary.rhs_expr->dt))
        {
            NLOGF_FLC_TOK(
                LOG_ERROR,
                expr->srctok,
                FMT_UOP_COUNTOF_MUST_ONLY_BE_APPLIED_TO_ARRAY_TYPES);
        }
        expr->dt.tag = DT_U;

        expr->valcat = VALCAT_RVALUE;
        expr->is_constexpr = true;
    }
    break;

    case EXPR_UNARY_COUNTOF_DT:
    {
        if (!data_type_is_array(expr->unary.rhs_dt))
        {
            NLOGF_FLC_TOK(
                LOG_ERROR,
                expr->srctok,
                FMT_UOP_COUNTOF_MUST_ONLY_BE_APPLIED_TO_ARRAY_TYPES);
        }
        expr->dt.tag = DT_U;

        expr->valcat = VALCAT_RVALUE;
        expr->is_constexpr = true;
    }
    break;

    case EXPR_UNARY_SIZEOF_EXPR:
    {
        if (data_type_is_void(&expr->unary.rhs_expr->dt))
        {
            NLOGF_FLC_TOK(
                LOG_ERROR, expr->srctok, FMT_UOP_CANNOT_TAKE_SIZEOF_VOID_TYPE);
            return STBOOL_FAILURE;
        }
        expr->dt.tag = DT_U;

        expr->valcat = VALCAT_RVALUE;
        expr->is_constexpr = true;
    }
    break;

    case EXPR_UNARY_SIZEOF_DT:
    {
        if (data_type_is_void(expr->unary.rhs_dt))
        {
            NLOGF_FLC_TOK(
                LOG_ERROR, expr->srctok, FMT_UOP_CANNOT_TAKE_SIZEOF_VOID_TYPE);
            return STBOOL_FAILURE;
        }
        expr->dt.tag = DT_U;

        expr->valcat = VALCAT_RVALUE;
        expr->is_constexpr = true;
    }
    break;

    default:
        NPANIC_DFLT_CASE();
    }

    nassert(DT_UNSET != expr->dt.tag);
    nassert(NULL == expr->dt.srctok);
    return STBOOL_SUCCESS;
}

//! Error message format string used for the semantic analysis of binary
//! expressions.
//!{
#define FMT_BOP_ILLEGAL_USE_OF_LHS_DATA_TYPE                                   \
    "Illegal data type '%s' as left hand side of binary operator '%s'."
#define FMT_BOP_ILLEGAL_USE_OF_RHS_DATA_TYPE                                   \
    "Illegal data type '%s' as right hand side of binary operator '%s'."
#define FMT_BOP_MISMATCHED_DATA_TYPES                                          \
    "Mismatched data types '%s' and '%s' in binary expression."
#define STR_EXPECTED(expected_cstr) "Expected " expected_cstr "."
#define STR_EXPECTED_X_TYPE(expected_type_cstr)                                \
    "Expected " expected_type_cstr " type."
//!}

//! Check if the LHS and RHS of the provided binary expression are both base
//! types.
//! @return
//!     #STBOOL_SUCCESS if the LHS and RHS are both base types.
//! @return
//!     #STBOOL_FAILURE otherwise.
//! @note
//!     Assumes @p bexpr is binary.
static stbool validate_xhs_are_base(struct expr const* bexpr)
{
    nassert(NULL != bexpr->srctok);
    nassert(expr_is_binary(bexpr));

    stbool status = STBOOL_SUCCESS;

    if (!data_type_is_base(&bexpr->binary.lhs_expr->dt))
    {
        DECLARE_DT_NSTR(lhs_dt_nstr, &bexpr->binary.lhs_expr->dt);
        NLOGF_FLC_TOK(
            LOG_ERROR,
            bexpr->srctok,
            FMT_BOP_ILLEGAL_USE_OF_LHS_DATA_TYPE " " STR_EXPECTED("base"),
            lhs_dt_nstr.data,
            token_type_to_cstr(bexpr->srctok->tag));
        status = STBOOL_FAILURE;
    }
    if (!data_type_is_base(&bexpr->binary.rhs_expr->dt))
    {
        DECLARE_DT_NSTR(rhs_dt_nstr, &bexpr->binary.rhs_expr->dt);
        NLOGF_FLC_TOK(
            LOG_ERROR,
            bexpr->srctok,
            FMT_BOP_ILLEGAL_USE_OF_RHS_DATA_TYPE " " STR_EXPECTED("base"),
            rhs_dt_nstr.data,
            token_type_to_cstr(bexpr->srctok->tag));
        status = STBOOL_FAILURE;
    }

    return status;
}

//! Check if the LHS and RHS of the provided binary expression are both
//! pointers.
//! @return
//!     #STBOOL_SUCCESS if the LHS and RHS are both pointers.
//! @return
//!     #STBOOL_FAILURE otherwise.
//! @note
//!     Assumes @p bexpr is binary.
static stbool validate_xhs_are_pointer(struct expr const* bexpr)
{
    nassert(NULL != bexpr->srctok);
    nassert(expr_is_binary(bexpr));

    stbool status = STBOOL_SUCCESS;

    if (!data_type_is_pointer(&bexpr->binary.lhs_expr->dt))
    {
        DECLARE_DT_NSTR(lhs_dt_nstr, &bexpr->binary.lhs_expr->dt);
        NLOGF_FLC_TOK(
            LOG_ERROR,
            bexpr->srctok,
            FMT_BOP_ILLEGAL_USE_OF_LHS_DATA_TYPE " " STR_EXPECTED("pointer"),
            lhs_dt_nstr.data,
            token_type_to_cstr(bexpr->srctok->tag));
        status = STBOOL_FAILURE;
    }
    if (!data_type_is_pointer(&bexpr->binary.rhs_expr->dt))
    {
        DECLARE_DT_NSTR(rhs_dt_nstr, &bexpr->binary.rhs_expr->dt);
        NLOGF_FLC_TOK(
            LOG_ERROR,
            bexpr->srctok,
            FMT_BOP_ILLEGAL_USE_OF_RHS_DATA_TYPE " " STR_EXPECTED("pointer"),
            rhs_dt_nstr.data,
            token_type_to_cstr(bexpr->srctok->tag));
        status = STBOOL_FAILURE;
    }

    return status;
}

//! Check if the LHS and RHS of the provided binary expression are not `void`.
//! @return
//!     #STBOOL_SUCCESS if the LHS and RHS are not `void`.
//! @return
//!     #STBOOL_FAILURE otherwise.
//! @note
//!     Assumes @p bexpr is binary.
static stbool validate_xhs_are_not_void(struct expr const* bexpr)
{
    nassert(NULL != bexpr->srctok);
    nassert(expr_is_binary(bexpr));

    stbool status = STBOOL_SUCCESS;

    if (data_type_is_void(&bexpr->binary.lhs_expr->dt))
    {
        DECLARE_DT_NSTR(lhs_dt_nstr, &bexpr->binary.lhs_expr->dt);
        NLOGF_FLC_TOK(
            LOG_ERROR,
            bexpr->srctok,
            FMT_BOP_ILLEGAL_USE_OF_LHS_DATA_TYPE
            " " STR_EXPECTED_X_TYPE("non-void"),
            lhs_dt_nstr.data,
            token_type_to_cstr(bexpr->srctok->tag));
        status = STBOOL_FAILURE;
    }
    if (data_type_is_void(&bexpr->binary.rhs_expr->dt))
    {
        DECLARE_DT_NSTR(rhs_dt_nstr, &bexpr->binary.rhs_expr->dt);
        NLOGF_FLC_TOK(
            LOG_ERROR,
            bexpr->srctok,
            FMT_BOP_ILLEGAL_USE_OF_RHS_DATA_TYPE
            " " STR_EXPECTED_X_TYPE("non-void"),
            rhs_dt_nstr.data,
            token_type_to_cstr(bexpr->srctok->tag));
        status = STBOOL_FAILURE;
    }

    return status;
}

//! Check if the LHS and RHS of the provided binary expression are not floating
//! point types.
//! @return
//!     #STBOOL_SUCCESS if the LHS and RHS are not floating point types.
//! @return
//!     #STBOOL_FAILURE otherwise.
//! @note
//!     Assumes @p bexpr is binary.
static stbool validate_xhs_are_not_floating_point(struct expr const* bexpr)
{
    nassert(NULL != bexpr->srctok);
    nassert(expr_is_binary(bexpr));

    stbool status = STBOOL_SUCCESS;

    if (data_type_is_float(&bexpr->binary.lhs_expr->dt))
    {
        DECLARE_DT_NSTR(lhs_dt_nstr, &bexpr->binary.lhs_expr->dt);
        NLOGF_FLC_TOK(
            LOG_ERROR,
            bexpr->srctok,
            FMT_BOP_ILLEGAL_USE_OF_LHS_DATA_TYPE
            " " STR_EXPECTED_X_TYPE("non-floating-point"),
            lhs_dt_nstr.data,
            token_type_to_cstr(bexpr->srctok->tag));
        status = STBOOL_FAILURE;
    }
    if (data_type_is_float(&bexpr->binary.rhs_expr->dt))
    {
        DECLARE_DT_NSTR(rhs_dt_nstr, &bexpr->binary.rhs_expr->dt);
        NLOGF_FLC_TOK(
            LOG_ERROR,
            bexpr->srctok,
            FMT_BOP_ILLEGAL_USE_OF_RHS_DATA_TYPE
            " " STR_EXPECTED_X_TYPE("non-floating-point"),
            rhs_dt_nstr.data,
            token_type_to_cstr(bexpr->srctok->tag));
        status = STBOOL_FAILURE;
    }

    return status;
}

//! Check if the LHS and RHS of the provided binary expression are not `bool`
//! @return
//!     #STBOOL_SUCCESS if the LHS and RHS are not `bool`.
//! @return
//!     #STBOOL_FAILURE otherwise.
//! @note
//!     Logs error messages as appropriate.
//! @note
//!     Assumes @p bexpr is binary.
static stbool validate_xhs_are_not_bool(struct expr const* bexpr)
{
    nassert(NULL != bexpr->srctok);
    nassert(expr_is_binary(bexpr));

    stbool status = STBOOL_SUCCESS;

    if (data_type_is_bool(&bexpr->binary.lhs_expr->dt))
    {
        DECLARE_DT_NSTR(lhs_dt_nstr, &bexpr->binary.lhs_expr->dt);
        NLOGF_FLC_TOK(
            LOG_ERROR,
            bexpr->srctok,
            FMT_BOP_ILLEGAL_USE_OF_LHS_DATA_TYPE
            " " STR_EXPECTED_X_TYPE("non-bool"),
            lhs_dt_nstr.data,
            token_type_to_cstr(bexpr->srctok->tag));
        status = STBOOL_FAILURE;
    }
    if (data_type_is_bool(&bexpr->binary.rhs_expr->dt))
    {
        DECLARE_DT_NSTR(rhs_dt_nstr, &bexpr->binary.rhs_expr->dt);
        NLOGF_FLC_TOK(
            LOG_ERROR,
            bexpr->srctok,
            FMT_BOP_ILLEGAL_USE_OF_RHS_DATA_TYPE
            " " STR_EXPECTED_X_TYPE("non-bool"),
            rhs_dt_nstr.data,
            token_type_to_cstr(bexpr->srctok->tag));
        status = STBOOL_FAILURE;
    }

    return status;
}

//! Check if the LHS and RHS of the provided binary expression are not `ascii`.
//! @return
//!     #STBOOL_SUCCESS if the LHS and RHS are not `char`.
//! @return
//!     #STBOOL_FAILURE otherwise.
//! @note
//!     Assumes @p bexpr is binary.
static stbool validate_xhs_are_not_ascii(struct expr const* bexpr)
{
    nassert(NULL != bexpr->srctok);
    nassert(expr_is_binary(bexpr));

    stbool status = STBOOL_SUCCESS;

    if (data_type_is_ascii(&bexpr->binary.lhs_expr->dt))
    {
        DECLARE_DT_NSTR(lhs_dt_nstr, &bexpr->binary.lhs_expr->dt);
        NLOGF_FLC_TOK(
            LOG_ERROR,
            bexpr->srctok,
            FMT_BOP_ILLEGAL_USE_OF_LHS_DATA_TYPE
            " " STR_EXPECTED_X_TYPE("non-ascii"),
            lhs_dt_nstr.data,
            token_type_to_cstr(bexpr->srctok->tag));
        status = STBOOL_FAILURE;
    }
    if (data_type_is_ascii(&bexpr->binary.rhs_expr->dt))
    {
        DECLARE_DT_NSTR(rhs_dt_nstr, &bexpr->binary.rhs_expr->dt);
        NLOGF_FLC_TOK(
            LOG_ERROR,
            bexpr->srctok,
            FMT_BOP_ILLEGAL_USE_OF_RHS_DATA_TYPE
            " " STR_EXPECTED_X_TYPE("non-ascii"),
            rhs_dt_nstr.data,
            token_type_to_cstr(bexpr->srctok->tag));
        status = STBOOL_FAILURE;
    }

    return status;
}

//! Check if the LHS of the provided assignment expression is an lvalue and the
//! RHS of the provided expression is an rvalue, i.e. the LHS and RHS of the
//! expression have valid value categories for an assignment expression.
//! @return
//!     #STBOOL_SUCCESS if the LHS and RHS are compatible.
//! @return
//!     #STBOOL_FAILURE otherwise.
//! @note
//!      Assumes @p bexpr is the binary assignment expression.
static stbool validate_assignment_value_categories(struct expr const* bexpr)
{
    nassert(NULL != bexpr->srctok);
    nassert(expr_is_binary(bexpr));

    stbool status = STBOOL_SUCCESS;

    if (!expr_is_lvalue(bexpr->binary.lhs_expr))
    {
        NLOGF_FLC_TOK(
            LOG_ERROR,
            bexpr->srctok,
            "Left hand side of assignment expression is not an lvalue.");
        status = STBOOL_FAILURE;
    }
    if (!expr_is_rvalue(bexpr->binary.rhs_expr))
    {
        NLOGF_FLC_TOK(
            LOG_ERROR,
            bexpr->srctok,
            "Right hand side of assignment expression is not an rvalue.");
        status = STBOOL_FAILURE;
    }

    return status;
}

//! Check if the LHS of the provided assignment expression yields a mutable
//! value.
//! @return
//!     #STBOOL_SUCCESS if the LHS is mutable.
//! @return
//!     #STBOOL_FAILURE otherwise.
//! @note
//!      Assumes @p bexpr is the binary assignment expression.
static stbool validate_assignment_mutability_constraints(
    struct expr const* bexpr)
{
    nassert(NULL != bexpr->srctok);
    nassert(expr_is_binary(bexpr));

    if (!bexpr->binary.lhs_expr->dt.qualifiers.is_mut)
    {
        NLOGF_FLC_TOK(
            LOG_ERROR,
            bexpr->srctok,
            "Left hand side of assignment expression is not mutable.");
        return STBOOL_FAILURE;
    }

    return STBOOL_SUCCESS;
}

//! Check if the RHS of the provided assignment expression can be implicitly
//! converted to the LHS of the assignment expression.
//! @return
//!     #STBOOL_SUCCESS if the RHS data type can be converted to the LHS data
//!     type.
//! @return
//!     #STBOOL_FAILURE otherwise.
//! @note
//!      Assumes @p bexpr is the binary assignment expression.
static stbool validate_assignment_implicit_conversion(struct expr const* bexpr)
{
    nassert(NULL != bexpr->srctok);
    nassert(expr_is_binary(bexpr));

    if (!data_type_can_implicitly_convert(
            &bexpr->binary.rhs_expr->dt, &bexpr->binary.lhs_expr->dt))
    {
        DECLARE_DT_NSTR(lhs_dt_nstr, &bexpr->binary.lhs_expr->dt);
        DECLARE_DT_NSTR(rhs_dt_nstr, &bexpr->binary.rhs_expr->dt);
        NLOGF_FLC_TOK(
            LOG_ERROR,
            bexpr->srctok,
            FMT_CANNOT_IMPLICITLY_CONVERT_RHS_TO_LHS,
            rhs_dt_nstr.data,
            lhs_dt_nstr.data);
        return STBOOL_FAILURE;
    }

    return STBOOL_SUCCESS;
}

//! Check if the LHS and RHS side of the provided binary expression are
//! compatible for a bit shift expression expression.
//! @return
//!     #STBOOL_SUCCESS if the LHS and RHS are compatible.
//! @return
//!     #STBOOL_FAILURE otherwise.
//! @note
//!     Assumes @p bexpr is binary.
static stbool validate_bit_shift_compatability(struct expr const* bexpr)
{
    nassert(NULL != bexpr->srctok);
    nassert(expr_is_binary(bexpr));

    return data_type_is_integer(&bexpr->binary.lhs_expr->dt)
        && data_type_is_integer_unsigned(&bexpr->binary.rhs_expr->dt);
}

//! Check if the LHS and RHS side of the provided binary expression are
//! compatible for an arithmetic binary expression acting on base types with the
//! same data type class. For example the binary addition expressions
//! ````
//! let foo : s64;
//! let bar : s64;
//! foo + bar;
//! ````
//! and
//! ````
//! let foo : s64;
//! let bar : mut s64;
//! foo + bar;
//! ````
//! are compatible but the binary addition expression in
//! ````
//! let foo : s8;
//! let bar : s64;
//! foo + bar;
//! ````
//! is not.
//! @return
//!     #STBOOL_SUCCESS if the LHS and RHS are compatible.
//! @return
//!     #STBOOL_FAILURE otherwise.
//! @note
//!     Assumes @p bexpr is binary.
//! @note
//!     Arithmetic operators are not allowed for `bool` types.
static stbool validate_type_equal_arithmetic_compatability(
    struct expr const* bexpr)
{
    nassert(NULL != bexpr->srctok);
    nassert(expr_is_binary(bexpr));

    if (!validate_xhs_are_base(bexpr))
    {
        return STBOOL_FAILURE;
    }

    if (!validate_xhs_are_not_void(bexpr))
    {
        return STBOOL_FAILURE;
    }

    if (!validate_xhs_are_not_bool(bexpr))
    {
        return STBOOL_FAILURE;
    }

    if (!validate_xhs_are_not_ascii(bexpr))
    {
        return STBOOL_FAILURE;
    }

    if (bexpr->binary.rhs_expr->dt.tag != bexpr->binary.lhs_expr->dt.tag)
    {
        DECLARE_DT_NSTR(lhs_dt_nstr, &bexpr->binary.lhs_expr->dt);
        DECLARE_DT_NSTR(rhs_dt_nstr, &bexpr->binary.rhs_expr->dt);
        NLOGF_FLC_TOK(
            LOG_ERROR,
            bexpr->srctok,
            FMT_BOP_MISMATCHED_DATA_TYPES,
            lhs_dt_nstr.data,
            rhs_dt_nstr.data);
        return STBOOL_FAILURE;
    }

    return STBOOL_SUCCESS;
}

//! Check if the LHS and RHS side of the provided binary expression are
//! compatible for a bitwise binary expression acting on base types with the
//! same data type class. Bitwise binary expressions are valid only for integer
//! types and the bool type.
//! @return
//!     #STBOOL_SUCCESS if the LHS and RHS are compatible.
//! @return
//!     #STBOOL_FAILURE otherwise.
//! @note
//!     Assumes @p bexpr is binary.
static stbool validate_type_equal_bitwise_compatability(
    struct expr const* bexpr)
{
    nassert(NULL != bexpr->srctok);
    nassert(expr_is_binary(bexpr));

    if (!validate_xhs_are_base(bexpr))
    {
        return STBOOL_FAILURE;
    }

    if (!validate_xhs_are_not_void(bexpr))
    {
        return STBOOL_FAILURE;
    }

    if (!validate_xhs_are_not_floating_point(bexpr))
    {
        return STBOOL_FAILURE;
    }

    if (!validate_xhs_are_not_bool(bexpr))
    {
        return STBOOL_FAILURE;
    }

    if (!validate_xhs_are_not_ascii(bexpr))
    {
        return STBOOL_FAILURE;
    }

    if (bexpr->binary.rhs_expr->dt.tag != bexpr->binary.lhs_expr->dt.tag)
    {
        nassert(
            data_type_is_integer(&bexpr->binary.lhs_expr->dt)
            && data_type_is_integer(&bexpr->binary.rhs_expr->dt));
        DECLARE_DT_NSTR(lhs_dt_nstr, &bexpr->binary.lhs_expr->dt);
        DECLARE_DT_NSTR(rhs_dt_nstr, &bexpr->binary.rhs_expr->dt);
        NLOGF_FLC_TOK(
            LOG_ERROR,
            bexpr->srctok,
            FMT_BOP_MISMATCHED_DATA_TYPES,
            lhs_dt_nstr.data,
            rhs_dt_nstr.data);
        return STBOOL_FAILURE;
    }

    return STBOOL_SUCCESS;
}

//! Check if the LHS and RHS side of the provided binary expression are
//! compatible for a relational binary expression.
//! @return
//!     #STBOOL_SUCCESS if the LHS and RHS are compatible.
//! @return
//!     #STBOOL_FAILURE otherwise.
//! @note
//!     Assumes @p bexpr is binary.
static stbool validate_type_equal_relational_compatability(
    struct expr const* bexpr)
{
    nassert(NULL != bexpr->srctok);
    nassert(expr_is_binary(bexpr));

    if (!validate_xhs_are_not_void(bexpr))
    {
        return STBOOL_FAILURE;
    }

    if (data_type_is_base(&bexpr->binary.lhs_expr->dt)
        || data_type_is_base(&bexpr->binary.rhs_expr->dt))
    {
        if (!validate_xhs_are_base(bexpr))
        {
            return STBOOL_FAILURE;
        }

        if (bexpr->binary.rhs_expr->dt.tag != bexpr->binary.lhs_expr->dt.tag)
        {
            DECLARE_DT_NSTR(lhs_dt_nstr, &bexpr->binary.lhs_expr->dt);
            DECLARE_DT_NSTR(rhs_dt_nstr, &bexpr->binary.rhs_expr->dt);
            NLOGF_FLC_TOK(
                LOG_ERROR,
                bexpr->srctok,
                FMT_BOP_MISMATCHED_DATA_TYPES,
                lhs_dt_nstr.data,
                rhs_dt_nstr.data);
            return STBOOL_FAILURE;
        }
    }
    else if (
        data_type_is_pointer(&bexpr->binary.lhs_expr->dt)
        || data_type_is_pointer(&bexpr->binary.rhs_expr->dt))
    {
        if (!validate_xhs_are_pointer(bexpr))
        {
            return STBOOL_FAILURE;
        }

        if (!(data_type_can_implicitly_convert(
                  &bexpr->binary.lhs_expr->dt, &bexpr->binary.rhs_expr->dt)
              || data_type_can_implicitly_convert(
                     &bexpr->binary.rhs_expr->dt, &bexpr->binary.lhs_expr->dt)))
        {
            DECLARE_DT_NSTR(lhs_dt_nstr, &bexpr->binary.lhs_expr->dt);
            DECLARE_DT_NSTR(rhs_dt_nstr, &bexpr->binary.rhs_expr->dt);
            NLOGF_FLC_TOK(
                LOG_ERROR,
                bexpr->srctok,
                FMT_BOP_MISMATCHED_DATA_TYPES,
                lhs_dt_nstr.data,
                rhs_dt_nstr.data);
            return STBOOL_FAILURE;
        }
    }
    else if (data_type_is_array(&bexpr->binary.lhs_expr->dt))
    {
        DECLARE_DT_NSTR(lhs_dt_nstr, &bexpr->binary.lhs_expr->dt);
        NLOGF_FLC_TOK(
            LOG_ERROR,
            bexpr->srctok,
            FMT_BOP_ILLEGAL_USE_OF_LHS_DATA_TYPE,
            lhs_dt_nstr.data,
            token_type_to_cstr(bexpr->srctok->tag));
        return STBOOL_FAILURE;
    }
    else if (data_type_is_array(&bexpr->binary.rhs_expr->dt))
    {
        DECLARE_DT_NSTR(rhs_dt_nstr, &bexpr->binary.rhs_expr->dt);
        NLOGF_FLC_TOK(
            LOG_ERROR,
            bexpr->srctok,
            FMT_BOP_ILLEGAL_USE_OF_RHS_DATA_TYPE,
            rhs_dt_nstr.data,
            token_type_to_cstr(bexpr->srctok->tag));
        return STBOOL_FAILURE;
    }
    else
    {
        NPANIC_UNEXPECTED_CTRL_FLOW();
    }

    return STBOOL_SUCCESS;
}

//! Check if the LHS and RHS side of the provided binary expression are
//! compatible for a logical binary expression.
//! @return
//!     #STBOOL_SUCCESS if the LHS and RHS are compatible.
//! @return
//!     #STBOOL_FAILURE otherwise.
//! @note
//!     Assumes @p bexpr is binary.
static stbool validate_type_equal_logical_compatability(
    struct expr const* bexpr)
{
    nassert(NULL != bexpr->srctok);
    nassert(expr_is_binary(bexpr));

    stbool status = STBOOL_SUCCESS;
    if (!data_type_is_bool(&bexpr->binary.lhs_expr->dt))
    {
        NLOGF_FLC_TOK(
            LOG_ERROR,
            bexpr->srctok,
            "Left hand side of logical expression must be a bool type.");
        status = STBOOL_FAILURE;
    }
    if (!data_type_is_bool(&bexpr->binary.rhs_expr->dt))
    {
        NLOGF_FLC_TOK(
            LOG_ERROR,
            bexpr->srctok,
            "Right hand side of logical expression must be a bool type.");
        status = STBOOL_FAILURE;
    }

    return status;
}

static stbool semantic_analysis_expr_binary(
    struct semantic_analysis_context* ctx, struct expr* expr)
{
    nassert(NULL != expr->srctok);
    nassert(expr_is_binary(expr));

    stbool status = STBOOL_SUCCESS;

    // Perform semantic analysis on the operands of the binary operation.
    // Left hand side.
    if (!semantic_analysis_expr(ctx, expr->binary.lhs_expr))
    {
        status = STBOOL_FAILURE;
    }
    // Right hand side.
    switch (expr->tag)
    {
    case EXPR_BINARY_ASSIGN:      /* intentional fallthrough */
    case EXPR_BINARY_LOGICAL_OR:  /* intentional fallthrough */
    case EXPR_BINARY_LOGICAL_AND: /* intentional fallthrough */
    case EXPR_BINARY_BITWISE_OR:  /* intentional fallthrough */
    case EXPR_BINARY_BITWISE_XOR: /* intentional fallthrough */
    case EXPR_BINARY_BITWISE_AND: /* intentional fallthrough */
    case EXPR_BINARY_REL_EQ:      /* intentional fallthrough */
    case EXPR_BINARY_REL_NE:      /* intentional fallthrough */
    case EXPR_BINARY_REL_LT:      /* intentional fallthrough */
    case EXPR_BINARY_REL_GT:      /* intentional fallthrough */
    case EXPR_BINARY_REL_LE:      /* intentional fallthrough */
    case EXPR_BINARY_REL_GE:      /* intentional fallthrough */
    case EXPR_BINARY_SHIFT_L:     /* intentional fallthrough */
    case EXPR_BINARY_SHIFT_R:     /* intentional fallthrough */
    case EXPR_BINARY_PLUS:        /* intentional fallthrough */
    case EXPR_BINARY_MINUS:       /* intentional fallthrough */
    case EXPR_BINARY_PTR_DIFF:    /* intentional fallthrough */
    case EXPR_BINARY_PTR_PLUS:    /* intentional fallthrough */
    case EXPR_BINARY_PTR_MINUS:   /* intentional fallthrough */
    case EXPR_BINARY_MULT:        /* intentional fallthrough */
    case EXPR_BINARY_DIV:
    {
        if (!semantic_analysis_expr(ctx, expr->binary.rhs_expr))
        {
            status = STBOOL_FAILURE;
        }
    }
    break;

    case EXPR_BINARY_CAST:
    {
        if (!semantic_analysis_data_type(ctx, expr->binary.rhs_dt))
        {
            status = STBOOL_FAILURE;
        }
    }
    break;

    default:
        NPANIC_DFLT_CASE();
    }
    if (!status)
    {
        return STBOOL_FAILURE;
    }

    // Perform operation specific semantic analysis on the binary expression as
    // a whole.
    switch (expr->tag)
    {
    case EXPR_BINARY_ASSIGN:
    {
        if (!validate_assignment_value_categories(expr))
        {
            return STBOOL_FAILURE;
        }
        if (!validate_assignment_mutability_constraints(expr))
        {
            return STBOOL_FAILURE;
        }
        if (!validate_assignment_implicit_conversion(expr))
        {
            return STBOOL_FAILURE;
        }

        // Assignment expressions always evaluate to a `void` rvalue.
        expr->dt.tag = DT_VOID;
        expr->valcat = VALCAT_RVALUE;
        expr->is_constexpr = false;
    }
    break;

    case EXPR_BINARY_LOGICAL_OR: /* intentional fallthrough */
    case EXPR_BINARY_LOGICAL_AND:
    {
        if (!validate_type_equal_logical_compatability(expr))
        {
            return STBOOL_FAILURE;
        }

        // Logical expressions always return a boolean value.
        struct data_type ATTR_DEFER_FINI(data_type_fini) dt;
        data_type_init(&dt);
        dt.tag = DT_BOOL;
        data_type_assign(&expr->dt, &dt, true);
        expr->valcat = VALCAT_RVALUE;

        // C treats Boolean values as integers, thus a logical operation is an
        // instance of an arithmetic constant expression.
        expr->is_constexpr = expr->binary.lhs_expr->is_constexpr
            && expr->binary.rhs_expr->is_constexpr;
    }
    break;

    case EXPR_BINARY_BITWISE_OR:  /* intentional fallthrough */
    case EXPR_BINARY_BITWISE_XOR: /* intentional fallthrough */
    case EXPR_BINARY_BITWISE_AND:
    {
        if (!validate_type_equal_bitwise_compatability(expr))
        {
            return STBOOL_FAILURE;
        }

        // LHS data type chosen arbitrarily.
        data_type_assign(&expr->dt, &expr->binary.lhs_expr->dt, true);
        expr->valcat = VALCAT_RVALUE;

        // Arithmetic constant expression.
        expr->is_constexpr = expr->binary.lhs_expr->is_constexpr
            && expr->binary.rhs_expr->is_constexpr;
    }
    break;

    case EXPR_BINARY_SHIFT_L: /* intentional fallthrough */
    case EXPR_BINARY_SHIFT_R:
    {
        if (!validate_bit_shift_compatability(expr))
        {
            return STBOOL_FAILURE;
        }

        // Bit-shift expressions always take on the type of the LHS.
        data_type_assign(&expr->dt, &expr->binary.lhs_expr->dt, true);
        expr->valcat = VALCAT_RVALUE;

        // Arithmetic constant expression.
        expr->is_constexpr = expr->binary.lhs_expr->is_constexpr
            && expr->binary.rhs_expr->is_constexpr;
    }
    break;

    case EXPR_BINARY_PTR_PLUS: /* intentional fallthrough */
    case EXPR_BINARY_PTR_MINUS:
    {
        if (!data_type_is_pointer(&expr->binary.lhs_expr->dt))
        {
            DECLARE_DT_NSTR(lhs_dt_nstr, &expr->binary.lhs_expr->dt);
            NLOGF_FLC_TOK(
                LOG_ERROR,
                expr->srctok,
                FMT_BOP_ILLEGAL_USE_OF_LHS_DATA_TYPE
                " " STR_EXPECTED_X_TYPE("pointer"),
                lhs_dt_nstr.data,
                token_type_to_cstr(expr->srctok->tag));
            return STBOOL_FAILURE;
        }
        if (!data_type_is_integer(&expr->binary.rhs_expr->dt))
        {
            DECLARE_DT_NSTR(rhs_dt_nstr, &expr->binary.rhs_expr->dt);
            NLOGF_FLC_TOK(
                LOG_ERROR,
                expr->srctok,
                FMT_BOP_ILLEGAL_USE_OF_RHS_DATA_TYPE
                " " STR_EXPECTED_X_TYPE("integer"),
                rhs_dt_nstr.data,
                token_type_to_cstr(expr->srctok->tag));
            return STBOOL_FAILURE;
        }

        // Typeof the result should be the lhs pointer type.
        data_type_assign(&expr->dt, &expr->binary.lhs_expr->dt, true);
        expr->valcat = VALCAT_RVALUE;

        // Constant address offset.
        // ISO/IEC 9899:TC3 - Section 6.6.7
        // _More latitude is permitted for constant expressions in initializers.
        // Such a constant expression shall be, or evaluate to, one of the
        // following: ... an address constant for an object type plus or minus
        // an integer constant expression. ..._
        expr->is_constexpr = expr->binary.lhs_expr->is_constexpr
            && expr->binary.rhs_expr->is_constexpr;
    }
    break;

    case EXPR_BINARY_PLUS:  /* intentional fallthrough */
    case EXPR_BINARY_MINUS: /* intentional fallthrough */
    case EXPR_BINARY_MULT:  /* intentional fallthrough */
    case EXPR_BINARY_DIV:
    {
        if (!validate_type_equal_arithmetic_compatability(expr))
        {
            return STBOOL_FAILURE;
        }

        // LHS chosen arbitrarily.
        data_type_assign(&expr->dt, &expr->binary.lhs_expr->dt, true);
        expr->valcat = VALCAT_RVALUE;

        // Arithmetic constant expression.
        expr->is_constexpr = expr->binary.lhs_expr->is_constexpr
            && expr->binary.rhs_expr->is_constexpr;
    }
    break;

    case EXPR_BINARY_PTR_DIFF:
    {
        if (!data_type_is_pointer(&expr->binary.lhs_expr->dt))
        {
            DECLARE_DT_NSTR(lhs_dt_nstr, &expr->binary.lhs_expr->dt);
            NLOGF_FLC_TOK(
                LOG_ERROR,
                expr->srctok,
                FMT_BOP_ILLEGAL_USE_OF_LHS_DATA_TYPE
                " " STR_EXPECTED_X_TYPE("pointer"),
                lhs_dt_nstr.data,
                token_type_to_cstr(expr->srctok->tag));
            return STBOOL_FAILURE;
        }

        if (!data_type_is_pointer(&expr->binary.rhs_expr->dt))
        {
            DECLARE_DT_NSTR(rhs_dt_nstr, &expr->binary.rhs_expr->dt);
            NLOGF_FLC_TOK(
                LOG_ERROR,
                expr->srctok,
                FMT_BOP_ILLEGAL_USE_OF_RHS_DATA_TYPE
                " " STR_EXPECTED_X_TYPE("pointer"),
                rhs_dt_nstr.data,
                token_type_to_cstr(expr->srctok->tag));
            return STBOOL_FAILURE;
        }

        // Pointer subtraction returns `u`, the N-lang equivalent to
        // `ptrdiff_t`.
        expr->dt.tag = DT_U;
        expr->valcat = VALCAT_RVALUE;
        expr->is_constexpr = false;
    }
    break;

    case EXPR_BINARY_REL_EQ: /* intentional fallthrough */
    case EXPR_BINARY_REL_NE: /* intentional fallthrough */
    case EXPR_BINARY_REL_LT: /* intentional fallthrough */
    case EXPR_BINARY_REL_LE: /* intentional fallthrough */
    case EXPR_BINARY_REL_GT: /* intentional fallthrough */
    case EXPR_BINARY_REL_GE:
    {
        if (!validate_type_equal_relational_compatability(expr))
        {
            return STBOOL_FAILURE;
        }

        // Relational expressions always return a boolean value.
        struct data_type dt;
        data_type_init(&dt);
        dt.tag = DT_BOOL;
        data_type_assign(&expr->dt, &dt, true);
        data_type_fini(&dt);
        expr->valcat = VALCAT_RVALUE;

        // C treats Boolean values as integers, thus a logical operation is an
        // instance of an arithmetic constant expression.
        expr->is_constexpr = expr->binary.lhs_expr->is_constexpr
            && expr->binary.rhs_expr->is_constexpr;
    }
    break;

    case EXPR_BINARY_CAST:
    {
        struct data_type const* const lhs_dt = &expr->binary.lhs_expr->dt;
        struct data_type const* const rhs_dt = expr->binary.rhs_dt;
        if (!data_type_can_explicitly_convert(lhs_dt, rhs_dt))
        {
            DECLARE_DT_NSTR(lhs_dt_nstr, lhs_dt);
            DECLARE_DT_NSTR(rhs_dt_nstr, rhs_dt);
            NLOGF_FLC_TOK(
                LOG_ERROR,
                expr->srctok,
                "Cannot explicitly convert left hand side data type '%s' to "
                "right hand side data type '%s'.",
                lhs_dt_nstr.data,
                rhs_dt_nstr.data);
            return STBOOL_FAILURE;
        }

        data_type_assign(&expr->dt, rhs_dt, true);
        expr->valcat = VALCAT_RVALUE;
        expr->is_constexpr = expr->binary.lhs_expr->is_constexpr;
    }
    break;

    default:
        NPANIC_DFLT_CASE();
    }

    return STBOOL_SUCCESS;
}

stbool semantic_analysis_expr(
    struct semantic_analysis_context* ctx, struct expr* expr)
{
    nassert(NULL != expr->srctok);

    if (expr_is_primary(expr))
    {
        if (!semantic_analysis_expr_primary(ctx, expr))
        {
            return STBOOL_FAILURE;
        }
    }
    else if (expr_is_postfix(expr))
    {
        if (!semantic_analysis_expr_postfix(ctx, expr))
        {
            return STBOOL_FAILURE;
        }
        nassert(EXPR_PRIMARY_LITERAL == expr->tag ||
            DT_UNSET != expr->postfix.lhs->dt.tag);
    }
    else if (expr_is_unary(expr))
    {
        if (!semantic_analysis_expr_unary(ctx, expr))
        {
            return STBOOL_FAILURE;
        }
        nassert(
            (EXPR_UNARY_COUNTOF_DT == expr->tag
             && DT_UNSET != expr->unary.rhs_dt->tag)
            || (EXPR_UNARY_SIZEOF_DT == expr->tag
                && DT_UNSET != expr->unary.rhs_dt->tag)
            || DT_UNSET != expr->unary.rhs_expr->dt.tag);
    }
    else if (expr_is_binary(expr))
    {
        if (!semantic_analysis_expr_binary(ctx, expr))
        {
            return STBOOL_FAILURE;
        }
        nassert(DT_UNSET != expr->binary.lhs_expr->dt.tag);
        nassert(
            (EXPR_BINARY_CAST == expr->tag
             && DT_UNSET != expr->binary.rhs_dt->tag)
            || DT_UNSET != expr->binary.rhs_expr->dt.tag);
    }
    else
    {
        NPANIC_UNEXPECTED_CTRL_FLOW();
    }
    nassert(DT_UNSET != expr->dt.tag);

    return STBOOL_SUCCESS;
}

//! Error message format string used for the semantic analysis of declarations.
//!{
static char const* const STR_EXPORT_OF_NON_GLOBAL_DECLARATION =
    "Export of non-global declaration.";
static char const* const STR_INTRODUCE_OF_NON_GLOBAL_DECLARATION =
    "Introduce of non-global declaration.";
//!}

stbool semantic_analysis_variable_decl(
    struct semantic_analysis_context* ctx, struct variable_decl* variable_decl)
{
    nassert(NULL != variable_decl->srctok);

    if (!semantic_analysis_identifier(ctx, &variable_decl->id))
    {
        return STBOOL_FAILURE;
    }
    if (!semantic_analysis_data_type(ctx, &variable_decl->dt))
    {
        return STBOOL_FAILURE;
    }

    if (variable_decl->is_export && !scope_is_global(ctx->curr_scope))
    {
        NLOGF_FLC_TOK(
            LOG_ERROR,
            variable_decl->srctok,
            STR_EXPORT_OF_NON_GLOBAL_DECLARATION);
        return STBOOL_FAILURE;
    }
    if (variable_decl->is_introduce && !scope_is_global(ctx->curr_scope))
    {
        NLOGF_FLC_TOK(
            LOG_ERROR,
            variable_decl->srctok,
            STR_INTRODUCE_OF_NON_GLOBAL_DECLARATION);
        return STBOOL_FAILURE;
    }

    struct symbol symbol;
    symbol_init_variable(&symbol, variable_decl);
    struct symbol* added =
        symbol_table_insert(ctx->curr_scope->symbol_table, &symbol);
    if (NULL == added)
    {
        struct identifier* id = &variable_decl->id;
        struct symbol* existing_symbol = symbol_table_find(
            ctx->curr_scope->symbol_table, id->start, id->length);
        nassert(NULL != existing_symbol);
        log_identifier_redeclaration(&variable_decl->id, existing_symbol->id);
        symbol_fini(&symbol);
        return STBOOL_FAILURE;
    }

    if (variable_decl->is_export)
    {
        module_add_latest_global_symbol_to_exports(ctx->module);
    }

    if (NULL != variable_decl->def)
    {
        if (!semantic_analysis_expr(ctx, variable_decl->def))
        {
            return STBOOL_FAILURE;
        }
        if (!data_type_can_implicitly_convert__variable_init(
                &variable_decl->def->dt, &variable_decl->dt))
        {
            DECLARE_DT_NSTR(lhs_nstr, &variable_decl->def->dt);
            DECLARE_DT_NSTR(rhs_nstr, &variable_decl->dt);
            NLOGF_FLC_TOK(
                LOG_ERROR,
                variable_decl->srctok,
                FMT_CANNOT_IMPLICITLY_CONVERT_RHS_TO_LHS,
                rhs_nstr.data,
                lhs_nstr.data);
            return STBOOL_FAILURE;
        }

        if (scope_is_global(ctx->curr_scope)
            && !variable_decl->def->is_constexpr)
        {
            NLOGF_FLC_TOK(
                LOG_ERROR,
                variable_decl->def->srctok,
                "Non-constexpr initializer not allowed at global scope.");
            return STBOOL_FAILURE;
        }
    }

    return STBOOL_SUCCESS;
}

stbool semantic_analysis_function_decl(
    struct semantic_analysis_context* ctx, struct function_decl* function_decl)
{
    nassert(NULL != function_decl->srctok);
    nassert(scope_is_global(ctx->curr_scope));

    struct data_type* const function_dt = &function_decl->dt;
    size_t const param_ids_length = narr_length(function_decl->param_ids);
    size_t const param_dts_length =
        narr_length(function_dt->func_sig->param_dts);
    nassert(param_ids_length == param_dts_length);

    //// Params and return type.
    if (!semantic_analysis_func_sig(ctx, function_dt->func_sig))
    {
        return STBOOL_FAILURE;
    }
    // Multiple parameters with the same name cannot exist within a function
    // definition by the one definition rule.
    // Since a function signature itself has no scope to check identifiers
    // against, every identifier is checked to make sure it has a unique name by
    // comparing it to every other identifier.
    for (size_t i = 0; i < param_ids_length; ++i)
    {
        for (size_t j = i + 1; j < param_ids_length; ++j)
        {
            if ((function_decl->param_ids[i].length
                 == function_decl->param_ids[j].length)
                && cstr_n_eq(
                       function_decl->param_ids[i].start,
                       function_decl->param_ids[j].start,
                       function_decl->param_ids[i].length))
            {
                return STBOOL_FAILURE;
            }
        }
    }
    // Currently there is no concept of passing an array to a function.
    // This code checks for array parameters and will signal an error if one
    // exists.
    //! @todo
    //!     Currently we cannot import a function from C that takes an array
    //!     reference of constant length.
    //!     It should be discussed whether we do want to allow this, and if so
    //!     appropriate action should be taken here.
    for (size_t i = 0; i < param_dts_length; ++i)
    {
        struct data_type const* dt = &function_dt->func_sig->param_dts[i];
        if (data_type_is_array(dt))
        {
            NLOGF_FLC_TOK(
                LOG_ERROR,
                dt->srctok,
                "Functions with array parameters are not yet supported.");
            return STBOOL_FAILURE;
        }
    }

    //// Function name
    struct symbol symbol;
    symbol_init_function(&symbol, function_decl);

    struct symbol* existing_symbol = symbol_table_find(
        ctx->curr_scope->symbol_table,
        function_decl->id.start,
        function_decl->id.length);
    if (NULL == existing_symbol)
    {
        // Add the symbol introduced by the function declaration.
        existing_symbol =
            symbol_table_insert(ctx->curr_scope->symbol_table, &symbol);
        nassert(NULL != existing_symbol);

        if (function_decl->is_export)
        {
            module_add_latest_global_symbol_to_exports(ctx->module);
        }
    }
    else
    {
        symbol_fini(&symbol);
        switch (existing_symbol->tag)
        {
        case SYMBOL_VARIABLE: /* intentional fallthrough */
        case SYMBOL_STRUCT:   /* intentional fallthrough */
        case SYMBOL_ALIAS:
            log_identifier_redeclaration(
                &function_decl->id, existing_symbol->id);
            return STBOOL_FAILURE;

        case SYMBOL_FUNCTION:
            // Redeclaration of a function is fine as long as the function is
            // not yet defined.
            if (existing_symbol->is_defined)
            {
                log_identifier_redeclaration(
                    &function_decl->id, existing_symbol->id);
                return STBOOL_FAILURE;
            }
            // This declaration should automatically inherit a set export
            // attribute from its previous declaration.
            if (existing_symbol->is_export)
            {
                function_decl->is_export = true;
            }
            break;

        default:
            NPANIC_DFLT_CASE();
        }
    }

    if (function_decl->is_export && !scope_is_global(ctx->curr_scope))
    {
        NLOGF_FLC_TOK(
            LOG_ERROR,
            function_decl->srctok,
            STR_EXPORT_OF_NON_GLOBAL_DECLARATION);
        return STBOOL_FAILURE;
    }
    if (function_decl->is_introduce && !scope_is_global(ctx->curr_scope))
    {
        NLOGF_FLC_TOK(
            LOG_ERROR,
            function_decl->srctok,
            STR_INTRODUCE_OF_NON_GLOBAL_DECLARATION);
        return STBOOL_FAILURE;
    }

    //// Function definition
    if (NULL != function_decl->def)
    {
        existing_symbol->is_defined = true;

        populate_scope_metadata(ctx, function_decl->def);
        typeof(ctx->curr_scope) save_curr_scope = ctx->curr_scope;
        typeof(ctx->curr_func) save_curr_func = ctx->curr_func;
        ctx->curr_scope = function_decl->def;
        ctx->curr_func = function_dt->func_sig;

        stbool status = STBOOL_SUCCESS;

        // Add all of the parameters as variables within the body of the
        // function.
        for (size_t i = 0; i < param_ids_length; ++i)
        {
            struct identifier* const param_id = &function_decl->param_ids[i];
            struct data_type* const param_dt =
                &function_dt->func_sig->param_dts[i];

            struct symbol* existing_symbol = symbol_table_find(
                ctx->curr_scope->symbol_table,
                param_id->start,
                param_id->length);
            if (NULL != existing_symbol)
            {
                log_identifier_redeclaration(param_id, existing_symbol->id);
                return STBOOL_FAILURE;
            }

            //! @todo
            //!     The fact that the symbol built here means that something is
            //!     messed up in the way we initialize symbols.
            //!     We should take a deep look at the way symbols are created
            //!     and added and see if we can clean this up.
            struct symbol symbol;
            symbol_init(&symbol);
            symbol.tag = SYMBOL_VARIABLE;
            symbol.id = param_id;
            symbol.dt = param_dt;
            symbol.srctok = param_id->srctok;
            if (NULL
                == symbol_table_insert(ctx->curr_scope->symbol_table, &symbol))
            {
                struct symbol* existing_symbol = symbol_table_find(
                    ctx->curr_scope->symbol_table,
                    param_id->start,
                    param_id->length);
                nassert(NULL != existing_symbol);
                log_identifier_redeclaration(param_id, existing_symbol->id);
                symbol_fini(&symbol);
                return STBOOL_FAILURE;
            }
        }
        if (STBOOL_SUCCESS != status)
        {
            return STBOOL_FAILURE;
        }

        if (!semantic_analysis_scope(ctx, function_decl->def))
        {
            return STBOOL_FAILURE;
        }

        ctx->curr_scope = save_curr_scope;
        ctx->curr_func = save_curr_func;
    }

    return STBOOL_SUCCESS;
}

stbool semantic_analysis_struct_decl(
    struct semantic_analysis_context* ctx, struct struct_decl* struct_decl)
{
    nassert(NULL != struct_decl->srctok);
    nassert(scope_is_global(ctx->curr_scope));

    if (!semantic_analysis_identifier(ctx, &struct_decl->id))
    {
        return STBOOL_FAILURE;
    }

    struct symbol new_symbol;
    symbol_init_struct(&new_symbol, struct_decl);

    struct symbol* symbol =
        symbol_table_insert(ctx->curr_scope->symbol_table, &new_symbol);
    if (NULL != symbol)
    {
        if (struct_decl->is_export)
        {
            module_add_latest_global_symbol_to_exports(ctx->module);
        }

        if (NULL != struct_decl->def)
        {
            if (!semantic_analysis_struct_def(ctx, struct_decl->def))
            {
                return STBOOL_FAILURE;
            }
            symbol->dt->struct_ref = struct_decl;
            symbol->is_defined = true;
        }
    }
    else
    {
        symbol_fini(&new_symbol);
        struct identifier* const struct_decl_id = &struct_decl->id;
        symbol = symbol_table_find(
            ctx->curr_scope->symbol_table,
            struct_decl_id->start,
            struct_decl_id->length);
        switch (symbol->tag)
        {
        case SYMBOL_VARIABLE: /* intentional fallthrough */
        case SYMBOL_FUNCTION: /* intentional fallthrough */
        case SYMBOL_ALIAS:
            log_identifier_redeclaration(&struct_decl->id, symbol->id);
            return STBOOL_FAILURE;
        case SYMBOL_STRUCT:
            // Redeclaration of a struct is fine as long as the struct is not
            // yet defined.
            if (symbol->is_defined)
            {
                log_identifier_redeclaration(&struct_decl->id, symbol->id);
                return STBOOL_FAILURE;
            }
            // This declaration should automatically inherit a set export
            // attribute from its previous declaration.
            if (symbol->is_export)
            {
                struct_decl->is_export = true;
            }
            break;
        default:
            NPANIC_DFLT_CASE();
        }

        if (NULL != struct_decl->def)
        {
            if (!semantic_analysis_struct_def(ctx, struct_decl->def))
            {
                return STBOOL_FAILURE;
            }
            // Add the definition of this struct to the symbol.
            nassert(NULL != symbol);
            nassert(NULL != symbol->dt);
            nassert(NULL == symbol->dt->struct_ref->def);
            symbol->dt->struct_ref = struct_decl;
            symbol->is_defined = true;
        }
    }

    return STBOOL_SUCCESS;
}

//! helper to check if the variant has a definition, and if so, semantic
//! semantic analyze that.
static stbool validate_variant(struct semantic_analysis_context* ctx,
    struct variant_decl* variant)
{
    if(variant->def != NULL)
    {
        struct variant_def* def = variant->def;
        if(!semantic_analysis_variant_def(ctx, def))
        {
            return STBOOL_FAILURE;
        }

        // TODO this parameters.
    }

    return STBOOL_SUCCESS;
}

stbool semantic_analysis_variant_decl(
    struct semantic_analysis_context* ctx, struct variant_decl* variant)
{
    nassert(scope_is_global(ctx->curr_scope));

    if (!semantic_analysis_identifier(ctx, &variant->id))
    {
        return STBOOL_FAILURE;
    }

    // check if it is in the symbol table and add it if not.
    struct symbol new;
    symbol_init_variant(&new, variant);
    struct symbol* symbol = symbol_table_insert(ctx->curr_scope->symbol_table,
            &new);

    if(symbol == NULL) // already exists in the table.
    {
        symbol = symbol_table_find(ctx->curr_scope->symbol_table,
                variant->id.start, variant->id.length);
        symbol_fini(&new);

        // it exists already, and is already defined.
        if(symbol->tag != SYMBOL_VARIANT || symbol->is_defined)
        {
            log_identifier_redeclaration(&variant->id, symbol->id);
            return STBOOL_FAILURE;
        }

        // it exists alread, but was only a forward declaration.
        if(variant->def != NULL && !validate_variant(ctx, variant))
        {
            return STBOOL_FAILURE;
        }

        // we're going to check if it is valid, and add to the table.
        if(variant->def != NULL)
        {
            nassert(symbol->ref.variant_decl != NULL);
            nassert(symbol->ref.variant_decl->def == NULL);
            nassert(symbol->dt != NULL);
            nassert(symbol->dt->variant_ref != NULL);
            nassert(symbol->dt->variant_ref->def == NULL);
            symbol->ref.variant_decl = variant;
            symbol->dt->variant_ref = variant;
        }
    }
    else // brand new symbol
    {
        // check if it needs to be exported.
        if(variant->is_export)
        {
            module_add_latest_global_symbol_to_exports(ctx->module);
        }

        // run validation on the variant def.
        if(variant->def != NULL && !validate_variant(ctx, variant))
        {
            return STBOOL_FAILURE;
        }
    }

    return STBOOL_SUCCESS;
}

stbool semantic_analysis_if_stmt(
    struct semantic_analysis_context* ctx, struct if_stmt* if_stmt)
{
    nassert(!scope_is_global(ctx->curr_scope));

    stbool status = STBOOL_SUCCESS;

    size_t const ifs_length = narr_length(if_stmt->if_conditions);
    for (size_t i = 0; i < ifs_length; ++i)
    {
        if (!semantic_analysis_expr(ctx, &if_stmt->if_conditions[i]))
        {
            status = STBOOL_FAILURE;
        }

        populate_scope_metadata(ctx, &if_stmt->if_blocks[i]);
        typeof(ctx->curr_scope) save_curr_scope = ctx->curr_scope;
        ctx->curr_scope = &if_stmt->if_blocks[i];
        if (!semantic_analysis_scope(ctx, &if_stmt->if_blocks[i]))
        {
            status = STBOOL_FAILURE;
        }
        ctx->curr_scope = save_curr_scope;
    }
    if (!status)
    {
        return STBOOL_FAILURE;
    }

    if (if_stmt->else_block != NULL)
    {
        populate_scope_metadata(ctx, if_stmt->else_block);
        typeof(ctx->curr_scope) save_curr_scope = ctx->curr_scope;
        ctx->curr_scope = if_stmt->else_block;
        if (!semantic_analysis_scope(ctx, if_stmt->else_block))
        {
            return STBOOL_FAILURE;
        }
        ctx->curr_scope = save_curr_scope;
    }

    return STBOOL_SUCCESS;
}

stbool semantic_analysis_loop_stmt(
    struct semantic_analysis_context* ctx, struct loop_stmt* loop_stmt)
{
    nassert(NULL != loop_stmt->srctok);
    nassert(!scope_is_global(ctx->curr_scope));

    populate_scope_metadata(ctx, loop_stmt->block);
    typeof(ctx->curr_scope) save_curr_scope = ctx->curr_scope;
    struct scope const* const save_loop_scope = ctx->curr_scope->loop_scope;
    ctx->curr_scope = loop_stmt->block;
    ctx->curr_scope->loop_scope = loop_stmt->block;

    switch (loop_stmt->tag)
    {
    case LOOP_CONDITIONAL:
        if (!semantic_analysis_expr(ctx, loop_stmt->conditional.expr))
        {
            return STBOOL_FAILURE;
        }
        break;
    case LOOP_RANGE:
    {
        nassert(
            LOWER_EXCLUSIVE == loop_stmt->range.lower_clusivity
            || LOWER_INCLUSIVE == loop_stmt->range.lower_clusivity);
        nassert(
            UPPER_EXCLUSIVE == loop_stmt->range.upper_clusivity
            || UPPER_INCLUSIVE == loop_stmt->range.upper_clusivity);
        struct data_type* const var_dt = &loop_stmt->range.variable_decl.dt;
        struct data_type* const lower_dt = &loop_stmt->range.variable_decl.dt;
        struct data_type* const upper_dt = &loop_stmt->range.variable_decl.dt;
        if (!semantic_analysis_variable_decl(
                ctx, &loop_stmt->range.variable_decl))
        {
            return STBOOL_FAILURE;
        }
        if (!semantic_analysis_expr(ctx, loop_stmt->range.lower_bound))
        {
            return STBOOL_FAILURE;
        }
        if (!semantic_analysis_expr(ctx, loop_stmt->range.upper_bound))
        {
            return STBOOL_FAILURE;
        }
        if (!data_type_can_implicitly_convert(var_dt, lower_dt))
        {
            DECLARE_DT_NSTR(var_type_nstr, var_dt);
            DECLARE_DT_NSTR(range_type_nstr, lower_dt);
            NLOGF_FLC_TOK(
                LOG_ERROR,
                loop_stmt->srctok,
                "Cannot implicitly convert loop variable type '%s' to lower "
                "range type '%s'.",
                var_type_nstr.data,
                range_type_nstr.data);
            return STBOOL_FAILURE;
        }
        if (!data_type_can_implicitly_convert(var_dt, upper_dt))
        {
            DECLARE_DT_NSTR(var_type_nstr, var_dt);
            DECLARE_DT_NSTR(range_type_nstr, upper_dt);
            NLOGF_FLC_TOK(
                LOG_ERROR,
                loop_stmt->srctok,
                "Cannot implicitly convert loop variable type '%s' to upper "
                "range type '%s'.",
                var_type_nstr.data,
                range_type_nstr.data);
            return STBOOL_FAILURE;
        }

        stbool var_type_status = STBOOL_SUCCESS;
        if (!data_type_is_integer(var_dt))
        {
            DECLARE_DT_NSTR(var_type_nstr, var_dt);
            NLOGF_FLC_TOK(
                LOG_ERROR,
                var_dt->srctok,
                FMT_EXPR_HAS_TYPE_X_EXPECTED_Y("integer"),
                var_type_nstr.data);
            var_type_status = STBOOL_FAILURE;
        }
        if (!(var_dt->qualifiers.is_mut))
        {
            DECLARE_DT_NSTR(var_type_nstr, var_dt);
            NLOGF_FLC_TOK(
                LOG_ERROR,
                var_dt->srctok,
                FMT_EXPR_HAS_TYPE_X_EXPECTED_Y("mutable"),
                var_type_nstr.data);
            var_type_status = STBOOL_FAILURE;
        }
        if (!var_type_status)
        {
            return STBOOL_FAILURE;
        }
    }
    break;
    default:
        NPANIC_DFLT_CASE();
    }

    if (!semantic_analysis_scope(ctx, loop_stmt->block))
    {
        return STBOOL_FAILURE;
    }
    ctx->curr_scope = save_curr_scope;
    ctx->curr_scope->loop_scope = save_loop_scope;

    return STBOOL_SUCCESS;
}

stbool semantic_analysis_alias_stmt(
    struct semantic_analysis_context* ctx, struct alias_stmt* alias_stmt)
{
    nassert(NULL != alias_stmt->srctok);
    nassert(scope_is_global(ctx->curr_scope));

    if (!semantic_analysis_identifier(ctx, &alias_stmt->new_id))
    {
        return STBOOL_FAILURE;
    }

    if (!semantic_analysis_data_type(ctx, &alias_stmt->dt))
    {
        return STBOOL_FAILURE;
    }

    struct data_type const* const aliased_type = &alias_stmt->dt;

    // The outermost modifier/base type of the data type must be
    // unqualified so that future uses of the alias can qualify the
    // type appropriately for the situation.
    //! @todo
    //!     This exists from an idea Victor had about simplifying qualifiers
    //!     long before Senior Design development on the compiler began.
    //!     It should be investigated and discussed if this is a feature we want
    //!     and if so make sure it is documented somewhere.
    if (!qualifiers_is_default(&aliased_type->qualifiers))
    {
        NLOGF_FLC_TOK(
            LOG_ERROR,
            alias_stmt->srctok,
            "Outermost data type modifier/data type base must be unqualified "
            "for aliasing.");
        return STBOOL_FAILURE;
    }

    struct identifier* const new_id = &alias_stmt->new_id;
    struct symbol* existing_symbol = symbol_table_find(
        ctx->curr_scope->symbol_table, new_id->start, new_id->length);
    if (NULL != existing_symbol)
    {
        log_identifier_redeclaration(&alias_stmt->new_id, existing_symbol->id);
        return STBOOL_FAILURE;
    }

    struct symbol alias_symbol;
    symbol_init_alias(&alias_symbol, alias_stmt);
    struct symbol* added =
        symbol_table_insert(ctx->curr_scope->symbol_table, &alias_symbol);
    SUPPRESS_UNUSED(added);
    nassert(NULL != added);

    if (alias_stmt->is_export)
    {
        module_add_latest_global_symbol_to_exports(ctx->module);
    }

    return STBOOL_SUCCESS;
}

//! Perform semantic analysis on a defer statement.
//! Defer statements do not have their own standalone representation in the NIR,
//! so this function is kept with static storage inside the semantic analysis
//! source file for use by #semantic_analysis_stmt.
static stbool semantic_analysis_defer_stmt(
    struct semantic_analysis_context* ctx, struct stmt* defer_stmt)
{
    nassert(NULL != defer_stmt->srctok);
    nassert(STMT_DEFER == defer_stmt->tag);
    nassert(!scope_is_global(ctx->curr_scope));

    populate_scope_metadata(ctx, defer_stmt->defer_stmt.block);
    typeof(ctx->curr_scope) save_curr_scope = ctx->curr_scope;
    ctx->curr_scope = defer_stmt->defer_stmt.block;
    ctx->curr_scope->defer_scope = defer_stmt->defer_stmt.block;
    if (!semantic_analysis_scope(ctx, defer_stmt->defer_stmt.block))
    {
        return STBOOL_FAILURE;
    }
    ctx->curr_scope = save_curr_scope;
    ctx->curr_scope->deferred_scopes = narr_push(
        ctx->curr_scope->deferred_scopes, &defer_stmt->defer_stmt.block);
    defer_stmt->defer_stmt.idx =
        narr_length(ctx->curr_scope->deferred_scopes) - 1;

    return STBOOL_SUCCESS;
}

//! A note about unconditional jumps within defer statements:
//!
//! ============================================================================
//!
//! The following code is perfectly valid.
//! ```
//! defer
//! {
//!     loop i : mut u in [1u, 3u]
//!     {
//!         break;
//!     }
//! }
//! ```
//! The break statement will break out of the loop without exiting the defer
//! block that the loop is contained within as expected.
//!
//! ----------------------------------------------------------------------------
//!
//! Conversely this code is not valid.
//! ```
//! loop i : mut u in [1u, 3u]
//! {
//!     defer
//!     {
//!         # What would happen here after the break below gets executed?
//!     }
//!     defer
//!     {
//!         # The break here say's go to the end of the loop scope and start
//!         # executing deferred blocks, but if control has reached this point
//!         # then we have already begun the process of executing deferred
//!         # blocks.
//!         break;
//!     }
//! }
//! ```
//! Defer blocks are always run as the last set of statements before control
//! leaves the scope containing the defer.
//! If the break statement (or any other unconditional jump for that matter) was
//! executed within the defer above, control would be transferred to a place
//! that does not have any semantic meaning, as the code at the end of the scope
//! would already be executing.
//! Therefore the second example forms an illegal program.
//!
//! ----------------------------------------------------------------------------
//!
//! Within this Doxygen bracket grouping are the semantic analysis functions for
//! statements that make unconditional jumps.
//! In each of these semantic analysis functions a check has been put into place
//! that will answer the question "Am I an unconditional jump that would attemt
//! to jump out of a defer block?", and if the asnwer is yes, then we declare
//! that statement to be illegal.
//!{

//! Perform semantic analysis on a break statement.
//! Break statements do not have their own standalone representation in the NIR,
//! so this function is kept with static storage inside the semantic analysis
//! source file for use by #semantic_analysis_stmt.
static stbool semantic_analysis_break_stmt(
    struct semantic_analysis_context* ctx, struct stmt* break_stmt)
{
    nassert(NULL != break_stmt->srctok);
    nassert(STMT_BREAK == break_stmt->tag);
    nassert(!scope_is_global(ctx->curr_scope));

    if (NULL == ctx->curr_scope->loop_scope)
    {
        NLOGF_FLC_TOK(
            LOG_ERROR,
            break_stmt->srctok,
            "Illegal 'break' outside of a loop block.");
        return STBOOL_FAILURE;
    }

    if (NULL != ctx->curr_scope->defer_scope)
    {
        // Check if the loop is inside the defer block or if the defer block
        // is inside of the loop.
        // If the loop is within the defer blocks then the statement is legal.
        struct scope const* scope = ctx->curr_scope;
        LOOP_FOREVER
        {
            if (scope == ctx->curr_scope->loop_scope)
            {
                // `break` within a loop that is inside a defer.
                // This is legal.
                //! @todo Code generation needs to support this, so for now we
                //! are shelving the changes and will make it an issue later.
                //! There should be a `break;` right here (commented out for
                //! now).
                //! When this gets fixed, remove the error message and failure
                //! return and uncomment the `break;`.

                // break; //< Uncomment me when fixed.

                NLOGF_FLC_TOK(
                    LOG_ERROR,
                    break_stmt->srctok,
                    "'break' within loop inside a defer block not yet "
                    "supported.");
                return STBOOL_FAILURE;
            }
            if (scope == ctx->curr_scope->defer_scope)
            {
                // `break` within a defer loop that is inside a loop.
                // This is illegal.
                NLOGF_FLC_TOK(
                    LOG_ERROR,
                    break_stmt->srctok,
                    "Illegal 'break' exits a defer block.");
                return STBOOL_FAILURE;
            }
            scope = ctx->curr_scope->parent;
        }
    }

    return STBOOL_SUCCESS;
}

//! Perform semantic analysis on a continue statement.
//! Continue statements do not have their own standalone representation in the
//! NIR, so this function is kept with static storage inside the semantic
//! analysis source file for use by #semantic_analysis_stmt.
static stbool semantic_analysis_continue_stmt(
    struct semantic_analysis_context* ctx, struct stmt* continue_stmt)
{
    nassert(NULL != continue_stmt->srctok);
    nassert(STMT_CONTINUE == continue_stmt->tag);
    nassert(!scope_is_global(ctx->curr_scope));

    if (NULL == ctx->curr_scope->loop_scope)
    {
        NLOGF_FLC_TOK(
            LOG_ERROR,
            continue_stmt->srctok,
            "Illegal 'continue' outside of a loop block.");
        return STBOOL_FAILURE;
    }

    if (NULL != ctx->curr_scope->defer_scope)
    {
        // Check if the loop is inside the defer block or if the defer block
        // is inside of the loop.
        // If the loop is within the defer blocks then the statement is legal.
        struct scope const* scope = ctx->curr_scope;
        LOOP_FOREVER
        {
            if (scope == ctx->curr_scope->loop_scope)
            {
                // `continue` within a loop that is inside a defer.
                // This is legal.
                // break; //< Uncomment me when fixed.

                //! @todo Code generation needs to support this, so for now we
                //! are shelving the changes and will make it an issue later.
                //! There should be a `break;` right here (commented out for
                //! now).
                //! When this gets fixed, remove the error message and failure
                //! return and uncomment the `break;`.

                NLOGF_FLC_TOK(
                    LOG_ERROR,
                    continue_stmt->srctok,
                    "'continue' within a loop inside a defer block not yet "
                    "supported.");
                return STBOOL_FAILURE;
            }
            if (scope == ctx->curr_scope->defer_scope)
            {
                // `continue` within a defer loop that is inside a loop.
                // This is illegal.
                NLOGF_FLC_TOK(
                    LOG_ERROR,
                    continue_stmt->srctok,
                    "Illegal 'continue' exits a defer block.");
                return STBOOL_FAILURE;
            }
            scope = ctx->curr_scope->parent;
        }
        return STBOOL_FAILURE;
    }

    return STBOOL_SUCCESS;
}

//!}

//! Perform semantic analysis on a return statement.
//! Return statements do not have their own standalone representation in the
//! NIR, so this function is kept with static storage inside the semantic
//! analysis source file for use by #semantic_analysis_stmt.
static stbool semantic_analysis_return_stmt(
    struct semantic_analysis_context* ctx, struct stmt* return_stmt)
{
    nassert(NULL != return_stmt->srctok);
    nassert(STMT_RETURN == return_stmt->tag);
    nassert(!scope_is_global(ctx->curr_scope));

    if (NULL == ctx->curr_func)
    {
        nassert(scope_is_global(ctx->curr_scope));
        NLOGF_FLC_TOK(
            LOG_ERROR,
            return_stmt->srctok,
            "Return statement is not within a function.");
        return STBOOL_FAILURE;
    }

    struct data_type* const return_type = &ctx->curr_func->return_dt;
    if (NULL == return_stmt->return_expr)
    {
        // Return statement without an expression should only be valid if the
        // return type of the current function is void.
        if (DT_VOID != return_type->tag)
        {
            NLOGF_FLC_TOK(
                LOG_ERROR,
                return_stmt->srctok,
                "Return statement lacks expression in function with non-void "
                "return type.");
            return STBOOL_FAILURE;
        }
    }
    else
    {
        if (!semantic_analysis_expr(ctx, return_stmt->return_expr))
        {
            return STBOOL_FAILURE;
        }

        // The type of the return expression must be convertible to the return
        // type of the function.
        struct data_type* const expression_type = &return_stmt->return_expr->dt;
        if (!data_type_can_implicitly_convert(expression_type, return_type))
        {
            DECLARE_DT_NSTR(return_expr_type_nstr, expression_type);
            DECLARE_DT_NSTR(func_rtn_type_nstr, return_type);
            NLOGF_FLC_TOK(
                LOG_ERROR,
                return_stmt->srctok,
                "Cannot implicitly convert expression of type '%s' to return "
                "type '%s'.",
                return_expr_type_nstr.data,
                func_rtn_type_nstr.data);
            return STBOOL_FAILURE;
        }
    }

    return STBOOL_SUCCESS;
}

//! Perform semantic analysis on an isolate statement.
//! Isolate statements do not have their own standalone representation in the
//! NIR, so this function is kept with static storage inside the semantic
//! analysis source file for use by #semantic_analysis_stmt.
static stbool semantic_analysis_isolate_stmt(
    struct semantic_analysis_context* ctx, struct stmt* isolate_stmt)
{
    nassert(NULL != isolate_stmt->srctok);
    nassert(STMT_BLOCK == isolate_stmt->tag);
    nassert(!scope_is_global(ctx->curr_scope));

    populate_scope_metadata(ctx, isolate_stmt->block);
    typeof(ctx->curr_scope) save_curr_scope = ctx->curr_scope;
    ctx->curr_scope = isolate_stmt->block;
    if (!semantic_analysis_scope(ctx, isolate_stmt->block))
    {
        return STBOOL_FAILURE;
    }
    ctx->curr_scope = save_curr_scope;

    return STBOOL_SUCCESS;
}

//! Perform semantic analysis on an import statement.
//! Import statements do not have their own standalone representation in the
//! NIR, so this function is kept with static storage inside the semantic
//! analysis source file for use by #semantic_analysis_stmt.
static stbool semantic_analysis_import_stmt(
    struct semantic_analysis_context* ctx, struct stmt* import_stmt)
{
    nassert(NULL != import_stmt->srctok);
    nassert(STMT_IMPORT == import_stmt->tag);
    nassert(scope_is_global(ctx->curr_scope));

    nstr_t nstr_defer_fini import_path;
    nstr_init_nstr(&import_path, &import_stmt->import_string);
    if (!resolve_module_path(&import_path, ctx->module->src_path.data))
    {
        NLOGF_FLC_TOK(
            LOG_ERROR,
            import_stmt->srctok,
            "Failed to resolve path '%s'.",
            import_stmt->import_string.data);
        return STBOOL_FAILURE;
    }

    // Index of the imported module.
    // If the module exists in the module cache already this variable
    // will eventually get set to that variable.
    // If the module does not exist in the module cache, this variable
    // will be set to the index of the next open slot in the module
    // cache.
    size_t import_idx = (size_t)-1;

    // Check if module is already in the cache.
    bool module_already_in_cache = false;
    nlogf(
        LOG_DEBUG,
        "Checking if module '%s' has already been compiled.",
        import_path.data);
    for (size_t i = 0; i < module_cache_len; ++i)
    {
        nlogf(
            LOG_DEBUG,
            "  Checking against module '%s'.",
            module_cache[i].src_path.data);
        if (nstr_eq(&import_path, &module_cache[i].src_path))
        {
            nlogf(
                LOG_DEBUG,
                "Module '%s' has already been compiled.",
                import_path.data);
            module_already_in_cache = true;
            import_idx = i;
            break;
        }
    }

    if (!module_already_in_cache)
    {
        nlogf(
            LOG_DEBUG,
            "Module '%s' has not already been compiled. Compiling now.",
            import_path.data);
        size_t const save_current_module_idx = current_module_idx;
        import_idx = module_cache_len;
        current_module_idx = import_idx;
        struct module imported_module;
        module_init(&imported_module);
        int const compile_result = compile(import_path.data, &imported_module);
        if (COMPILE_SUCCESS != compile_result)
        {
            NLOGF_FLC_TOK(
                LOG_ERROR,
                import_stmt->srctok,
                "Import Failed. Return status (%d).",
                compile_result);
            return STBOOL_FAILURE;
        }
        // Restore current module index.
        current_module_idx = save_current_module_idx;
    }
    struct module* const compiled_import = &module_cache[import_idx];

    // Add the module's exports to both this scope's symbol table as
    // well as it's exports.
    nlogf(
        LOG_DEBUG,
        "Importing %zu symbols from module '%s' into module '%s'.",
        narr_length(compiled_import->exports),
        compiled_import->src_path.data,
        ctx->module->src_path.data);
    size_t const exports_len = narr_length(compiled_import->exports);
    for (size_t i = 0; i < exports_len; ++i)
    {
        struct identifier* const symbol_id = &compiled_import->exports[i];
        struct symbol* export = symbol_table_find(
            compiled_import->global_scope.symbol_table,
            symbol_id->start,
            symbol_id->length);
        nassert(NULL != export);
        nlogf(
            LOG_DEBUG,
            "  Importing symbol '%.*s'.",
            (int)export->id->length,
            export->id->start);

        struct symbol* existing_symbol = symbol_table_find(
            ctx->curr_scope->symbol_table,
            export->id->start,
            export->id->length);
        if (NULL != existing_symbol)
        {
            nassert(NULL != export->srctok);
            nassert(NULL != existing_symbol->srctok);
            if (nstr_eq(
                    &export->srctok->module->src_path,
                    &existing_symbol->srctok->module->src_path))
            {
                nlogf(
                    LOG_DEBUG,
                    "  Symbol '%.*s' already imported.",
                    (int)export->id->length,
                    export->id->start);
                continue;
            }

            log_identifier_redeclaration(export->id, existing_symbol->id);
            return STBOOL_FAILURE;
        }

        // Symbol table.
        struct symbol* symbol =
            symbol_table_insert(ctx->curr_scope->symbol_table, export);
        nassert(NULL != symbol);
        SUPPRESS_UNUSED(symbol);

        // Module exports.
        nassert(ctx->curr_scope == &ctx->module->global_scope);
        module_add_latest_global_symbol_to_exports(ctx->module);
    }

    return STBOOL_SUCCESS;
}

//! Populate a scope with context-specific metadata for use when running
//! semantic analysis on the scope.
//! This function must always be invoked @p scope called before
//! #semantic_analysis_scope is run on @p scope.
static void populate_scope_metadata(
    struct semantic_analysis_context* ctx, struct scope* scope)
{
    scope->parent = ctx->curr_scope;
    scope->defer_scope =
        scope_is_global(scope) ? NULL : scope->parent->defer_scope;
    scope->loop_scope =
        scope_is_global(scope) ? NULL : scope->parent->loop_scope;
}

stbool semantic_analysis_scope(
    struct semantic_analysis_context* ctx, struct scope* scope)
{
    nassert(NULL != scope->srctok);

    scope->uid = module_generate_uid(&module_cache[current_module_idx]);
    for (size_t i = 0; i < narr_length(scope->stmts); ++i)
    {
        if (!semantic_analysis_stmt(ctx, &scope->stmts[i]))
        {
            return STBOOL_FAILURE;
        }
    }
    return STBOOL_SUCCESS;
}

stbool semantic_analysis_stmt(
    struct semantic_analysis_context* ctx, struct stmt* stmt)
{
    nassert(NULL != stmt->srctok);

    switch (stmt->tag)
    {
    case STMT_IMPORT:
        return semantic_analysis_import_stmt(ctx, stmt);
    case STMT_ALIAS:
        return semantic_analysis_alias_stmt(ctx, stmt->alias_stmt);
    case STMT_VARIABLE_DECLARATION:
        return semantic_analysis_variable_decl(ctx, stmt->variable_decl);
    case STMT_FUNCTION_DECLARATION:
        return semantic_analysis_function_decl(ctx, stmt->function_decl);
    case STMT_STRUCT_DECLARATION:
        return semantic_analysis_struct_decl(ctx, stmt->struct_decl);
    case STMT_VARIANT_DECLARATION:
        return semantic_analysis_variant_decl(ctx, stmt->variant_decl);
    case STMT_IF:
        return semantic_analysis_if_stmt(ctx, stmt->if_stmt);
    case STMT_LOOP:
        return semantic_analysis_loop_stmt(ctx, stmt->loop_stmt);
    case STMT_DEFER:
        return semantic_analysis_defer_stmt(ctx, stmt);
    case STMT_BREAK:
        return semantic_analysis_break_stmt(ctx, stmt);
    case STMT_CONTINUE:
        return semantic_analysis_continue_stmt(ctx, stmt);
    case STMT_RETURN:
        return semantic_analysis_return_stmt(ctx, stmt);
    case STMT_EXPR:
        return semantic_analysis_expr(ctx, stmt->expr);
    case STMT_BLOCK:
        return semantic_analysis_isolate_stmt(ctx, stmt);
    case STMT_EMPTY:
        /* nothing */
        return STBOOL_SUCCESS;
    default:
        NPANIC_DFLT_CASE();
    }
}

stbool dophase_semantic_analysis(struct module* module)
{
    if (narr_length(module->tokens) > 1)
    {
        struct semantic_analysis_context ctx = {
            .module = module,
            .curr_scope = NULL, // Start out in no scope.
            .curr_func = NULL};
        populate_scope_metadata(&ctx, &module->global_scope);
        ctx.curr_scope = &module->global_scope;
        return semantic_analysis_scope(&ctx, &module->global_scope);
    }
    else
    {
        return STBOOL_SUCCESS;
    }
}
