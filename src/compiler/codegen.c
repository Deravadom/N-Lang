/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "codegen.h"

stbool dophase_codegen(struct module* module)
{
    switch (config.target)
    {
    case TARGET_NATIVE:
        return native_codegen(module);
    case TARGET_C99:
        return c99_codegen(module, NULL);
    default:
        NPANIC_DFLT_CASE();
    }
}

//============================================================================//
//      NATIVE                                                                //
//============================================================================//
stbool native_codegen(struct module* module)
{
    nstr_t nstr_defer_fini intermediate_c_file_name;
    nstr_init_nstr(&intermediate_c_file_name, &module->out_path);
    nstr_cat_cstr(&intermediate_c_file_name, ".c");

    if (!c99_codegen(module, intermediate_c_file_name.data))
    {
        return STBOOL_FAILURE;
    }

    nstr_t nstr_defer_fini posix_c99_system_cmd;
    nstr_init_fmt(
        &posix_c99_system_cmd,
        "c99 -c %s %s -fwrapv -o %s %s",
        config.is_gen_debug_info ? "-g" : "",
        config.is_optimize ? "-O2" : "",
        module->out_path.data,
        intermediate_c_file_name.data);
    nlogf(LOG_DEBUG, "Invoking the POSIX c99 compiler.");
    nlogf(LOG_DEBUG, "$ %s", posix_c99_system_cmd.data);
    if (0 != system(posix_c99_system_cmd.data))
    {
        nlogf(LOG_ERROR, "Failed to compile using the POSIX c99 compiler.");
        return STBOOL_FAILURE;
    }

    if (!config.is_save_temps)
    {
        nlogf(LOG_DEBUG, "Removing file '%s'", intermediate_c_file_name.data);
        remove(intermediate_c_file_name.data);
    }

    return STBOOL_SUCCESS;
}

//============================================================================//
//      C99                                                                   //
//============================================================================//
#define C99_CODEGEN_MAIN__ENTRY_FUNC_NO_ARGS_RTN_VOID                          \
    "int main(void)\n"                                                         \
    "{\n"                                                                      \
    "    /* entry : () -> void */\n"                                           \
    "    entry();\n"                                                           \
    "    return 0;\n"                                                          \
    "}\n"

#define C99_CODEGEN_MAIN__ENTRY_FUNC_NO_ARGS_RTN_U                             \
    "int main(void)\n"                                                         \
    "{\n"                                                                      \
    "    /* entry : () -> u */\n"                                              \
    "    int rtn = (int)entry();\n"                                            \
    "    return rtn >= 0 ? rtn : -rtn;\n"                                      \
    "}\n"

#define C99_CODEGEN_MAIN__ENTRY_FUNC_NO_ARGS_RTN_S                             \
    "int main(void)\n"                                                         \
    "{\n"                                                                      \
    "    /* entry : () -> s */\n"                                              \
    "    return (int)entry();\n"                                               \
    "}\n"

#define C99_CODEGEN_MAIN__ENTRY_FUNC_ARGS_RTN_VOID                             \
    "int main(int argc, char** argv)\n"                                        \
    "{\n"                                                                      \
    "    /* func entry : (argc : u, argv : **char) -> u */\n"                  \
    "    entry(argc, (char const* const*)argv);\n"                             \
    "    return 0;\n"                                                          \
    "}\n"

#define C99_CODEGEN_MAIN__ENTRY_FUNC_ARGS_RTN_U                                \
    "int main(int argc, char** argv)\n"                                        \
    "{\n"                                                                      \
    "    /* func entry : (argc : u, argv : **char) -> u */\n"                  \
    "    int rtn = (int)entry(argc, (char const* const*)argv);\n"              \
    "    return rtn >= 0 ? rtn : -rtn;\n"                                      \
    "}\n"

#define C99_CODEGEN_MAIN__ENTRY_FUNC_ARGS_RTN_S                                \
    "int main(int argc, char** argv)\n"                                        \
    "{\n"                                                                      \
    "    /* func entry : (argc : u, argv : **char) -> s */\n"                  \
    "    return (int)entry(argc, (char const* const*)argv);\n"                 \
    "}\n"

#define C99_CODEGEN_INDENT "    "

#define C99_SCOPE_END_LABEL "___scope_end_%zu"

#define C99_RETURN_VALUE_IDENTIFIER "___ret_value"

#define C99_BREAK_CONDITIONAL_DECLARATION "int ___do_break = 0;\n"
#define C99_BREAK_CONDITIONAL_SET_TRUE "/*break[0]*/___do_break = 1;\n"
#define C99_BREAK_SCOPE_END__NESTED                                            \
    "if (___do_break){goto " C99_SCOPE_END_LABEL ";}\n"
#define C99_BREAK_SCOPE_END__NOT_NESTED "if (___do_break){break;}\n"

#define C99_DEFER_CONDITIONAL_DECLARATION "int ___do_defer_%u = 0;\n"
#define C99_DEFER_CONDITIONAL_SET_TRUE "/*defer*/___do_defer_%u = 1;\n"
#define C99_DEFER_SCOPE_END_IF_STATEMENT                                       \
    "/*deferred scope*/if (___do_defer_%u)\n"

#define FMT_IFACE_IDENTIFIER "n_variant_%.*s_iface_%.*s"

static inline void c99_codegen_do_indent(struct c99_codegen_context* ctx)
{
    for (size_t i = 0; i < ctx->indent_level; ++i)
    {
        C99_SRC_APPEND(ctx, C99_CODEGEN_INDENT);
    }
}

void c99_codegen_context_init(
    struct c99_codegen_context* ctx,
    struct module* module)
{
    ctx->module = module;
    ctx->indent_level = 0;
    ctx->cur_func = NULL;
    nstr_init(&ctx->c_code_nstr);
}

void c99_codegen_context_fini(struct c99_codegen_context* ctx)
{
    nstr_fini(&ctx->c_code_nstr);
}

void c99_codegen_identifier(
    struct c99_codegen_context* ctx,
    struct identifier* id)
{
    C99_SRC_APPEND(ctx, "%.*s", (int)id->length, id->start);
}

void c99_codegen_data_type(
    struct c99_codegen_context* ctx,
    struct data_type* dt,
    struct identifier* id)
{
    nstr_t nstr_defer_fini dt_nstr = data_type_to_c_source_nstr(dt, id);
    C99_SRC_APPEND(ctx, "%s", dt_nstr.data);
}

void c99_codegen_literal(
    struct c99_codegen_context* ctx,
    struct literal* literal)
{
    switch (literal->tag)
    {
    case LITERAL_U8:
        C99_SRC_APPEND(ctx, "(/*literal*/(n_u8)(%" PRIu8 "ULL))", literal->u8);
        break;

    case LITERAL_U16:
        C99_SRC_APPEND(
            ctx, "(/*literal*/(n_u16)(%" PRIu16 "ULL))", literal->u16);
        break;

    case LITERAL_U32:
        C99_SRC_APPEND(
            ctx, "(/*literal*/(n_u32)(%" PRIu32 "ULL))", literal->u32);
        break;

    case LITERAL_U64:
        C99_SRC_APPEND(
            ctx, "(/*literal*/(n_u64)(%" PRIu64 "ULL))", literal->u64);
        break;

    case LITERAL_U:
        C99_SRC_APPEND(ctx, "(/*literal*/(n_u)(%" PRIuMAX "ULL))", literal->u);
        break;

    case LITERAL_S8:
        C99_SRC_APPEND(ctx, "(/*literal*/(n_s8)(%" PRIi8 "ULL))", literal->s8);
        break;

    case LITERAL_S16:
        C99_SRC_APPEND(
            ctx, "(/*literal*/(n_s16)(%" PRIi16 "ULL))", literal->s16);
        break;

    case LITERAL_S32:
        C99_SRC_APPEND(
            ctx, "(/*literal*/(n_s32)(%" PRIi32 "ULL))", literal->s32);
        break;

    case LITERAL_S64:
        C99_SRC_APPEND(
            ctx, "(/*literal*/(n_s64)(%" PRIi64 "ULL))", literal->s64);
        break;

    case LITERAL_S:
        C99_SRC_APPEND(ctx, "(/*literal*/(n_s)(%" PRIdMAX "ULL))", literal->s);
        break;

    case LITERAL_F32:
    {
        size_t const length = literal->f32.length - STR_N_FLOAT_F32_LENGTH;
        C99_SRC_APPEND(ctx, "%.*s", (int)length, literal->f32.start);
    }
    break;

    case LITERAL_F64:
    {
        size_t const length = literal->f64.length - STR_N_FLOAT_F32_LENGTH;
        C99_SRC_APPEND(ctx, "%.*s", (int)length, literal->f64.start);
    }
    break;

    case LITERAL_BOOL:
        C99_SRC_APPEND(ctx, "%s", literal->boolean ? "n_true" : "n_false");
        break;

    case LITERAL_ACHAR:
        C99_SRC_APPEND(ctx, "\'%s\'", n_ascii_to_c_source_char(literal->achar));
        break;

    case LITERAL_ASTR:
        C99_SRC_APPEND(ctx, "\"");
        for (size_t i = 0; i < literal->astr.len; ++i)
        {
            char const* const c_source_char =
                n_ascii_to_c_source_char((n_ascii)literal->astr.data[i]);
            C99_SRC_APPEND(ctx, "%s", c_source_char);
        }
        C99_SRC_APPEND(ctx, "\"");
        break;

    case LITERAL_NULL:
        C99_SRC_APPEND(ctx, "n_null");
        break;

    case LITERAL_ARRAY:
    {
        C99_SRC_APPEND(ctx, "{"); // Opening array bracket.

        struct data_type ATTR_DEFER_FINI(data_type_fini) dt =
            literal_get_data_type(literal);
        nassert(data_type_is_array(&dt));
        size_t const num_elems = dt.array.length;

        struct data_type const* const elem_dt = dt.inner;
        bool const elements_are_integer = data_type_is_integer(elem_dt);
        bool const elements_are_ascii = data_type_is_ascii(elem_dt);

        bool is_fill = false;
        size_t last_defined_idx = (size_t)-1;
        size_t i = 0;
        while (i < num_elems)
        {
            size_t idx;
            if (is_fill)
            {
                idx = last_defined_idx;
            }
            else if (LITERAL_ARRAY_FILL == literal->array[i].tag)
            {
                // By the C standard, a last value of zero in an array
                // declaration that is not fully initialized will fill the
                // rest of the array with zeros.
                // We take advantage of that here as a way to save space in
                // time in outputted C99 files.
                struct literal const* const last =
                    &(literal->array[last_defined_idx]);
                bool const last_is_integer_zero =
                    elements_are_integer && (0 == last->u8);
                bool const last_is_char_zero =
                    elements_are_ascii && (0 == last->achar);
                if (last_is_integer_zero || last_is_char_zero)
                {
                    C99_SRC_APPEND(ctx, "0/* ZERO FILL */");
                    break;
                }
                is_fill = true;
                continue;
            }
            else
            {
                last_defined_idx = i;
                idx = i;
            }

            c99_codegen_literal(ctx, &literal->array[idx]);
            if (!is_fill || i != num_elems - 1)
            {
                nstr_cat_fmt(&ctx->c_code_nstr, ", ");
            }
            i += 1;
        }

        C99_SRC_APPEND(ctx, "}"); // Closing array bracket.
    }
    break;

    case LITERAL_ARRAY_FILL:
        NPANIC_UNEXPECTED_CTRL_FLOW();

    default:
        NPANIC_DFLT_CASE();
    }
}

static inline void c99_codegen_expr_primary(
    struct c99_codegen_context* ctx,
    struct expr* expr)
{
    switch (expr->tag)
    {
    case EXPR_PRIMARY_IDENTIFIER:
        c99_codegen_identifier(ctx, &expr->id);
        break;

    case EXPR_PRIMARY_LITERAL:
        c99_codegen_literal(ctx, expr->literal);
        break;

    case EXPR_PRIMARY_PAREN:
        C99_SRC_APPEND(ctx, "(");
        c99_codegen_expr(ctx, expr->paren);
        C99_SRC_APPEND(ctx, ")");
        break;

    default:
        NPANIC_DFLT_CASE();
    }
}

static inline void c99_codegen_expr_postfix(
    struct c99_codegen_context* ctx,
    struct expr* expr)
{
    struct expr* const lhs_expr = expr->postfix.lhs;
    switch (expr->tag)
    {
    case EXPR_POSTFIX_FUNCTION_CALL:
    {
        nassert(data_type_is_function(&lhs_expr->dt));
        size_t const args_length = narr_length(expr->postfix.args);

        // If the expression is an invocation of a member function, we
        // need to translate to a regular function call using the member
        // function's target.
        // If it is an iface call, then the identifier for the function call
        // must also be generated.
        // We will preemptively check if the left hand side of the
        // expression is accessing a member_function and if so generate
        // code for the corresponding function.
        bool const is_dot_member_func_call =
            (EXPR_POSTFIX_ACCESS_DOT == lhs_expr->tag)
            && lhs_expr->postfix.access.is_member_func;
        bool const is_arrow_member_func_call =
            (EXPR_POSTFIX_ACCESS_ARROW == lhs_expr->tag)
            && lhs_expr->postfix.access.is_member_func;
        bool const is_member_func_call =
            is_dot_member_func_call || is_arrow_member_func_call;
        if (is_member_func_call)
        {
            struct expr* const member_expr = lhs_expr;
            struct expr* const instance_expr = member_expr->postfix.lhs;

            struct data_type* const dt = is_dot_member_func_call
                ? &instance_expr->dt
                : instance_expr->dt.inner;
            struct identifier const* const access_id =
                &member_expr->postfix.access.id;
            struct member_func* member_func;

            if(dt->tag == DT_VARIANT &&
                !lhs_expr->postfix.lhs->postfix.access.is_member_tag)
            {
                struct variant_decl* const variant = dt->variant_ref;

                ssize_t const member_func_idx =
                    variant_def_member_func_idx(variant->def, access_id);
                ssize_t const member_iface_idx =
                    variant_def_member_iface_idx(variant->def, access_id);
                nassert((MEMBER_FUNC_NOT_FOUND != member_func_idx) !=
                        (MEMBER_IFACE_NOT_FOUND != member_iface_idx));

                if(MEMBER_FUNC_NOT_FOUND != member_func_idx)
                {
                    member_func = &variant->def->member_funcs[member_func_idx];
                }
                else
                {
                    struct member_iface* const mi =
                        &variant->def->member_ifaces[member_iface_idx];

                    C99_SRC_APPEND(ctx, "/*member iface call*/");
                    nstr_t nstr_defer_fini iface_name;
                    struct identifier iface_id;
                    nstr_init(&iface_name);
                    nstr_assign_fmt(&iface_name, FMT_IFACE_IDENTIFIER,
                            (int) variant->id.length, variant->id.start,
                            (int) mi->id.length, mi->id.start);
                    iface_id.start = iface_name.data;
                    iface_id.length = iface_name.len;
                    iface_id.srctok = NULL;
                    c99_codegen_identifier(ctx, &iface_id);

                    member_func = NULL;
                }
            }
            else if(dt->tag == DT_VARIANT &&
                lhs_expr->postfix.lhs->postfix.access.is_member_tag)
            {
                ssize_t const tag_idx = variant_def_member_tag_idx(
                    dt->variant_ref->def,
                    &lhs_expr->postfix.lhs->postfix.access.id);
                nassert(MEMBER_TAG_NOT_FOUND != tag_idx);
                struct struct_def* const struct_def =
                    &dt->variant_ref->def->member_tags[tag_idx].section;

                ssize_t const member_func_idx =
                    struct_def_member_func_idx(struct_def, access_id);
                nassert(MEMBER_FUNC_NOT_FOUND != member_func_idx);

                member_func = &struct_def->member_funcs[member_func_idx];
            }
            else
            {
                struct struct_def* const struct_def = dt->struct_ref->def;

                ssize_t const member_func_idx =
                    struct_def_member_func_idx(struct_def, access_id);
                nassert(MEMBER_FUNC_NOT_FOUND != member_func_idx);

                member_func = &struct_def->member_funcs[member_func_idx];
            }

            if(member_func != NULL)
            {
                C99_SRC_APPEND(ctx, "/*member function call*/");
                c99_codegen_identifier(ctx, &member_func->target.id);
            }
        }
        else
        {
            c99_codegen_expr(ctx, lhs_expr);
        }

        C99_SRC_APPEND(ctx, "("); // Opening paren for the funcion call.
        for (size_t i = 0; i < args_length; ++i)
        {
            bool const is_possibly_this_arg = i == 0;
            if (is_possibly_this_arg && is_dot_member_func_call)
            {
                C99_SRC_APPEND(ctx, "&(");
            }
            c99_codegen_expr(ctx, &expr->postfix.args[i]);
            if (is_possibly_this_arg && is_dot_member_func_call)
            {
                C99_SRC_APPEND(ctx, ")");
            }
            if (i != args_length - 1)
            {
                C99_SRC_APPEND(ctx, ", ");
            }
        }
        C99_SRC_APPEND(ctx, ")"); // Closing paren for the funcion call.
    }
    break;

    case EXPR_POSTFIX_SUBSCRIPT:
        nassert(
            data_type_is_pointer(&lhs_expr->dt)
            || data_type_is_array(&lhs_expr->dt));
        c99_codegen_expr(ctx, lhs_expr);

        C99_SRC_APPEND(ctx, "[");
        c99_codegen_expr(ctx, expr->postfix.subscript);
        C99_SRC_APPEND(ctx, "]");
        break;

    case EXPR_POSTFIX_ACCESS_DOT:
        nassert(data_type_is_struct(&lhs_expr->dt) ||
                data_type_is_variant(&lhs_expr->dt));
        c99_codegen_expr(ctx, lhs_expr);
        C99_SRC_APPEND(ctx, ".");
        c99_codegen_identifier(ctx, &expr->postfix.access.id);
        break;

    case EXPR_POSTFIX_ACCESS_ARROW:
        nassert(DT_STRUCT == lhs_expr->dt.inner->tag ||
                DT_VARIANT == lhs_expr->dt.inner->tag);
        c99_codegen_expr(ctx, lhs_expr);
        C99_SRC_APPEND(ctx, "->");
        c99_codegen_identifier(ctx, &expr->postfix.access.id);
        break;

    default:
        NPANIC_DFLT_CASE();
    }
}

static inline void c99_codegen_expr_unary(
    struct c99_codegen_context* ctx,
    struct expr* expr)
{
    struct expr* const rhs_expr = expr->unary.rhs_expr;
    struct data_type* const rhs_dt = expr->unary.rhs_dt;

    C99_SRC_APPEND(ctx, "(");
    switch (expr->tag)
    {
    case EXPR_UNARY_ADDR_OF:
        C99_SRC_APPEND(ctx, "&");
        c99_codegen_expr(ctx, rhs_expr);
        break;

    case EXPR_UNARY_DEREF:
        C99_SRC_APPEND(ctx, "*");
        c99_codegen_expr(ctx, rhs_expr);
        break;

    case EXPR_UNARY_DECAY:
        C99_SRC_APPEND(ctx, "/*$*/");
        c99_codegen_expr(ctx, rhs_expr);
        break;

    case EXPR_UNARY_PLUS:
        C99_SRC_APPEND(ctx, "+");
        c99_codegen_expr(ctx, rhs_expr);
        break;

    case EXPR_UNARY_MINUS:
        C99_SRC_APPEND(ctx, "-");
        c99_codegen_expr(ctx, rhs_expr);
        break;

    case EXPR_UNARY_BITWISE_NOT:
        C99_SRC_APPEND(ctx, "~");
        c99_codegen_expr(ctx, rhs_expr);
        break;

    case EXPR_UNARY_LOGICAL_NOT:
        C99_SRC_APPEND(ctx, "!");
        c99_codegen_expr(ctx, rhs_expr);
        break;

    case EXPR_UNARY_COUNTOF_EXPR:
    {
        nassert(data_type_is_array(&rhs_expr->dt));
        size_t const array_length = rhs_expr->dt.array.length;
        C99_SRC_APPEND(ctx, "/*countof*/%zu", array_length);
    }
    break;

    case EXPR_UNARY_COUNTOF_DT:
    {
        nassert(data_type_is_array(rhs_dt));
        size_t const array_length = rhs_dt->array.length;
        C99_SRC_APPEND(ctx, "/*countof*/%zu", array_length);
    }
    break;

    case EXPR_UNARY_SIZEOF_EXPR:
        //! @todo
        //!     In the future if we allow passing arrays to functions then we
        //!     will need to update this code, as C arrays passed to functions
        //!     without static typing decay into pointers.
        C99_SRC_APPEND(ctx, "sizeof(");
        c99_codegen_data_type(ctx, &rhs_expr->dt, NULL);
        C99_SRC_APPEND(ctx, ")");
        break;

    case EXPR_UNARY_SIZEOF_DT:
        C99_SRC_APPEND(ctx, "sizeof(");
        c99_codegen_data_type(ctx, rhs_dt, NULL);
        C99_SRC_APPEND(ctx, ")");
        break;

    default:
        NPANIC_DFLT_CASE();
    }
    C99_SRC_APPEND(ctx, ")");
}

static inline void c99_codegen_expr_binary(
    struct c99_codegen_context* ctx,
    struct expr* expr)
{
    C99_SRC_APPEND(ctx, "(");
    if (EXPR_BINARY_CAST == expr->tag)
    {
        nstr_t nstr_defer_fini dt_nstr =
            data_type_to_c_source_nstr(expr->binary.rhs_dt, NULL);
        C99_SRC_APPEND(ctx, "(%s)", dt_nstr.data);
        c99_codegen_expr(ctx, expr->binary.lhs_expr);
    }
    else
    {
        c99_codegen_expr(ctx, expr->binary.lhs_expr);
        switch (expr->tag)
        {
        case EXPR_BINARY_ASSIGN:
            C99_SRC_APPEND(ctx, " = ");
            break;
        case EXPR_BINARY_LOGICAL_OR:
            C99_SRC_APPEND(ctx, " || ");
            break;
        case EXPR_BINARY_LOGICAL_AND:
            C99_SRC_APPEND(ctx, " && ");
            break;
        case EXPR_BINARY_BITWISE_OR:
            C99_SRC_APPEND(ctx, " | ");
            break;
        case EXPR_BINARY_BITWISE_XOR:
            C99_SRC_APPEND(ctx, " ^ ");
            break;
        case EXPR_BINARY_BITWISE_AND:
            C99_SRC_APPEND(ctx, " & ");
            break;
        case EXPR_BINARY_REL_EQ:
            C99_SRC_APPEND(ctx, " == ");
            break;
        case EXPR_BINARY_REL_NE:
            C99_SRC_APPEND(ctx, " != ");
            break;
        case EXPR_BINARY_REL_LT:
            C99_SRC_APPEND(ctx, " < ");
            break;
        case EXPR_BINARY_REL_GT:
            C99_SRC_APPEND(ctx, " > ");
            break;
        case EXPR_BINARY_REL_LE:
            C99_SRC_APPEND(ctx, " <= ");
            break;
        case EXPR_BINARY_REL_GE:
            C99_SRC_APPEND(ctx, " >= ");
            break;
        case EXPR_BINARY_SHIFT_L:
            C99_SRC_APPEND(ctx, " << ");
            break;
        case EXPR_BINARY_SHIFT_R:
            C99_SRC_APPEND(ctx, " >> ");
            break;
        case EXPR_BINARY_PLUS:
            C99_SRC_APPEND(ctx, " + ");
            break;
        case EXPR_BINARY_MINUS:
            C99_SRC_APPEND(ctx, " - ");
            break;
        case EXPR_BINARY_PTR_DIFF:
            C99_SRC_APPEND(ctx, " - ");
            break;
        case EXPR_BINARY_PTR_PLUS:
            C99_SRC_APPEND(ctx, " + ");
            break;
        case EXPR_BINARY_PTR_MINUS:
            C99_SRC_APPEND(ctx, " - ");
            break;
        case EXPR_BINARY_MULT:
            C99_SRC_APPEND(ctx, " * ");
            break;
        case EXPR_BINARY_DIV:
            C99_SRC_APPEND(ctx, " / ");
            break;
        default:
            NPANIC_DFLT_CASE();
        }
        c99_codegen_expr(ctx, expr->binary.rhs_expr);
    }
    C99_SRC_APPEND(ctx, ")");
}

void c99_codegen_expr(struct c99_codegen_context* ctx, struct expr* expr)
{
    if (expr_is_primary(expr))
    {
        c99_codegen_expr_primary(ctx, expr);
    }
    else if (expr_is_postfix(expr))
    {
        c99_codegen_expr_postfix(ctx, expr);
    }
    else if (expr_is_unary(expr))
    {
        c99_codegen_expr_unary(ctx, expr);
    }
    else if (expr_is_binary(expr))
    {
        c99_codegen_expr_binary(ctx, expr);
    }
    else
    {
        NPANIC_UNEXPECTED_CTRL_FLOW();
    }
}

static inline void c99_codegen_variable_decl_forward(
    struct c99_codegen_context* ctx,
    struct variable_decl* variable_decl)
{
    bool const ctx_is_global_scope = 0 == ctx->indent_level;
    if (variable_decl->is_introduce)
    {
        C99_SRC_APPEND(ctx, "extern ");
    }
    else if (ctx_is_global_scope && !variable_decl->is_export)
    {
        C99_SRC_APPEND(ctx, "static ");
    }
    c99_codegen_data_type(ctx, &variable_decl->dt, &variable_decl->id);
}

void c99_codegen_variable_decl_stmt(
    struct c99_codegen_context* ctx,
    struct variable_decl* variable_decl)
{
    c99_codegen_variable_decl_forward(ctx, variable_decl);

    //// Append the initializing expression if it exists.
    if (NULL != variable_decl->def)
    {
        C99_SRC_APPEND(ctx, " = ");
        c99_codegen_expr(ctx, variable_decl->def);
    }

    //// Append the statement-ending semicolon.
    C99_SRC_APPEND(ctx, ";");
}

static inline void c99_codegen_function_decl_forward(
    struct c99_codegen_context* ctx,
    struct function_decl* function_decl)
{
    nassert(data_type_is_function(&function_decl->dt));

    bool const is_export = function_decl->is_export;
    struct identifier* const id = &function_decl->id;
    struct data_type* const dt = &function_decl->dt;
    narr_t(struct identifier) const param_ids = function_decl->param_ids;
    bool const ctx_is_global_scope = 0 == ctx->indent_level;

    //// Storage class specifier.
    nstr_t nstr_defer_fini storage;
    nstr_init(&storage);
    if (ctx_is_global_scope && !is_export)
    {
        nstr_cat_cstr(&storage, "static ");
    }

    //// Return type.
    nstr_t nstr_defer_fini return_type =
        data_type_to_c_source_nstr(&dt->func_sig->return_dt, NULL);

    //// Parameters.
    size_t const params_length = narr_length(param_ids);
    nstr_t nstr_defer_fini params;
    nstr_init(&params);
    if (0 == params_length)
    {
        nstr_cat_cstr(&params, "void");
    }
    for (size_t i = 0; i < params_length; ++i)
    {
        struct data_type* const param_dt = &dt->func_sig->param_dts[i];
        nstr_t nstr_defer_fini param =
            data_type_to_c_source_nstr(param_dt, &(param_ids[i]));
        nstr_cat_cstr(&params, param.data);
        if (i != params_length - 1)
        {
            nstr_cat_cstr(&params, ", ");
        }
    }

    nstr_t nstr_defer_fini decl;
    nstr_init_fmt(
        &decl,
        "%s %s %.*s(%s)",
        storage.data,
        return_type.data,
        (int)id->length,
        id->start,
        params.data);
    C99_SRC_APPEND(ctx, decl.data);
}

void c99_codegen_function_decl_stmt(
    struct c99_codegen_context* ctx,
    struct function_decl* function_decl)
{
    c99_codegen_function_decl_forward(ctx, function_decl);

    //// Append the function definition if it exists or append the
    //// statement-ending for forward declared functions.
    if (NULL != function_decl->def)
    {
        // Save the current function within the codegen context and set the
        // current function to the one being generated.
        struct function_decl* save_cur_func = ctx->cur_func;
        ctx->cur_func = function_decl;

        C99_SRC_APPEND(ctx, "\n");
        c99_codegen_scope(ctx, function_decl->def);

        // Restore the current function.
        ctx->cur_func = save_cur_func;
    }
    else
    {
        C99_SRC_APPEND(ctx, ";");
    }
}

static inline void c99_codegen_struct_forward(
    struct c99_codegen_context* ctx,
    struct struct_decl* struct_decl)
{
    C99_SRC_APPEND(ctx, "struct ");
    c99_codegen_identifier(ctx, &struct_decl->id);
}

static inline void c99_codegen_struct_definition(
    struct c99_codegen_context* ctx,
    struct struct_def* def)
{
    C99_SRC_APPEND(ctx, "{\n");
    ctx->indent_level += 1;

    size_t const member_vars_length = narr_length(def->member_vars);
    for (size_t i = 0; i < member_vars_length; ++i)
    {
        struct member_var* memb = &def->member_vars[i];
        c99_codegen_do_indent(ctx);
        c99_codegen_data_type(ctx, &memb->dt, &memb->id);
        C99_SRC_APPEND(ctx, ";\n");
    }

    ctx->indent_level -= 1;
    C99_SRC_APPEND(ctx, "}\n");
}

void c99_codegen_struct_decl_stmt(
    struct c99_codegen_context* ctx,
    struct struct_decl* struct_decl)
{
    c99_codegen_struct_forward(ctx, struct_decl);
    if (NULL != struct_decl->def)
    {
        c99_codegen_struct_definition(ctx, struct_decl->def);
    }
    C99_SRC_APPEND(ctx, ";");
}

void c99_codegen_if_stmt(
    struct c99_codegen_context* ctx,
    struct if_stmt* if_stmt)
{
    size_t const ifs_length = narr_length(if_stmt->if_conditions);
    for (size_t i = 0; i < ifs_length; ++i)
    {
        c99_codegen_do_indent(ctx);
        C99_SRC_APPEND(ctx, "%s (", i == 0 ? "if" : "else if");
        c99_codegen_expr(ctx, &if_stmt->if_conditions[i]);
        C99_SRC_APPEND(ctx, ")\n");
        c99_codegen_scope(ctx, &if_stmt->if_blocks[i]);
    }
    if (if_stmt->else_block != NULL)
    {
        c99_codegen_do_indent(ctx);
        C99_SRC_APPEND(ctx, "else\n");
        c99_codegen_scope(ctx, if_stmt->else_block);
    }
}

void c99_codegen_loop_stmt(
    struct c99_codegen_context* ctx,
    struct loop_stmt* loop_stmt)
{
    switch (loop_stmt->tag)
    {
    case LOOP_CONDITIONAL:
        c99_codegen_do_indent(ctx);
        C99_SRC_APPEND(ctx, "while (");
        c99_codegen_expr(ctx, loop_stmt->conditional.expr);
        C99_SRC_APPEND(ctx, ")\n");
        c99_codegen_scope(ctx, loop_stmt->block);
        break;

    case LOOP_RANGE:
    {
        //// Declare the loop variable.
        c99_codegen_do_indent(ctx);
        C99_SRC_APPEND(ctx, "for (");
        c99_codegen_variable_decl_forward(ctx, &loop_stmt->range.variable_decl);
        C99_SRC_APPEND(ctx, " = ");

        //// The lower bound, i.e. the initial variable definition.
        C99_SRC_APPEND(ctx, "(");
        c99_codegen_expr(ctx, loop_stmt->range.lower_bound);
        if (LOWER_INCLUSIVE == loop_stmt->range.lower_clusivity)
        {
            C99_SRC_APPEND(ctx, ")/*inclusive*/; ");
        }
        else if (LOWER_EXCLUSIVE == loop_stmt->range.lower_clusivity)
        {
            C99_SRC_APPEND(ctx, ")/*exclusive*/+1; ");
        }
        else
        {
            NPANIC_UNEXPECTED_CTRL_FLOW();
        }

        //// The upper bound, i.e. the end condition for the variable.
        int const id_length = (int)loop_stmt->range.variable_decl.id.length;
        char const* const id_start = loop_stmt->range.variable_decl.id.start;
        if (UPPER_INCLUSIVE == loop_stmt->range.upper_clusivity)
        {
            C99_SRC_APPEND(ctx, "%.*s <= (", id_length, id_start);
            c99_codegen_expr(ctx, loop_stmt->range.upper_bound);
            C99_SRC_APPEND(ctx, ")/*inclusive*/; ");
        }
        else if (UPPER_EXCLUSIVE == loop_stmt->range.upper_clusivity)
        {
            C99_SRC_APPEND(ctx, "%.*s < (", id_length, id_start);
            c99_codegen_expr(ctx, loop_stmt->range.upper_bound);
            C99_SRC_APPEND(ctx, ")/*exclusive*/; ");
        }
        else
        {
            NPANIC_UNEXPECTED_CTRL_FLOW();
        }

        //// Increment the loop variable through the range.
        C99_SRC_APPEND(ctx, "++%.*s)\n", id_length, id_start);
        c99_codegen_scope(ctx, loop_stmt->block);
    }
    break;

    default:
        NPANIC_DFLT_CASE();
    }
}

void c99_codegen_alias_stmt(
    struct c99_codegen_context* ctx,
    struct alias_stmt* alias_stmt)
{
    C99_SRC_APPEND(ctx, "typedef ");
    c99_codegen_data_type(ctx, &alias_stmt->dt, NULL);
    C99_SRC_APPEND(ctx, " ");
    c99_codegen_identifier(ctx, &alias_stmt->new_id);
    C99_SRC_APPEND(ctx, ";");
}

void c99_codegen_scope(struct c99_codegen_context* ctx, struct scope* scope)
{
    // Save the current scope uid within the codegen context and update the
    // context.
    uint32_t const current_scope_uid_save = ctx->current_scope_uid;
    ctx->current_scope_uid = scope->uid;

    bool const is_global = scope_is_global(scope);
    bool const is_within_function = NULL != ctx->cur_func;
    SUPPRESS_UNUSED(is_within_function);
    uint64_t const deferred_scopes_length = narr_length(scope->deferred_scopes);

    //// Beginning of non-global scope variables.
    if (!is_global)
    {
        c99_codegen_do_indent(ctx);
        C99_SRC_APPEND(ctx, "{\n");
        ctx->indent_level += 1;

        // Setup return value if we are within a function.
        nassert(is_within_function);
        struct data_type* const return_type =
            &ctx->cur_func->dt.func_sig->return_dt;
        if (!data_type_is_void(return_type) && scope_is_global(scope->parent))
        {
            struct data_type return_type_mut;
            data_type_init(&return_type_mut);
            data_type_assign(&return_type_mut, return_type, true);
            return_type_mut.qualifiers.is_mut = true;

            struct identifier return_value_identifier;
            identifier_init(&return_value_identifier);
            return_value_identifier.start = C99_RETURN_VALUE_IDENTIFIER;
            return_value_identifier.length =
                cstr_len(C99_RETURN_VALUE_IDENTIFIER);

            nstr_t nstr_defer_fini return_type_nstr =
                data_type_to_c_source_nstr(
                    &return_type_mut, &return_value_identifier);

            c99_codegen_do_indent(ctx);
            C99_SRC_APPEND(ctx, "%s;\n", return_type_nstr.data);

            identifier_fini(&return_value_identifier);
            data_type_fini(&return_type_mut);
        }

        // Break conditional declaration.
        if (NULL == scope->defer_scope && scope == scope->loop_scope)
        {
            c99_codegen_do_indent(ctx);
            C99_SRC_APPEND(ctx, C99_BREAK_CONDITIONAL_DECLARATION);
        }

        for (size_t i = 0; i < deferred_scopes_length; ++i)
        {
            c99_codegen_do_indent(ctx);
            C99_SRC_APPEND(ctx, C99_DEFER_CONDITIONAL_DECLARATION, i);
        }
    }

    //// The true "body" of the scope.
    //// Generate code for all statements in the scope.
    for (size_t i = 0; i < narr_length(scope->stmts); ++i)
    {
        c99_codegen_stmt(ctx, &(scope->stmts[i]));
        C99_SRC_APPEND(ctx, "\n");
    }

    //// End of non-global scope defer blocks and labels.
    if (!is_global)
    {
        C99_SRC_APPEND(ctx, "\n");
        c99_codegen_do_indent(ctx);
        C99_SRC_APPEND(ctx, C99_SCOPE_END_LABEL ":;\n", scope->uid);

        for (size_t i = 0; i < deferred_scopes_length; ++i)
        {
            size_t const idx = (size_t) deferred_scopes_length - i - 1;
            c99_codegen_do_indent(ctx);
            C99_SRC_APPEND(ctx, C99_DEFER_SCOPE_END_IF_STATEMENT, idx);
            c99_codegen_scope(ctx, scope->deferred_scopes[idx]);
        }

        //// Jump labels that will correctly execute defer statements up the
        //// scope stack.
        if (NULL == scope->defer_scope)
        {
            if (scope == scope->loop_scope)
            {
                c99_codegen_do_indent(ctx);
                C99_SRC_APPEND(ctx, C99_BREAK_SCOPE_END__NOT_NESTED);
            }
            else if (NULL != scope->loop_scope)
            {
                c99_codegen_do_indent(ctx);
                uint32_t parent_uid = scope->parent->uid;
                C99_SRC_APPEND(ctx, C99_BREAK_SCOPE_END__NESTED, parent_uid);
            }
        }

        // Handle function return type.
        nassert(is_within_function);
        struct data_type* const return_type =
            &ctx->cur_func->dt.func_sig->return_dt;
        if (!data_type_is_void(return_type) && scope_is_global(scope->parent))
        {
            c99_codegen_do_indent(ctx);
            C99_SRC_APPEND(ctx, "return %s;\n", C99_RETURN_VALUE_IDENTIFIER);
        }

        ctx->indent_level -= 1;
        c99_codegen_do_indent(ctx);
        C99_SRC_APPEND(ctx, "}\n");
    }

    // Restore the current scope uid.
    ctx->current_scope_uid = current_scope_uid_save;
}

void c99_codegen_stmt(struct c99_codegen_context* ctx, struct stmt* stmt)
{
    switch (stmt->tag)
    {
    case STMT_IMPORT:
        /* nothing */
        break;

    case STMT_ALIAS:
        c99_codegen_alias_stmt(ctx, stmt->alias_stmt);
        break;

    case STMT_VARIABLE_DECLARATION:
        c99_codegen_do_indent(ctx);
        c99_codegen_variable_decl_stmt(ctx, stmt->variable_decl);
        break;

    case STMT_FUNCTION_DECLARATION:
        c99_codegen_do_indent(ctx);
        c99_codegen_function_decl_stmt(ctx, stmt->function_decl);
        break;

    case STMT_STRUCT_DECLARATION:
        c99_codegen_struct_decl_stmt(ctx, stmt->struct_decl);
        break;

    case STMT_VARIANT_DECLARATION:
        c99_codegen_variant_decl(ctx, stmt->variant_decl);
        break;

    case STMT_IF:
        c99_codegen_if_stmt(ctx, stmt->if_stmt);
        break;

    case STMT_LOOP:
        c99_codegen_loop_stmt(ctx, stmt->loop_stmt);
        break;

    case STMT_DEFER:
        c99_codegen_do_indent(ctx);
        C99_SRC_APPEND(
            ctx, C99_DEFER_CONDITIONAL_SET_TRUE, stmt->defer_stmt.idx);
        break;

    case STMT_BREAK:
    {
        c99_codegen_do_indent(ctx);
        C99_SRC_APPEND(ctx, C99_BREAK_CONDITIONAL_SET_TRUE);
        c99_codegen_do_indent(ctx);
        static char const* const fmt_break_jmp =
            "/*break[1]*/goto " C99_SCOPE_END_LABEL ";";
        C99_SRC_APPEND(ctx, fmt_break_jmp, ctx->current_scope_uid);
    }
    break;

    case STMT_CONTINUE:
    {
        static char const* const fmt_continue_jmp =
            "/*continue*/goto " C99_SCOPE_END_LABEL ";";
        c99_codegen_do_indent(ctx);
        C99_SRC_APPEND(ctx, fmt_continue_jmp, ctx->current_scope_uid);
    }
    break;

    case STMT_RETURN:
        c99_codegen_do_indent(ctx);
        if (NULL != stmt->return_expr)
        {
            C99_SRC_APPEND(ctx, "/*return[0]*/%s = ", C99_RETURN_VALUE_IDENTIFIER);
            c99_codegen_expr(ctx, stmt->return_expr);
            C99_SRC_APPEND(ctx, ";\n");
        }
        c99_codegen_do_indent(ctx);
        C99_SRC_APPEND(
            ctx,
            "/*return[1]*/goto " C99_SCOPE_END_LABEL ";",
            ctx->cur_func->def->uid);
        break;

    case STMT_EXPR:
        c99_codegen_do_indent(ctx);
        c99_codegen_expr(ctx, stmt->expr);
        C99_SRC_APPEND(ctx, ";");
        break;

    case STMT_BLOCK:
        c99_codegen_scope(ctx, stmt->block);
        break;

    case STMT_EMPTY:
        C99_SRC_APPEND(ctx, ";");
        break;

    default:
        NPANIC_DFLT_CASE();
    }
}

stbool c99_codegen(struct module* module, char const* out_path_override)
{
    char const* const out_path =
        out_path_override ? out_path_override : module->out_path.data;

    struct c99_codegen_context ctx;
    c99_codegen_context_init(&ctx, module);

    nstr_t nstr_defer_fini nlang_core_c99;
    nstr_init(&nlang_core_c99);
    // clang-format off
    ssize_t bytes_read = file_read_into_nstr(
        STRINGIFY(N_INCLUDE_INSTALL_DIR/core.c99.h), &nlang_core_c99);
    // clang-format on
    if (-1 == bytes_read)
    {
        nlogf(LOG_ERROR, "Failed to read in nlang c99 core.");
        return STBOOL_FAILURE;
    }

    //// Append nlang core.
    C99_SRC_APPEND(&ctx, nlang_core_c99.data);
    C99_SRC_APPEND(&ctx, "\n");

    //// Append all imports.
    //! @todo
    //!     This process of importing all symbols, looking them up, and then
    //!     using them for code generation is both slow and confusing.
    //!     This whole process of adding imports to this file needs to be made
    //!     much more simple.
    C99_SRC_APPEND(&ctx, "/* BEGIN IMPORTS */\n");
    size_t const exports_len = narr_length(module->exports);
    for (size_t i = 0; i < exports_len; ++i)
    {
        struct identifier* const export = &module->exports[i];
        if (is_import(module, export->start, export->length))
        {
            struct symbol* import = symbol_table_find(
                module->global_scope.symbol_table,
                export->start,
                export->length);
            switch (import->tag)
            {
            case SYMBOL_VARIABLE:
            {
                struct variable_decl* const variable_decl =
                    import->ref.variable_decl;
                c99_codegen_variable_decl_forward(&ctx, variable_decl);
                C99_SRC_APPEND(&ctx, ";");
            }
            break;
            case SYMBOL_FUNCTION:
            {
                struct function_decl* const function_decl =
                    import->ref.function_decl;
                c99_codegen_function_decl_forward(&ctx, function_decl);
                C99_SRC_APPEND(&ctx, ";");
            }
            break;
            case SYMBOL_STRUCT:
            {
                c99_codegen_struct_forward(&ctx, import->ref.struct_decl);
                struct struct_def* struct_def = import->dt->struct_ref->def;
                if (NULL != struct_def)
                {
                    nassert(data_type_is_struct(import->dt));
                    c99_codegen_struct_definition(&ctx, struct_def);
                }
                C99_SRC_APPEND(&ctx, ";");
            }
            break;
            case SYMBOL_VARIANT:
            {
                c99_codegen_variant_decl(&ctx, import->ref.variant_decl);
            }
            break;
            case SYMBOL_ALIAS:
                c99_codegen_alias_stmt(&ctx, import->ref.alias);
                break;
            default:
                printf("Error: %i", import->tag);
                NPANIC_DFLT_CASE();
            }
            C99_SRC_APPEND(&ctx, "\n");
        }
    }
    C99_SRC_APPEND(&ctx, "/* END IMPORTS */\n\n");

    //// Append this module's contents.
    c99_codegen_scope(&ctx, &module->global_scope);

    int const contains_entry_func_result =
        scope_contains_entry_func(&module->global_scope);
    if (INVALID_ENTRY_FUNC == contains_entry_func_result)
    {
        nlogf(LOG_ERROR, "Invalid type signature for entry function.");
        return STBOOL_FAILURE;
    }
    else if (ENTRY_FUNC_NO_ARGS_RTN_VOID == contains_entry_func_result)
    {
        C99_SRC_APPEND(&ctx, C99_CODEGEN_MAIN__ENTRY_FUNC_NO_ARGS_RTN_VOID);
    }
    else if (ENTRY_FUNC_NO_ARGS_RTN_U == contains_entry_func_result)
    {
        C99_SRC_APPEND(&ctx, C99_CODEGEN_MAIN__ENTRY_FUNC_NO_ARGS_RTN_U);
    }
    else if (ENTRY_FUNC_NO_ARGS_RTN_S == contains_entry_func_result)
    {
        C99_SRC_APPEND(&ctx, C99_CODEGEN_MAIN__ENTRY_FUNC_NO_ARGS_RTN_S);
    }
    else if (ENTRY_FUNC_ARGS_RTN_VOID == contains_entry_func_result)
    {
        C99_SRC_APPEND(&ctx, C99_CODEGEN_MAIN__ENTRY_FUNC_ARGS_RTN_VOID);
    }
    else if (ENTRY_FUNC_ARGS_RTN_U == contains_entry_func_result)
    {
        C99_SRC_APPEND(&ctx, C99_CODEGEN_MAIN__ENTRY_FUNC_ARGS_RTN_U);
    }
    else if (ENTRY_FUNC_ARGS_RTN_S == contains_entry_func_result)
    {
        C99_SRC_APPEND(&ctx, C99_CODEGEN_MAIN__ENTRY_FUNC_ARGS_RTN_S);
    }

    FILE* outfile = fopen(out_path, "w");
    if (NULL == outfile)
    {
        nlogf(LOG_ERROR, "Failed to open C file '%s'.", out_path);
        c99_codegen_context_fini(&ctx);
        return STBOOL_FAILURE;
    }
    fprintf(outfile, "%s", ctx.c_code_nstr.data);
    fclose(outfile);

    c99_codegen_context_fini(&ctx);
    return STBOOL_SUCCESS;
}

void c99_codegen_variant_decl(struct c99_codegen_context* ctx,
        struct variant_decl* variant)
{
    C99_SRC_APPEND(ctx, "/* variant */ struct ");
    c99_codegen_identifier(ctx, &variant->id);

    if(variant->def != NULL)
    {
        c99_codegen_variant_def(ctx, variant->def);
    }
    C99_SRC_APPEND(ctx, ";\n");

    if(variant->def != NULL)
    {
        size_t n_ifaces = narr_length(variant->def->member_ifaces);
        for(size_t i = 0; i < n_ifaces; ++i)
        {
            c99_codegen_member_iface(ctx, &variant->def->member_ifaces[i],
                    variant);
        }
    }
}

void c99_codegen_variant_def(struct c99_codegen_context* ctx,
        struct variant_def* variant)
{
    C99_SRC_APPEND(ctx, "\n");
    c99_codegen_do_indent(ctx);
    C99_SRC_APPEND(ctx, "{\n");
    ctx->indent_level++;

    size_t n_attrs = narr_length(variant->member_vars);
    for(size_t i = 0; i < n_attrs; ++i)
    {
        c99_codegen_member_var(ctx, &variant->member_vars[i]);
    }

    struct identifier tag_id;
    identifier_init(&tag_id);
    tag_id.start = "tag";
    tag_id.length = strlen("tag");
    tag_id.srctok = NULL;

    c99_codegen_do_indent(ctx);
    c99_codegen_data_type(ctx, &variant->tag_dt, &tag_id);
    C99_SRC_APPEND(ctx, ";\n");


    C99_SRC_APPEND(ctx, "\n");
    c99_codegen_do_indent(ctx);
    C99_SRC_APPEND(ctx, "/* tags */ union\n");
    c99_codegen_do_indent(ctx);
    C99_SRC_APPEND(ctx, "{\n");
    ctx->indent_level++;

    size_t n_tags = narr_length(variant->member_tags);
    for(size_t i = 0; i < n_tags; ++i)
    {
        c99_codegen_member_tag(ctx, &variant->member_tags[i]);
    }

    ctx->indent_level--;
    c99_codegen_do_indent(ctx);
    C99_SRC_APPEND(ctx, "};\n");
    ctx->indent_level--;
    c99_codegen_do_indent(ctx);
    C99_SRC_APPEND(ctx, "}");

    identifier_fini(&tag_id);
}

void c99_codegen_member_var(struct c99_codegen_context* ctx,
        struct member_var* mv)
{
    c99_codegen_do_indent(ctx);
    c99_codegen_data_type(ctx, &mv->dt, &mv->id);
    C99_SRC_APPEND(ctx, ";\n");
}

void c99_codegen_member_tag(struct c99_codegen_context* ctx,
        struct member_tag* mt)
{
    c99_codegen_do_indent(ctx);
    C99_SRC_APPEND(ctx, "/* tag */ struct\n");
    c99_codegen_do_indent(ctx);
    ctx->indent_level++;
    C99_SRC_APPEND(ctx, "{\n");

    size_t n_attrs = narr_length(mt->section.member_vars);
    for(size_t i = 0; i < n_attrs; ++i)
    {
        c99_codegen_member_var(ctx, &mt->section.member_vars[i]);
    }

    ctx->indent_level--;
    c99_codegen_do_indent(ctx);
    C99_SRC_APPEND(ctx, "} ");
    c99_codegen_identifier(ctx, &mt->id);
    C99_SRC_APPEND(ctx, ";\n");
}

void c99_codegen_member_iface(struct c99_codegen_context* ctx,
        struct member_iface* mi, struct variant_decl* variant)
{
    nstr_t nstr_defer_fini iface_name;
    struct identifier id;
    nstr_init(&iface_name);
    nstr_assign_fmt(&iface_name, FMT_IFACE_IDENTIFIER,
            (int) variant->id.length, variant->id.start,
            (int) mi->id.length, mi->id.start);
    id.start = iface_name.data;
    id.length = iface_name.len;
    id.srctok = NULL;

    c99_codegen_do_indent(ctx);
    c99_codegen_data_type(ctx, &mi->sig.return_dt, &id);

    C99_SRC_APPEND(ctx, "(");
    const size_t n_args = narr_length(mi->sig.param_dts);
    for(size_t i = 0; i < n_args; ++i)
    {
        nstr_t nstr_defer_fini param;
        nstr_init(&param);
        nstr_assign_fmt(&param, "n_iface_param_%i", (int) i);
        id.start = param.data;
        id.length = param.len;
        c99_codegen_data_type(ctx, &mi->sig.param_dts[i], &id);
        if(i < n_args-1)
        {
            C99_SRC_APPEND(ctx, ", ");
        }
    }

    C99_SRC_APPEND(ctx, ")\n");
    c99_codegen_do_indent(ctx);
    C99_SRC_APPEND(ctx, "{\n");
    ctx->indent_level++;

    c99_codegen_do_indent(ctx);
    C99_SRC_APPEND(ctx, "switch(n_iface_param_0->tag)\n");
    c99_codegen_do_indent(ctx);
    C99_SRC_APPEND(ctx, "{\n");

    const bool return_void = mi->sig.return_dt.tag == DT_VOID;
    const size_t n_tags = narr_length(variant->def->member_tags);
    for(size_t i = 0; i < n_tags; ++i)
    {
        c99_codegen_do_indent(ctx);
        C99_SRC_APPEND(ctx, "case ");
        c99_codegen_literal(ctx, &variant->def->member_tags[i].value);
        C99_SRC_APPEND(ctx, ":\n");
        ctx->indent_level++;
        c99_codegen_do_indent(ctx);
        if(!return_void)
        {
            C99_SRC_APPEND(ctx, "return ");
        }
        c99_codegen_identifier(ctx,
            &variant->def->member_tags[i].section.member_funcs[
            struct_def_member_func_idx(&variant->def->member_tags[i].section,
            &mi->id)].target.id);
        C99_SRC_APPEND(ctx, "(");
        for(size_t j = 0; j < n_args; ++j)
        {
            nstr_t nstr_defer_fini param;
            nstr_init(&param);
            nstr_assign_fmt(&param, "n_iface_param_%i", (int) j);
            id.start = param.data;
            id.length = param.len;
            c99_codegen_identifier(ctx, &id);
            if(j < n_args-1)
            {
                C99_SRC_APPEND(ctx, ", ");
            }
        }

        C99_SRC_APPEND(ctx, ");\n");
        if(return_void)
        {
            c99_codegen_do_indent(ctx);
            C99_SRC_APPEND(ctx, "break;\n");
        }
        ctx->indent_level--;
    }

    c99_codegen_do_indent(ctx);
    C99_SRC_APPEND(ctx, "default:\n");
    ctx->indent_level++;
    c99_codegen_do_indent(ctx);
    if(return_void)
    {
        C99_SRC_APPEND(ctx, "break;\n");
    }
    else
    {
        C99_SRC_APPEND(ctx, "return n_null;\n");
    }
    ctx->indent_level--;

    c99_codegen_do_indent(ctx);
    C99_SRC_APPEND(ctx, "}\n");

    ctx->indent_level--;
    c99_codegen_do_indent(ctx);
    C99_SRC_APPEND(ctx, "}\n");
}
