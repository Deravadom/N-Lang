/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file lowering.h
 * @brief Lowering the Parse Tree into NIR
 *
 */

#include "parser.h"

//! Lower the parse tree to NIR
//!
//! The parse tree has the shape of the grammar. Keeping it
//! this way makes reasoning about the parser much simpler.
//! To do anything useful with the parse tree however, we
//! first need to lower the parse tree into NIR.
//!
//! Most of these functions have the form:
//! function(destination, source, module)
//!
//! where destination is the NIR representation
//!       source is the parse tree representation
//!       module contains useful information for NIR
//!
//! {
void lower_to_nir(struct module* module, struct parse_tree* parse_tree);

void lower_global_statement(
    struct stmt* stmt, struct parse_tree* parse_tree, struct module* module);

void lower_list_block_statement(
    struct scope* scope, struct parse_tree* parse_tree, struct module* module);

void lower_block_statement(
    struct stmt* stmt, struct parse_tree* parse_tree, struct module* module);

void lower_data_type(
    struct data_type* dt, struct parse_tree* parse_tree, struct module* module);

void lower_data_type_unqualified(
    struct data_type* dt, struct parse_tree* parse_tree, struct module* module);

void lower_expression(
    struct expr* expr, struct parse_tree* parse_tree, struct module* module);

void lower_literal(
    struct literal* literal,
    struct parse_tree* parse_tree,
    struct module* module);

void lower_list_struct_member(
    struct struct_def* def,
    struct parse_tree* parse_tree,
    struct module* module);

void lower_argwithtype(
    struct function_decl* decl,
    struct parse_tree* parse_tree,
    struct module* module);
void lower_list_argwithtype(
    struct function_decl* decl,
    struct parse_tree* parse_tree,
    struct module* module);

void lower_list_arg(
    narr_t(struct literal) * array,
    struct parse_tree* parse_tree,
    struct module* module);

void lower_if_branch(
    struct if_stmt* ifstmt,
    struct parse_tree* parse_tree,
    struct module* module);

void lower_binary_expression(
    struct expr* expr, struct parse_tree* parse_tree, struct module* module);

void lower_unary_expression(
    struct expr* expr, struct parse_tree* parse_tree, struct module* module);

void lower_postfix_expression(
    struct expr* expr, struct parse_tree* parse_tree, struct module* module);

void lower_identifier(struct identifier* ident, struct token* id);

void lower_variant_def(struct variant_def*, struct parse_tree*, struct module*);

void lower_member_var(struct member_var*, struct parse_tree*, struct module*);

void lower_member_func(struct member_func*, struct parse_tree*, struct module*);

void lower_member_iface(struct member_iface*, struct parse_tree*,
        struct module*);

void lower_member_sections(struct variant_def*, struct parse_tree*,
        struct module*);
//! }
