/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "nir.h"

//============================================================================//
//      ASCII CHARACTER                                                       //
//============================================================================//
bool n_ascii_is_valid(n_ascii ch)
{
    DIAGNOSTIC_PUSH
    DIAGNOSTIC_IGNORE("-Wtype-limits");
    return (ch >= N_ASCII_MIN) && (ch <= N_ASCII_MAX);
    DIAGNOSTIC_POP
}

bool n_ascii_is_printable(n_ascii ch)
{
    return (ch >= N_ASCII_PRINTABLE_MIN) && (ch <= N_ASCII_PRINTABLE_MAX);
}

char const* n_ascii_to_n_source_char(n_ascii ch)
{
    nassert(n_ascii_is_valid(ch));
    switch (ch)
    {
    case ' ':
        return " ";
    case '!':
        return "!";
    case '\"':
        return "\\\"";
    case '#':
        return "#";
    case '$':
        return "$";
    case '%':
        return "%";
    case '&':
        return "&";
    case '\'':
        return "\\\'";
    case '(':
        return "(";
    case ')':
        return ")";
    case '*':
        return "*";
    case '+':
        return "+";
    case ',':
        return ",";
    case '-':
        return "-";
    case '.':
        return ".";
    case '/':
        return "/";
    case '0':
        return "0";
    case '1':
        return "1";
    case '2':
        return "2";
    case '3':
        return "3";
    case '4':
        return "4";
    case '5':
        return "5";
    case '6':
        return "6";
    case '7':
        return "7";
    case '8':
        return "8";
    case '9':
        return "9";
    case ':':
        return ":";
    case ';':
        return ";";
    case '<':
        return "<";
    case '=':
        return "=";
    case '>':
        return ">";
    case '\?':
        return "\?";
    case '@':
        return "@";
    case 'A':
        return "A";
    case 'B':
        return "B";
    case 'C':
        return "C";
    case 'D':
        return "D";
    case 'E':
        return "E";
    case 'F':
        return "F";
    case 'G':
        return "G";
    case 'H':
        return "H";
    case 'I':
        return "I";
    case 'J':
        return "J";
    case 'K':
        return "K";
    case 'L':
        return "L";
    case 'M':
        return "M";
    case 'N':
        return "N";
    case 'O':
        return "O";
    case 'P':
        return "P";
    case 'Q':
        return "Q";
    case 'R':
        return "R";
    case 'S':
        return "S";
    case 'T':
        return "T";
    case 'U':
        return "U";
    case 'V':
        return "V";
    case 'W':
        return "W";
    case 'X':
        return "X";
    case 'Y':
        return "Y";
    case 'Z':
        return "Z";
    case '[':
        return "[";
    case '\\':
        return "\\\\";
    case ']':
        return "]";
    case '^':
        return "^";
    case '_':
        return "_";
    case '`':
        return "`";
    case 'a':
        return "a";
    case 'b':
        return "b";
    case 'c':
        return "c";
    case 'd':
        return "d";
    case 'e':
        return "e";
    case 'f':
        return "f";
    case 'g':
        return "g";
    case 'h':
        return "h";
    case 'i':
        return "i";
    case 'j':
        return "j";
    case 'k':
        return "k";
    case 'l':
        return "l";
    case 'm':
        return "m";
    case 'n':
        return "n";
    case 'o':
        return "o";
    case 'p':
        return "p";
    case 'q':
        return "q";
    case 'r':
        return "r";
    case 's':
        return "s";
    case 't':
        return "t";
    case 'u':
        return "u";
    case 'v':
        return "v";
    case 'w':
        return "w";
    case 'x':
        return "x";
    case 'y':
        return "y";
    case 'z':
        return "z";
    case '{':
        return "{";
    case '|':
        return "|";
    case '}':
        return "}";
    case '~':
        return "~";
    case '\0':
        return "\\0";
    case '\a':
        return "\\a";
    case '\b':
        return "\\b";
    case N_ASCII_ESC:
        return "\\x1B";
    case '\f':
        return "\\f";
    case '\n':
        return "\\n";
    case '\r':
        return "\\r";
    case '\t':
        return "\\t";
    case '\v':
        return "\\v";
    default:
        npanic("unknown character");
    }
}

char const* n_ascii_to_c_source_char(n_ascii ch)
{
    nassert(n_ascii_is_valid(ch));
    // The only difference between N characters and C characters is that the
    // question mark chatacter '?' in N must be escaped in C, '\?'. Check for
    // this special case; if the character is a question mark then print its
    // escaped C version, otherwise print the equivalent N version.
    if (ch == '\?')
    {
        return "\\\?";
    }
    return n_ascii_to_n_source_char(ch);
}

int parse_n_ascii(char const* start, n_ascii* ascii)
{
    char const c = *start;

    // Escaped.
    if (c == '\\')
    {
        switch (*(start + 1))
        {
        case '0':
            *ascii = '\0';
            break;
        case 'a':
            *ascii = '\a';
            break;
        case 'b':
            *ascii = '\b';
            break;
        case 'e':
            *ascii = N_ASCII_ESC;
            break;
        case 'f':
            *ascii = '\f';
            break;
        case 'n':
            *ascii = '\n';
            break;
        case 'r':
            *ascii = '\r';
            break;
        case 't':
            *ascii = '\t';
            break;
        case 'v':
            *ascii = '\v';
            break;
        case '\"':
            *ascii = '\"';
            break;
        case '\'':
            *ascii = '\'';
            break;
        case '\\':
            *ascii = '\\';
            break;
        default:
            return -1;
        }
        return 2;
    }

    // Unescaped.
    if (isprint(c) && ('\'' != c) && ('\"' != c))
    {
        *ascii = c;
        return 1;
    }

    return -1;
}

int parse_n_ascii_literal(char const* start, n_ascii* ascii)
{
    char const* cur = start;
    if (*cur++ != '\'')
    {
        return -1;
    }
    int parse_n_ascii_result = parse_n_ascii(cur, ascii);
    if (-1 == parse_n_ascii_result)
    {
        return -1;
    }
    cur += parse_n_ascii_result;

    if (*cur++ != '\'')
    {
        return -1;
    }
    // Check the type suffix.
    if ('a' != *cur++)
    {
        return -1;
    }

    return (int)(cur - start);
}

//============================================================================//
//      ASCII STRING                                                          //
//============================================================================//
int parse_n_astr_literal(char const* start, nstr_t* nstr)
{
    nstr_clear(nstr);
    char const* cur = start;
    if ('\"' != *cur++)
    {
        return -1;
    }

    int parse_n_ascii_result;
    n_ascii ascii;
    while ((parse_n_ascii_result = parse_n_ascii(cur, &ascii)) >= 0)
    {
        cur += parse_n_ascii_result;
        nstr_cat_fmt(nstr, "%c", ascii);
    }

    // Check to see if the parse failure was due to a legitimate bad character
    // or just the end double quote being reached.
    if ('\"' != *cur++)
    {
        return -1;
    }
    // Check the type suffix.
    if ('a' != *cur++)
    {
        return -1;
    }

    return (int)(cur - start);
}

//============================================================================//
//      IDENTIFIER                                                            //
//============================================================================//
INIT_FINI_COUNTER_SOURCE(identifier);

void identifier_init(struct identifier* this)
{
    INIT_FINI_COUNTER_INCR_INIT(identifier);
    MEMZERO_INSTANCE(this, typeof(*this));
}

void identifier_fini(struct identifier* this)
{
    INIT_FINI_COUNTER_INCR_FINI(identifier);
    SUPPRESS_UNUSED(this);
}

void identifier_assign(
    struct identifier* dest,
    struct identifier const* src,
    bool clear_srctok
)
{
    POD_COPY(*dest, *src);
    dest->srctok = clear_srctok ? NULL : src->srctok;
}

bool identifier_eq(struct identifier const* lhs, struct identifier const* rhs)
{
    return (lhs->length == rhs->length)
        && cstr_n_eq(lhs->start, rhs->start, lhs->length);
}
//============================================================================//
//      DATA TYPE                                                             //
//============================================================================//
INIT_FINI_COUNTER_SOURCE(data_type);

void data_type_init(struct data_type* this)
{
    INIT_FINI_COUNTER_INCR_INIT(data_type);
    MEMZERO_INSTANCE(this, typeof(*this));
}

void data_type_fini(struct data_type* this)
{
    INIT_FINI_COUNTER_INCR_FINI(data_type);

    if (NULL != this->inner)
    {
        data_type_fini(this->inner);
        nfree(this->inner);
    }

    switch (this->tag)
    {
    case DT_UNSET:  /* intentional fallthrough */
    case DT_VOID:   /* intentional fallthrough */
    case DT_U8:     /* intentional fallthrough */
    case DT_U16:    /* intentional fallthrough */
    case DT_U32:    /* intentional fallthrough */
    case DT_U64:    /* intentional fallthrough */
    case DT_U:      /* intentional fallthrough */
    case DT_S8:     /* intentional fallthrough */
    case DT_S16:    /* intentional fallthrough */
    case DT_S32:    /* intentional fallthrough */
    case DT_S64:    /* intentional fallthrough */
    case DT_S:      /* intentional fallthrough */
    case DT_F16:    /* intentional fallthrough */
    case DT_F32:    /* intentional fallthrough */
    case DT_F64:    /* intentional fallthrough */
    case DT_F128:   /* intentional fallthrough */
    case DT_BOOL:   /* intentional fallthrough */
    case DT_ASCII:  /* intentional fallthrough */
    case DT_ENUM:   /* intentional fallthrough */
    case DT_UNION:  /* intentional fallthrough */
    case DT_STRUCT: /* intentional fallthrough */
    case DT_VARIANT:
        /* nothing */
        break;
    case DT_FUNCTION:
        func_sig_fini(this->func_sig);
        nfree(this->func_sig);
        break;
    case DT_POINTER: /* intentional fallthrough */
    case DT_ARRAY:
        /* nothing */
        break;
    case DT_UNRESOLVED_ID:
        identifier_fini(&this->unresolved.id);
        break;
    case DT_UNRESOLVED_TYPEOF_EXPR:
        expr_fini(this->unresolved.typeof_expr);
        nfree(this->unresolved.typeof_expr);
        break;
    case DT_UNRESOLVED_TYPEOF_DT:
        data_type_fini(this->unresolved.typeof_dt);
        nfree(this->unresolved.typeof_dt);
        break;
    default:
        NPANIC_DFLT_CASE();
    }
}

void data_type_reset(struct data_type* this)
{
    data_type_fini(this);
    data_type_init(this);
}

void data_type_assign(
    struct data_type* dest,
    struct data_type const* src,
    bool clear_srctok)
{
    data_type_reset(dest);

    if (NULL != src->inner)
    {
        dest->inner = nalloc(sizeof(typeof(*dest->inner)));
        data_type_init(dest->inner);
        data_type_assign(dest->inner, src->inner, clear_srctok);
    }

    // POD copy of qualifiers.
    dest->qualifiers = src->qualifiers;

    dest->tag = src->tag;
    switch (src->tag)
    {
    case DT_UNSET: /* intentional fallthrough */
    case DT_VOID:  /* intentional fallthrough */
    case DT_U8:    /* intentional fallthrough */
    case DT_U16:   /* intentional fallthrough */
    case DT_U32:   /* intentional fallthrough */
    case DT_U64:   /* intentional fallthrough */
    case DT_U:     /* intentional fallthrough */
    case DT_S8:    /* intentional fallthrough */
    case DT_S16:   /* intentional fallthrough */
    case DT_S32:   /* intentional fallthrough */
    case DT_S64:   /* intentional fallthrough */
    case DT_S:     /* intentional fallthrough */
    case DT_F16:   /* intentional fallthrough */
    case DT_F32:   /* intentional fallthrough */
    case DT_F64:   /* intentional fallthrough */
    case DT_F128:  /* intentional fallthrough */
    case DT_BOOL:  /* intentional fallthrough */
    case DT_ASCII:
        /* nothing */
        break;
    case DT_ENUM:
        dest->enum_ref = src->enum_ref;
        break;
    case DT_UNION:
        dest->union_ref = src->union_ref;
        break;
    case DT_STRUCT:
        dest->struct_ref = src->struct_ref;
        break;
    case DT_VARIANT:
        dest->variant_ref = src->variant_ref;
        break;
    case DT_FUNCTION:
        dest->func_sig = nalloc(sizeof(typeof(*dest->func_sig)));
        func_sig_init(dest->func_sig);
        func_sig_assign(dest->func_sig, src->func_sig, clear_srctok);
        break;
    case DT_POINTER:
        /* nothing */
        break;
    case DT_ARRAY:
        dest->array.length = src->array.length;
        break;
    case DT_UNRESOLVED_ID:
        identifier_init(&dest->unresolved.id);
        identifier_assign(
            &dest->unresolved.id, &src->unresolved.id, clear_srctok);
        break;
    case DT_UNRESOLVED_TYPEOF_EXPR:
        dest->unresolved.typeof_expr =
            nalloc(sizeof(typeof(*dest->unresolved.typeof_expr)));
        expr_init(dest->unresolved.typeof_expr);
        expr_assign(
            dest->unresolved.typeof_expr,
            src->unresolved.typeof_expr,
            clear_srctok);
        break;
    case DT_UNRESOLVED_TYPEOF_DT:
        dest->unresolved.typeof_dt =
            nalloc(sizeof(typeof(*dest->unresolved.typeof_dt)));
        data_type_init(dest->unresolved.typeof_dt);
        data_type_assign(
            dest->unresolved.typeof_dt,
            src->unresolved.typeof_dt,
            clear_srctok);
        break;
    default:
        NPANIC_DFLT_CASE();
    }

    dest->srctok = clear_srctok ? NULL : src->srctok;
}

size_t data_type_length_all(struct data_type const* this)
{
    if (NULL == this->inner)
    {
        return 1;
    }
    return data_type_length_all(this->inner) + 1;
}

size_t data_type_length_modifiers(struct data_type const* this)
{
    return data_type_length_all(this) - 1;
}

void data_type_push_modifier(struct data_type* this, struct data_type* modifier)
{
    modifier->inner = modifier;
    memswap(this, modifier, sizeof(typeof(*this)));
}

void data_type_pop_modifier(struct data_type* this)
{
    nassert(data_type_length_all(this) != 1);
    struct data_type* const inner = this->inner;
    this->inner = NULL;
    data_type_fini(this);
    *this = *inner;
    nfree(inner);
}

bool data_type_is_void(struct data_type const* this)
{
    return data_type_is_base(this) && (DT_VOID == this->tag);
}

bool data_type_is_integer(struct data_type const* this)
{
    return data_type_is_base(this) && (this->tag >= DT_U8 && this->tag <= DT_S);
}

bool data_type_is_integer_unsigned(struct data_type const* this)
{
    return data_type_is_base(this) && (this->tag >= DT_U8 && this->tag <= DT_U);
}

bool data_type_is_integer_signed(struct data_type const* this)
{
    return data_type_is_base(this) && (this->tag >= DT_S8 && this->tag <= DT_S);
}

bool data_type_is_float(struct data_type const* this)
{
    return data_type_is_base(this)
        && (this->tag >= DT_F16 && this->tag <= DT_F128);
}

bool data_type_is_bool(struct data_type const* this)
{
    return data_type_is_base(this) && (DT_BOOL == this->tag);
}

bool data_type_is_ascii(struct data_type const* this)
{
    return data_type_is_base(this) && (DT_ASCII == this->tag);
}

bool data_type_is_enum(struct data_type const* this)
{
    return data_type_is_base(this) && (DT_ENUM == this->tag);
}

bool data_type_is_union(struct data_type const* this)
{
    return data_type_is_base(this) && (DT_UNION == this->tag);
}

bool data_type_is_struct(struct data_type const* this)
{
    return data_type_is_base(this) && (DT_STRUCT == this->tag);
}

bool data_type_is_variant(struct data_type const* this)
{
    return data_type_is_base(this) && (DT_VARIANT == this->tag);
}

bool data_type_is_function(struct data_type const* this)
{
    return data_type_is_base(this) && (DT_FUNCTION == this->tag);
}

bool data_type_is_pointer(struct data_type const* this)
{
    return data_type_is_modifier(this) && (DT_POINTER == this->tag);
}

bool data_type_is_array(struct data_type const* this)
{
    return data_type_is_modifier(this) && (DT_ARRAY == this->tag);
}

bool data_type_is_base(struct data_type const* this)
{
    return NULL == this->inner;
}

bool data_type_is_modifier(struct data_type const* this)
{
    return NULL != this->inner;
}

bool data_type_is_numeric(struct data_type const* this)
{
    return data_type_is_base(this)
        && (this->tag >= DT_U8 && this->tag <= DT_F128);
}

bool data_type_is_primitive(struct data_type const* this)
{
    return data_type_is_base(this)
        && (this->tag >= DT_VOID && this->tag <= DT_ASCII);
}

bool data_type_is_composite(struct data_type const* this)
{
    return data_type_is_base(this) && (DT_STRUCT == this->tag);
}

bool data_type_is_void_pointer(struct data_type const* this)
{
    return (data_type_length_modifiers(this) == 1) && (DT_POINTER == this->tag)
        && (DT_VOID == this->inner->tag);
}

nstr_t data_type_to_n_source_nstr(struct data_type const* dt)
{
    nstr_t nstr;
    if (NULL == dt->inner)
    {
        nstr_init(&nstr);
    }
    else
    {
        nstr = data_type_to_n_source_nstr(dt->inner);
    }

    //// Tag-based behavior
    switch (dt->tag)
    {
    case DT_VOID:
        nstr_assign_cstr(&nstr, token_type_to_cstr(TOKEN_KEYWORD_VOID));
        break;
    case DT_U8:
        nstr_assign_cstr(&nstr, token_type_to_cstr(TOKEN_KEYWORD_U8));
        break;
    case DT_U16:
        nstr_assign_cstr(&nstr, token_type_to_cstr(TOKEN_KEYWORD_U16));
        break;
    case DT_U32:
        nstr_assign_cstr(&nstr, token_type_to_cstr(TOKEN_KEYWORD_U32));
        break;
    case DT_U64:
        nstr_assign_cstr(&nstr, token_type_to_cstr(TOKEN_KEYWORD_U64));
        break;
    case DT_U:
        nstr_assign_cstr(&nstr, token_type_to_cstr(TOKEN_KEYWORD_U));
        break;
    case DT_S8:
        nstr_assign_cstr(&nstr, token_type_to_cstr(TOKEN_KEYWORD_S8));
        break;
    case DT_S16:
        nstr_assign_cstr(&nstr, token_type_to_cstr(TOKEN_KEYWORD_S16));
        break;
    case DT_S32:
        nstr_assign_cstr(&nstr, token_type_to_cstr(TOKEN_KEYWORD_S32));
        break;
    case DT_S64:
        nstr_assign_cstr(&nstr, token_type_to_cstr(TOKEN_KEYWORD_S64));
        break;
    case DT_S:
        nstr_assign_cstr(&nstr, token_type_to_cstr(TOKEN_KEYWORD_S));
        break;
    case DT_F16:
        nstr_assign_cstr(&nstr, token_type_to_cstr(TOKEN_KEYWORD_F16));
        break;
    case DT_F32:
        nstr_assign_cstr(&nstr, token_type_to_cstr(TOKEN_KEYWORD_F32));
        break;
    case DT_F64:
        nstr_assign_cstr(&nstr, token_type_to_cstr(TOKEN_KEYWORD_F64));
        break;
    case DT_F128:
        nstr_assign_cstr(&nstr, token_type_to_cstr(TOKEN_KEYWORD_F128));
        break;
    case DT_BOOL:
        nstr_assign_cstr(&nstr, token_type_to_cstr(TOKEN_KEYWORD_BOOL));
        break;
    case DT_ASCII:
        nstr_assign_cstr(&nstr, token_type_to_cstr(TOKEN_KEYWORD_ASCII));
        break;
    case DT_ENUM:
        nstr_assign_fmt(
            &nstr,
            "%.*s",
            (int)dt->enum_ref->id.length,
            dt->enum_ref->id.start);
        break;
    case DT_UNION:
        nstr_assign_fmt(
            &nstr,
            "%.*s",
            (int)dt->union_ref->id.length,
            dt->union_ref->id.start);
        break;
    case DT_STRUCT:
        nstr_assign_fmt(
            &nstr,
            "%.*s",
            (int)dt->struct_ref->id.length,
            dt->struct_ref->id.start);
        break;
    case DT_VARIANT:
        nstr_assign_fmt(
            &nstr,
            "%.*s",
            (int)dt->variant_ref->id.length,
            dt->variant_ref->id.start);
        break;
    case DT_FUNCTION:
    {
        nstr_assign_cstr(&nstr, "(");
        size_t const param_dts_len = narr_length(dt->func_sig->param_dts);
        for (size_t p = 0; p < param_dts_len; ++p)
        {
            nstr_t nstr_defer_fini param =
                data_type_to_n_source_nstr(&dt->func_sig->param_dts[p]);
            nstr_cat_nstr(&nstr, &param);
            if (p != param_dts_len - 1)
            {
                nstr_cat_cstr(&nstr, ", ");
            }
        }
        nstr_t nstr_defer_fini rtn =
            data_type_to_n_source_nstr(&dt->func_sig->return_dt);
        nstr_cat_fmt(&nstr, ") -> %s", rtn.data);
    }
    break;
    case DT_POINTER:
        nstr_assign_fmt(&nstr, "^%s", nstr.data);
        break;
    case DT_ARRAY:
        nstr_assign_fmt(
            &nstr,
            "[%zu%s]%s",
            dt->array.length,
            token_type_to_cstr(TOKEN_KEYWORD_U),
            nstr.data);
        break;
    case DT_UNRESOLVED_ID: /* intentional fallthrough */
    case DT_UNRESOLVED_TYPEOF_EXPR:
    case DT_UNRESOLVED_TYPEOF_DT:
        NPANIC_UNEXPECTED_CTRL_FLOW();
    case DT_UNSET:
        NPANIC_UNSET_CASE();
    default:
        NPANIC_DFLT_CASE();
    }

    //// Qualifiers
    if (dt->qualifiers.is_volatile)
    {
        nstr_assign_fmt(&nstr, "volatile %s", nstr.data);
    }
    if (dt->qualifiers.is_unique)
    {
        nstr_assign_fmt(&nstr, "unique %s", nstr.data);
    }
    if (dt->qualifiers.is_mut)
    {
        nstr_assign_fmt(&nstr, "mut %s", nstr.data);
    }

    return nstr;
}

//! Array-list where the 0th element is the outermost data type, the 1st element
//! is the 2nd outermost data type, ..., and the Nth element is the base type.
//! @br
//! This array is used by #_data_type_to_c_source_nstr for manage the reversed
//! onion-like way C data types are written.
static thread_local struct data_type const(*((_data_type_reverse_list)[4096]));
//! Number of elements in #_data_type_reverse_list.
static thread_local size_t _data_type_reverse_list_len;
//! Tracked index into #_data_type_reverse_list.
static thread_local size_t _data_type_reverse_list_idx;
// C data types can best be described as an onion.
// The outermost layer of the type onion is the base type.
// The second-to-outermost layer (if it exists) is the innermost modifier.
// The third-to-outermost layer (if it exists) is the second-innermost modifier.
// Etc.
//
// For non function pointer types this takes the form:
//      <base-type> (<modifiers-and-id>)
// For function pointer types this take the form:
//      <return-type> (* (<modifiers-and-id>))(<parameter-types>)
//
// Examples:
// (1)  int (* volatile ((* const (ID))[3]));
//      "Declare ID as const pointer to array 3 of volatile pointer to int."
// (2)  int (* volatile ((* const (ID))[3]))(char, short)
//      "Declare ID as const pointer to array 3 of volatile pointer to function
//      (char, short) returning int."
//
// For data types that do not have identifiers, you omit the "(ID)" portion of
// the string.
// (2) Without the identifier "foo" would be written as:
//      int (* volatile ((* const)[3]))(char, short)
static inline nstr_t _data_type_to_c_source_nstr(
    struct data_type const* _dt,
    struct identifier const* id)
{
    nstr_t nstr;

    // Populate the reverse list.
    _data_type_reverse_list[_data_type_reverse_list_len++] = _dt;
    if (NULL == _dt->inner)
    {
        nstr_init(&nstr);
        if (NULL != id)
        {
            nstr_assign_fmt(&nstr, "(%.*s)", (int)id->length, id->start);
        }
    }
    else
    {
        nstr = _data_type_to_c_source_nstr(_dt->inner, id);
    }

    // Get the target data type at this level of the onion.
    struct data_type const* target_dt =
        _data_type_reverse_list[_data_type_reverse_list_idx++];
    bool const target_is_void_base =
        data_type_is_void(target_dt) && (_data_type_reverse_list_len == 1);

    //// Qualifiers
    nstr_t nstr_defer_fini qualifiers_nstr;
    nstr_init(&qualifiers_nstr);
    if (!target_dt->qualifiers.is_mut && !target_is_void_base)
    {
        nstr_cat_cstr(&qualifiers_nstr, " const");
    }
    if (target_dt->qualifiers.is_unique)
    {
        nassert(!target_is_void_base);
        nstr_cat_cstr(&qualifiers_nstr, " restrict");
    }
    if (target_dt->qualifiers.is_volatile && !target_is_void_base)
    {
        nstr_cat_cstr(&qualifiers_nstr, " volatile");
    }
    //// Tag-based behavior
    nstr_t nstr_defer_fini tag_nstr;
    nstr_init(&tag_nstr);
    switch (target_dt->tag)
    {
    case DT_VOID:
        nstr_assign_fmt(
            &nstr, "%s %s %s", "void", qualifiers_nstr.data, nstr.data);
        break;
    case DT_U8:
        nstr_assign_fmt(
            &nstr, "%s %s %s", "n_u8", qualifiers_nstr.data, nstr.data);
        break;
    case DT_U16:
        nstr_assign_fmt(
            &nstr, "%s %s %s", "n_u16", qualifiers_nstr.data, nstr.data);
        break;
    case DT_U32:
        nstr_assign_fmt(
            &nstr, "%s %s %s", "n_u32", qualifiers_nstr.data, nstr.data);
        break;
    case DT_U64:
        nstr_assign_fmt(
            &nstr, "%s %s %s", "n_u64", qualifiers_nstr.data, nstr.data);
        break;
    case DT_U:
        nstr_assign_fmt(
            &nstr, "%s %s %s", "n_u", qualifiers_nstr.data, nstr.data);
        break;
    case DT_S8:
        nstr_assign_fmt(
            &nstr, "%s %s %s", "n_s8", qualifiers_nstr.data, nstr.data);
        break;
    case DT_S16:
        nstr_assign_fmt(
            &nstr, "%s %s %s", "n_s16", qualifiers_nstr.data, nstr.data);
        break;
    case DT_S32:
        nstr_assign_fmt(
            &nstr, "%s %s %s", "n_s32", qualifiers_nstr.data, nstr.data);
        break;
    case DT_S64:
        nstr_assign_fmt(
            &nstr, "%s %s %s", "n_s64", qualifiers_nstr.data, nstr.data);
        break;
    case DT_S:
        nstr_assign_fmt(
            &nstr, "%s %s %s", "n_s", qualifiers_nstr.data, nstr.data);
        break;
    case DT_F16:
        nstr_assign_fmt(
            &nstr, "%s %s %s", "n_f16", qualifiers_nstr.data, nstr.data);
        break;
    case DT_F32:
        nstr_assign_fmt(
            &nstr, "%s %s %s", "n_f32", qualifiers_nstr.data, nstr.data);
        break;
    case DT_F64:
        nstr_assign_fmt(
            &nstr, "%s %s %s", "n_f64", qualifiers_nstr.data, nstr.data);
        break;
    case DT_F128:
        nstr_assign_fmt(
            &nstr, "%s %s %s", "n_f128", qualifiers_nstr.data, nstr.data);
        break;
    case DT_BOOL:
        nstr_assign_fmt(
            &nstr, "%s %s %s", "n_bool", qualifiers_nstr.data, nstr.data);
        break;
    case DT_ASCII:
        nstr_assign_fmt(
            &nstr, "%s %s %s", "n_ascii", qualifiers_nstr.data, nstr.data);
        break;
    case DT_FUNCTION:
    {
        nstr_t nstr_defer_fini rtn =
            data_type_to_c_source_nstr(&target_dt->func_sig->return_dt, NULL);

        size_t const param_dts_len =
            narr_length(target_dt->func_sig->param_dts);
        nstr_t nstr_defer_fini params;
        nstr_init(&params);
        if (0 == param_dts_len)
        {
            nstr_cat_cstr(&params, "void");
        }
        for (size_t p = 0; p < param_dts_len; ++p)
        {
            nstr_t nstr_defer_fini param = data_type_to_c_source_nstr(
                &target_dt->func_sig->param_dts[p], NULL);

            nstr_cat_nstr(&params, &param);
            if (p != param_dts_len - 1)
            {
                nstr_cat_cstr(&params, ", ");
            }
        }

        nstr_assign_fmt(
            &nstr,
            "%s (*%s%s)(%s)",
            rtn.data,
            qualifiers_nstr.data,
            nstr.data,
            params.data);
    }
    break;
    case DT_ENUM:
        nstr_assign_fmt(
            &nstr,
            "enum %.*s %s",
            (int)target_dt->enum_ref->id.length,
            target_dt->enum_ref->id.start,
            nstr.data);
        break;
    case DT_UNION:
        nstr_assign_fmt(
            &nstr,
            "union %.*s %s",
            (int)target_dt->union_ref->id.length,
            target_dt->union_ref->id.start,
            nstr.data);
        break;
    case DT_STRUCT:
        nstr_assign_fmt(
            &nstr,
            "struct %.*s %s",
            (int)target_dt->struct_ref->id.length,
            target_dt->struct_ref->id.start,
            nstr.data);
        break;
    case DT_VARIANT:
        nstr_assign_fmt(
            &nstr,
            "/* variant */ struct %.*s %s",
            (int)target_dt->variant_ref->id.length,
            target_dt->variant_ref->id.start,
            nstr.data);
        break;
    case DT_POINTER:
        nstr_assign_fmt(&tag_nstr, "* %s", qualifiers_nstr.data);
        nstr_assign_fmt(
            &nstr,
            _data_type_reverse_list_len == _data_type_reverse_list_idx + 1
                ? "%s%s"
                : "(%s%s)",
            tag_nstr.data,
            nstr.data);
        break;
    case DT_ARRAY:
        nassert(target_dt->array.length > 0);
        nstr_assign_fmt(&tag_nstr, "[%zu]", target_dt->array.length);
        nstr_assign_fmt(
            &nstr,
            _data_type_reverse_list_len == _data_type_reverse_list_idx + 1
                ? "%s%s"
                : "(%s%s)",
            nstr.data,
            tag_nstr.data);
        break;
    case DT_UNRESOLVED_ID: /* intentional fallthrough */
    case DT_UNRESOLVED_TYPEOF_EXPR:
    case DT_UNRESOLVED_TYPEOF_DT:
        NPANIC_UNEXPECTED_CTRL_FLOW();
    case DT_UNSET:
        NPANIC_UNSET_CASE();
    default:
        NPANIC_DFLT_CASE();
    }

    return nstr;
}

nstr_t data_type_to_c_source_nstr(
    struct data_type const* dt,
    struct identifier const* id)
{
    _data_type_reverse_list_len = 0;
    _data_type_reverse_list_idx = 0;
    return _data_type_to_c_source_nstr(dt, id);
}

static bool check_data_type_without_qualifiers_eq(
    struct data_type const* from,
    struct data_type const* to)
{
    return (NULL == from->inner
            || check_data_type_without_qualifiers_eq(from->inner, to->inner))
        && (from->tag == to->tag)
        && (DT_ARRAY != from->tag || from->array.length == to->array.length);
}

static bool check_qualifier_restricting_conversion(
    struct data_type const* from,
    struct data_type const* to,
    bool is_first_outtermost,
    bool is_second_outtermost)
{
    return (NULL == from->inner
            || check_qualifier_restricting_conversion(
                   from->inner, to->inner, false, is_first_outtermost))
        && (is_first_outtermost
            || (is_second_outtermost
                && qualifiers_is_restricting(
                       &from->qualifiers, &to->qualifiers))
            || qualifiers_eq(&from->qualifiers, &to->qualifiers));
}

static bool check_T_pointer_to_void_pointer_conversion(
    struct data_type const* from,
    struct data_type const* to)
{
    return data_type_is_pointer(from) && data_type_is_void_pointer(to)
        && qualifiers_is_restricting(
               &from->inner->qualifiers, &to->inner->qualifiers);
}

static bool check_void_pointer_to_T_pointer_conversion(
    struct data_type const* from,
    struct data_type const* to)
{
    return data_type_is_void_pointer(from) && data_type_is_pointer(to)
        && qualifiers_is_restricting(
               &from->inner->qualifiers, &to->inner->qualifiers);
}

bool data_type_can_implicitly_convert(
    struct data_type const* from,
    struct data_type const* to)
{
    if (data_type_is_void(from) || data_type_is_void(to))
    {
        return false;
    }

    if (check_T_pointer_to_void_pointer_conversion(from, to)
        || check_void_pointer_to_T_pointer_conversion(from, to))
    {
        return true;
    }
    if (data_type_length_all(from) != data_type_length_all(to))
    {
        return false;
    }
    if (!check_data_type_without_qualifiers_eq(from, to))
    {
        return false;
    }
    return check_qualifier_restricting_conversion(from, to, true, false);
}

bool data_type_can_implicitly_convert__variable_init(
    struct data_type const* from,
    struct data_type const* to)
{
    if (data_type_is_array(from) && data_type_is_array(to))
    {
        return from->array.length == to->array.length
            && data_type_can_implicitly_convert__variable_init(
                   from->inner, to->inner);
    }
    return data_type_can_implicitly_convert(from, to);
}

static bool check_integer_to_integer_conversion(
    struct data_type const* from,
    struct data_type const* to)
{
    return data_type_is_integer(from) && data_type_is_integer(to);
}

static bool check_floating_point_to_floating_point_conversion(
    struct data_type const* from,
    struct data_type const* to)
{
    return data_type_is_float(from) && data_type_is_float(to);
}

static bool check_char_to_integer_conversion(
    struct data_type const* from,
    struct data_type const* to)
{
    return data_type_is_ascii(from) && data_type_is_integer(to);
}

static bool check_integer_to_char_conversion(
    struct data_type const* from,
    struct data_type const* to)
{
    return data_type_is_integer(from) && data_type_is_ascii(to);
}

static bool check_integer_to_floating_point_conversion(
    struct data_type const* from,
    struct data_type const* to)
{
    return data_type_is_integer(from) && data_type_is_float(to);
}

static bool check_floating_point_to_integer_conversion(
    struct data_type const* from,
    struct data_type const* to)
{
    return data_type_is_float(from) && data_type_is_integer(to);
}

static bool check_pointer_to_integer_conversion(
    struct data_type const* from,
    struct data_type const* to)
{
    return data_type_is_pointer(from) && data_type_is_integer(to);
}

static bool check_integer_to_pointer_conversion(
    struct data_type const* from,
    struct data_type const* to)
{
    return data_type_is_integer(from) && data_type_is_pointer(to);
}

static bool check_pointer_to_pointer_conversion(
    struct data_type const* from,
    struct data_type const* to)
{
    return data_type_is_pointer(from) && data_type_is_pointer(to);
}

bool data_type_can_explicitly_convert(
    struct data_type const* from,
    struct data_type const* to)
{
    if (data_type_is_void(from) || data_type_is_void(to))
    {
        return false;
    }
    return data_type_can_implicitly_convert(from, to)
        || check_integer_to_integer_conversion(from, to)
        || check_floating_point_to_floating_point_conversion(from, to)
        || check_char_to_integer_conversion(from, to)
        || check_integer_to_char_conversion(from, to)
        || check_integer_to_floating_point_conversion(from, to)
        || check_floating_point_to_integer_conversion(from, to)
        || check_pointer_to_integer_conversion(from, to)
        || check_integer_to_pointer_conversion(from, to)
        || check_pointer_to_pointer_conversion(from, to);
}

bool qualifiers_eq(struct qualifiers const* lhs, struct qualifiers const* rhs)
{
    return lhs->is_mut == rhs->is_mut && lhs->is_unique == rhs->is_unique
        && lhs->is_volatile == rhs->is_volatile;
}

void qualifers_union(
    struct qualifiers* dest,
    struct qualifiers const* A,
    struct qualifiers const* B)
{
    dest->is_mut = A->is_mut || B->is_mut;
    dest->is_unique = A->is_unique || B->is_unique;
    dest->is_volatile = A->is_volatile || B->is_volatile;
}

bool qualifiers_is_default(struct qualifiers const* q)
{
    return !q->is_mut && !q->is_unique && !q->is_volatile;
}

bool qualifiers_is_restricting(
    struct qualifiers const* from,
    struct qualifiers const* to)
{
    return !(to->is_mut && !from->is_mut);
}

//============================================================================//
//      LITERAL VALUE                                                         //
//============================================================================//
INIT_FINI_COUNTER_SOURCE(literal);

void literal_init(struct literal* this)
{
    INIT_FINI_COUNTER_INCR_INIT(literal);
    MEMZERO_INSTANCE(this, typeof(*this));
}

void literal_fini(struct literal* this)
{
    INIT_FINI_COUNTER_INCR_FINI(literal);

    if (LITERAL_ASTR == this->tag)
    {
        nstr_fini(&this->astr);
    }
    else if (LITERAL_ARRAY == this->tag)
    {
        for (size_t i = 0; i < narr_length(this->array); ++i)
        {
            literal_fini(&this->array[i]);
        }
        narr_free(this->array);
    }

    this->tag = LITERAL_UNSET;

    this->srctok = NULL;
}

void literal_reset(struct literal* literal)
{
    literal_fini(literal);
    literal_init(literal);
}

void literal_assign(
    struct literal* dest,
    struct literal const* src,
    bool clear_srctok)
{
    literal_reset(dest);

    dest->tag = src->tag;

    switch (src->tag)
    {
    case LITERAL_U8:
        dest->u8 = src->u8;
        break;
    case LITERAL_U16:
        dest->u16 = src->u16;
        break;
    case LITERAL_U32:
        dest->u32 = src->u32;
        break;
    case LITERAL_U64:
        dest->u64 = src->u64;
        break;
    case LITERAL_U:
        dest->u = src->u;
        break;
    case LITERAL_S8:
        dest->s8 = src->s8;
        break;
    case LITERAL_S16:
        dest->s16 = src->s16;
        break;
    case LITERAL_S32:
        dest->s32 = src->s32;
        break;
    case LITERAL_S64:
        dest->s64 = src->s64;
        break;
    case LITERAL_S:
        dest->s = src->s;
        break;
    case LITERAL_F32:
        dest->f32 = src->f32;
        break;
    case LITERAL_F64:
        dest->f64 = src->f64;
        break;
    case LITERAL_BOOL:
        dest->boolean = src->boolean;
        break;
    case LITERAL_ACHAR:
        dest->achar = src->achar;
        break;
    case LITERAL_ASTR:
        nstr_init_nstr(&dest->astr, &src->astr);
        break;
    case LITERAL_NULL:
        /* nothing */
        break;
    case LITERAL_ARRAY:
        dest->array = narr_alloc(0, sizeof(struct literal));
        for (size_t i = 0; i < narr_length(src->array); ++i)
        {
            struct literal literal;
            literal_init(&literal);
            literal_assign(&literal, &src->array[i], clear_srctok);
            dest->array = narr_push(dest->array, &literal);
        }
        break;
    case LITERAL_ARRAY_FILL:
        dest->array_fill_length = src->array_fill_length;
        break;
    default:
        NPANIC_DFLT_CASE();
    }

    dest->srctok = clear_srctok ? NULL : src->srctok;
}

struct data_type literal_get_data_type(struct literal const* literal)
{
    struct data_type dt;
    data_type_init(&dt);

    switch (literal->tag)
    {
    case LITERAL_UNSET:
        dt.tag = DT_UNSET;
        break;
    case LITERAL_U8:
        dt.tag = DT_U8;
        break;
    case LITERAL_U16:
        dt.tag = DT_U16;
        break;
    case LITERAL_U32:
        dt.tag = DT_U32;
        break;
    case LITERAL_U64:
        dt.tag = DT_U64;
        break;
    case LITERAL_U:
        dt.tag = DT_U;
        break;
    case LITERAL_S8:
        dt.tag = DT_S8;
        break;
    case LITERAL_S16:
        dt.tag = DT_S16;
        break;
    case LITERAL_S32:
        dt.tag = DT_S32;
        break;
    case LITERAL_S64:
        dt.tag = DT_S64;
        break;
    case LITERAL_S:
        dt.tag = DT_S;
        break;
    case LITERAL_F32:
        dt.tag = DT_F32;
        break;
    case LITERAL_F64:
        dt.tag = DT_F64;
        break;
    case LITERAL_BOOL:
        dt.tag = DT_BOOL;
        break;
    case LITERAL_ACHAR:
        dt.tag = DT_ASCII;
        break;
    case LITERAL_ASTR:
    {
        struct data_type* const inner = nalloc(sizeof(struct data_type));
        data_type_init(inner);
        inner->tag = DT_ASCII;
        dt.inner = inner;
        dt.tag = DT_ARRAY;
        dt.array.length = literal->astr.len + NULL_TERMINATOR_LENGTH;
    }
    break;
    case LITERAL_NULL:
    {
        struct data_type* const inner = nalloc(sizeof(struct data_type));
        data_type_init(inner);
        inner->tag = DT_VOID;
        inner->qualifiers.is_mut = true;
        dt.inner = inner;
        dt.tag = DT_POINTER;
    }
    break;
    case LITERAL_ARRAY:
    {
        struct data_type* const inner = nalloc(sizeof(struct data_type));
        *inner = literal_get_data_type(&literal->array[0]);
        dt.inner = inner;
        dt.tag = DT_ARRAY;

        struct literal const* const back = narr_back(literal->array);
        if (LITERAL_ARRAY_FILL == back->tag)
        {
            dt.array.length = back->array_fill_length;
        }
        else
        {
            dt.array.length = narr_length(literal->array);
        }
        nassert(dt.array.length > 0);
    }
    break;
    case LITERAL_ARRAY_FILL:
        NPANIC_UNEXPECTED_CTRL_FLOW();
    default:
        NPANIC_DFLT_CASE();
    }

    return dt;
}

//============================================================================//
//      FUNCTION SIGNATURE                                                    //
//============================================================================//
INIT_FINI_COUNTER_SOURCE(func_sig);

void func_sig_init(struct func_sig* this)
{
    INIT_FINI_COUNTER_INCR_INIT(func_sig);
    MEMZERO_INSTANCE(this, typeof(*this));

    data_type_init(&this->return_dt);
    this->param_dts = narr_alloc(0, sizeof(struct data_type));
}

void func_sig_fini(struct func_sig* this)
{
    INIT_FINI_COUNTER_INCR_FINI(func_sig);

    data_type_fini(&this->return_dt);
    for (size_t i = 0; i < narr_length(this->param_dts); ++i)
    {
        data_type_fini(&this->param_dts[i]);
    }
    narr_free(this->param_dts);
}

void func_sig_reset(struct func_sig* this)
{
    func_sig_fini(this);
    func_sig_init(this);
}

void func_sig_assign(
    struct func_sig* dest,
    struct func_sig const* src,
    bool clear_srctok)
{
    func_sig_reset(dest);

    data_type_assign(&dest->return_dt, &src->return_dt, clear_srctok);

    for (size_t i = 0; i < narr_length(src->param_dts); ++i)
    {
        struct data_type param;
        data_type_init(&param);
        data_type_assign(&param, &(src->param_dts[i]), clear_srctok);
        dest->param_dts = narr_push(dest->param_dts, &param);
    }
}

//============================================================================//
//      MEMBER VARIABLE                                                       //
//============================================================================//
INIT_FINI_COUNTER_SOURCE(member_var);

void member_var_init(struct member_var* this)
{
    INIT_FINI_COUNTER_INCR_INIT(member_var);
    identifier_init(&this->id);
    data_type_init(&this->dt);
    this->srctok = NULL;
}

void member_var_fini(struct member_var* this)
{
    INIT_FINI_COUNTER_INCR_FINI(member_var);
    identifier_fini(&this->id);
    data_type_fini(&this->dt);
}

void member_var_init_member_var(struct member_var const* og,
    struct member_var* nc)
{
    member_var_init(nc);
    identifier_assign(&nc->id, &og->id, false);
    data_type_assign(&nc->dt, &og->dt, false);
    nc->srctok = og->srctok;
}

//============================================================================//
//      MEMBER FUNCTION                                                       //
//============================================================================//
INIT_FINI_COUNTER_SOURCE(member_func);

void member_func_init(struct member_func* this)
{
    INIT_FINI_COUNTER_INCR_INIT(member_func);

    identifier_init(&this->id);
    data_type_init(&this->dt);
    identifier_init(&this->target.id);
    this->srctok = NULL;
}

void member_func_fini(struct member_func* this)
{
    INIT_FINI_COUNTER_INCR_FINI(member_func);

    identifier_fini(&this->id);
    data_type_fini(&this->dt);
    identifier_fini(&this->target.id);
}

void member_func_init_member_func(struct member_func const* og,
    struct member_func* nc)
{
    member_func_init(nc);
    identifier_assign(&nc->id, &og->id, false);
    data_type_assign(&nc->dt, &og->dt, false);
    identifier_assign(&nc->target.id, &og->target.id, false);
    nc->srctok = og->srctok;
}

//============================================================================//
//      MEMBER INTERFACE                                                      //
//============================================================================//
INIT_FINI_COUNTER_SOURCE(member_iface);

void member_iface_init(struct member_iface* iface)
{
    INIT_FINI_COUNTER_INCR_INIT(member_iface);
    identifier_init(&iface->id);
    func_sig_init(&iface->sig);
    iface->srctok = NULL;
}

void member_iface_fini(struct member_iface* iface)
{
    INIT_FINI_COUNTER_INCR_FINI(member_iface);
    identifier_fini(&iface->id);
    func_sig_fini(&iface->sig);
}

//============================================================================//
//      ENUM DEF                                                              //
//============================================================================//
INIT_FINI_COUNTER_SOURCE(enum_def);

void enum_def_init(struct enum_def* edef)
{
    INIT_FINI_COUNTER_INCR_INIT(enum_def);
    edef->member_ids  = narr_alloc(0, sizeof(struct identifier));
    edef->member_vals = narr_alloc(0, sizeof(struct literal));
    edef->srctok = NULL;
}

void enum_def_fini(struct enum_def* edef)
{
    INIT_FINI_COUNTER_INCR_FINI(enum_def);
    
    size_t const ids_len = narr_length(edef->member_ids);
    for(size_t i = 0; i < ids_len; i++)
    {
        identifier_fini(&(edef->member_ids[i]));
    }
    for(size_t i = 0; i < ids_len; i++)
    {
        literal_fini(&(edef->member_vals[i]));
    }
    narr_free(edef->member_ids);
    narr_free(edef->member_vals);
}

ssize_t enum_def_member_id_idx(
    struct enum_def const* this,
    struct identifier const* key)
{
    ssize_t ret = MEMBER_ID_NOT_FOUND;
    ssize_t const n_ids = (ssize_t) narr_length(this->member_ids);
    for(ssize_t i = 0; i < n_ids; ++i)
    {
        if(identifier_eq(key, &this->member_ids[i]))
        {
            ret = i;
            break;
        }
    }
    return ret;
}

//============================================================================//
//      UNION DEFINITION                                                      //
//============================================================================//
INIT_FINI_COUNTER_SOURCE(union_def);

void union_def_init(struct union_def* udef)
{
    INIT_FINI_COUNTER_INCR_INIT(union_def);

    udef->member_vars = narr_alloc(0, sizeof(struct member_var));
    udef->srctok = NULL;
}

void union_def_fini(struct union_def* udef)
{
    INIT_FINI_COUNTER_INCR_FINI(union_def);
    size_t const nmemb = narr_length(udef->member_vars);
    for(size_t i = 0; i < nmemb; ++i)
    {
        member_var_fini(&udef->member_vars[i]);
    }
    narr_free(udef->member_vars);
}

ssize_t union_def_member_var_idx(
     struct union_def const* this,
     struct identifier const* key)
{
    ssize_t ret = MEMBER_VAR_NOT_FOUND;
    ssize_t len = (ssize_t) narr_length(&this->member_vars);
    for(ssize_t i = 0; i < len; ++i)
    {
        if(identifier_eq(&this->member_vars[i].id, key))
        {
            ret = i;
            break;
        }
    }
    return ret;
}

//============================================================================//
//      STRUCT DEFINITION                                                     //
//============================================================================//
INIT_FINI_COUNTER_SOURCE(struct_def);

void struct_def_init(struct struct_def* this)
{
    INIT_FINI_COUNTER_INCR_INIT(struct_def);
    MEMZERO_INSTANCE(this, typeof(*this));

    this->member_vars = narr_alloc(0, sizeof(struct member_var));
    this->member_funcs = narr_alloc(0, sizeof(struct member_func));
    this->srctok = NULL;
}

void struct_def_fini(struct struct_def* this)
{
    INIT_FINI_COUNTER_INCR_FINI(struct_def);

    size_t const member_vars_len = narr_length(this->member_vars);
    size_t const member_funcs_len = narr_length(this->member_funcs);

    for (size_t i = 0; i < member_vars_len; ++i)
    {
        member_var_fini(&(this->member_vars[i]));
    }
    narr_free(this->member_vars);

    for (size_t i = 0; i < member_funcs_len; ++i)
    {
        member_func_fini(&(this->member_funcs[i]));
    }
    narr_free(this->member_funcs);
}

void struct_def_init_struct_def(struct struct_def const* og,
    struct struct_def* nc)
{
    for(size_t i = 0; i < narr_length(og->member_vars); ++i)
    {
        struct member_var mv;
        member_var_init_member_var(&og->member_vars[i], &mv);
        nc->member_vars = narr_push(nc->member_vars, &mv);
    }

    for(size_t i = 0; i < narr_length(og->member_funcs); ++i)
    {
        struct member_func mf;
        member_func_init_member_func(&og->member_funcs[i], &mf);
        nc->member_funcs = narr_push(nc->member_funcs, &mf);
    }
    nc->srctok = og->srctok;
}

ssize_t struct_def_member_var_idx(
    struct struct_def const* def,
    struct identifier const* id)
{
    ssize_t const len = (ssize_t)narr_length(def->member_vars);
    for (ssize_t idx = 0; idx < len; ++idx)
    {
        if (identifier_eq(id, &def->member_vars[idx].id))
        {
            return idx;
        }
    }
    return MEMBER_VAR_NOT_FOUND;
}

ssize_t struct_def_member_func_idx(
    struct struct_def const* def,
    struct identifier const* id)
{
    ssize_t const len = (ssize_t)narr_length(def->member_funcs);
    for (ssize_t idx = 0; idx < len; ++idx)
    {
        if (identifier_eq(id, &def->member_funcs[idx].id))
        {
            return idx;
        }
    }
    return MEMBER_FUNC_NOT_FOUND;
}

//============================================================================//
//      MEMBER_TAG                                                            //
//============================================================================//
INIT_FINI_COUNTER_SOURCE(member_tag);

void member_tag_init(struct member_tag* tagd)
{
    INIT_FINI_COUNTER_INCR_INIT(member_tag);

    identifier_init(&tagd->id);
    literal_init(&tagd->value);
    struct_def_init(&tagd->section);
    tagd->srctok = NULL;
}

void member_tag_fini(struct member_tag* tagd)
{
    INIT_FINI_COUNTER_INCR_FINI(member_tag);

    identifier_fini(&tagd->id);
    literal_fini(&tagd->value);
    struct_def_fini(&tagd->section);
}

//============================================================================//
//      VARIANT DEFINITION                                                    //
//============================================================================//
INIT_FINI_COUNTER_SOURCE(variant_def);

void variant_def_init(struct variant_def* vdef)
{
    INIT_FINI_COUNTER_INCR_INIT(variant_def);
    data_type_init(&vdef->tag_dt);
    vdef->member_vars = narr_alloc(0, sizeof(struct member_var));
    vdef->member_funcs = narr_alloc(0, sizeof(struct member_func));
    vdef->member_tags = narr_alloc(0, sizeof(struct member_tag));
    vdef->member_ifaces = narr_alloc(0, sizeof(struct member_iface));
    vdef->srctok = NULL;
}

void variant_def_fini(struct variant_def* vdef)
{
    INIT_FINI_COUNTER_INCR_FINI(variant_def);

    data_type_fini(&vdef->tag_dt);

    size_t const n_vars   = narr_length(vdef->member_vars);
    size_t const n_funcs  = narr_length(vdef->member_funcs);
    size_t const n_tags   = narr_length(vdef->member_tags);
    size_t const n_ifaces = narr_length(vdef->member_ifaces);

    for(size_t i = 0; i < n_vars; i++)
    {
        member_var_fini(&vdef->member_vars[i]);
    }
    narr_free(vdef->member_vars);
    
    for(size_t i = 0; i < n_funcs; i++)
    {
        member_func_fini(&vdef->member_funcs[i]);
    }
    narr_free(vdef->member_funcs);
    
    for(size_t i = 0; i < n_tags; i++)
    {
        member_tag_fini(&vdef->member_tags[i]);
    }
    narr_free(vdef->member_tags);
    
    for(size_t i = 0; i < n_ifaces; i++)
    {
        member_iface_fini(&vdef->member_ifaces[i]);
    }
    narr_free(vdef->member_ifaces);
}

ssize_t variant_def_member_var_idx(
    struct variant_def const* this,
    struct identifier const* key)
{
    ssize_t ret = MEMBER_VAR_NOT_FOUND;
    ssize_t len = (ssize_t) narr_length(this->member_vars);
    for(ssize_t i = 0; i < len; ++i)
    {
        if(identifier_eq(&this->member_vars[i].id, key))
        {
            ret = i;
            break;
        }
    }
    return ret;
}

ssize_t variant_def_member_func_idx(
    struct variant_def const* this,
    struct identifier const* key)
{
    ssize_t ret = MEMBER_FUNC_NOT_FOUND;
    ssize_t len = (ssize_t) narr_length(this->member_funcs);
    for(ssize_t i = 0; i < len; ++i)
    {
        if(identifier_eq(&this->member_funcs[i].id, key))
        {
            ret = i;
            break;
        }
    }
    return ret;
}

ssize_t variant_def_member_tag_idx(
    struct variant_def const* this,
    struct identifier const* key)
{
    ssize_t ret = MEMBER_TAG_NOT_FOUND;
    ssize_t len = (ssize_t) narr_length(this->member_tags);
    for(ssize_t i = 0; i < len; ++i)
    {
        if(identifier_eq(&this->member_tags[i].id, key))
        {
            ret = i;
            break;
        }
    }
    return ret;
}

ssize_t variant_def_member_iface_idx(
    struct variant_def const* this,
    struct identifier const* key)
{
    ssize_t ret = MEMBER_IFACE_NOT_FOUND;
    ssize_t len = (ssize_t) narr_length(this->member_ifaces);
    for(ssize_t i = 0; i < len; ++i)
    {
        if(identifier_eq(&this->member_ifaces[i].id, key))
        {
            ret = i;
            break;
        }
    }
    return ret;
}

//============================================================================//
//      EXPRESSION                                                            //
//============================================================================//
INIT_FINI_COUNTER_SOURCE(expr);

void expr_init(struct expr* expr)
{
    INIT_FINI_COUNTER_INCR_INIT(expr);
    MEMZERO_INSTANCE(expr, typeof(*expr));

    data_type_init(&expr->dt);
}

void expr_fini(struct expr* expr)
{
    INIT_FINI_COUNTER_INCR_FINI(expr);

    if (expr_is_postfix(expr))
    {
        expr_fini(expr->postfix.lhs);
        nfree(expr->postfix.lhs);
    }

    switch (expr->tag)
    {
    case EXPR_UNSET:
        /* nothing */
        break;

    case EXPR_PRIMARY_IDENTIFIER:
        identifier_fini(&expr->id);
        break;

    case EXPR_PRIMARY_LITERAL:
        literal_fini(expr->literal);
        nfree(expr->literal);
        break;

    case EXPR_PRIMARY_PAREN:
        expr_fini(expr->paren);
        nfree(expr->paren);
        break;

    case EXPR_POSTFIX_FUNCTION_CALL:
        for (size_t i = 0; i < narr_length(expr->postfix.args); ++i)
        {
            expr_fini(&expr->postfix.args[i]);
        }
        narr_free(expr->postfix.args);
        break;

    case EXPR_POSTFIX_SUBSCRIPT:
        expr_fini(expr->postfix.subscript);
        nfree(expr->postfix.subscript);
        break;

    case EXPR_POSTFIX_ACCESS_DOT: /* intentional fallthrough */
    case EXPR_POSTFIX_ACCESS_ARROW:
        identifier_fini(&expr->postfix.access.id);
        break;

    case EXPR_UNARY_ADDR_OF:      /* intentional fallthrough */
    case EXPR_UNARY_DEREF:        /* intentional fallthrough */
    case EXPR_UNARY_DECAY:        /* intentional fallthrough */
    case EXPR_UNARY_PLUS:         /* intentional fallthrough */
    case EXPR_UNARY_MINUS:        /* intentional fallthrough */
    case EXPR_UNARY_BITWISE_NOT:  /* intentional fallthrough */
    case EXPR_UNARY_LOGICAL_NOT:  /* intentional fallthrough */
    case EXPR_UNARY_COUNTOF_EXPR: /* intentional fallthrough */
    case EXPR_UNARY_SIZEOF_EXPR:
        expr_fini(expr->unary.rhs_expr);
        nfree(expr->unary.rhs_expr);
        break;

    case EXPR_UNARY_COUNTOF_DT: /* intentional fallthrough */
    case EXPR_UNARY_SIZEOF_DT:
        data_type_fini(expr->unary.rhs_dt);
        nfree(expr->unary.rhs_dt);
        break;

    case EXPR_BINARY_ASSIGN:      /* intentional fallthrough */
    case EXPR_BINARY_LOGICAL_OR:  /* intentional fallthrough */
    case EXPR_BINARY_LOGICAL_AND: /* intentional fallthrough */
    case EXPR_BINARY_BITWISE_OR:  /* intentional fallthrough */
    case EXPR_BINARY_BITWISE_XOR: /* intentional fallthrough */
    case EXPR_BINARY_BITWISE_AND: /* intentional fallthrough */
    case EXPR_BINARY_REL_EQ:      /* intentional fallthrough */
    case EXPR_BINARY_REL_NE:      /* intentional fallthrough */
    case EXPR_BINARY_REL_LT:      /* intentional fallthrough */
    case EXPR_BINARY_REL_GT:      /* intentional fallthrough */
    case EXPR_BINARY_REL_LE:      /* intentional fallthrough */
    case EXPR_BINARY_REL_GE:      /* intentional fallthrough */
    case EXPR_BINARY_SHIFT_L:     /* intentional fallthrough */
    case EXPR_BINARY_SHIFT_R:     /* intentional fallthrough */
    case EXPR_BINARY_PLUS:        /* intentional fallthrough */
    case EXPR_BINARY_MINUS:       /* intentional fallthrough */
    case EXPR_BINARY_PTR_DIFF:    /* intentional fallthrough */
    case EXPR_BINARY_PTR_PLUS:    /* intentional fallthrough */
    case EXPR_BINARY_PTR_MINUS:   /* intentional fallthrough */
    case EXPR_BINARY_MULT:        /* intentional fallthrough */
    case EXPR_BINARY_DIV:
        expr_fini(expr->binary.lhs_expr);
        nfree(expr->binary.lhs_expr);

        expr_fini(expr->binary.rhs_expr);
        nfree(expr->binary.rhs_expr);
        break;

    case EXPR_BINARY_CAST:
        expr_fini(expr->binary.lhs_expr);
        nfree(expr->binary.lhs_expr);

        data_type_fini(expr->binary.rhs_dt);
        nfree(expr->binary.rhs_dt);
        break;

    default:
        NPANIC_DFLT_CASE();
    }

    data_type_fini(&expr->dt);
}

void expr_reset(struct expr* expr)
{
    expr_fini(expr);
    expr_init(expr);
}

void expr_assign(struct expr* dest, struct expr const* src, bool clear_srctok)
{
    expr_reset(dest);

    dest->tag = src->tag;

    if (expr_is_postfix(src))
    {
        dest->postfix.lhs = nalloc(sizeof(struct expr));
        expr_init(dest->postfix.lhs);
        expr_assign(dest->postfix.lhs, src->postfix.lhs, clear_srctok);
    }
    switch (src->tag)
    {
    case EXPR_UNSET:
        /* nothing */
        break;

    case EXPR_PRIMARY_IDENTIFIER:
        identifier_init(&dest->id);
        identifier_assign(&dest->id, &src->id, clear_srctok);
        break;

    case EXPR_PRIMARY_LITERAL:
        dest->literal = nalloc(sizeof(struct literal));
        literal_init(dest->literal);
        literal_assign(dest->literal, src->literal, clear_srctok);
        break;

    case EXPR_PRIMARY_PAREN:
        dest->paren = nalloc(sizeof(struct expr));
        expr_init(dest->paren);
        expr_assign(dest->paren, src->paren, clear_srctok);
        break;

    case EXPR_POSTFIX_FUNCTION_CALL:
        nassert(NULL != src->postfix.args);
        dest->postfix.args = narr_alloc(0, sizeof(struct expr));
        for (size_t i = 0; i < narr_length(src->postfix.args); ++i)
        {
            struct expr param;
            expr_init(&param);
            expr_assign(&param, &src->postfix.args[i], clear_srctok);
            dest->postfix.args = narr_push(dest->postfix.args, &param);
        }
        nassert(NULL != dest->postfix.args);
        nassert(NULL != dest->postfix.lhs);
        break;

    case EXPR_POSTFIX_SUBSCRIPT:
        dest->postfix.subscript = nalloc(sizeof(struct expr));
        expr_init(dest->postfix.subscript);
        expr_assign(
            dest->postfix.subscript, src->postfix.subscript, clear_srctok);
        nassert(NULL != dest->postfix.lhs);
        break;

    case EXPR_POSTFIX_ACCESS_DOT: /* intentional fallthrough */
    case EXPR_POSTFIX_ACCESS_ARROW:
        identifier_init(&dest->postfix.access.id);
        identifier_assign(
            &dest->postfix.access.id, &src->postfix.access.id, clear_srctok);
        nassert(NULL != dest->postfix.lhs);
        break;

    case EXPR_UNARY_ADDR_OF:      /* intentional fallthrough */
    case EXPR_UNARY_DEREF:        /* intentional fallthrough */
    case EXPR_UNARY_DECAY:        /* intentional fallthrough */
    case EXPR_UNARY_PLUS:         /* intentional fallthrough */
    case EXPR_UNARY_MINUS:        /* intentional fallthrough */
    case EXPR_UNARY_BITWISE_NOT:  /* intentional fallthrough */
    case EXPR_UNARY_LOGICAL_NOT:  /* intentional fallthrough */
    case EXPR_UNARY_COUNTOF_EXPR: /* intentional fallthrough */
    case EXPR_UNARY_SIZEOF_EXPR:
        dest->unary.rhs_expr = nalloc(sizeof(struct expr));
        expr_init(dest->unary.rhs_expr);
        expr_assign(dest->unary.rhs_expr, src->unary.rhs_expr, clear_srctok);
        break;

    case EXPR_UNARY_COUNTOF_DT: /* intentional fallthrough */
    case EXPR_UNARY_SIZEOF_DT:
        dest->unary.rhs_dt = nalloc(sizeof(struct data_type));
        data_type_init(dest->unary.rhs_dt);
        data_type_assign(dest->unary.rhs_dt, src->unary.rhs_dt, clear_srctok);
        break;

    case EXPR_BINARY_ASSIGN:      /* intentional fallthrough */
    case EXPR_BINARY_LOGICAL_OR:  /* intentional fallthrough */
    case EXPR_BINARY_LOGICAL_AND: /* intentional fallthrough */
    case EXPR_BINARY_BITWISE_OR:  /* intentional fallthrough */
    case EXPR_BINARY_BITWISE_XOR: /* intentional fallthrough */
    case EXPR_BINARY_BITWISE_AND: /* intentional fallthrough */
    case EXPR_BINARY_REL_EQ:      /* intentional fallthrough */
    case EXPR_BINARY_REL_NE:      /* intentional fallthrough */
    case EXPR_BINARY_REL_LT:      /* intentional fallthrough */
    case EXPR_BINARY_REL_GT:      /* intentional fallthrough */
    case EXPR_BINARY_REL_LE:      /* intentional fallthrough */
    case EXPR_BINARY_REL_GE:      /* intentional fallthrough */
    case EXPR_BINARY_SHIFT_L:     /* intentional fallthrough */
    case EXPR_BINARY_SHIFT_R:     /* intentional fallthrough */
    case EXPR_BINARY_PLUS:        /* intentional fallthrough */
    case EXPR_BINARY_MINUS:       /* intentional fallthrough */
    case EXPR_BINARY_PTR_DIFF:    /* intentional fallthrough */
    case EXPR_BINARY_PTR_PLUS:    /* intentional fallthrough */
    case EXPR_BINARY_PTR_MINUS:   /* intentional fallthrough */
    case EXPR_BINARY_MULT:        /* intentional fallthrough */
    case EXPR_BINARY_DIV:
        dest->binary.lhs_expr = nalloc(sizeof(struct expr));
        expr_init(dest->binary.lhs_expr);
        expr_assign(dest->binary.lhs_expr, src->binary.lhs_expr, clear_srctok);

        dest->binary.rhs_expr = nalloc(sizeof(struct expr));
        expr_init(dest->binary.rhs_expr);
        expr_assign(dest->binary.rhs_expr, src->binary.rhs_expr, clear_srctok);
        break;

    case EXPR_BINARY_CAST:
        dest->binary.lhs_expr = nalloc(sizeof(struct expr));
        expr_init(dest->binary.lhs_expr);
        expr_assign(dest->binary.lhs_expr, src->binary.lhs_expr, clear_srctok);

        dest->binary.rhs_dt = nalloc(sizeof(struct data_type));
        data_type_init(dest->binary.rhs_dt);
        data_type_assign(dest->binary.rhs_dt, src->binary.rhs_dt, clear_srctok);
        break;

    default:
        NPANIC_DFLT_CASE();
    }

    data_type_assign(&dest->dt, &src->dt, clear_srctok);

    dest->valcat = src->valcat;

    dest->srctok = clear_srctok ? NULL : src->srctok;
}

bool expr_is_primary(struct expr const* expr)
{
    return expr->tag > __EXPR_PRIMARY_BEGIN__
        && expr->tag < __EXPR_PRIMARY_END__;
}

bool expr_is_postfix(struct expr const* expr)
{
    return expr->tag > __EXPR_POSTFIX_BEGIN__
        && expr->tag < __EXPR_POSTFIX_END__;
}

bool expr_is_unary(struct expr const* expr)
{
    return expr->tag > __EXPR_UNARY_BEGIN__ && expr->tag < __EXPR_UNARY_END__;
}

bool expr_is_binary(struct expr const* expr)
{
    return expr->tag > __EXPR_BINARY_BEGIN__ && expr->tag < __EXPR_BINARY_END__;
}

bool expr_is_lvalue(struct expr const* expr)
{
    switch (expr->valcat)
    {
    case VALCAT_NONE:
        return false;
    case VALCAT_RVALUE:
        return false;
    case VALCAT_LVALUE:
        return true;
    default:
        NPANIC_DFLT_CASE();
    }
}

bool expr_is_rvalue(struct expr const* expr)
{
    switch (expr->valcat)
    {
    case VALCAT_NONE:
        return false;
    case VALCAT_RVALUE:
        return true;
    case VALCAT_LVALUE:
        return true; // lvalues are by definition also rvalues.
    default:
        NPANIC_DFLT_CASE();
    }
}

//============================================================================//
//      IN-SOURCE IDENTIFIER DECLARATION                                      //
//============================================================================//
//====================================//
//      VARIABLE DECLARATION          //
//====================================//
INIT_FINI_COUNTER_SOURCE(variable_decl);

void variable_decl_init(struct variable_decl* this)
{
    INIT_FINI_COUNTER_INCR_INIT(variable_decl);
    MEMZERO_INSTANCE(this, typeof(*this));

    identifier_init(&this->id);
    data_type_init(&this->dt);
}

void variable_decl_fini(struct variable_decl* this)
{
    INIT_FINI_COUNTER_INCR_FINI(variable_decl);

    identifier_fini(&this->id);
    data_type_fini(&this->dt);
    if (this->def != NULL)
    {
        expr_fini(this->def);
        nfree(this->def);
    }
}

//====================================//
//      FUNCTION DECLARATION          //
//====================================//
INIT_FINI_COUNTER_SOURCE(function_decl);

void function_decl_init(struct function_decl* this)
{
    INIT_FINI_COUNTER_INCR_INIT(function_decl);
    MEMZERO_INSTANCE(this, typeof(*this));

    identifier_init(&this->id);

    data_type_init(&this->dt);
    this->dt.tag = DT_FUNCTION;
    this->dt.func_sig = nalloc(sizeof(struct func_sig));
    func_sig_init(this->dt.func_sig);

    this->param_ids = narr_alloc(0, sizeof(struct identifier));
}

void function_decl_fini(struct function_decl* this)
{
    INIT_FINI_COUNTER_INCR_FINI(function_decl);

    identifier_fini(&this->id);
    data_type_fini(&this->dt);

    size_t const param_ids_len = narr_length(this->param_ids);
    for (size_t i = 0; i < param_ids_len; ++i)
    {
        identifier_fini(&this->param_ids[i]);
    }
    narr_free(this->param_ids);

    if (NULL != this->def)
    {
        scope_fini(this->def);
        nfree(this->def);
    }
}

//====================================//
//      ENUM DECLARATION              //
//====================================//
INIT_FINI_COUNTER_SOURCE(enum_decl);

void enum_decl_init(struct enum_decl* edec)
{
    INIT_FINI_COUNTER_INCR_INIT(enum_decl);
    MEMZERO_INSTANCE(edec, typeof(*edec));

    edec->is_introduce = false;
    edec->is_export = false;
    identifier_init(&edec->id);
    edec->def = NULL;
    edec->srctok = NULL;
}

void enum_decl_fini(struct enum_decl* edec)
{
    INIT_FINI_COUNTER_INCR_FINI(enum_decl);

    identifier_fini(&edec->id);
    if(edec->def != NULL)
    {
        enum_def_fini(edec->def);
        nfree(edec->def);
    }
}

//====================================//
//      UNION DECLARATION             //
//====================================//
INIT_FINI_COUNTER_SOURCE(union_decl);

void union_decl_init(struct union_decl* udec)
{
    INIT_FINI_COUNTER_INCR_INIT(union_decl);
    MEMZERO_INSTANCE(udec, typeof(*udec));

    udec->is_introduce = false;
    udec->is_export = false;
    identifier_init(&udec->id);
    udec->def = NULL;
    udec->srctok = NULL;
}

void union_decl_fini(struct union_decl* udec)
{
    INIT_FINI_COUNTER_INCR_FINI(union_decl);

    identifier_fini(&udec->id);
    if(udec->def != NULL)
    {
        union_def_fini(udec->def);
        nfree(udec->def);
    }
}


//====================================//
//      STRUCT DECLARATION            //
//====================================//
INIT_FINI_COUNTER_SOURCE(struct_decl);

void struct_decl_init(struct struct_decl* this)
{
    INIT_FINI_COUNTER_INCR_INIT(struct_decl);
    MEMZERO_INSTANCE(this, typeof(*this));

    identifier_init(&this->id);
}

void struct_decl_fini(struct struct_decl* this)
{
    INIT_FINI_COUNTER_INCR_FINI(struct_decl);

    identifier_fini(&this->id);
    if (NULL != this->def)
    {
        struct_def_fini(this->def);
        nfree(this->def);
    }
}

//====================================//
//      VARIANT DECLARATION           //
//====================================//
INIT_FINI_COUNTER_SOURCE(variant_decl);

void variant_decl_init(struct variant_decl* vdec)
{
    INIT_FINI_COUNTER_INCR_INIT(variant_decl);
    MEMZERO_INSTANCE(vdec, typeof(*vdec));

    vdec->is_introduce = false;
    vdec->is_export = false;
    identifier_init(&vdec->id);
    vdec->def = NULL;
    vdec->srctok = NULL;
}

void variant_decl_fini(struct variant_decl* vdec)
{
    INIT_FINI_COUNTER_INCR_FINI(variant_decl);

    identifier_fini(&vdec->id);
    if(vdec->def != NULL)
    {
        variant_def_fini(vdec->def);
        nfree(vdec->def);
    }
}

//============================================================================//
//      IF STATEMTNT                                                          //
//============================================================================//
INIT_FINI_COUNTER_SOURCE(if_stmt);

void if_stmt_init(struct if_stmt* this)
{
    this->if_conditions = narr_alloc(0, sizeof(struct expr));
    this->if_blocks = narr_alloc(0, sizeof(struct scope));
    this->else_block = NULL;
}

void if_stmt_fini(struct if_stmt* this)
{
    size_t const ifs_length = narr_length(this->if_conditions);
    for (size_t i = 0; i < ifs_length; ++i)
    {
        expr_fini(&this->if_conditions[i]);
        scope_fini(&this->if_blocks[i]);
    }
    narr_free(this->if_conditions);
    narr_free(this->if_blocks);

    if (NULL != this->else_block)
    {
        scope_fini(this->else_block);
        nfree(this->else_block);
    }
}

//============================================================================//
//      LOOP STATEMENT                                                        //
//============================================================================//
INIT_FINI_COUNTER_SOURCE(loop_stmt);

void loop_stmt_init(struct loop_stmt* this)
{
    INIT_FINI_COUNTER_INCR_INIT(loop_stmt);
    MEMZERO_INSTANCE(this, typeof(*this));

    this->block = nalloc(sizeof(struct scope));
    scope_init(this->block, UNDEFINED_SCOPE_PARENT);
}

void loop_stmt_fini(struct loop_stmt* this)
{
    INIT_FINI_COUNTER_INCR_FINI(loop_stmt);

    switch (this->tag)
    {
    case LOOP_UNSET:
        /* nothing */
        break;

    case LOOP_CONDITIONAL:
        expr_fini(this->conditional.expr);
        nfree(this->conditional.expr);
        break;

    case LOOP_RANGE:
        variable_decl_fini(&this->range.variable_decl);
        expr_fini(this->range.lower_bound);
        nfree(this->range.lower_bound);
        expr_fini(this->range.upper_bound);
        nfree(this->range.upper_bound);
        break;

    default:
        NPANIC_DFLT_CASE();
    }

    scope_fini(this->block);
    nfree(this->block);
}

//============================================================================//
//      ALIAS STATEMENT                                                       //
//============================================================================//
INIT_FINI_COUNTER_SOURCE(alias_stmt);

void alias_stmt_init(struct alias_stmt* this)
{
    INIT_FINI_COUNTER_INCR_INIT(alias_stmt);
    MEMZERO_INSTANCE(this, typeof(*this));

    identifier_init(&this->new_id);
    data_type_init(&this->dt);
}

void alias_stmt_fini(struct alias_stmt* this)
{
    INIT_FINI_COUNTER_INCR_FINI(alias_stmt);

    identifier_fini(&this->new_id);
    data_type_fini(&this->dt);
}

//============================================================================//
//      STATEMENT                                                             //
//============================================================================//
INIT_FINI_COUNTER_SOURCE(stmt);

void stmt_init(struct stmt* this)
{
    INIT_FINI_COUNTER_INCR_INIT(stmt);
    MEMZERO_INSTANCE(this, typeof(*this));
}

void stmt_fini(struct stmt* this)
{
    INIT_FINI_COUNTER_INCR_FINI(stmt);

    switch (this->tag)
    {
    case STMT_UNSET:
        /* nothing */
        break;

    case STMT_IMPORT:
        nstr_fini(&this->import_string);
        break;

    case STMT_ALIAS:
        alias_stmt_fini(this->alias_stmt);
        nfree(this->alias_stmt);
        break;

    case STMT_VARIABLE_DECLARATION:
        variable_decl_fini(this->variable_decl);
        nfree(this->variable_decl);
        break;

    case STMT_FUNCTION_DECLARATION:
        function_decl_fini(this->function_decl);
        nfree(this->function_decl);
        break;

    case STMT_ENUM_DECLARATION:
        enum_decl_fini(this->enum_decl);
        nfree(this->enum_decl);
        break;

    case STMT_UNION_DECLARATION:
        union_decl_fini(this->union_decl);
        nfree(this->union_decl);
        break;

    case STMT_STRUCT_DECLARATION:
        struct_decl_fini(this->struct_decl);
        nfree(this->struct_decl);
        break;

    case STMT_VARIANT_DECLARATION:
        variant_decl_fini(this->variant_decl);
        nfree(this->variant_decl);
        break;

    case STMT_IF:
        if_stmt_fini(this->if_stmt);
        nfree(this->if_stmt);
        break;

    case STMT_LOOP:
        loop_stmt_fini(this->loop_stmt);
        nfree(this->loop_stmt);
        break;

    case STMT_DEFER:
        scope_fini(this->defer_stmt.block);
        nfree(this->defer_stmt.block);
        break;

    case STMT_BREAK:
        /*nothing*/
        break;

    case STMT_CONTINUE:
        /*nothing*/
        break;

    case STMT_RETURN:
        if (NULL != this->return_expr)
        {
            expr_fini(this->return_expr);
            nfree(this->return_expr);
        }
        break;

    case STMT_EXPR:
        expr_fini(this->expr);
        nfree(this->expr);
        break;

    case STMT_BLOCK:
        scope_fini(this->block);
        nfree(this->block);
        break;

    case STMT_EMPTY:
        /* nothing */
        break;

    default:
        NPANIC_DFLT_CASE();
    }
}

//============================================================================//
//      LEXICAL SCOPE                                                         //
//============================================================================//
//====================================//
//      SYMBOL                        //
//====================================//
INIT_FINI_COUNTER_SOURCE(symbol);

void symbol_init(struct symbol* this)
{
    INIT_FINI_COUNTER_INCR_INIT(symbol);
    MEMZERO_INSTANCE(this, typeof(*this));

    this->tag = SYMBOL_UNSET;

    this->id = NULL;
    this->dt = NULL;
    this->srctok = NULL;

    this->module_idx = current_module_idx;

    this->uses = 0;

    this->is_export = false;

    this->is_defined = false;
}

void symbol_init_variable(
    struct symbol* this,
    struct variable_decl* variable_decl)
{
    nassert(NULL != variable_decl);
    nassert(NULL != variable_decl->srctok);
    symbol_init(this);

    this->tag = SYMBOL_VARIABLE;

    this->ref.variable_decl = variable_decl;

    this->id = &variable_decl->id;
    this->dt = &variable_decl->dt;
    this->is_export = variable_decl->is_export;
    this->is_export = NULL != variable_decl->def;
    this->srctok = variable_decl->srctok;
}

void symbol_init_function(
    struct symbol* this,
    struct function_decl* function_decl)
{
    nassert(NULL != function_decl);
    nassert(NULL != function_decl->srctok);
    symbol_init(this);

    this->tag = SYMBOL_FUNCTION;

    this->ref.function_decl = function_decl;

    this->id = &function_decl->id;
    this->dt = &function_decl->dt;
    this->is_export = function_decl->is_export;
    this->srctok = function_decl->srctok;
}

void symbol_init_enum(struct symbol* this, struct enum_decl* enum_decl)
{
    nassert(NULL != enum_decl);
    nassert(NULL != enum_decl->srctok);
    symbol_init(this);

    this->tag = SYMBOL_ENUM;

    this->ref.enum_decl = enum_decl;

    this-> id = &enum_decl->id;

    this->dt = nalloc(sizeof(struct data_type));
    data_type_init(this->dt);
    this->dt->tag = DT_ENUM;
    this->dt->enum_ref = enum_decl;

    this->is_export = enum_decl->is_export;
    this->is_defined = NULL != enum_decl->def;
    this->srctok = enum_decl->srctok;
}

void symbol_init_union(struct symbol* this, struct union_decl* union_decl)
{
    nassert(NULL != union_decl);
    nassert(NULL != union_decl->srctok);
    symbol_init(this);

    this->tag = SYMBOL_UNION;

    this->ref.union_decl = union_decl;

    this-> id = &union_decl->id;

    this->dt = nalloc(sizeof(struct data_type));
    data_type_init(this->dt);
    this->dt->tag = DT_UNION;
    this->dt->union_ref = union_decl;

    this->is_export = union_decl->is_export;
    this->is_defined = NULL != union_decl->def;
    this->srctok = union_decl->srctok;
}

void symbol_init_struct(struct symbol* this, struct struct_decl* struct_decl)
{
    nassert(NULL != struct_decl);
    nassert(NULL != struct_decl->srctok);
    symbol_init(this);

    this->tag = SYMBOL_STRUCT;

    this->ref.struct_decl = struct_decl;

    this->id = &struct_decl->id;

    this->dt = nalloc(sizeof(struct data_type));
    data_type_init(this->dt);
    this->dt->tag = DT_STRUCT;
    this->dt->struct_ref = struct_decl;

    this->is_export = struct_decl->is_export;
    this->is_defined = NULL != struct_decl->def;
    this->srctok = struct_decl->srctok;
}

void symbol_init_variant(
    struct symbol* this,
    struct variant_decl* variant_decl)
{
    nassert(NULL != variant_decl);
    nassert(NULL != variant_decl->srctok);
    symbol_init(this);

    this->tag = SYMBOL_VARIANT;

    this->ref.variant_decl = variant_decl;

    this-> id = &variant_decl->id;

    this->dt = nalloc(sizeof(struct data_type));
    data_type_init(this->dt);
    this->dt->tag = DT_VARIANT;
    this->dt->variant_ref = variant_decl;

    this->is_export = variant_decl->is_export;
    this->is_defined = NULL != variant_decl->def;
    this->srctok = variant_decl->srctok;
}

void symbol_init_alias(struct symbol* this, struct alias_stmt* alias)
{
    nassert(NULL != alias);
    nassert(NULL != alias->srctok);
    symbol_init(this);

    this->tag = SYMBOL_ALIAS;

    this->ref.alias = alias;

    this->id = &alias->new_id;

    nassert(DT_UNSET != alias->dt.tag);
    this->dt = &alias->dt;
    this->is_export = alias->is_export;

    this->srctok = alias->srctok;

    this->is_defined = true;
}

void symbol_fini(struct symbol* this)
{
    INIT_FINI_COUNTER_INCR_FINI(symbol);

    if (SYMBOL_STRUCT == this->tag || SYMBOL_VARIANT == this->tag)
    {
        data_type_fini(this->dt);
        nfree(this->dt);
    }
}

void symbol_mark_use(struct symbol* this)
{
    this->uses += 1;
}

//====================================//
//      SYMBOL TABLE                  //
//====================================//
INIT_FINI_COUNTER_SOURCE(symbol_table);

#define SYMBOL_TABLE_MAX_ELEMENTS DICT_MAX_ELEMENTS
#define SYMBOL_TABLE_MAX_LOAD_FACTOR ((double)0.75)

static void symbol_table_dict_grow(struct symbol_table* symbol_table)
{
    uint32_t const old_dict_prime_idx = symbol_table->_dict_prime_idx;
    uint32_t const new_dict_prime_idx = symbol_table->_dict_prime_idx + 1;
    if (UNLIKELY(new_dict_prime_idx == DICT_PRIMES_LENGTH))
    {
        NPANIC_DICT_TOO_LARGE();
    }
    uint32_t const old_dict_length = dict_primes[old_dict_prime_idx];
    uint32_t const new_dict_length = dict_primes[new_dict_prime_idx];

    // Allocate memory for the resized dictionary.
    DIAGNOSTIC_PUSH
    DIAGNOSTIC_IGNORE("-Wconversion")
    uint32_t const new_dict_size =
        new_dict_length * sizeof(struct _stdict_slot);
    DIAGNOSTIC_POP
    struct _stdict_slot* const old_dict = symbol_table->_dict;
    struct _stdict_slot* const new_dict = nalloc(new_dict_size);
    memset(new_dict, 0, new_dict_size);

    // Copy elements from the old dictionary into the new dictionary.
    for (uint32_t i = 0; i < old_dict_length; ++i)
    {
        if (!old_dict[i].in_use)
        {
            continue;
        }

        uint32_t probe_idx = DICT_HASH_IDX(old_dict[i].hash, new_dict_length);
        while (new_dict[probe_idx].in_use)
        {
            DICT_INCR_LINEAR_PROBE(probe_idx, new_dict_length);
        }
        new_dict[probe_idx] = old_dict[i];
    }

    // Update symbol table members and free the old dictionary.
    symbol_table->_dict = new_dict;
    symbol_table->_dict_prime_idx = new_dict_prime_idx;
    nfree(old_dict);
}

static struct symbol* symbol_table_find_from_hash(
    struct _stdict_slot* dict,
    uint32_t dict_length,
    char const* symstr,
    size_t symstr_length,
    uint32_t hash)
{
    uint32_t probe_idx = DICT_HASH_IDX(hash, dict_length);
    while (dict[probe_idx].in_use)
    {
        struct symbol* const symbol = &dict[probe_idx].symbol;
        struct identifier const* const id = symbol->id;
        char const* const id_str = id->start;
        size_t const id_length = id->length;
        bool const is_found = symstr_length == id_length
            && cstr_n_eq(symstr, id_str, symstr_length);
        if (is_found)
        {
            return symbol;
        }
        DICT_INCR_LINEAR_PROBE(probe_idx, dict_length);
    }
    return NULL;
}

void symbol_table_init(struct symbol_table* symbol_table)
{
    INIT_FINI_COUNTER_INCR_INIT(symbol_table);
    MEMZERO_INSTANCE(symbol_table, typeof(*symbol_table));

    uint32_t const initial_dict_prime_idx = 0;
    uint32_t const initial_dict_length = dict_primes[initial_dict_prime_idx];
    symbol_table->_dict =
        calloc(initial_dict_length, sizeof(struct _stdict_slot));
    symbol_table->_dict_prime_idx = initial_dict_prime_idx;
    symbol_table->_dict_slots_in_use = 0;

    symbol_table->module_idx = current_module_idx;
}

void symbol_table_fini(struct symbol_table* symbol_table)
{
    INIT_FINI_COUNTER_INCR_FINI(symbol_table);

    struct _stdict_slot* const dict = symbol_table->_dict;
    uint32_t const dict_prime_idx = symbol_table->_dict_prime_idx;
    uint32_t const dict_length = dict_primes[dict_prime_idx];
    for (size_t i = 0; i < dict_length; ++i)
    {
        if (dict[i].in_use
            && symbol_table->module_idx == dict[i].symbol.module_idx)
        {
            symbol_fini(&dict[i].symbol);
        }
    }
    nfree(dict);
}

struct symbol* symbol_table_insert(
    struct symbol_table* symbol_table,
    struct symbol const* symbol)
{
    struct _stdict_slot* dict = symbol_table->_dict;
    uint32_t dict_prime_idx = symbol_table->_dict_prime_idx;
    uint32_t dict_length = dict_primes[dict_prime_idx];

    char const* const symstr = symbol->id->start;
    size_t const symstr_length = symbol->id->length;
    uint32_t const hash = hash_char_slice(symstr, symstr_length);

    // Check if the symbol already exists in the table.
    struct symbol const* existing_symbol = symbol_table_find_from_hash(
        dict, dict_length, symstr, symstr_length, hash);
    if (NULL != existing_symbol)
    {
        return NULL;
    }

    // Since the symbol does not exist in the table, check if the table needs
    // resizing, resize the table, and then insert the new symbol.
    symbol_table->_dict_slots_in_use += 1;
    double const load_factor =
        dict_slot_load_factor(symbol_table->_dict_slots_in_use, dict_length);
    if (load_factor > SYMBOL_TABLE_MAX_LOAD_FACTOR)
    {
        symbol_table_dict_grow(symbol_table);
        dict = symbol_table->_dict;
        dict_prime_idx = symbol_table->_dict_prime_idx;
        dict_length = dict_primes[dict_prime_idx];
    }

    uint32_t probe_idx = DICT_HASH_IDX(hash, dict_length);
    while (dict[probe_idx].in_use)
    {
        DICT_INCR_LINEAR_PROBE(probe_idx, dict_length);
    }

    dict[probe_idx].symbol = *symbol;
    dict[probe_idx].in_use = true;
    dict[probe_idx].hash = hash;
    symbol_table->most_recent_symbol = &dict[probe_idx].symbol;
    return &dict[probe_idx].symbol;
}

struct symbol* symbol_table_find(
    struct symbol_table* symbol_table,
    char const* symstr,
    size_t symstr_length)
{
    struct _stdict_slot* dict = symbol_table->_dict;
    uint32_t dict_prime_idx = symbol_table->_dict_prime_idx;
    uint32_t dict_length = dict_primes[dict_prime_idx];
    uint32_t const hash = hash_char_slice(symstr, symstr_length);

    struct symbol* const symbol = symbol_table_find_from_hash(
        dict, dict_length, symstr, symstr_length, hash);

    return symbol;
}

//====================================//
//      SCOPE                         //
//====================================//
INIT_FINI_COUNTER_SOURCE(scope);

void scope_init(struct scope* this, struct scope const* parent_scope)
{
    INIT_FINI_COUNTER_INCR_INIT(scope);
    MEMZERO_INSTANCE(this, typeof(*this));

    this->uid = (uint32_t)-1;

    this->parent = parent_scope;

    this->symbol_table = nalloc(sizeof(struct symbol_table));
    symbol_table_init(this->symbol_table);

    this->stmts = narr_alloc(0, sizeof(struct stmt));

    this->deferred_scopes = narr_alloc(0, sizeof(struct scope*));
}

void scope_fini(struct scope* this)
{
    INIT_FINI_COUNTER_INCR_FINI(scope);

    symbol_table_fini(this->symbol_table);
    nfree(this->symbol_table);

    size_t const stmts_length = narr_length(this->stmts);
    for (size_t i = 0; i < stmts_length; ++i)
    {
        stmt_fini(&this->stmts[i]);
    }
    narr_free(this->stmts);

    narr_free(this->deferred_scopes);
}

bool scope_is_global(struct scope const* this)
{
    return this->parent == GLOBAL_SCOPE_PARENT_PTR;
}

int scope_contains_entry_func(struct scope* scope)
{
    nlogf(LOG_DEBUG, "Checking if scope contains entry function.");

    struct symbol* const symbol =
        symbol_table_find(scope->symbol_table, "entry", cstr_len("entry"));

    if (NULL == symbol)
    {
        nlogf(LOG_DEBUG, "Scope contains no symbols with the name 'entry'.");
        return NO_ENTRY_FUNC;
    }

    struct data_type const* const symbol_dt = symbol->dt;
    if (!data_type_is_function(symbol_dt))
    {
        nlogf(LOG_DEBUG, "The 'entry' is not a function.");
        return INVALID_ENTRY_FUNC;
    }

    struct func_sig const* const func_sig = symbol_dt->func_sig;

    // func entry : () -> void
    if (data_type_is_void(&func_sig->return_dt)
        && (narr_length(func_sig->param_dts) == 0))
    {
        return ENTRY_FUNC_NO_ARGS_RTN_VOID;
    } // func entry : () -> u
    if (data_type_is_integer(&func_sig->return_dt)
        && (DT_U == func_sig->return_dt.tag)
        && (narr_length(func_sig->param_dts) == 0))
    {
        return ENTRY_FUNC_NO_ARGS_RTN_U;
    } // func entry : () -> s
    if (data_type_is_integer(&func_sig->return_dt)
        && (DT_S == func_sig->return_dt.tag)
        && (narr_length(func_sig->param_dts) == 0))
    {
        return ENTRY_FUNC_NO_ARGS_RTN_S;
    }

    // func entry : (argc : u, argv : **char) -> void
    if (data_type_is_void(&func_sig->return_dt)
        && (narr_length(func_sig->param_dts) == 2)

        && data_type_is_base(&func_sig->param_dts[0])
        && (DT_U == func_sig->param_dts[0].tag)

        && (data_type_length_modifiers(&func_sig->param_dts[1]) == 2)
        && (DT_POINTER == func_sig->param_dts[1].tag)
        && (DT_POINTER == func_sig->param_dts[1].inner->tag)
        && (DT_ASCII == func_sig->param_dts[1].inner->inner->tag))
    {
        return ENTRY_FUNC_ARGS_RTN_VOID;
    }

    // func entry : (argc : u, argv : **char) -> u
    if (data_type_is_integer(&func_sig->return_dt)
        && (DT_U == func_sig->return_dt.tag)
        && (narr_length(func_sig->param_dts) == 2)

        && data_type_is_base(&func_sig->param_dts[0])
        && (DT_U == func_sig->param_dts[0].tag)

        && (data_type_length_modifiers(&func_sig->param_dts[1]) == 2)
        && (DT_POINTER == func_sig->param_dts[1].tag)
        && (DT_POINTER == func_sig->param_dts[1].inner->tag)
        && (DT_ASCII == func_sig->param_dts[1].inner->inner->tag))
    {
        return ENTRY_FUNC_ARGS_RTN_U;
    }

    // func entry : (argc : u, argv : **char) -> s
    if (data_type_is_integer(&func_sig->return_dt)
        && (DT_S == func_sig->return_dt.tag)
        && (narr_length(func_sig->param_dts) == 2)

        && data_type_is_base(&func_sig->param_dts[0])
        && (DT_U == func_sig->param_dts[0].tag)

        && (data_type_length_modifiers(&func_sig->param_dts[1]) == 2)
        && (DT_POINTER == func_sig->param_dts[1].tag)
        && (DT_POINTER == func_sig->param_dts[1].inner->tag)
        && (DT_ASCII == func_sig->param_dts[1].inner->inner->tag))
    {
        return ENTRY_FUNC_ARGS_RTN_S;
    }
    return INVALID_ENTRY_FUNC;
}

//============================================================================//
//      MODULE                                                                //
//============================================================================//
INIT_FINI_COUNTER_SOURCE(module);

void module_init(struct module* module)
{
    nstr_init(&module->src_path);
    nstr_init(&module->out_path);
    nstr_init(&module->src);
    module->tokens = narr_alloc(0, sizeof(struct token));
    scope_init(&module->global_scope, GLOBAL_SCOPE_PARENT_PTR);
    module->exports = narr_alloc(0, sizeof(struct identifier));
    module->uid_counter = 0;
}

void module_fini(struct module* module)
{
    nstr_fini(&module->src_path);
    nstr_fini(&module->out_path);
    nstr_fini(&module->src);
    narr_free(module->tokens);
    scope_fini(&module->global_scope);
    narr_free(module->exports);
}

uint32_t module_generate_uid(struct module* module)
{
    return module->uid_counter++;
}

void module_add_latest_global_symbol_to_exports(struct module* module)
{
    struct symbol* const latest_symbol =
        module->global_scope.symbol_table->most_recent_symbol;
    module->exports = narr_push(module->exports, latest_symbol->id);
}

bool is_import(struct module* module, char const* symstr, size_t symstr_length)
{
    struct symbol* const export = symbol_table_find(
        module->global_scope.symbol_table, symstr, symstr_length);

    nassert(NULL != export);
    nassert(NULL != export->srctok);
    nassert(NULL != export->srctok->module);

    nstr_t const* const cur_src_path = &module->src_path;
    nstr_t const* const export_src_path = &export->srctok->module->src_path;
    return nstr_ne(cur_src_path, export_src_path);
}

stbool resolve_module_path(nstr_t* path, char const* relto)
{
    // If the path starts with '.' the path contains a "." or ".." at its start,
    // signifying that the module is not coming from the include path. In this
    // case we'll skip over searching the include path.
    if ('.' != path->data[0])
    {
        nlogf(
            LOG_DEBUG,
            "Resolving module path '%s' relative to include path.",
            path->data);

        nstr_t nstr_defer_fini path_include;
        nstr_init_cstr(&path_include, STRINGIFY(N_DEFAULT_INCLUDE_PATH));
        nstr_cat_fmt(&path_include, "/%s", path->data);
        if (0 == access(path_include.data, R_OK))
        {
            // File exists in the include path. Use path relative to the include
            // path rather than relative to relto.
            nstr_assign_nstr(path, &path_include);
            nlogf(LOG_DEBUG, "Resolved path as '%s'.", path->data);
            return STBOOL_SUCCESS;
        }
    }

    nlogf(
        LOG_DEBUG,
        "Resolving module path '%s' relative to '%s'.",
        path->data,
        relto);

    char relto_dirname_buf[4096] = { 0 };
    cstr_cpy(relto_dirname_buf, relto);
    char* const relto_dirname = dirname(relto_dirname_buf);

    nstr_t nstr_defer_fini path_rel;
    nstr_init_cstr(&path_rel, relto_dirname);
    nstr_cat_fmt(&path_rel, "/%s", path->data);
    if (0 == access(path_rel.data, R_OK))
    {
        nstr_assign_nstr(path, &path_rel);
        nlogf(LOG_DEBUG, "Resolved path as '%s'.", path->data);
        return STBOOL_SUCCESS;
    }

    return STBOOL_FAILURE;
}

void module_cache_init(void)
{
    nlogf(LOG_DEBUG, "Init module cache.");

    memset(module_cache, 0, sizeof(module_cache));
    module_cache_len = 0;
    current_module_idx = 0;
}

void module_cache_fini(void)
{
    nlogf(LOG_DEBUG, "Fini module cache (%zu modules).", module_cache_len);

    size_t save_current_module_idx = current_module_idx;
    for (size_t i = 0; i < module_cache_len; ++i)
    {
        nlogf(LOG_DEBUG, "  Fini module %zu in module cache.", i);
        current_module_idx = i;
        module_fini(&module_cache[i]);
    }
    current_module_idx = save_current_module_idx;
}

size_t module_cache_len;
size_t current_module_idx;
