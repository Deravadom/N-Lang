/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file parser_errors.h
 * @brief Expressive error diagnostics
 */

#include "parser.h"

//! Handle errors in the parser by analyzing the Chart data structure.
//!
//! @param items
//!     The entire Chart containing Earley items
//! @param o
//!     The current index into items
//! @param first_set
//!     Array of sets containing list of terminals that could start the rule
//! @param module
//!     The global module, used to lookup tokens
void handle_error(
    struct earley_set* items,
    size_t o,
    struct first_set first_set[NUMBER_OF_NONTERMINALS],
    struct module* module);

