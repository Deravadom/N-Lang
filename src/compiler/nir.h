/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#pragma once
#include "token.h"

//============================================================================//
//      ASCII CHARACTER                                                       //
//============================================================================//
//! Character type of the N language.
//! Valid characters cover the 7-bit ASCII character encoding within numeric
//! integer range [#N_ASCII_MIN, #N_ASCII_MAX].
typedef char n_ascii;
#define N_ASCII_MIN ((n_ascii)0x00)
#define N_ASCII_MAX ((n_ascii)0x7F)
#define N_ASCII_ESC ((n_ascii)0x1B) // '\e'

//! @return
//!     #true if the value of @p ch is within the range of valid characters.
bool n_ascii_is_valid(n_ascii ch);

#define N_ASCII_PRINTABLE_MIN 0x20
#define N_ASCII_PRINTABLE_MAX 0x7e
//! @return
//!     #true if the value of @p ch corresponds to a printable character.
bool n_ascii_is_printable(n_ascii ch);

//! Translate an N ascii character into the cstring corresponding to that
//! character's representation in an N source file.
char const* n_ascii_to_n_source_char(n_ascii ch);

//! Translate an N ascii character into the cstring corresponding to that
//! character's representation in a C source file.
char const* n_ascii_to_c_source_char(n_ascii ch);

//! Attempt to parse an N ascii character from the provided cstring.
//! @param start
//!     Start of the string to parse.
//! @param ascii
//!     Populated with the parsed ascii character on success.
//! @return
//!     Number of characters (non-negative) from @p start parsed on success.
//! @return
//!     -1 on failure.
int parse_n_ascii(char const* start, n_ascii* ascii);

//! Attempt to parse an N ascii character literal from the provided cstring.
//! @param start
//!     Start of the string to parse.
//! @param ascii
//!     Populated with the parsed N ascii character on success.
//! @return
//!     Number of characters (non-negative) from @p start parsed on success.
//! @return
//!     -1 on failure.
int parse_n_ascii_literal(char const* start, n_ascii* ascii);

//============================================================================//
//      ASCII STRING                                                          //
//============================================================================//
//! Attempt to match an N string literal from the provided cstring.
//! @param start
//!     Start of the string to match.
//! @param nstr
//!     Populated with the characters of the parsed N string literal (without
//!     the opening and closing quotes) on success.
//! @return
//!     Number of characters (non-negative) from @p start parsed on success.
//! @return
//!     -1 on failure.
int parse_n_astr_literal(char const* start, nstr_t* nstr);

//============================================================================//
//      INTEGER                                                               //
//============================================================================//
typedef uint8_t n_u8;
typedef uint16_t n_u16;
typedef uint32_t n_u32;
typedef uint64_t n_u64;
typedef int8_t n_s8;
typedef int16_t n_s16;
typedef int32_t n_s32;
typedef int64_t n_s64;

// Data types `u` and `s` are machine dependent sizes, but will be stored in the
// maximum bit width integer within the C language for machine independent
// processing/manipulation.
typedef uintmax_t n_u; //!< @note The actual size of #n_u is machine dependent.
typedef intmax_t n_s;  //!< @note The actual size of #n_s is machine dependent.
static_assert(
    sizeof(n_u) == sizeof(n_s),
    "u and s types must have the same bit width.");

#define U8_BITS 8
#define U16_BITS 16
#define U32_BITS 32
#define U64_BITS 64
#define U128_BITS 128
#define S8_BITS 8
#define S16_BITS 16
#define S32_BITS 32
#define S64_BITS 64
#define S128_BITS 128

#define STR_U8 "u8"
#define STR_U16 "u16"
#define STR_U32 "u32"
#define STR_U64 "u64"
#define STR_U128 "u128"
#define STR_U "u"
#define STR_S8 "s8"
#define STR_S16 "s16"
#define STR_S32 "s32"
#define STR_S64 "s64"
#define STR_S128 "s128"
#define STR_S "s"

#define STR_U8_LENGTH 2   //!< u8
#define STR_U16_LENGTH 3  //!< u16
#define STR_U32_LENGTH 3  //!< u32
#define STR_U64_LENGTH 3  //!< u64
#define STR_U128_LENGTH 4 //!< u128
#define STR_U_LENGTH 1    //!< u
#define STR_S8_LENGTH 2   //!< s8
#define STR_S16_LENGTH 3  //!< s16
#define STR_S32_LENGTH 3  //!< s32
#define STR_S64_LENGTH 3  //!< s64
#define STR_S128_LENGTH 4 //!< s128
#define STR_S_LENGTH 1    //!< s

#define STR_HEX_PREFIX_LOWER "0x"
#define STR_HEX_PREFIX_UPPER "0X"
#define STR_BIN_PREFIX_LOWER "0b"
#define STR_BIN_PREFIX_UPPER "0B"
#define STR_HEX_PREFIX_LENGTH 2 //!< 0x or 0X
#define STR_BIN_PREFIX_LENGTH 2 //!< 0b or 0B

//============================================================================//
//      FLOATING POINT                                                        //
//============================================================================//
#define F32_BITS 32
#define F64_BITS 64

#define STR_N_FLOAT_F16 "f16"
#define STR_N_FLOAT_F32 "f32"
#define STR_N_FLOAT_F64 "f64"
#define STR_N_FLOAT_F128 "f128"

#define STR_N_FLOAT_F16_LENGTH 3  //!< f16
#define STR_N_FLOAT_F32_LENGTH 3  //!< f32
#define STR_N_FLOAT_F64_LENGTH 3  //!< f64
#define STR_N_FLOAT_F128_LENGTH 4 //!< f128

//============================================================================//

//// Forward declarations of structs/enums describing the NIR.
//// These structures, declared here and defined below, sufficiently describe
//// an in-memory representation of the N language.
struct identifier;
struct data_type;
struct literal;
struct func_sig;
struct member_var;
struct member_func;
struct member_iface;
struct member_tag;
struct enum_def;
struct union_def;
struct struct_def;
struct variant_def;
struct expr;
struct variable_decl;
struct function_decl;
struct enum_decl;
struct union_decl;
struct struct_decl;
struct variant_decl;
struct if_stmt;
struct loop_stmt;
struct alias_stmt;
struct stmt;
struct symbol_table;
struct symbol;
struct scope;
struct module;

#define NIR_INIT_FINI_COUNTER_VERIFY_BALANCED()                                \
    INIT_FINI_COUNTER_VERIFY_BALANCED(identifier);                             \
    INIT_FINI_COUNTER_VERIFY_BALANCED(data_type);                              \
    INIT_FINI_COUNTER_VERIFY_BALANCED(literal);                                \
    INIT_FINI_COUNTER_VERIFY_BALANCED(func_sig);                               \
    INIT_FINI_COUNTER_VERIFY_BALANCED(member_var);                             \
    INIT_FINI_COUNTER_VERIFY_BALANCED(member_func);                            \
    INIT_FINI_COUNTER_VERIFY_BALANCED(member_iface);                           \
    INIT_FINI_COUNTER_VERIFY_BALANCED(member_tag);                             \
    INIT_FINI_COUNTER_VERIFY_BALANCED(enum_def);                               \
    INIT_FINI_COUNTER_VERIFY_BALANCED(union_def);                              \
    INIT_FINI_COUNTER_VERIFY_BALANCED(struct_def);                             \
    INIT_FINI_COUNTER_VERIFY_BALANCED(variant_def);                            \
    INIT_FINI_COUNTER_VERIFY_BALANCED(expr);                                   \
    INIT_FINI_COUNTER_VERIFY_BALANCED(variable_decl);                          \
    INIT_FINI_COUNTER_VERIFY_BALANCED(function_decl);                          \
    INIT_FINI_COUNTER_VERIFY_BALANCED(enum_decl);                              \
    INIT_FINI_COUNTER_VERIFY_BALANCED(union_decl);                             \
    INIT_FINI_COUNTER_VERIFY_BALANCED(struct_decl);                            \
    INIT_FINI_COUNTER_VERIFY_BALANCED(variant_decl);                           \
    INIT_FINI_COUNTER_VERIFY_BALANCED(if_stmt);                                \
    INIT_FINI_COUNTER_VERIFY_BALANCED(loop_stmt);                              \
    INIT_FINI_COUNTER_VERIFY_BALANCED(alias_stmt);                             \
    INIT_FINI_COUNTER_VERIFY_BALANCED(stmt);                                   \
    INIT_FINI_COUNTER_VERIFY_BALANCED(symbol_table);                           \
    INIT_FINI_COUNTER_VERIFY_BALANCED(symbol);                                 \
    INIT_FINI_COUNTER_VERIFY_BALANCED(scope);                                  \
    INIT_FINI_COUNTER_VERIFY_BALANCED(module);

//============================================================================//
//      IDENTIFIER                                                            //
//============================================================================//
//! Identifier.
struct identifier
{
    //! Pointer to the first character of this identifier in the source file it
    //! was parsed from.
    //! @note
    //!     It is the responsibility of the programmer to ensure the memory
    //!     pointed to by #start is valid.
    char const* start;

    //! Number of characters that make up the identifier.
    size_t length;

    //! Pointer to the token that this identifier was parsed from.
    //! #NULL if this identifier was created via a transformation.
    struct token const* srctok;
};
INIT_FINI_COUNTER_HEADER(identifier);
//! @init
void identifier_init(struct identifier* this);
//! @fini
void identifier_fini(struct identifier* this);
//! @assign
//! @param dest
//!     Instance being assigned to.
//! @param src
//!     Instance being assigned from.
//! @param clear_srctok
//!     Sets `dest->srctok` to #NULL if #true, indicating that @p dest was
//!     created via a transformation.
void identifier_assign(
    struct identifier* dest,
    struct identifier const* src,
    bool clear_srctok);
//! @eq
//! @return
//!     #true if strings that make up identifiers @p A and @p B are equivalent.
bool identifier_eq(struct identifier const* lhs, struct identifier const* rhs);

//============================================================================//
//      DATA TYPE                                                             //
//============================================================================//
struct data_type
{
    enum
    {
        DT_UNSET,

        DT_VOID,  //!< `void`
        DT_U8,    //!< `u8`
        DT_U16,   //!< `u16`
        DT_U32,   //!< `u32`
        DT_U64,   //!< `u64`
        DT_U,     //!< `u`
        DT_S8,    //!< `s8`
        DT_S16,   //!< `s16`
        DT_S32,   //!< `s32`
        DT_S64,   //!< `s64`
        DT_S,     //!< `s`
        DT_F16,   //!< `f16`
        DT_F32,   //!< `f32`
        DT_F64,   //!< `f64`
        DT_F128,  //!< `f128`
        DT_BOOL,  //!< `bool`
        DT_ASCII, //!< `ascii`

        DT_ENUM,    //!< `enum`
        DT_UNION,   //!< `union`
        DT_STRUCT,  //!< `struct`
        DT_VARIANT, //!< `variant`

        DT_FUNCTION,

        DT_POINTER,
        DT_ARRAY,

        DT_UNRESOLVED_ID,
        DT_UNRESOLVED_TYPEOF_EXPR,
        DT_UNRESOLVED_TYPEOF_DT
    } tag;

    //! Qualifiers of this data type.
    struct qualifiers
    {
        //! Indicates the qualified subject is modifiable.
        bool is_mut;

        //! Indicates that if some object accessible though the qualified
        //! subject (directly or indirectly) is modified, by any means, then all
        //! accesses to that object (both reads and writes) in that block must
        //! occur through the subject (directly or indirectly), otherwise the
        //! behavior is undefined.
        //! @br
        //! Taken from https://en.cppreference.com/w/c/language/restrict
        bool is_unique;

        //! Indicates that the read and write operations on the qualified
        //! subjects are considered an observable side effect to all other
        //! operations within the executing thread.
        bool is_volatile;
    } qualifiers;

    union
    {
        //! #DT_ENUM
        struct enum_decl* enum_ref;
        //! #DT_UNION
        struct union_decl* union_ref;
        //! #DT_STRUCT
        struct struct_decl* struct_ref;
        //! #DT_VARIANT
        struct variant_decl* variant_ref;

        //! #DT_FUNCTION
        /*!*/ struct func_sig* func_sig;

        //! #DT_ARRAY
        struct
        {
            size_t length;
        } array;

        union
        {
            //! #DT_UNRESOLVED_ID
            struct identifier id;

            //! #DT_UNRESOLVED_TYPEOF_EXPR
            /*!*/ struct expr* typeof_expr;

            //! #DT_UNRESOLVED_TYPEOF_DT
            /*!*/ struct data_type* typeof_dt;
        } unresolved;
    };

    //! Pointer to the first token the data type base was parsed from.
    //! #NULL if this data type base was created via a transformation.
    struct token const* srctok;

    //! Pointer to the next inner layer of this data type.
    /*!*/ struct data_type* inner;
};
INIT_FINI_COUNTER_HEADER(data_type);
//! @init
void data_type_init(struct data_type* this);
//! @fini
void data_type_fini(struct data_type* this);
//! @reset
void data_type_reset(struct data_type* this);
//! @assign
//! @param dest
//!     Instance being assigned to.
//! @param src
//!     Instance being assigned from.
//! @param clear_srctok
//!     Sets `srctok` members in @p dest to #NULL if #true, indicating that
//!     @p dest was created via a transformation.
void data_type_assign(
    struct data_type* dest,
    struct data_type const* src,
    bool clear_srctok);

//! Number of #data_type structs that make up  @p this, including the base type
//! and all data type modifiers.
size_t data_type_length_all(struct data_type const* this);
//! Number of #data_type structs that make up the modifiers of @p this.
//! The expression `data_type_length_modifiers(foo)` is equivalent to the
//! expression `data_type_length_all(foo)-1`.
size_t data_type_length_modifiers(struct data_type const* this);

//! @p modifier should be an initialized #data_type allocated via #nalloc.
//! `modifier->inner` should NOT be set, as it will be set automatically.
void data_type_push_modifier(
    struct data_type* this,
    struct data_type* modifier);
//! Pop and free the outermost modifier of @p dt.
void data_type_pop_modifier(struct data_type* this);

//! @return
//!     #true if @p this is the `void` base type.
bool data_type_is_void(struct data_type const* this);
//! @return
//!     #true if @p this is one of the integer base types.
bool data_type_is_integer(struct data_type const* this);
//! @return
//!     #true if @p this is one of the unsigned integer base types.
bool data_type_is_integer_unsigned(struct data_type const* this);
//! @return
//!     #true if @p this is one of the signed integer base types.
bool data_type_is_integer_signed(struct data_type const* this);
//! @return
//!     #true if @p this is one of the floating point base types.
bool data_type_is_float(struct data_type const* this);
//! @return
//!     #true if @p this is a `bool` base type.
bool data_type_is_bool(struct data_type const* this);
//! @return
//!     #true if @p this is a `ascii` base type.
bool data_type_is_ascii(struct data_type const* this);
//! @return
//!     #true if @p this is a `enum` base type.
bool data_type_is_enum(struct data_type const* this);
//! @return
//!     #true if @p this is a `union` base type.
bool data_type_is_union(struct data_type const* this);
//! @return
//!     #true if @p this is a `struct` base type.
bool data_type_is_struct(struct data_type const* this);
//! @return
//!     #true if @p this is a `variant` base type.
bool data_type_is_variant(struct data_type const* this);
//! @return
//!     #true if @p this is a function base type.
bool data_type_is_function(struct data_type const* this);
//! @return
//!     #true if outermost modifier of @p this is a pointer.
bool data_type_is_pointer(struct data_type const* this);
//! @return
//!     #true if outermost modifier of @p this is an array.
bool data_type_is_array(struct data_type const* this);

//! @return
//!     #true if @p this is solely a base type, i.e. @p this has no modifiers.
bool data_type_is_base(struct data_type const* this);
//! @return
//!     #true if @p this modifies a base type.
bool data_type_is_modifier(struct data_type const* this);

//! @return
//!     #true if @p this is one of the integer or floating point base types.
bool data_type_is_numeric(struct data_type const* this);
//! @return
//!     #true if @p this is a void, arithmetic, bool, or character type.
bool data_type_is_primitive(struct data_type const* this);
//! @return
//!     #true if @p this is a type composed from a combination of other types.
bool data_type_is_composite(struct data_type const* this);
//! @return
//!     #true if @p this is a pointer to `void`.
bool data_type_is_void_pointer(struct data_type const* this);

//! Returns a newly initialized #nstr_t containing @p this formatted as a data
//! type within the N programming language.
//! @param dt
//!     Target #data_type to be be stringified.
//! @return
//!     Newly initialized #nstr_t containing @p this translated into N source
//!     code.
//! @note
//!     The string returned from this function is returned initialized and
//!     *must* be fini'd by the user.
nstr_t data_type_to_n_source_nstr(struct data_type const* dt);

//! Returns a newly initialized #nstr_t containing @p dt formatted as a data
//! type within the C programming language.
//! @param dt
//!     Target #data_type to be be stringified.
//! @param id @nullable
//!     The identifier associated with @p dt.
//!     Parsing of data types in C is weird since portions of the data type go
//!     on either side of the identifier.
//!     To combat this jank, #data_type_to_c_source_nstr can generate a type
//!     alongside its identifier to make variable and parameter declarations
//!     easier to translate.
//!     @br
//!     If #NULL then an identifier will not be included with the type.
//! @note
//!     The string returned from this function is returned initialized and
//!     *must* be fini'd by the user.
nstr_t data_type_to_c_source_nstr(
    struct data_type const* dt,
    struct identifier const* id);

//! Check if data type @p from can be converted to data type @p to without
//! requiring an explicit cast from the user.
//!
//! ### List of allowable implicit conversions:
//!
//! #### Identity conversion
//!
//! By definition, any data type can be implicitly converted to itself.
//!
//! #### Qualifier restricting conversion
//!
//! Data type `Ta` can be converted to data type `Tb` if the qualifiers second
//! outer most modifier `Ta` are stricter then the corresponding qualifiers of
//! `Tb`, and all other inner qualifiers of `Ta` and `Tb` are equivalent.
//! This conversion requires `Ta` and `Tb` to have the same base type and
//! modifier types.
//!
//! ```
//! Valid qualifier restricting conversion examples:
//!
//! char          --> mut char
//! mut char      --> char
//! mut char      --> volatile char
//! mut char      --> volatile mut char
//! ^mut char     --> ^char
//! mut ^mut char --> ^char
//!
//! ----------------------------------------------
//!
//! Invalid qualifier restricting conversion examples:
//!
//! **char --> **mut char
//! ```
//!
//! #### Pointer conversion to `void` pointer
//!
//! Let `* Qa T` represent the data type 'pointer to `T`' where `Qa` is the set
//! of qualifiers on `T`.
//! @br
//! The implicit conversion `^ Qa T --> ^ Qb void` is allowed where qualifiers
//! `Qa` are equally or less restrictive than qualifiers `Qb`.
//!
//! #### Pointer conversion from `void` pointer
//!
//! Let `^ Qa void` represent the data type 'pointer to `void`' where `Qa` is
//! the set of qualifiers on `void`.
//! @br
//! The implicit conversion `^ Qa void --> ^ Qb T` is allowed where qualifiers
//! `Qa` are equally or less restrictive than qualifiers `Qb`.
//!
//! @return
//!     #true if data type @p from can be implicitly converted to data type
//!     @p to.
//! @note
//!     Conversions from `void` and conversions to `void` are always illegal,
//!     implicit or explicit.
bool data_type_can_implicitly_convert(
    struct data_type const* from,
    struct data_type const* to);

//! Similar to #data_type_can_implicitly_convert, but accounts for array
//! initialization which is the exception to implicit conversion rules.
bool data_type_can_implicitly_convert__variable_init(
    struct data_type const* from,
    struct data_type const* to);

//! Check if data type @p from can be converted to data type @p to via an
//! explicit cast from the user.
//!
//! ### List of allowable explicit conversions:
//!
//! #### Any implicit conversion
//!
//! If an explicit conversion would perform the same conversion an implicit
//! conversion, then that conversion is obviously allowed.
//!
//! #### Integer to integer conversion
//!
//! Integers of any bitwidth may be converted to integers of any other bitwidth.
//! Downsizing truncates excess high bits.
//!
//! #### Char to integer conversion
//!
//! Characters may be converted to integers of any bitwidth.
//!
//! #### Integer to char conversion
//!
//! Integers of any bitwidth may be converted to a character.
//! Downsizing truncates excess high bits.
//!
//! #### Integer to floating point conversion.
//!
//! Integers of any bitwidth may be converted to any floating point type.
//! Precision may be lost as a result of the conversion.
//!
//! #### Floating point to integer conversion.
//!
//! Any bitwidth floating point type may be converted to any integer type.
//! The fractional component of the floating point value will be truncated.
//! Undefined behavior will result if the range of the integer type does not
//! contain the value of a floating point value.
//!
//! #### Pointer to integer conversion
//!
//! Convert a pointer to an integer of any bitwidth.
//! Downsizing truncates excess high bits.
//!
//! #### Integer to pointer conversion
//!
//! Convert an integer of any bitwidth to a pointer.
//! Downsizing truncates excess high bits.
//!
//! #### Pointer to pointer conversion
//!
//! Convert a pointer of any type to a pointer of any other type.
//!
//! @return
//!     #true if data type @p from can be explicitly converted to data type
//!     @p to.
//! @note
//!     Conversions from `void` and conversions to `void` are always illegal,
//!     implicit or explicit.
bool data_type_can_explicitly_convert(
    struct data_type const* from,
    struct data_type const* to);

#define DECLARE_DT_NSTR(_identifier, _p_dt)                                    \
    nstr_t nstr_defer_fini _identifier = data_type_to_n_source_nstr(_p_dt)

//! @eq
//! @return
//!     #true if qualifiers @p A and @p B are equivalent.
bool qualifiers_eq(struct qualifiers const* lhs, struct qualifiers const* rhs);

//! Populates @p dest with the union of the qualifier sets @p A and @p B.
//! This function will NOT change the srctok of @p dest.
void qualifers_union(
    struct qualifiers* dest,
    struct qualifiers const* A,
    struct qualifiers const* B);

//! Check if the provided qualifiers are equivalent to default qualifiers on a
//! #data_type.
//! @return
//!     #true if @p q is default qualifiers.
bool qualifiers_is_default(struct qualifiers const* q);

//! Check whether the qualifier transformation @p from --> @p to is a
//! restricting conversion.
//! @return
//!     #true if @p from is equally or less restrictive than @p to.
bool qualifiers_is_restricting(
    struct qualifiers const* from,
    struct qualifiers const* to);

//============================================================================//
//      LITERAL VALUE                                                         //
//============================================================================//
struct literal
{
    enum
    {
        LITERAL_UNSET = 0,

        LITERAL_U8,
        LITERAL_U16,
        LITERAL_U32,
        LITERAL_U64,
        LITERAL_U,
        LITERAL_S8,
        LITERAL_S16,
        LITERAL_S32,
        LITERAL_S64,
        LITERAL_S,
        LITERAL_F32,
        LITERAL_F64,
        LITERAL_BOOL,
        LITERAL_ACHAR,
        LITERAL_ASTR,
        LITERAL_NULL,
        LITERAL_ARRAY,
        LITERAL_ARRAY_FILL //!< Special value marking '..' in an array literal.
    } tag;

    union
    {
        n_u8 u8;
        n_u16 u16;
        n_u32 u32;
        n_u64 u64;
        n_u u;
        n_s8 s8;
        n_s16 s16;
        n_s32 s32;
        n_s64 s64;
        n_s s;
        struct
        {
            char const* start;
            size_t length;
        } f16, f32, f64, f128;
        bool boolean;
        n_ascii achar;
        nstr_t astr;
        /*!*/ narr_t(struct literal) array;
        size_t array_fill_length;
    };

    //! Pointer to the first token the literal was parsed from.
    //! #NULL if this literal was created via a transformation.
    struct token const* srctok;
};
INIT_FINI_COUNTER_HEADER(literal);
//! @init
void literal_init(struct literal* this);
//! @fini
void literal_fini(struct literal* this);
//! @reset
void literal_reset(struct literal* this);
//! @assign
//! @param dest
//!     Instance being assigned to.
//! @param src
//!     Instance being assigned from.
//! @param clear_srctok
//!     Sets `srctok` variables in the dest to #NULL if #true, indicating that
//!     @p dest was created via a transformation.
void literal_assign(
    struct literal* dest, struct literal const* src, bool clear_srctok);
//! Get the `typeof` the provided literal value.
//! @param literal
//!     Target literal.
struct data_type literal_get_data_type(struct literal const* literal);

//============================================================================//
//      FUNCTION SIGNATURE                                                    //
//============================================================================//
//! Function signature.
struct func_sig
{
    struct data_type return_dt;
    /*!*/ narr_t(struct data_type) param_dts;
};
INIT_FINI_COUNTER_HEADER(func_sig);
//! @init
void func_sig_init(struct func_sig* this);
//! @fini
void func_sig_fini(struct func_sig* this);
//! @reset
void func_sig_reset(struct func_sig* this);
//! @assign
//! @param dest
//!     Instance being assigned to.
//! @param src
//!     Instance being assigned from.
//! @param clear_srctok
//!     Sets `srctok` variables in the dest to #NULL if #true, indicating that
//!     @p dest was created via a transformation.
void func_sig_assign(
    struct func_sig* dest,
    struct func_sig const* src,
    bool clear_srctok);

//============================================================================//
//      MEMBER VARIABLE                                                       //
//============================================================================//
struct member_var
{
    //! Identifier of the member variable.
    struct identifier id;

    //! Data type of the member variable.
    struct data_type dt;

    //! Pointer to the first token the member variable was parsed from.
    //! #NULL if this member variable was created via a transformation.
    struct token const* srctok;
};
INIT_FINI_COUNTER_HEADER(member_var);
//! @init
void member_var_init(struct member_var* this);
//! @fini
void member_var_fini(struct member_var* this);
//! duplicates the #member_var
void member_var_init_member_var(struct member_var const* og,
    struct member_var* nc);

//============================================================================//
//      MEMBER FUNCTION                                                       //
//============================================================================//
struct member_func
{
    //! Identifier of the member function.
    struct identifier id;

    //! Data type of the member function.
    //! @note
    //!     Set during semantic analysis.
    struct data_type dt;

    struct
    {
        //! Identifier of the target function.
        //! Set during syntax analysis.
        struct identifier id;
    } target;

    //! Pointer to the first token the member function was parsed from.
    //! #NULL if this member function was created via a transformation.
    struct token const* srctok;
};
INIT_FINI_COUNTER_HEADER(member_func);
//! @init
void member_func_init(struct member_func* this);
//! @fini
void member_func_fini(struct member_func* this);
//! Duplicate the #member_func
void member_func_init_member_func(struct member_func const* og,
    struct member_func* nc);

//============================================================================//
//      MEMBER INTERFACE                                                      //
//============================================================================//

//! defines an iface member of the variant type.
struct member_iface
{
    //! Identifier of the interface.
    struct identifier id;

    //! Function Signature of the interface.
    //! @note set during syntax analysis
    struct func_sig sig;

    //! Pointer to the first token of the member iface.
    //! #NULL if this iface was generated during semantic analysis.
    struct token const* srctok;
};
INIT_FINI_COUNTER_HEADER(member_iface);
//! @init
void member_iface_init(struct member_iface* iface);
//! @fini
void member_iface_fini(struct member_iface* iface);

//============================================================================//
//      ENUM DEFINITION                                                       //
//============================================================================//

//! defines the enum type.
struct enum_def
{
    //! The identifiers naming the enum tags.
    /*!*/ narr_t(struct identifier) member_ids;
    //! The literal values of the enum tags.
    /*!*/ narr_t(struct literal) member_vals;

    //! pointer to the first token of the enum.
    //! #NULL if this was generated during semantic analysis.
    struct token const* srctok;
};
INIT_FINI_COUNTER_HEADER(enum_def);
//! @init
void enum_def_init(struct enum_def* edef);
//! @fini
void enum_def_fini(struct enum_def* edef);

#define MEMBER_ID_NOT_FOUND (-1)
//! @return
//!     The index of the ID in this enum. MEMBER_ID_NOT_FOUND if the ID is
//!     not contained by this enum_def
//! @param this
//!     The enum_def to search.
//! @param id
//!     The identifier to search for.
ssize_t enum_def_member_id_idx(
    struct enum_def const* this,
    struct identifier const* id);

//============================================================================//
//      UNION DEFINITION                                                      //
//============================================================================//

//! Definition of a union.
struct union_def
{
    //! The variables contained within this union.
    /*!*/ narr_t(struct member_var) member_vars;

    //! pointer to the first token of the union.
    //! #NULL if this union was generated during semantic analysis.
    struct token const* srctok;
};
INIT_FINI_COUNTER_HEADER(union_def);
//! @init
void union_def_init(struct union_def* udef);
//! @fini
void union_def_fini(struct union_def* udef);

//! @return
//!     The index of the member var in this union. MEMBER_VAR_NOT_FOUND if the
//!     ID is not found in this union
//! @param this
//!     The union_def to search.
//! @param id
//!     The ID to seach for.
ssize_t union_def_member_var_idx(
    struct union_def const* this,
    struct identifier const* id);

//============================================================================//
//      STRUCT DEFINITION                                                     //
//============================================================================//
struct struct_def
{
    /*!*/ narr_t(struct member_var) member_vars;

    /*!*/ narr_t(struct member_func) member_funcs;

    //! Pointer to the first token the struct definition was parsed from.
    //! #NULL if this struct definition was created via a transformation.
    struct token const* srctok;
};
INIT_FINI_COUNTER_HEADER(struct_def);
//! @init
void struct_def_init(struct struct_def* this);
//! @fini
void struct_def_fini(struct struct_def* this);
//! Duplicate a #struct_def
void struct_def_init_struct_def(struct struct_def const* og,
    struct struct_def* nc);

#define MEMBER_VAR_NOT_FOUND (-1)
//! @return
//!     Non-negative index of the #member_var with identifier @p id in the
//!     member_vars list of @p def if it exists.
//! @return
//!     #MEMBER_VAR_NOT_FOUND if no member variable with identifier @p id exists
//!     within the member_vars list of @p def.
ssize_t struct_def_member_var_idx(
    struct struct_def const* def,
    struct identifier const* id);
#define MEMBER_FUNC_NOT_FOUND (-1)
//! @return
//!     Non-negative index of the #member_func with identifier @p id in the
//!     member_funcs list of @p def if it exists.
//! @return
//!     #MEMBER_FUNC_NOT_FOUND if no member function with identifier @p id
//!     exists within the member_funcs list of @p def.
ssize_t struct_def_member_func_idx(
    struct struct_def const* def,
    struct identifier const* id);

//============================================================================//
//      MEMBER TAG                                                            //
//============================================================================//

//! The member_tag type describes one valid tag of a variant, including the
//! section which is associated with the tag.
struct member_tag
{
    //! Identifier of the tag
    struct identifier id;

    //! numerical value of the tag.
    //! @note
    //! this attribute is set by syntax analysis if it was set by the user.
    //! if it was not set by the user, semantic analysis will generate a value.
    struct literal value;

    //! The section owned by this tag.
    struct struct_def section;

    //! Pointer to the first token in this member_tag.
    //! #NULL if this was generated during semantic analysis.
    struct token const* srctok;
};
INIT_FINI_COUNTER_HEADER(member_tag);
//! @init
void member_tag_init(struct member_tag* tag);
//! @fini
void member_tag_fini(struct member_tag* tag);

//============================================================================//
//      VARIANT DEFINITION                                                    //
//============================================================================//

//! The variant_def type describes the contents of a variant.
struct variant_def
{
    //! The type of all tags within the variant.
    struct data_type tag_dt;

    //! The member variables of the common section
    /*!*/ narr_t(struct member_var) member_vars;
    //! The member functions of the common section
    /*!*/ narr_t(struct member_func) member_funcs;
    //! The interfaces of this varaint
    /*!*/ narr_t(struct member_iface) member_ifaces;
    //! The tags/sections defined in this varaint.
    /*!*/ narr_t(struct member_tag) member_tags;

    //! Pointer to the first token in this variant.
    //! #NULL if this was generated during semantic analysis.
    struct token const* srctok;
};
INIT_FINI_COUNTER_HEADER(variant_def);

//! @init
void variant_def_init(struct variant_def* vdef);
//! @fini
void variant_def_fini(struct variant_def* vdef);

//! @return
//!     The index into the member_var of the identifier, if it exists.
//!     Otherwise returns MEMBER_VAR_NOT_FOUND
//! @param this
//!     The variant_def to search
//! @param id
//!     The id to search for.
ssize_t variant_def_member_var_idx(
    struct variant_def const* this,
    struct identifier const* id);

//! @return
//!     The index into the member_func of the identifier, if it exists.
//!     Otherwise returns MEMBER_FUNC_NOT_FOUND
//! @param this
//!     The variant_def to search
//! @param id
//!     The id to search for.
ssize_t variant_def_member_func_idx(
    struct variant_def const* this,
    struct identifier const* id);

#define MEMBER_IFACE_NOT_FOUND (-1)
//! @return
//!     The index into the member_iface of the identifier, if it exists.
//!     Otherwise returns MEMBER_IFACE_NOT_FOUND
//! @param this
//!     The variant_def to search
//! @param id
//!     The id to search for.
ssize_t variant_def_member_iface_idx(
    struct variant_def const* this,
    struct identifier const* id);

#define MEMBER_TAG_NOT_FOUND (-1)
//! @return
//!     The index into the member_tag of the identifier, if it exists.
//!     Otherwise returns MEMBER_TAG_NOT_FOUND
//! @param this
//!     The variant_def to search
//! @param id
//!     The id to search for.
ssize_t variant_def_member_tag_idx(
    struct variant_def const* this,
    struct identifier const* id);

//============================================================================//
//      EXPRESSION                                                            //
//============================================================================//
struct expr
{
    enum
    {
        EXPR_UNSET = 0,

        __EXPR_PRIMARY_BEGIN__,
        EXPR_PRIMARY_IDENTIFIER,
        EXPR_PRIMARY_LITERAL,
        EXPR_PRIMARY_PAREN,
        __EXPR_PRIMARY_END__,

        __EXPR_POSTFIX_BEGIN__,
        EXPR_POSTFIX_FUNCTION_CALL,
        EXPR_POSTFIX_SUBSCRIPT,
        EXPR_POSTFIX_ACCESS_DOT,
        EXPR_POSTFIX_ACCESS_ARROW,
        __EXPR_POSTFIX_END__,

        __EXPR_UNARY_BEGIN__,
        EXPR_UNARY_ADDR_OF,      //!< `?EXPR`
        EXPR_UNARY_DEREF,        //!< `@EXPR`
        EXPR_UNARY_DECAY,        //!< `$EXPR`
        EXPR_UNARY_PLUS,         //!< `+EXPR`
        EXPR_UNARY_MINUS,        //!< `-EXPR`
        EXPR_UNARY_BITWISE_NOT,  //!< `~EXPR`
        EXPR_UNARY_LOGICAL_NOT,  //!< `!EXPR`
        EXPR_UNARY_COUNTOF_EXPR, //!< `countof(EXPR)`
        EXPR_UNARY_SIZEOF_EXPR,  //!< `sizeof(EXPR)`
        EXPR_UNARY_COUNTOF_DT,   //!< `countof(DATA_TYPE)`
        EXPR_UNARY_SIZEOF_DT,    //!< `sizeof(DATA_TYPE)`
        __EXPR_UNARY_END__,

        __EXPR_BINARY_BEGIN__,
        EXPR_BINARY_ASSIGN,      //!< `EXPR = EXPR`
        EXPR_BINARY_LOGICAL_OR,  //!< `EXPR || EXPR`
        EXPR_BINARY_LOGICAL_AND, //!< `EXPR && EXPR`
        EXPR_BINARY_BITWISE_OR,  //!< `EXPR | EXPR`
        EXPR_BINARY_BITWISE_XOR, //!< `EXPR ^ EXPR`
        EXPR_BINARY_BITWISE_AND, //!< `EXPR & EXPR`
        EXPR_BINARY_REL_EQ,      //!< `EXPR == EXPR`
        EXPR_BINARY_REL_NE,      //!< `EXPR != EXPR`
        EXPR_BINARY_REL_LT,      //!< `EXPR < EXPR`
        EXPR_BINARY_REL_GT,      //!< `EXPR > EXPR`
        EXPR_BINARY_REL_LE,      //!< `EXPR <= EXPR`
        EXPR_BINARY_REL_GE,      //!< `EXPR >= EXPR`
        EXPR_BINARY_SHIFT_L,     //!< `EXPR << EXPR`
        EXPR_BINARY_SHIFT_R,     //!< `EXPR >> EXPR`
        EXPR_BINARY_PLUS,        //!< `EXPR + EXPR`
        EXPR_BINARY_MINUS,       //!< `EXPR - EXPR`
        EXPR_BINARY_PTR_DIFF,    //!< `EXPR -** EXPR`
        EXPR_BINARY_PTR_PLUS,    //!< `EXPR +* EXPR`
        EXPR_BINARY_PTR_MINUS,   //!< `EXPR -* EXPR`
        EXPR_BINARY_MULT,        //!< `EXPR * EXPR`
        EXPR_BINARY_DIV,         //!< `EXPR / EXPR`
        EXPR_BINARY_CAST,        //!< `EXPR as DATA_TYPE`
        __EXPR_BINARY_END__
    } tag;

    union
    {
        //! #EXPR_PRIMARY_IDENTIFIER
        struct identifier id;

        //! #EXPR_PRIMARY_LITERAL
        /*!*/ struct literal* literal;

        //! #EXPR_PRIMARY_PAREN
        /*!*/ struct expr* paren;

        //! Postfix expression.
        struct
        {
            //! Expr with function type in the case of a function call.
            //! @br
            //! Expr with array in the case of a subscript.
            //! @br
            //! Expr resolving to a struct, union, or enum in the case of a
            //! member dot or member arrow.
            /*!*/ struct expr* lhs;

            union
            {
                //! #EXPR_POSTFIX_FUNCTION_CALL
                /*!*/ narr_t(struct expr) args;

                //! #EXPR_POSTFIX_SUBSCRIPT
                /*!*/ struct expr* subscript;

                //! #EXPR_POSTFIX_ACCESS_DOT
                //! #EXPR_POSTFIX_ACCESS_ARROW
                struct
                {
                    //! Member identifier.
                    struct identifier id;

                    //! #true if accessing a #member_func.
                    //! #false if accessing a #member_var.
                    //! @note
                    //!     Set during semantic analysis.
                    //! @todo
                    //!     This data member should be removed from the expr
                    //!     struct, at least as a user facing data member.
                    //!     This information is possible to calculate on the fly
                    //!     and its inclusion here is mostly for caching, but
                    //!     overall not a settable property of the expression.
                    bool is_member_func;
                    bool is_member_tag;
                } access;
            };
        } postfix;

        //! Unary expression operand.
        union
        {
            //! #EXPR_UNARY_ADDR_OF
            //! #EXPR_UNARY_DEREF
            //! #EXPR_UNARY_DECAY
            //! #EXPR_UNARY_PLUS
            //! #EXPR_UNARY_MINUS
            //! #EXPR_UNARY_BITWISE_NOT
            //! #EXPR_UNARY_LOGICAL_NOT
            //! #EXPR_UNARY_COUNTOF_EXPR
            //! #EXPR_UNARY_SIZEOF_EXPR
            /*!*/ struct expr* rhs_expr;

            //! #EXPR_UNARY_COUNTOF_DT
            //! #EXPR_UNARY_SIZEOF_DT
            /*!*/ struct data_type* rhs_dt;
        } unary;

        //! Binary expression operands.
        struct
        {
            /*!*/ struct expr* lhs_expr;
            union
            {
                //! #EXPR_BINARY_ASSIGN
                //! #EXPR_BINARY_LOGICAL_OR
                //! #EXPR_BINARY_LOGICAL_AND
                //! #EXPR_BINARY_BITWISE_OR
                //! #EXPR_BINARY_BITWISE_XOR
                //! #EXPR_BINARY_BITWISE_AND
                //! #EXPR_BINARY_REL_EQ
                //! #EXPR_BINARY_REL_NE
                //! #EXPR_BINARY_REL_LT
                //! #EXPR_BINARY_REL_GT
                //! #EXPR_BINARY_REL_LE
                //! #EXPR_BINARY_REL_GE
                //! #EXPR_BINARY_SHIFT_L
                //! #EXPR_BINARY_SHIFT_R
                //! #EXPR_BINARY_PLUS
                //! #EXPR_BINARY_MINUS
                //! #EXPR_BINARY_PTR_DIFF
                //! #EXPR_BINARY_PTR_PLUS
                //! #EXPR_BINARY_PTR_MINUS
                //! #EXPR_BINARY_MULT
                //! #EXPR_BINARY_DIV
                /*!*/ struct expr* rhs_expr;

                //! #EXPR_BINARY_CAST
                /*!*/ struct data_type* rhs_dt;
            };
        } binary;
    };

    //! The `typeof` this expression when evaluated.
    //! @note
    //!     Set during semantic analysis.
    //! @todo
    //!     We may want to remove this data member from the expr struct.
    //!     This information can be calculated on the fly, but expression type
    //!     information is used by code generation, as well as for type checking
    //!     during semantic analysis.
    //!     For now this data member is being kept in, but in the future we
    //!     should look at all the uses of this data member, see if we can
    //!     eliminate some, and then consider removing the data member all
    //!     together.
    struct data_type dt;

    //! The value category of this expression.
    //! @note
    //!     Set during semantic analysis.
    //! @todo
    //!     We may want to remove this data member from the expr struct.
    //!     It may be that the value categories of an experssion are only
    //!     relevant during semantic analysis and may only be used once.
    //!     For now this data member is being kept in, but in the future we
    //!     should look at all the uses of this data member and see if there is
    //!     any use for it outside of semantic analysis.
    enum value_category
    {
        VALCAT_NONE = 0,
        VALCAT_RVALUE,
        VALCAT_LVALUE
    } valcat;

    //! #true if this expression fits the definition of `constexpr`.
    //! @note
    //!     Set during semantic analysis.
    //! @todo
    //!     We may want to remove this data member from the expr struct.
    //!     It may be that this data is only used during semantic analysis, in
    //!     which case it may be worth removing.
    bool is_constexpr;

    //! Pointer to the relevant first token the expression was parsed from.
    //! #NULL if this expression was created via a transformation.
    //! @note
    //!     For primary expressions `srctok` should be the first token of the
    //!     primary expression.
    //! @note
    //!     For function call expressions `srctok` should be the function
    //!     identifier or the first token of the expression that evaluates to
    //!     the function.
    //! @note
    //!     For unary expressions `srctok` should be the unary operator token.
    //! @note
    //!     For binary expressions `srctok` should be the binary operator token.
    struct token const* srctok;
};
INIT_FINI_COUNTER_HEADER(expr);
//! @init
void expr_init(struct expr* this);
//! @fini
void expr_fini(struct expr* this);
//! @reset
void expr_reset(struct expr* this);
//! @assign
//! @param dest
//!     Instance being assigned to.
//! @param src
//!     Instance being assigned from.
//! @param clear_srctok
//!     Sets `srctok` variables in the dest to #NULL if #true, indicating that
//!     @p dest was created via a transformation.
void expr_assign(struct expr* dest, struct expr const* src, bool clear_srctok);

bool expr_is_primary(struct expr const* expr);
bool expr_is_postfix(struct expr const* expr);
bool expr_is_unary(struct expr const* expr);
bool expr_is_binary(struct expr const* expr);

bool expr_is_lvalue(struct expr const* expr);
bool expr_is_rvalue(struct expr const* expr);

//============================================================================//
//      VARIABLE DECLARATION                                                  //
//============================================================================//
struct variable_decl
{
    //! Identifier of the declared variable.
    struct identifier id;

    //! Data type of the declared variable.
    struct data_type dt;

    //! Initial definition of the declared variable.
    //! @br
    //! If #NULL, this variable was declared as uninitialized:
    //! ```
    //! var <identifier> : <data-type> = uninit;
    //! ```
    //! @br
    //! If non-#NULL, this variable was declared with a definition:
    //! ```
    //! var <identifier> : <data-type> = <expr>;
    //! ```
    /*!*/ struct expr* def;

    //! If this variable is one of the exports from its module.
    bool is_export;
    //! If this is a pre-declaration variable using the `introduce` keyword.
    bool is_introduce;

    //! Pointer to the first token the declaration was parsed from.
    //! #NULL if this declaration was created via a transformation.
    struct token const* srctok;
};
INIT_FINI_COUNTER_HEADER(variable_decl);
//! @init
void variable_decl_init(struct variable_decl* this);
//! @fini
void variable_decl_fini(struct variable_decl* this);

//============================================================================//
//      FUNCTION DECLARATION                                                  //
//============================================================================//
struct function_decl
{
    //! Identifier of the declared function.
    struct identifier id;

    //! Data type of the declared function.
    struct data_type dt;

    //! Identifiers of each parameter in the function's #data_type.
    narr_t(struct identifier) param_ids;

    //! Definition/body of the declared function.
    //! @br
    //! If #NULL, this function was forward declared:
    //! ```
    //! func <identifier> : <data-type-with-params>;
    //! ```
    //! @br
    //! If non-#NULL, this function was declared with a definition/body:
    //! ```
    //! func <identifier> : <data-type-with-params> { <stmts> }
    //! ```
    /*!*/ struct scope* def;

    //! If this function is one of the exports from its module.
    bool is_export;
    //! If this function is a pre-declaration using the `introduce` keyword.
    bool is_introduce;

    //! Pointer to the first token the declaration was parsed from.
    //! #NULL if this declaration was created via a transformation.
    struct token const* srctok;
};
INIT_FINI_COUNTER_HEADER(function_decl);
//! @init
void function_decl_init(struct function_decl* this);
//! @fini
void function_decl_fini(struct function_decl* this);

//============================================================================//
//      ENUM DECLARATION                                                      //
//============================================================================//

//! The declaration of an enum. this can be either a pre-declaration,
//! contain the full definition.
struct enum_decl
{
    //! The name of this enum
    struct identifier id;

    //! if this enum is exported or not.
    bool is_export;
    //! if this enum is a pre-declaration using the `introduce` keyword.
    bool is_introduce;

    //! Definition of the enum.
    //! #NULL if its a forward declaration. Otherwise its an actual declaration
    /*!*/ struct enum_def* def;

    //! Pointer to the token where it starts.
    //! #NULL if this was generated.
    struct token const* srctok;
};
INIT_FINI_COUNTER_HEADER(enum_decl);

//! @init
void enum_decl_init(struct enum_decl* edec);
//! @fini
void enum_decl_fini(struct enum_decl* edec);

//============================================================================//
//      UNION DECLARATION                                                     //
//============================================================================//

//! The declaration of a union. Either pre-declaration or with definition.
struct union_decl
{
    //! The unions name
    struct identifier id;

    //! If this union is exported from the module
    bool is_export;
    //! if this union is a pre-declaration using the `introduce` keyword.
    bool is_introduce;

    //! Definition of the union.
    //! #NULL if its a forward declaration. Otherwise its an actual declaration
    /*!*/ struct union_def* def;

    //! Pointer to the token where it starts.
    //! #NULL if this was generated during semantic analysis.
    struct token const* srctok;
};
INIT_FINI_COUNTER_HEADER(union_decl);

//! @init
void union_decl_init(struct union_decl* udec);
//! @fini
void union_decl_fini(struct union_decl* udec);

//============================================================================//
//      STRUCT DECLARATION                                                    //
//============================================================================//
struct struct_decl
{
    struct identifier id;

    //! Definition of the declared struct.
    //! @br
    //! If #NULL, this struct was forward declared:
    //! ```
    //! struct <identifier>;
    //! ```
    //! @br
    //! If non-#NULL, this struct was declared with a definition:
    //! ```
    //! struct <identifier> <struct-definition>
    //! ```
    /*!*/ struct struct_def* def;

    //! If this struct is an export of the module.
    bool is_export;
    //! If this struct is a pre-declaration using the `introduce` keyword.
    bool is_introduce;

    //! Pointer to the first token the declaration was parsed from.
    //! #NULL if this declaration was created via a transformation.
    struct token const* srctok;
};
INIT_FINI_COUNTER_HEADER(struct_decl);
//! @init
void struct_decl_init(struct struct_decl* this);
//! @fini
void struct_decl_fini(struct struct_decl* this);

//============================================================================//
//      VARIANT DECLARATION                                                   //
//============================================================================//

//! The declaration of a variant. Either pre-declaration, or with definition.
struct variant_decl
{
    //! the name of this variant.
    struct identifier id;

    //! if this variant is exported from the module.
    bool is_export;
    //! if this variant is a pre-declaration using the `introduce` keyword.
    bool is_introduce;

    //! Definition of the variant.
    //! #NULL if its a forward declaration. Otherwise its an actual declaration
    /*!*/ struct variant_def* def;

    //! Pointer to the token where it starts.
    //! #NULL if this was generated during semantic analysis
    struct token const* srctok;
};
INIT_FINI_COUNTER_HEADER(variant_decl);

//! @init
void variant_decl_init(struct variant_decl* vdec);
//! @fini
void variant_decl_fini(struct variant_decl* vdec);

//============================================================================//
//      IF STATEMENT                                                          //
//============================================================================//
struct if_stmt
{
    //! Conditional expressions for each `if` and `elif` statement.
    //! @note
    //!     if_conditions[0] is the original `if` conditional.
    //!     if_conditions[1..n-1] are `elif` conditionals after the `if`.
    /*!*/ narr_t(struct expr) if_conditions;
    //! Conditional expressions for each `if` and `elif` statement.
    //! @note
    //!     if_bodies[0] is the original `if` body.
    //!     if_bodies[1..n-1] are `elif` bodies after the `if`.
    /*!*/ narr_t(struct scope) if_blocks;

    //! Body of the final else statement.
    //! `else_body == NULL` indicates no else statement was present.
    //! @todo
    //!     In the future it may be useful to transform this variable into
    //!     the condition `elif true { STATEMENTS }` so that this struct
    //!     may be represented by two variables.
    //!     Although this would no longer create a lossless conversion of a
    //!     the parsed file to the NIR, it may be simpler.
    /*!*/ struct scope* else_block;
};
INIT_FINI_COUNTER_HEADER(if_stmt);
//! @init
void if_stmt_init(struct if_stmt* this);
//! @fini
void if_stmt_fini(struct if_stmt* this);

//============================================================================//
//      LOOP STATEMENT                                                        //
//============================================================================//
//! @todo
//!     In the future it may be useful to reduce loop statements into one struct
//!     rather than a variant of conditional and range loops.
struct loop_stmt
{
    enum
    {
        LOOP_UNSET = 0,

        LOOP_CONDITIONAL,
        LOOP_RANGE
    } tag;

    union
    {
        //! #LOOP_CONDITIONAL
        struct
        {
            /*!*/ struct expr* expr;
        } conditional;

        //! #LOOP_RANGE
        struct
        {
            //! @note
            //!     This variable declaration is parsed using loop variable
            //!     declaration syntax.
            //!     Analysis via #semantic_analysis_variable_decl is expected
            //!     however.
            struct variable_decl variable_decl;

#define LOWER_INCLUSIVE '['
#define LOWER_EXCLUSIVE '('
            //! `[` : inclusive
            //! @br
            //! `(` : exclusive
            char lower_clusivity;

#define UPPER_INCLUSIVE ']'
#define UPPER_EXCLUSIVE ')'
            //! `]` : inclusive
            //! @br
            //! `)` : exclusive
            char upper_clusivity;
            /*!*/ struct expr* lower_bound;
            /*!*/ struct expr* upper_bound;
        } range;
    };

    /*!*/ struct scope* block;

    //! Pointer to the first token the loop was parsed from.
    //! #NULL if this loop was created via a transformation.
    struct token const* srctok;
};
INIT_FINI_COUNTER_HEADER(loop_stmt);
//! @init
void loop_stmt_init(struct loop_stmt* this);
//! @fini
void loop_stmt_fini(struct loop_stmt* this);

//============================================================================//
//      ALIAS STATEMENT                                                       //
//============================================================================//
struct alias_stmt
{
    struct identifier new_id;
    struct data_type dt;

    bool is_export;

    //! Pointer to the first token the alias was parsed from.
    //! #NULL if this alias was created via a transformation.
    struct token const* srctok;
};
INIT_FINI_COUNTER_HEADER(alias_stmt);
//! @init
void alias_stmt_init(struct alias_stmt* this);
//! @fini
void alias_stmt_fini(struct alias_stmt* this);

//============================================================================//
//      STATEMENT                                                             //
//============================================================================//
struct stmt
{
    enum
    {
        STMT_UNSET = 0,

        STMT_IMPORT,
        STMT_ALIAS,
        STMT_VARIABLE_DECLARATION,
        STMT_FUNCTION_DECLARATION,
        STMT_ENUM_DECLARATION,
        STMT_UNION_DECLARATION,
        STMT_STRUCT_DECLARATION,
        STMT_VARIANT_DECLARATION,
        STMT_IF,
        STMT_LOOP,
        STMT_DEFER,
        STMT_BREAK,
        STMT_CONTINUE,
        STMT_RETURN,
        STMT_EXPR,
        STMT_BLOCK,
        STMT_EMPTY
    } tag;

    union
    {
        //! #STMT_IMPORT
        nstr_t import_string;

        //! #STMT_ALIAS
        /*!*/ struct alias_stmt* alias_stmt;

        //! #STMT_VARIABLE_DECLARATION
        /*!*/ struct variable_decl* variable_decl;

        //! #STMT_FUNCTION_DECLARATION
        /*!*/ struct function_decl* function_decl;

        //! #STMT_ENUM_DECLARATION
        /*!*/ struct enum_decl* enum_decl;

        //! #STMT_UNION_DECLARATION
        /*!*/ struct union_decl* union_decl;

        //! #STMT_STRUCT_DECLARATION
        /*!*/ struct struct_decl* struct_decl;

        //! #STMT_VARIANT_DECLARATION
        /*!*/ struct variant_decl* variant_decl;

        //! #STMT_IF
        /*!*/ struct if_stmt* if_stmt;

        //! #STMT_LOOP
        /*!*/ struct loop_stmt* loop_stmt;

        //! #STMT_DEFER
        struct
        {
            //! The idx'th defer statement in the scope that this statement
            //! is within.
            //! This data member is used for keeping track of how to execute
            //! defer statements within a C program.
            //! @todo
            //!     This data member is only required for code generation, and
            //!     has only two uses (at the time of writing this comment).
            //!     In the future it should be removed.
            size_t idx;

            /*!*/ struct scope* block;
        } defer_stmt;

        //! #STMT_RETURN
        //! @note
        //!     `return_expr == NULL` indicates the return statement does not
        //!     have a return expression.
        /*!*/ struct expr* return_expr;

        //! #STMT_EXPR
        /*!*/ struct expr* expr;

        //! #STMT_BLOCK
        /*!*/ struct scope* block;
    };

    //! Pointer to the first token the statement was parsed from.
    //! #NULL if this statement was created via a transformation.
    struct token const* srctok;
};
INIT_FINI_COUNTER_HEADER(stmt);
//! @init
void stmt_init(struct stmt* this);
//! @fini
void stmt_fini(struct stmt* this);

//============================================================================//
//      LEXICAL SCOPE                                                         //
//============================================================================//
//====================================//
//      SYMBOL                        //
//====================================//
struct symbol
{
    enum
    {
        SYMBOL_UNSET = 0,

        SYMBOL_VARIABLE,
        SYMBOL_FUNCTION,
        SYMBOL_ENUM,
        SYMBOL_UNION,
        SYMBOL_STRUCT,
        SYMBOL_VARIANT,
        SYMBOL_ALIAS
    } tag;

    union
    {
        //! #SYMBOL_VARIABLE
        struct variable_decl* variable_decl;

        //! #SYMBOL_FUNCTION
        struct function_decl* function_decl;

        //! #SYMBOL_ENUM
        struct enum_decl* enum_decl;

        //! #SYMBOL_UNION
        struct union_decl* union_decl;

        //! #SYMBOL_STRUCT
        struct struct_decl* struct_decl;

        //! #SYMBOL_VARIANT
        struct variant_decl* variant_decl;

        //! #SYMBOL_ALIAS
        struct alias_stmt* alias;
    } ref;

    //! Reference to the symbol's identifier.
    //! For #SYMBOL_VARIABLE, #SYMBOL_FUNCTION, and #SYMBOL_STRUCT this is the
    //! identifier of the symbol introduced via a declaration.
    //! For #SYMBOL_ALIAS this is the identifier of the new symbol created as
    //! an alias for the older symbol.
    struct identifier* id;

    //! Reference to the symbol's data type.
    //! For #SYMBOL_VARIABLE this is the data type of the variable.
    //! For #SYMBOL_FUNCTION this is the data type of the function's signature.
    //! For #SYMBOL_STRUCT this is the data type of the struct.
    //! For #SYMBOL_ALIAS this is the data type being aliased.
    //! Ownership:
    //! + `SYMBOL_STRUCT == tag`
    //! + `SYMBOL_VARIANT == tag`
    /*?*/ struct data_type* dt;

    //! Reference to the token that this symbol was created from.
    struct token const* srctok;

    //! Module in the #module_cache that this symbol belongs to.
    size_t module_idx;

    //! Number of times that this symbol referred to by identifier.
    uint32_t uses;

    bool is_export;
    bool is_defined;
};
INIT_FINI_COUNTER_HEADER(symbol);
//! @init
void symbol_init(struct symbol* this);
//! @init
void symbol_init_variable(
    struct symbol* this,
    struct variable_decl* variable_decl);
//! @init
void symbol_init_function(
    struct symbol* this,
    struct function_decl* function_decl);
//! @init
void symbol_init_enum(struct symbol* this, struct enum_decl* enum_decl);
//! @init
void symbol_init_union(struct symbol* this, struct union_decl* union_decl);
//! @init
void symbol_init_struct(struct symbol* this, struct struct_decl* struct_decl);
//! @init
void symbol_init_variant(
    struct symbol* this,
    struct variant_decl* variant_decl);
//! @init
void symbol_init_alias(struct symbol* this, struct alias_stmt* alias);
//! @fini
void symbol_fini(struct symbol* this);

void symbol_mark_use(struct symbol* this);

//====================================//
//      SYMBOL TABLE                  //
//====================================//
struct _stdict_slot
{
    struct symbol symbol;
    bool in_use;
    uint32_t hash;
};

struct symbol_table
{
    //! Insert-only dictionary containing all symbols in this symbol table.
    struct _stdict_slot* _dict;
    //! Index into the internal table of primes used for calculating the size of
    //! the symbol table.
    uint32_t _dict_prime_idx;
    //! Number of slots in use in the symbol table's dictionary.
    uint32_t _dict_slots_in_use;

    //! Pointer to the most recent symbol in this symbol table.
    //! This is a pointer to dynamic memory and should be considered invalid
    //! after any other addition to the symbol table.
    //! @br
    //! #NULL if no symbols have been added to the symbol table.
    struct symbol* most_recent_symbol;

    //! Module in the #module_cache that this symbol table belongs to.
    size_t module_idx;
};
INIT_FINI_COUNTER_HEADER(symbol_table);
//! @init
void symbol_table_init(struct symbol_table* symbol_table);
//! @fini
void symbol_table_fini(struct symbol_table* symbol_table);

//! Insert the provided symbol to the symbol table.
//! @return
//!     Pointer to the added symbol on success.
//!     This is a pointer to dynamic memory and should be considered invalid
//!     after any other addition to the symbol table.
//! @return
//!     #NULL on failure.
struct symbol* symbol_table_insert(
    struct symbol_table* symbol_table,
    struct symbol const* symbol);

//! @return
//!     Pointer to the symbol table symbol if a symbol with the provided symbol
//!     string exists in the table.
//!     This is a pointer to dynamic memory and should be considered invalid
//!     after any other addition to the symbol table.
//! @return
//!     #NULL if an symbol does not exist in the table.
struct symbol* symbol_table_find(
    struct symbol_table* symbol_table,
    char const* symstr,
    size_t symstr_length);

//====================================//
//      SCOPE                         //
//====================================//
#define GLOBAL_SCOPE_PARENT_PTR ((struct scope const*)NULL)
//! Default value for the parent pointer that can be set during syntax analysis
//! before it is later updated during semantic analysis.
//! Since this can be a valid address of a pointer this macro should not be
//! used for a comparison, and should instead only be used during debugging.
#define UNDEFINED_SCOPE_PARENT ((struct scope const*)0xBAD)
struct scope
{
    //! An integer unique to the module containing this scope.
    //! This integer is used for generating unique variable and label names
    //! during code generation.
    //! @todo
    //!     In the future we should consider removing this data member from the
    //!     the struct if it is possible or useful to store this information
    //!     outside of the struct.
    uint32_t uid;

    //! @todo
    //!     Move these variables outside of the actual scope struct.
    //!{

    //! The immediate scope that this scope is contained within.
    //! If #GLOBAL_SCOPE_PARENT_PTR then this scope is the global scope.
    struct scope const* parent;
    //! The scope directly under the closest defer statement that this scope is
    //! a part of.
    //! #NULL if this scope is not within a defer statement.
    struct scope const* defer_scope;
    //! The scope directly under the closest loop statement that this scope is
    //! a part of.
    //! #NULL if this scope is not within a loop statement.
    struct scope const* loop_scope;

    //!}

    //! The symbol table for this scope.
    //! @todo
    //!     In the future we should consider removing this data member from the
    //!     the struct if it is possible or useful to store this information
    //!     outside of the struct.
    /*!*/ struct symbol_table* symbol_table;

    //! In-order list of statements within this scope.
    /*!*/ narr_t(struct stmt) stmts;

    //! List of pointers to deferred scopes.
    //! When a scope is processed during semantic analysis, defer statements
    //! will push the pointer to each statement's deferred scope onto the back
    //! of this list.
    //! The pointers in this list do NOT need to be freed, as they are owned
    //! by their respective statements.
    //! @todo
    //!     In the future we should consider removing this data member from the
    //!     the struct if it is possible or useful to store this information
    //!     outside of the struct.
    /*!*/ narr_t(struct scope*) deferred_scopes;

    //! Pointer to the first token the scope was parsed from.
    //! #NULL if this scope was created via a transformation.
    struct token const* srctok;
};
INIT_FINI_COUNTER_HEADER(scope);
//! @init
void scope_init(struct scope* this, struct scope const* parent_scope);
//! @fini
void scope_fini(struct scope* this);

//! @return
//!     #true if @p scope is the global scope.
bool scope_is_global(struct scope const* this);

#define NO_ENTRY_FUNC 0
#define INVALID_ENTRY_FUNC 1
#define ENTRY_FUNC_NO_ARGS_RTN_VOID 2
#define ENTRY_FUNC_NO_ARGS_RTN_U 3
#define ENTRY_FUNC_NO_ARGS_RTN_S 4
#define ENTRY_FUNC_ARGS_RTN_VOID 5
#define ENTRY_FUNC_ARGS_RTN_U 6
#define ENTRY_FUNC_ARGS_RTN_S 7
//! Check if the scope contains one of the valid declarations of `entry`, the
//! functions defining the entry point to a program.
//! @br
//! Valid declarations of entry are:
//! * `func entry : () -> void`
//! * `func entry : () -> u`
//! * `func entry : () -> s`
//! * `func entry : (argc : u, argv : **char) -> void`
//! * `func entry : (argc : u, argv : **char) -> u`
//! * `func entry : (argc : u, argv : **char) -> s`
//! @return
//!     #NO_ENTRY_FUNC if the scope does not contain a declaration of `entry`.
//! @return
//!     #INVALID_ENTRY_FUNC if the scope contains a declaration of `entry` that
//!     does not match one the valid declarations of `entry`.
//! @return
//!     #ENTRY_FUNC_NO_ARGS_RTN_VOID if the scope contains a declaration of
//!     entry with the type `() -> void`.
//! @return
//!     #ENTRY_FUNC_NO_ARGS_RTN_U if the scope contains a declaration of
//!     entry with the type `() -> u`.
//! @return
//!     #ENTRY_FUNC_NO_ARGS_RTN_S if the scope contains a declaration of
//!     entry with the type `() -> s`.
//! @return
//!     #ENTRY_FUNC_ARGS_RTN_VOID if the scope contains a declaration of
//!     entry with the type `(argc : u, argv : ** char) -> void`.
//! @return
//!     #ENTRY_FUNC_ARGS_RTN_U if the scope contains a declaration of
//!     entry with the type `(argc : u, argv : ** char) -> u`.
//! @return
//!     #ENTRY_FUNC_ARGS_RTN_S if the scope contains a declaration of
//!     entry with the type `(argc : u, argv : ** char) -> s`.
int scope_contains_entry_func(struct scope* scope);

//============================================================================//
//      MODULE                                                                //
//============================================================================//
struct module
{
    //! Relative file path of the module source.
    nstr_t src_path;

    //! File path of the compiled output for this module.
    nstr_t out_path;

    //! Contents of the #src_path.
    nstr_t src;

    //! Tokenization of #src.
    /*!*/ narr_t(struct token) tokens;

    //! The global scope of this module.
    struct scope global_scope;

    //! List of each #identifier for symbols exported from the global #scope of
    //! this module.
    /*!*/ narr_t(struct identifier) exports;

    //! Used for generating unique identifiers within this module.
    //! These integer identifiers are used for generating unique variable and
    //! label names within the module.
    uint32_t uid_counter;
};
INIT_FINI_COUNTER_HEADER(module);
//! @init
void module_init(struct module* module);
//! @fini
void module_fini(struct module* module);
//! Get a new uid from the module and increment the module's uid counter.
uint32_t module_generate_uid(struct module* module);
//! Adds the index of the most recently added global symbol from the global
//! symbol table of @p module to the list of exported symbol indices of @p
//! module.
void module_add_latest_global_symbol_to_exports(struct module* module);

//! Detect whether the export of a module came from an import.
bool is_import(struct module* module, char const* symstr, size_t symstr_length);

//! Resolve the path of a module.
//! @param path
//!     User provided path of the module. #resolve_module_path will modify @p
//!     path to contain the absolute path of the module on success.
//! @param relto
//!     If the module cannot be located in the include path, the module will
//!     attempt to be located relative to path @p relto.
//! @return
//!     #STBOOL_SUCCESS if a readable file can be located.
//! @return
//!     #STBOOL_FAILURE otherwise.
stbool resolve_module_path(nstr_t* path, char const* relto);

#define MODULE_CACHE_MAX_LEN 4096
//! Global list of modules that have been or are in the process of being
//! compiled.
struct module module_cache[MODULE_CACHE_MAX_LEN];
void module_cache_init(void);
void module_cache_fini(void);

//! Number of modules within the #module_cache.
extern size_t module_cache_len;

//! Index in the #module_cache of the current module being compiled.
//! Updated upon entrance to #compile.
//! This variable will NOT be automatically updated upon exiting #compile.
extern size_t current_module_idx;
