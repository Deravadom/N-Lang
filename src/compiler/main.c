/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "config.h"
#include "compile.h"

__attribute__((constructor))
void nc_constructor(void)
{
    install_generic_sighandler();
}

int main(int argc, char** argv)
{
    if (1 == argc)
    {
        print_usage();
        return EXIT_FAILURE;
    }
    if (!prod_config(argc, argv))
    {
        return EXIT_FAILURE;
    }
    if (!proc_config())
    {
        return EXIT_FAILURE;
    }

    log_config = (LOG_DEBUG * config.is_debug_mode)
        | (LOG_METRIC * config.is_metric_mode)
        | LOG_INFO
        | LOG_WARNING
        | LOG_ERROR
        | LOG_FATAL;

    struct module input_module;
    module_init(&input_module);

    if (COMPILE_SUCCESS != compilep(config.input_file, &input_module))
    {
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
