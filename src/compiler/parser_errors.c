/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file parser_errors.c
 * @brief Expressive error diagnostics
 */

#include "parser_errors.h"

// Disable clang-format so we can clearly see the line-by-line output of each
// form message, even if it overflows the 80 character terminal limit for the
// C source.
// clang-format off
static void log_form_of_loop_stmt(void)
{
    nlogf(
        LOG_INFO,
        "Loop statements are of the following form:\n"
        "loop i : mut u in [0u, 5u) { STATEMENTS };\n"
        "loop EXPRESSION { STATEMENTS };");
}

static void log_form_of_if_stmt(void)
{
    nlogf(
        LOG_INFO,
        "If statements are of the following form:\n"
        "if EXPRESSION { STATEMENTS }\n"
        "    elif EXPRESSION { STATEMENTS }\n"
        "    else { STATEMENTS }");
}

static void log_form_of_import_stmt(void)
{
    nlogf(
        LOG_INFO,
        "Imports are of the following form:\n"
        "import \"std/io.n\"a;");
}

static void log_form_of_alias_stmt(void)
{
    nlogf(
        LOG_INFO,
        "Aliases are of the following form:\n"
        "[export] alias NEW_TYPE_NAME = OLD_TYPE_NAME;");
}

static void log_form_of_global_variable_decl(void)
{
    nlogf(
        LOG_INFO,
        "Global variable declarations are of the following form:\n"
        "[export] let NAME : TYPE = CONSTANT_EXPRESSION;\n"
        "[export] let NAME : TYPE;");
    nlogf(
        LOG_INFO,
        "Global variable declarations may be declared with external storage:\n"
        "[export] introduce let identifier : TYPE;");
}

static void log_form_of_block_variable_decl(void)
{
    nlogf(
        LOG_INFO,
        "Variable declarations are of the following form:\n"
        "let NAME : TYPE = EXPRESSION;\n"
        "let NAME : TYPE;");
}

static void log_form_of_function_decl(void)
{
    nlogf(
        LOG_INFO,
        "Functions are of the following form:\n"
        "func NAME : (ARG1 : TYPE1, ARG2 : TYPE2) -> RETURN_TYPE { STATEMENTS }");
    nlogf(
        LOG_INFO,
        "Functions may be forward declared:\n"
        "introduce func NAME : (ARG1 : TYPE1, ARG2 : TYPE2) -> RETURN_TYPE;");
}

static void log_form_of_function_args(void)
{
    nlogf(
        LOG_INFO,
        "Functions arguments are of the following form:\n"
        "(ARG1 : TYPE1, ARG2 : TYPE2)");
}

static void log_form_of_struct_decl(void)
{
    nlogf(
        LOG_INFO,
        "Struct declarations are of the following form:\n"
        "[export] struct NAME { MEMBERS }");
    nlogf(
        LOG_INFO,
        "Struct declarations may be forward declared:\n"
        "[export] introduce struct NAME;");
}

static void log_form_of_struct_members(void)
{
    nlogf(
        LOG_INFO,
        "Struct members are of the following form:\n"
        "let MEMBER_VARIABLE_NAME : TYPE;\n"
        "func MEMBER_FUNCTION_NAME = OTHER_FUNCTION_NAME;");
}

static void log_form_of_isolate_stmt(void)
{
    nlogf(
        LOG_INFO,
        "Isolate blocks are of the following form:\n"
        "isolate { STATEMENTS }");
}

static void log_form_of_assignment_stmt(void)
{
    nlogf(
        LOG_INFO,
        "Assignment statements are of the following form:\n"
        "VARIABLE = EXPRESSION;");
}

static void log_form_of_defer_stmt(void)
{
    nlogf(
        LOG_INFO,
        "Defer blocks are of the following form:\n"
        "defer { STATEMENTS }");
}
// clang-format on

void handle_error(
    struct earley_set* items,
    size_t o,
    struct first_set first_set[NUMBER_OF_NONTERMINALS],
    struct module* module)
{
    // Incorrect loop keyword
    if (module->tokens[o - 1 - 1].length == 3
        && cstr_n_eq(
               "for",
               module->tokens[o - 1 - 1].start,
               module->tokens[o - 1 - 1].length))
    {
        nlogf_pretty(
            LOG_ERROR,
            module->src,
            module->src_path.data,
            module->tokens[o - 1 - 1].line,
            module->tokens[o - 1 - 1].column,
            "Did you mean to use the %s keyword?",
            EMPHASIZE("loop"));
        log_form_of_loop_stmt();
        return;
    }

    // Missing semicolon
    for (size_t i = 0; i < items[o - 1].size; ++i)
    {
        struct parser_earley_item r = items[o - 1].data[i];

        struct parser_symbol sym =
            parser_grammar[r.rule_index].symbol_list[r.next_item];

        if (TERMINAL == sym.tag && TOKEN_OPERATOR_SEMICOLON == sym.terminal
            && (r.rule_index == RULE_BLOCKSTATEMENT_VARIABLE_DECLARATION_EMPTY
                || r.rule_index
                    == RULE_BLOCKSTATEMENT_VARIABLE_DECLARATION_ASSIGNMENT
                || r.rule_index == RULE_BLOCKSTATEMENT_VARIABLE_ASSIGNMENT
                || r.rule_index == RULE_BLOCKSTATEMENT_BREAK
                || r.rule_index == RULE_BLOCKSTATEMENT_CONTINUE
                || r.rule_index == RULE_BLOCKSTATEMENT_RETURN_NONEMPTY
                || r.rule_index == RULE_BLOCKSTATEMENT_RETURN_EMPTY
                || r.rule_index == RULE_GLOBALSTATEMENT_IMPORT
                || r.rule_index == RULE_GLOBALSTATEMENT_ALIAS
                || r.rule_index
                    == RULE_GLOBALSTATEMENT_VARIABLE_DECLARATION_INTRODUCE
                || r.rule_index
                    == RULE_GLOBALSTATEMENT_VARIABLE_DECLARATION_ASSIGNMENT
                || r.rule_index
                    == RULE_GLOBALSTATEMENT_VARIABLE_DECLARATION_EMPTY
                || r.rule_index == RULE_GLOBALSTATEMENT_STRUCT_DECLARATION
                || r.rule_index == RULE_GLOBALSTATEMENT_FUNCTION_DECLARATION))
        {
            nlogf_pretty(
                LOG_ERROR,
                module->src,
                module->src_path.data,
                module->tokens[o - 1 - 1].line,
                module->tokens[o - 1 - 1].column
                    + module->tokens[o - 1 - 1].length,
                "Missing semicolon.");
            return;
        }
    }

    // Unexpected ';' after '}'
    if (module->tokens[o - 1 - 1].tag == TOKEN_OPERATOR_RIGHTBRACE
        && module->tokens[o - 1].tag == TOKEN_OPERATOR_SEMICOLON)
    {
        nlogf_pretty(
            LOG_ERROR,
            module->src,
            module->src_path.data,
            module->tokens[o - 1].line,
            module->tokens[o - 1].column,
            "Unexpected %s after %s.",
            EMPHASIZE(";"),
            EMPHASIZE("}"));

        return;
    }

    // Else If
    if (module->tokens[o - 1 - 1].tag == TOKEN_KEYWORD_ELSE
        && module->tokens[o - 1].tag == TOKEN_KEYWORD_IF)
    {
        nlogf_pretty(
            LOG_ERROR,
            module->src,
            module->src_path.data,
            module->tokens[o - 1 - 1].line,
            module->tokens[o - 1 - 1].column + module->tokens[o - 1 - 1].length,
            "Did you mean to use the %s keyword?",
            EMPHASIZE("elif"));
        log_form_of_if_stmt();
        return;
    }

    // Missing 'expression' before ';' in Variable Declaration
    for (size_t i = 0; i < items[o - 1].size; ++i)
    {
        struct parser_earley_item r = items[o - 1].data[i];

        struct parser_symbol sym =
            parser_grammar[r.rule_index].symbol_list[r.next_item];

        if (NON_TERMINAL == sym.tag && NONTERMINAL_EXPRESSION == sym.nonterminal
            && module->tokens[o - 1].tag == TOKEN_OPERATOR_SEMICOLON
            && (r.rule_index
                    == RULE_GLOBALSTATEMENT_VARIABLE_DECLARATION_ASSIGNMENT
                || r.rule_index
                    == RULE_BLOCKSTATEMENT_VARIABLE_DECLARATION_ASSIGNMENT
                || r.rule_index == RULE_BLOCKSTATEMENT_VARIABLE_ASSIGNMENT))
        {
            nlogf_pretty(
                LOG_ERROR,
                module->src,
                module->src_path.data,
                module->tokens[o - 1 - 1].line,
                module->tokens[o - 1 - 1].column
                    + module->tokens[o - 1 - 1].length,
                "Expected an expression before ';'.");
            return;
        }
    }

    // Missing ': type' before '=' in Variable Declaration
    for (size_t i = 0; i < items[o - 1].size; ++i)
    {
        struct parser_earley_item r = items[o - 1].data[i];

        struct parser_symbol sym =
            parser_grammar[r.rule_index].symbol_list[r.next_item];

        if (TERMINAL == sym.tag && TOKEN_OPERATOR_COLON == sym.terminal
            && module->tokens[o - 1].tag == TOKEN_OPERATOR_EQUAL
            && (r.rule_index
                    == RULE_GLOBALSTATEMENT_VARIABLE_DECLARATION_ASSIGNMENT
                || r.rule_index
                    == RULE_BLOCKSTATEMENT_VARIABLE_DECLARATION_ASSIGNMENT
                || r.rule_index
                    == RULE_GLOBALSTATEMENT_VARIABLE_DECLARATION_EMPTY
                || r.rule_index
                    == RULE_BLOCKSTATEMENT_VARIABLE_DECLARATION_EMPTY))
        {
            nlogf_pretty(
                LOG_ERROR,
                module->src,
                module->src_path.data,
                module->tokens[o - 1 - 1].line,
                module->tokens[o - 1 - 1].column
                    + module->tokens[o - 1 - 1].length,
                "Expected %s before %s.",
                EMPHASIZE(": TYPE"),
                EMPHASIZE("="));
            return;
        }
    }

    // Missing ': type' before ';' in Introduce Variable Declaration
    for (size_t i = 0; i < items[o - 1].size; ++i)
    {
        struct parser_earley_item r = items[o - 1].data[i];

        struct parser_symbol sym =
            parser_grammar[r.rule_index].symbol_list[r.next_item];

        if (TERMINAL == sym.tag && TOKEN_OPERATOR_COLON == sym.terminal
            && module->tokens[o - 1].tag == TOKEN_OPERATOR_SEMICOLON
            && (r.rule_index
                == RULE_GLOBALSTATEMENT_VARIABLE_DECLARATION_INTRODUCE))
        {
            nlogf_pretty(
                LOG_ERROR,
                module->src,
                module->src_path.data,
                module->tokens[o - 1 - 1].line,
                module->tokens[o - 1 - 1].column
                    + module->tokens[o - 1 - 1].length,
                "Expected %s before %s.",
                EMPHASIZE(": TYPE"),
                EMPHASIZE(";"));
            return;
        }
    }

    // Missing ': type' before '(' in Function Declaration
    for (size_t i = 0; i < items[o - 1].size; ++i)
    {
        struct parser_earley_item r = items[o - 1].data[i];

        struct parser_symbol sym =
            parser_grammar[r.rule_index].symbol_list[r.next_item];

        if (TERMINAL == sym.tag && TOKEN_OPERATOR_COLON == sym.terminal
            && module->tokens[o - 1].tag == TOKEN_OPERATOR_LEFTPARENTHESIS
            && (r.rule_index == RULE_GLOBALSTATEMENT_FUNCTION_DECLARATION
                || r.rule_index
                    == RULE_GLOBALSTATEMENT_FUNCTION_DECLARATION_ASSIGNMENT))
        {
            nlogf_pretty(
                LOG_ERROR,
                module->src,
                module->src_path.data,
                module->tokens[o - 1 - 1].line,
                module->tokens[o - 1 - 1].column
                    + module->tokens[o - 1 - 1].length,
                "Expected %s before %s.",
                EMPHASIZE(": TYPE"),
                EMPHASIZE("("));
            return;
        }
    }

    // Missing '-> type' before ';' in Function Declaration
    for (size_t i = 0; i < items[o - 1].size; ++i)
    {
        struct parser_earley_item r = items[o - 1].data[i];

        struct parser_symbol sym =
            parser_grammar[r.rule_index].symbol_list[r.next_item];

        if (TERMINAL == sym.tag
            && TOKEN_OPERATOR_DASH_GREATERTHAN == sym.terminal
            && module->tokens[o - 1].tag == TOKEN_OPERATOR_SEMICOLON
            && (r.rule_index == RULE_GLOBALSTATEMENT_FUNCTION_DECLARATION))
        {
            nlogf_pretty(
                LOG_ERROR,
                module->src,
                module->src_path.data,
                module->tokens[o - 1 - 1].line,
                module->tokens[o - 1 - 1].column
                    + module->tokens[o - 1 - 1].length,
                "Expected %s before %s.",
                EMPHASIZE("-> TYPE"),
                EMPHASIZE(";"));
            return;
        }
    }

    // Missing '-> type' before ';' in Function Declaration
    for (size_t i = 0; i < items[o - 1].size; ++i)
    {
        struct parser_earley_item r = items[o - 1].data[i];

        struct parser_symbol sym =
            parser_grammar[r.rule_index].symbol_list[r.next_item];

        if (TERMINAL == sym.tag
            && TOKEN_OPERATOR_DASH_GREATERTHAN == sym.terminal
            && module->tokens[o - 1].tag == TOKEN_OPERATOR_LEFTBRACE
            && (r.rule_index
                == RULE_GLOBALSTATEMENT_FUNCTION_DECLARATION_ASSIGNMENT))
        {
            nlogf_pretty(
                LOG_ERROR,
                module->src,
                module->src_path.data,
                module->tokens[o - 1 - 1].line,
                module->tokens[o - 1 - 1].column
                    + module->tokens[o - 1 - 1].length,
                "Expected %s before %s.",
                EMPHASIZE("-> TYPE"),
                EMPHASIZE(";"));
            return;
        }
    }

    // Struct cannot be empty
    for (size_t i = 0; i < items[o - 1].size; ++i)
    {
        struct parser_earley_item r = items[o - 1].data[i];

        struct parser_symbol sym =
            parser_grammar[r.rule_index].symbol_list[r.next_item];

        SUPPRESS_UNUSED(sym);

        if (module->tokens[o - 1 - 1].tag == TOKEN_OPERATOR_LEFTBRACE
            && module->tokens[o - 1].tag == TOKEN_OPERATOR_RIGHTBRACE
            && (r.rule_index
                == RULE_GLOBALSTATEMENT_STRUCT_DECLARATION_ASSIGNMENT))
        {
            nlogf_pretty(
                LOG_ERROR,
                module->src,
                module->src_path.data,
                module->tokens[o - 1 - 1].line,
                module->tokens[o - 1 - 1].column
                    + module->tokens[o - 1 - 1].length,
                "Struct cannot be empty.");
            return;
        }
    }

    // Missing conditional 'expression' in If Statement
    for (size_t i = 0; i < items[o - 1].size; ++i)
    {
        struct parser_earley_item r = items[o - 1].data[i];

        struct parser_symbol sym =
            parser_grammar[r.rule_index].symbol_list[r.next_item];

        if (sym.tag == NON_TERMINAL && sym.nonterminal == NONTERMINAL_IFBRANCH
            && (module->tokens[o - 1 - 1].tag == TOKEN_KEYWORD_IF
                || module->tokens[o - 1 - 1].tag == TOKEN_KEYWORD_ELIF))
        {
            nlogf_pretty(
                LOG_ERROR,
                module->src,
                module->src_path.data,
                module->tokens[o - 1 - 1].line,
                module->tokens[o - 1 - 1].column
                    + module->tokens[o - 1 - 1].length,
                "Expected %s in if statement.",
                EMPHASIZE("expression"));
            log_form_of_if_stmt();
            return;
        }
    }

    // Missing braces in If statement
    for (size_t i = 0; i < items[o - 1].size; ++i)
    {
        struct parser_earley_item r = items[o - 1].data[i];

        struct parser_symbol sym =
            parser_grammar[r.rule_index].symbol_list[r.next_item];

        if (sym.tag == TERMINAL && sym.terminal == TOKEN_OPERATOR_LEFTBRACE
            && r.rule_index == RULE_IFBRANCH)
        {
            nlogf_pretty(
                LOG_ERROR,
                module->src,
                module->src_path.data,
                module->tokens[o - 1 - 1].line,
                module->tokens[o - 1 - 1].column
                    + module->tokens[o - 1 - 1].length,
                "Must use braces for if statement.");
            log_form_of_if_stmt();
            return;
        }
    }

    // Missing ': type' in range loop statement
    for (size_t i = 0; i < items[o - 1].size; ++i)
    {
        struct parser_earley_item r = items[o - 1].data[i];

        struct parser_symbol sym =
            parser_grammar[r.rule_index].symbol_list[r.next_item];

        if (sym.tag == TERMINAL && sym.terminal == TOKEN_OPERATOR_COLON
            && r.rule_index == RULE_BLOCKSTATEMENT_LOOP_RANGE
            && module->tokens[o - 1].tag == TOKEN_KEYWORD_IN)
        {
            nlogf_pretty(
                LOG_ERROR,
                module->src,
                module->src_path.data,
                module->tokens[o - 1 - 1].line,
                module->tokens[o - 1 - 1].column
                    + module->tokens[o - 1 - 1].length,
                "Expected %s in range loop statement.",
                EMPHASIZE(": TYPE"));
            return;
        }
    }

    // Missing braces in loop statement
    for (size_t i = 0; i < items[o - 1].size; ++i)
    {
        struct parser_earley_item r = items[o - 1].data[i];

        struct parser_symbol sym =
            parser_grammar[r.rule_index].symbol_list[r.next_item];

        if (sym.tag == TERMINAL && sym.terminal == TOKEN_OPERATOR_LEFTBRACE
            && (r.rule_index == RULE_BLOCKSTATEMENT_LOOP_RANGE
                || r.rule_index == RULE_BLOCKSTATEMENT_LOOP_EXPRESSION))
        {
            nlogf_pretty(
                LOG_ERROR,
                module->src,
                module->src_path.data,
                module->tokens[o - 1 - 1].line,
                module->tokens[o - 1 - 1].column
                    + module->tokens[o - 1 - 1].length,
                "Must use braces for loop statement.");
            return;
        }
    }

    // Missing braces in defer statement
    for (size_t i = 0; i < items[o - 1].size; ++i)
    {
        struct parser_earley_item r = items[o - 1].data[i];

        struct parser_symbol sym =
            parser_grammar[r.rule_index].symbol_list[r.next_item];

        if (sym.tag == TERMINAL && sym.terminal == TOKEN_OPERATOR_LEFTBRACE
            && r.rule_index == RULE_BLOCKSTATEMENT_DEFER)
        {
            nlogf_pretty(
                LOG_ERROR,
                module->src,
                module->src_path.data,
                module->tokens[o - 1 - 1].line,
                module->tokens[o - 1 - 1].column
                    + module->tokens[o - 1 - 1].length,
                "Must use braces for defer statement.");
            return;
        }
    }

    // Expected one of the following tokens
    struct first_set error_tokens;
    first_set_init(&error_tokens);

    bool expression = false;
    // Expected one of the following tokens
    for (size_t i = 0; i < items[o - 1].size; ++i)
    {
        // Grab a rule
        struct parser_earley_item r = items[o - 1].data[i];

        // Get the symbol from the rule
        struct parser_symbol sym =
            parser_grammar[r.rule_index].symbol_list[r.next_item];

        if (r.rule_index == RULE_EXPRESSION
            || r.rule_index == RULE_EXPRESSION_EXPRESSION1_A
            || r.rule_index == RULE_EXPRESSION_EXPRESSION1_B
            || r.rule_index == RULE_EXPRESSION_EXPRESSION2_A
            || r.rule_index == RULE_EXPRESSION_EXPRESSION2_B
            || r.rule_index == RULE_EXPRESSION_EXPRESSION3_A
            || r.rule_index == RULE_EXPRESSION_EXPRESSION3_B
            || r.rule_index == RULE_EXPRESSION_EXPRESSION4_A
            || r.rule_index == RULE_EXPRESSION_EXPRESSION4_B
            || r.rule_index == RULE_EXPRESSION_EXPRESSION5_A
            || r.rule_index == RULE_EXPRESSION_EXPRESSION5_B
            || r.rule_index == RULE_EXPRESSION_EXPRESSION6_A
            || r.rule_index == RULE_EXPRESSION_EXPRESSION6_B
            || r.rule_index == RULE_EXPRESSION_EXPRESSION6_C
            || r.rule_index == RULE_EXPRESSION_EXPRESSION7_A
            || r.rule_index == RULE_EXPRESSION_EXPRESSION7_B
            || r.rule_index == RULE_EXPRESSION_EXPRESSION7_C
            || r.rule_index == RULE_EXPRESSION_EXPRESSION7_D
            || r.rule_index == RULE_EXPRESSION_EXPRESSION7_E
            || r.rule_index == RULE_EXPRESSION_EXPRESSION8_A
            || r.rule_index == RULE_EXPRESSION_EXPRESSION8_B
            || r.rule_index == RULE_EXPRESSION_EXPRESSION8_C
            || r.rule_index == RULE_EXPRESSION_EXPRESSION9_A
            || r.rule_index == RULE_EXPRESSION_EXPRESSION9_B
            || r.rule_index == RULE_EXPRESSION_EXPRESSION9_C
            || r.rule_index == RULE_EXPRESSION_EXPRESSION9_D
            || r.rule_index == RULE_EXPRESSION_EXPRESSION9_E
            || r.rule_index == RULE_EXPRESSION_EXPRESSION9_F
            || r.rule_index == RULE_EXPRESSION_EXPRESSION10_A
            || r.rule_index == RULE_EXPRESSION_EXPRESSION10_B
            || r.rule_index == RULE_EXPRESSION_EXPRESSION10_C
            || r.rule_index == RULE_EXPRESSION_EXPRESSION11_A
            || r.rule_index == RULE_EXPRESSION_EXPRESSION11_B
            || r.rule_index == RULE_EXPRESSION_EXPRESSION_UNARY_SIZEOF
            || r.rule_index == RULE_EXPRESSION_EXPRESSION_UNARY_BANG
            || r.rule_index == RULE_EXPRESSION_EXPRESSION_UNARY_TILDE
            || r.rule_index == RULE_EXPRESSION_EXPRESSION_UNARY_DASH
            || r.rule_index == RULE_EXPRESSION_EXPRESSION_UNARY_DOLLAR
            || r.rule_index == RULE_EXPRESSION_EXPRESSION_UNARY_AT
            || r.rule_index == RULE_EXPRESSION_EXPRESSION_UNARY_QUESTION
            || r.rule_index == RULE_EXPRESSION_EXPRESSION_UNARY_COUNTOF
            || r.rule_index == RULE_EXPRESSION_EXPRESSION_UNARY_POSTFIX
            || r.rule_index == RULE_EXPRESSION_POSTFIX_A
            || r.rule_index == RULE_EXPRESSION_POSTFIX_B
            || r.rule_index == RULE_EXPRESSION_POSTFIX_C
            || r.rule_index == RULE_EXPRESSION_LIST_POSTFIX_EMPTY
            || r.rule_index == RULE_EXPRESSION_LIST_POSTFIX_NONEMPTY
            || r.rule_index == RULE_EXPRESSION_LIST_POSTFIX_NONEMPTY_RECURSE
            || r.rule_index == RULE_EXPRESSION_LIST_POSTFIX_NONEMPTY_BASE
            || r.rule_index == RULE_EXPRESSION_POSTFIX_BASE_A
            || r.rule_index == RULE_EXPRESSION_POSTFIX_BASE_B
            || r.rule_index == RULE_EXPRESSION_POSTFIX_BASE_C
            || r.rule_index == RULE_EXPRESSION_POSTFIX_BASE_D
            || r.rule_index == RULE_EXPRESSION_LITERAL_ARRAY_A
            || r.rule_index == RULE_EXPRESSION_LITERAL_ARRAY_B
            || r.rule_index == RULE_EXPRESSION_LITERAL_U
            || r.rule_index == RULE_EXPRESSION_LITERAL_U8
            || r.rule_index == RULE_EXPRESSION_LITERAL_U16
            || r.rule_index == RULE_EXPRESSION_LITERAL_U32
            || r.rule_index == RULE_EXPRESSION_LITERAL_U64
            || r.rule_index == RULE_EXPRESSION_LITERAL_S
            || r.rule_index == RULE_EXPRESSION_LITERAL_S8
            || r.rule_index == RULE_EXPRESSION_LITERAL_S16
            || r.rule_index == RULE_EXPRESSION_LITERAL_S32
            || r.rule_index == RULE_EXPRESSION_LITERAL_S64
            || r.rule_index == RULE_EXPRESSION_LITERAL_F32
            || r.rule_index == RULE_EXPRESSION_LITERAL_F64
            || r.rule_index == RULE_EXPRESSION_LITERAL_ASCII
            || r.rule_index == RULE_EXPRESSION_LITERAL_TRUE
            || r.rule_index == RULE_EXPRESSION_LITERAL_FALSE
            || r.rule_index == RULE_EXPRESSION_LITERAL_NULL)

        {
            expression = true;
            continue;
        }

        // Predict if the symbol is a non-terminal
        if (TERMINAL == sym.tag)
        {
            first_set_append(&error_tokens, sym.terminal);
        }
        else if (sym.tag == NON_TERMINAL)
        {
            for (size_t i = 0; i < first_set[sym.nonterminal].size; ++i)
            {
                first_set_append(
                    &error_tokens, first_set[sym.nonterminal].data[i]);
            }
        }
    }
    if (module->tokens[o - 1].tag == TOKEN_EOF)
    {
        nlogf_pretty(
            LOG_ERROR,
            module->src,
            module->src_path.data,
            module->tokens[o - 1 - 1].line,
            module->tokens[o - 1 - 1].column + module->tokens[o - 1 - 1].length,
            "Parse failed.");
    }
    else
    {
        nlogf_pretty(
            LOG_ERROR,
            module->src,
            module->src_path.data,
            module->tokens[o - 1].line,
            module->tokens[o - 1].column,
            "Parse failed.");
    }

    nstr_t nstr_defer_fini expected_token_error;
    nstr_init_cstr(&expected_token_error, "Expected one of the following:");
    for (size_t i = 0; i < error_tokens.size; ++i)
    {
        nstr_cat_fmt(
            &expected_token_error,
            "\nTOKEN %s",
            token_type_to_cstr(error_tokens.data[i]));
    }
    if (expression)
    {
        nstr_cat_cstr(&expected_token_error, "\nExpression");
    }
    nlogf(LOG_ERROR, "%s", expected_token_error.data);
    first_set_fini(&error_tokens);

    // Helpful hints
    for (size_t i = 0; i < items[o - 1].size; ++i)
    {
        struct parser_earley_item r = items[o - 1].data[i];

        if ((r.next_item > 1
             && r.rule_index
                 == RULE_GLOBALSTATEMENT_FUNCTION_DECLARATION_ASSIGNMENT)
            || (r.next_item > 2
                && r.rule_index == RULE_GLOBALSTATEMENT_FUNCTION_DECLARATION))
        {
            log_form_of_function_decl();
            break;
        }
        else if (
            r.rule_index == RULE_ARGUMENTWITHTYPE
            || r.rule_index == RULE_LIST_ARGUMENTWITHTYPE_NONEMPTY_RECURSE
            || r.rule_index == RULE_LIST_ARGUMENTWITHTYPE_NONEMPTY
            || r.rule_index == RULE_LIST_ARGUMENTWITHTYPE_RECURSE
            || r.rule_index == RULE_LIST_ARGUMENTWITHTYPE_EMPTY)
        {
            log_form_of_function_args();
            break;
        }
        else if (r.rule_index == RULE_GLOBALSTATEMENT_IMPORT)
        {
            log_form_of_import_stmt();
            break;
        }
        else if (r.next_item > 1 && r.rule_index == RULE_GLOBALSTATEMENT_ALIAS)
        {
            log_form_of_alias_stmt();
            break;
        }
        else if (
            (r.next_item > 2
             && r.rule_index
                 == RULE_GLOBALSTATEMENT_VARIABLE_DECLARATION_INTRODUCE)
            || (r.next_item > 1
                && r.rule_index
                    == RULE_GLOBALSTATEMENT_VARIABLE_DECLARATION_ASSIGNMENT)
            || (r.next_item > 1
                && r.rule_index
                    == RULE_GLOBALSTATEMENT_VARIABLE_DECLARATION_EMPTY))
        {
            log_form_of_global_variable_decl();
            break;
        }
        else if (
            (r.next_item > 2
             && r.rule_index == RULE_GLOBALSTATEMENT_STRUCT_DECLARATION)
            || (r.next_item > 1
                && r.rule_index
                    == RULE_GLOBALSTATEMENT_STRUCT_DECLARATION_ASSIGNMENT))
        {
            log_form_of_struct_decl();
            break;
        }
        else if (
            r.rule_index == RULE_LIST_STRUCTMEMBER_RECURSE
            || r.rule_index == RULE_LIST_STRUCTMEMBER
            || r.rule_index == RULE_STRUCTMEMBER_FUNCTION
            || r.rule_index == RULE_STRUCTMEMBER_VARIABLE)
        {
            log_form_of_struct_members();
            break;
        }
        else if (r.rule_index == RULE_BLOCKSTATEMENT_ISOLATE)
        {
            log_form_of_isolate_stmt();
            break;
        }
        else if (
            r.rule_index == RULE_BLOCKSTATEMENT_VARIABLE_DECLARATION_EMPTY
            || r.rule_index
                == RULE_BLOCKSTATEMENT_VARIABLE_DECLARATION_ASSIGNMENT)
        {
            log_form_of_block_variable_decl();
            break;
        }
        else if (
            r.next_item > 1
            && r.rule_index == RULE_BLOCKSTATEMENT_VARIABLE_ASSIGNMENT)
        {
            log_form_of_assignment_stmt();
            break;
        }
        else if (
            r.rule_index == RULE_BLOCKSTATEMENT_LOOP_RANGE
            || r.rule_index == RULE_BLOCKSTATEMENT_LOOP_EXPRESSION)
        {
            log_form_of_loop_stmt();
            break;
        }
        else if (r.rule_index == RULE_BLOCKSTATEMENT_DEFER)
        {
            log_form_of_defer_stmt();
            break;
        }
    }
}
