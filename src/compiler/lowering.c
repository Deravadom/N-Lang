/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file lowering.c
 * @brief Lowering the Abstract Syntax Tree representation to NIR
 *
 */

#include "lowering.h"

//! Convert the provided, decimal, hexadecimal, or binary digit to an integer
//! value.
//! @param digit
//!     Digit to convert.
//! @return
//!     Integer representation of @p digit on success.
//! @return
//!     -1 if digit is not a valid decimal, hexadecimal, or binary digit.
static int digit_to_int(char ch)
{
    if (ch >= '0' && ch <= '9')
    {
        return ch - '0';
    }
    else if (ch >= 'A' && ch <= 'F')
    {
        return ch - 'A' + 10;
    }
    else if (ch >= 'a' && ch <= 'f')
    {
        return ch - 'a' + 10;
    }
    return -1;
}

//! Parse an N integer from an integer literal token.
//! @param type
//!     C type of the N integer to parse (`n_u8`, `n_s32`, `n_u`, etc.).
//! @param tok
//!     Token of the integer to parse. This token is assumed to be a valid
//!     integer literal token parsed during the tokenization phase.
//! @return
//!     An integer of @p type data type corresponding to the integer parsed
//!     from @p tok.
#define PARSE_N_INTEGER(/* TYPE */ type, /* struct token const* */ tok)        \
    ({                                                                         \
        type ret = 0;                                                          \
        char const* cur = tok->start;                                          \
                                                                               \
        /* Parse + or - symbol. */                                             \
        bool is_negative = false;                                              \
        if (*cur == '+' || *cur == '-')                                        \
        {                                                                      \
            is_negative = *cur++ == '-';                                       \
        }                                                                      \
                                                                               \
        /* Parse prefix. */                                                    \
        int radix;                                                             \
        if (*cur == '0' && tolower(*(cur + 1)) == 'x')                         \
        {                                                                      \
            radix = 16;                                                        \
            cur += 2;                                                          \
        }                                                                      \
        else if (*cur == '0' && tolower(*(cur + 1)) == 'o')                    \
        {                                                                      \
            radix = 8;                                                         \
            cur += 2;                                                          \
        }                                                                      \
        else if (*cur == '0' && tolower(*(cur + 1)) == 'b')                    \
        {                                                                      \
            radix = 2;                                                         \
            cur += 2;                                                          \
        }                                                                      \
        else                                                                   \
        {                                                                      \
            nassert(isdigit(*cur));                                            \
            radix = 10;                                                        \
        }                                                                      \
                                                                               \
        /* Populate integer value. */                                          \
        DIAGNOSTIC_PUSH                                                        \
        DIAGNOSTIC_IGNORE("-Wconversion");                                     \
        while (*cur != 'u' && *cur != 's')                                     \
        {                                                                      \
            ret *= (type)radix;              /* Shift digits. */               \
            ret += (type)digit_to_int(*cur); /* Add current digit. */          \
            cur += 1;                                                          \
        }                                                                      \
                                                                               \
        if (is_negative)                                                       \
        {                                                                      \
            ret *= (type)-1;                                                   \
        }                                                                      \
        DIAGNOSTIC_POP                                                         \
                                                                               \
        /* return */ ret;                                                      \
    })

void lower_to_nir(struct module* module, struct parse_tree* parse_tree)
{
    // TOP -> Program 'EOF'
    parse_tree = &parse_tree->children[0];

    // Program -> EMPTY
    if (parse_tree->children_length == 0)
    {
        return;
    }

    // Program -> ListGlobalStatement
    struct parse_tree* root = &parse_tree->children[0];

    // ListGlobalStatement -> ListGlobalStatement GlobalStatement
    narr_t(struct parse_tree) statements =
        narr_alloc(0, sizeof(struct parse_tree));
    while (root->rule_index == RULE_LIST_GLOBALSTATEMENT_RECURSE)
    {
        statements = narr_push(statements, &root->children[1]);
        root = &root->children[0];
    }
    // ListGlobalStatement -> GlobalStatement
    statements = narr_push(statements, &root->children[0]);

    for (uint64_t i = 0; i < narr_length(statements); ++i)
    {
        uint64_t j = narr_length(statements) - i - 1;
        struct stmt stmt;
        stmt_init(&stmt);
        lower_global_statement(&stmt, &statements[j], module);
        module->global_scope.stmts = narr_push(module->global_scope.stmts, &stmt);
    }
    narr_free(statements);

    module->global_scope.srctok = &module->tokens[parse_tree->children[0].start];

    // Postconditions.
    nassert(NULL != module->global_scope.srctok);
}

void lower_global_statement(
    struct stmt* stmt, struct parse_tree* parse_tree, struct module* module)
{
    stmt->srctok = &module->tokens[parse_tree->start];

    switch (parse_tree->rule_index)
    {
    case RULE_GLOBALSTATEMENT_IMPORT:
    {
        // GlobalStatement -> 'IMPORT' 'STRING' ';'
        nstr_init(&stmt->import_string);
        struct token* str = parse_tree->children[1].terminal;
        parse_n_astr_literal(str->start, &stmt->import_string);
        stmt->tag = STMT_IMPORT;

        break;
    }
    case RULE_GLOBALSTATEMENT_ALIAS:
    {
        // GlobalStatement ->
        //     ['EXPORT'] 'ALIAS' 'IDENTIFIER' '=' DATATYPE ';'
        struct alias_stmt alias;
        alias_stmt_init(&alias);

        alias.srctok = &module->tokens[parse_tree->start];

        alias.is_export = PARSE_TREE_EPSILON != parse_tree->children[0].tag;

        struct token* id = parse_tree->children[2].terminal;
        lower_identifier(&alias.new_id, id);

        struct parse_tree* data_type = &parse_tree->children[4];
        lower_data_type_unqualified(&alias.dt, data_type, module);

        stmt->tag = STMT_ALIAS;

        stmt->alias_stmt = nalloc(sizeof(struct alias_stmt));
        *stmt->alias_stmt = alias;

        break;
    }
    case RULE_GLOBALSTATEMENT_VARIABLE_DECLARATION_INTRODUCE:
    {
        // GlobalStatement -> ['EXPORT'] 'INTRODUCE'
        //     'LET' 'IDENTIFIER' ':' DATATYPE ';'
        struct variable_decl var_decl;
        variable_decl_init(&var_decl);

        var_decl.srctok = &module->tokens[parse_tree->start];

        var_decl.is_export = PARSE_TREE_EPSILON != parse_tree->children[0].tag;
        var_decl.is_introduce = true;

        struct token* id = parse_tree->children[3].terminal;
        lower_identifier(&var_decl.id, id);

        struct parse_tree* data_type = &parse_tree->children[5];
        lower_data_type(&var_decl.dt, data_type, module);

        stmt->tag = STMT_VARIABLE_DECLARATION;
        stmt->variable_decl = nalloc(sizeof(struct variable_decl));
        *stmt->variable_decl = var_decl;

        break;
    }
    case RULE_GLOBALSTATEMENT_VARIABLE_DECLARATION_ASSIGNMENT:
    {
        // GlobalStatement -> ['EXPORT']
        //     'LET' 'IDENTIFIER' ':' DATATYPE = EXPRESSION ';'
        struct variable_decl var_decl;
        variable_decl_init(&var_decl);

        var_decl.srctok = &module->tokens[parse_tree->start];

        var_decl.is_export = PARSE_TREE_EPSILON != parse_tree->children[0].tag;

        struct token* id = parse_tree->children[2].terminal;
        lower_identifier(&var_decl.id, id);

        struct parse_tree* data_type = &parse_tree->children[4];
        lower_data_type(&var_decl.dt, data_type, module);

        struct parse_tree* expression = &parse_tree->children[6];

        struct expr exp;
        expr_init(&exp);
        lower_expression(&exp, expression, module);

        var_decl.def = nalloc(sizeof(struct expr));
        *var_decl.def = exp;

        stmt->tag = STMT_VARIABLE_DECLARATION;
        stmt->variable_decl = nalloc(sizeof(struct variable_decl));
        *stmt->variable_decl = var_decl;

        break;
    }
    case RULE_GLOBALSTATEMENT_VARIABLE_DECLARATION_EMPTY:
    {
        // GlobalStatement -> ['EXPORT']
        //     'LET' 'IDENTIFIER' ':' DATATYPE ';'
        struct variable_decl var_decl;
        variable_decl_init(&var_decl);

        var_decl.srctok = &module->tokens[parse_tree->start];

        var_decl.is_export = PARSE_TREE_EPSILON != parse_tree->children[0].tag;

        struct token* id = parse_tree->children[2].terminal;
        lower_identifier(&var_decl.id, id);

        struct parse_tree* data_type = &parse_tree->children[4];
        lower_data_type(&var_decl.dt, data_type, module);

        stmt->tag = STMT_VARIABLE_DECLARATION;
        stmt->variable_decl = nalloc(sizeof(struct variable_decl));
        *stmt->variable_decl = var_decl;
        break;
    }
    case RULE_GLOBALSTATEMENT_STRUCT_DECLARATION:
    {
        // GlobalStatement -> ['EXPORT'] 'INTRODUCE'
        //     'STRUCT' 'IDENTIFIER' ';'
        struct struct_decl struct_decl;
        struct_decl_init(&struct_decl);

        struct_decl.srctok = &module->tokens[parse_tree->start];

        struct_decl.is_export =
            PARSE_TREE_EPSILON != parse_tree->children[0].tag;
        struct_decl.is_introduce = true;

        struct token* id = parse_tree->children[3].terminal;
        lower_identifier(&struct_decl.id, id);

        stmt->tag = STMT_STRUCT_DECLARATION;
        stmt->struct_decl = nalloc(sizeof(struct struct_decl));
        *stmt->struct_decl = struct_decl;

        break;
    }
    case RULE_GLOBALSTATEMENT_STRUCT_DECLARATION_ASSIGNMENT:
    {
        // GlobalStatement -> ['EXPORT']
        //     'STRUCT' 'IDENTIFIER' '{' ListStructMember '}'
        struct struct_decl struct_decl;
        struct_decl_init(&struct_decl);

        struct_decl.srctok = &module->tokens[parse_tree->start];

        struct_decl.is_export =
            PARSE_TREE_EPSILON != parse_tree->children[0].tag;

        struct token* id = parse_tree->children[2].terminal;
        lower_identifier(&struct_decl.id, id);

        struct parse_tree* list_struct_member = &parse_tree->children[4];
        struct struct_def struct_def;
        struct_def_init(&struct_def);

        struct_def.srctok = &module->tokens[list_struct_member->start];

        lower_list_struct_member(&struct_def, list_struct_member, module);

        struct_decl.def = nalloc(sizeof(struct struct_def));
        *struct_decl.def = struct_def;

        stmt->tag = STMT_STRUCT_DECLARATION;
        stmt->struct_decl = nalloc(sizeof(struct struct_decl));
        *stmt->struct_decl = struct_decl;

        break;
    }
    case RULE_GLOBALSTATEMENT_VARIANT_DECLARATION:
    {
        // variant_declaration -> [ 'export' ] 'introduce'
        //                        'variant' <identifier> ';'
        struct variant_decl variant_decl;
        variant_decl_init(&variant_decl);

        variant_decl.is_export =
            PARSE_TREE_EPSILON != parse_tree->children[0].tag;
        variant_decl.is_introduce = true;

        struct token* id = parse_tree->children[3].terminal;
        lower_identifier(&variant_decl.id, id);

        variant_decl.srctok = &module->tokens[parse_tree->start];

        stmt->tag = STMT_VARIANT_DECLARATION;
        stmt->variant_decl = nalloc(sizeof(struct variant_decl));
        *stmt->variant_decl = variant_decl;

        break;
    }
    case RULE_GLOBALSTATEMENT_VARIANT_DECLARATION_ASSIGNMENT_TAGTYPE:
    {
        // variant_declaration -> [ 'export' ] 'variant' <identifier>
        //                        ':' <int-type>
        //                        '{' <varaint-definition> '}'
        struct variant_decl variant_decl;
        variant_decl_init(&variant_decl);

        variant_decl.is_export = PARSE_TREE_EPSILON != parse_tree->children[0].tag;

        struct token* id = parse_tree->children[2].terminal;
        lower_identifier(&variant_decl.id, id);

        struct variant_def* variant_def =
            nalloc(sizeof(struct variant_def));
        variant_def_init(variant_def);

        lower_data_type_unqualified(&variant_def->tag_dt,
                &parse_tree->children[4], module); 
        variant_def->tag_dt.qualifiers.is_mut = true;

        lower_variant_def(variant_def, &parse_tree->children[6],
                module);

        variant_decl.def = variant_def;
        variant_decl.srctok = &module->tokens[parse_tree->start];

        stmt->tag = STMT_VARIANT_DECLARATION;
        stmt->variant_decl = nalloc(sizeof(struct variant_decl));
        *stmt->variant_decl = variant_decl;

        break;
    }
    case RULE_GLOBALSTATEMENT_VARIANT_DECLARATION_ASSIGNMENT:
    {
        // variant_declaration -> [ export ] variant <identifier>
        //                        '{' <variant-definition> '}'
        struct variant_decl variant_decl;
        variant_decl_init(&variant_decl);

        variant_decl.is_export = PARSE_TREE_EPSILON != parse_tree->children[0].tag;

        struct token* id = parse_tree->children[2].terminal;
        lower_identifier(&variant_decl.id, id);

        struct variant_def* variant_def =
            nalloc(sizeof(struct variant_def));
        variant_def_init(variant_def);

        variant_def->tag_dt.qualifiers.is_mut = true;
        variant_def->tag_dt.srctok = &module->tokens[parse_tree->start];

        lower_variant_def(variant_def, &parse_tree->children[4],
                module);

        variant_decl.def = variant_def;
        variant_decl.srctok = &module->tokens[parse_tree->start];

        stmt->tag = STMT_VARIANT_DECLARATION;
        stmt->variant_decl = nalloc(sizeof(struct variant_decl));
        *stmt->variant_decl = variant_decl;

        break;
    }
    case RULE_GLOBALSTATEMENT_FUNCTION_DECLARATION:
    {
        // GlobalStatement -> ['EXPORT'] 'INTRODUCE' 'FUNC' 'IDENTIFIER'
        //     ':' '(' ListArgWithType ')' '->' DataType ';'

        struct function_decl func_decl;
        function_decl_init(&func_decl);

        func_decl.srctok = &module->tokens[parse_tree->start];

        func_decl.is_export = PARSE_TREE_EPSILON != parse_tree->children[0].tag;
        func_decl.is_introduce = true;

        struct token* id = parse_tree->children[3].terminal;
        lower_identifier(&func_decl.id, id);

        struct parse_tree* arguments = &parse_tree->children[6];
        lower_list_argwithtype(&func_decl, arguments, module);

        struct parse_tree* data_type = &parse_tree->children[9];
        lower_data_type(&func_decl.dt.func_sig->return_dt, data_type, module);

        stmt->tag = STMT_FUNCTION_DECLARATION;
        stmt->function_decl = nalloc(sizeof(struct function_decl));
        *stmt->function_decl = func_decl;

        break;
    }
    case RULE_GLOBALSTATEMENT_FUNCTION_DECLARATION_ASSIGNMENT:
    {
        // GlobalStatement -> ['EXPORT']
        //     'FUNC' 'IDENTIFIER' ':' '(' ListArgWithType ')'
        //         '->' DataType '{' ListBlockStatement '}'

        struct function_decl func_decl;
        function_decl_init(&func_decl);

        func_decl.srctok = &module->tokens[parse_tree->start];

        func_decl.is_export = PARSE_TREE_EPSILON != parse_tree->children[0].tag;

        struct token* id = parse_tree->children[2].terminal;
        lower_identifier(&func_decl.id, id);

        struct parse_tree* arguments = &parse_tree->children[5];
        lower_list_argwithtype(&func_decl, arguments, module);

        struct parse_tree* data_type = &parse_tree->children[8];
        lower_data_type(&func_decl.dt.func_sig->return_dt, data_type, module);

        struct parse_tree* statements = &parse_tree->children[10];

        struct scope def;
        scope_init(&def, UNDEFINED_SCOPE_PARENT);
        def.srctok = &module->tokens[parse_tree->children[9].start];

        lower_list_block_statement(&def, statements, module);

        func_decl.def = nalloc(sizeof(struct scope));
        *func_decl.def = def;

        stmt->tag = STMT_FUNCTION_DECLARATION;
        stmt->function_decl = nalloc(sizeof(struct function_decl));
        *stmt->function_decl = func_decl;

        break;
    }
    default:
    {
        NPANIC_DFLT_CASE();
        break;
    }
    }

    // Postconditions.
    nassert(NULL != stmt->srctok);
}

void lower_list_block_statement(
    struct scope* scope, struct parse_tree* parse_tree, struct module* module)
{
    if (parse_tree->children_length == 0)
    {
        return;
    }

    // ListBlockStatement -> ListBlockStatementRecurse
    struct parse_tree* root = &parse_tree->children[0];

    // ListBlockStatement -> ListBlockStatement BlockStatement
    narr_t(struct parse_tree) statements =
        narr_alloc(0, sizeof(struct parse_tree));
    while (root->rule_index == RULE_LIST_BLOCKSTATEMENT_RECURSE)
    {
        statements = narr_push(statements, &root->children[1]);
        root = &root->children[0];
    }
    // ListBlockStatement -> BlockStatement
    statements = narr_push(statements, &root->children[0]);

    for (uint64_t i = 0; i < narr_length(statements); ++i)
    {
        uint64_t j = narr_length(statements) - i - 1;
        struct stmt stmt;
        stmt_init(&stmt);
        lower_block_statement(&stmt, &statements[j], module);
        scope->stmts = narr_push(scope->stmts, &stmt);
    }
    narr_free(statements);

    scope->srctok = &module->tokens[parse_tree->children[0].start];

    // Postconditions.
    nassert(NULL != scope->srctok);
}

void lower_block_statement(
    struct stmt* stmt, struct parse_tree* parse_tree, struct module* module)
{
    stmt->srctok = &module->tokens[parse_tree->start];

    switch (parse_tree->rule_index)
    {
    case RULE_BLOCKSTATEMENT_ISOLATE:
    {
        // BlockStatement -> 'ISOLATE' '{' ListBlockStatement '}'

        struct scope sc;
        scope_init(&sc, UNDEFINED_SCOPE_PARENT);
        sc.srctok = &module->tokens[parse_tree->children[1].start];

        lower_list_block_statement(&sc, &parse_tree->children[2], module);

        stmt->tag = STMT_BLOCK;
        stmt->block = nalloc(sizeof(struct scope));
        *stmt->block = sc;

        break;
    }
    case RULE_BLOCKSTATEMENT_VARIABLE_DECLARATION_EMPTY:
    {
        // BlockStatement -> 'LET' 'IDENTIFIER' ':' DataType ';'

        struct variable_decl var_decl;
        variable_decl_init(&var_decl);

        var_decl.srctok = &module->tokens[parse_tree->start];

        struct token* id = parse_tree->children[1].terminal;
        lower_identifier(&var_decl.id, id);

        struct parse_tree* data_type = &parse_tree->children[3];
        lower_data_type(&var_decl.dt, data_type, module);

        stmt->tag = STMT_VARIABLE_DECLARATION;
        stmt->variable_decl = nalloc(sizeof(struct variable_decl));
        *stmt->variable_decl = var_decl;

        break;
    }
    case RULE_BLOCKSTATEMENT_VARIABLE_DECLARATION_ASSIGNMENT:
    {
        // BlockStatement -> 'LET' 'IDENTIFIER' ':' DataType
        //     '=' Expression ';'

        struct variable_decl var_decl;
        variable_decl_init(&var_decl);

        var_decl.srctok = &module->tokens[parse_tree->start];

        struct token* id = parse_tree->children[1].terminal;
        lower_identifier(&var_decl.id, id);

        struct parse_tree* data_type = &parse_tree->children[3];
        lower_data_type(&var_decl.dt, data_type, module);

        struct parse_tree* expression = &parse_tree->children[5];
        struct expr exp;
        expr_init(&exp);
        lower_expression(&exp, expression, module);

        var_decl.def = nalloc(sizeof(struct expr));
        *var_decl.def = exp;

        stmt->tag = STMT_VARIABLE_DECLARATION;
        stmt->variable_decl = nalloc(sizeof(struct variable_decl));
        *stmt->variable_decl = var_decl;

        break;
    }
    case RULE_BLOCKSTATEMENT_VARIABLE_ASSIGNMENT:
    {
        // BlockStatement -> ExpressionPostfix '=' Expression ';'

        struct expr assign;
        expr_init(&assign);
        assign.tag = EXPR_BINARY_ASSIGN;

        assign.srctok = &module->tokens[parse_tree->start];

        struct expr lhs;
        expr_init(&lhs);
        lower_expression(&lhs, &parse_tree->children[0], module);
        assign.binary.lhs_expr = nalloc(sizeof(struct expr));
        *assign.binary.lhs_expr = lhs;

        struct expr rhs;
        expr_init(&rhs);
        lower_expression(&rhs, &parse_tree->children[2], module);
        assign.binary.rhs_expr = nalloc(sizeof(struct expr));
        *assign.binary.rhs_expr = rhs;

        stmt->tag = STMT_EXPR;
        stmt->expr = nalloc(sizeof(struct expr));
        *stmt->expr = assign;

        break;
    }
    case RULE_BLOCKSTATEMENT_IF:
    {
        // BlockStatement -> 'IF' IfBranch ListElifBranch ElseBranch

        stmt->tag = STMT_IF;
        stmt->if_stmt = nalloc(sizeof(struct if_stmt));
        if_stmt_init(stmt->if_stmt);

        struct parse_tree* ifbranch = &parse_tree->children[1];
        lower_if_branch(stmt->if_stmt, ifbranch, module);

        struct parse_tree* list_elifbranch = &parse_tree->children[2];

        while (list_elifbranch->rule_index == RULE_LIST_ELIFBRANCH_RECURSE)
        {
            // ListElifBranch -> 'ELIF' IfBranch ListElifBranch
            struct parse_tree* ib = &list_elifbranch->children[1];
            lower_if_branch(stmt->if_stmt, ib, module);

            list_elifbranch = &list_elifbranch->children[2];
        }
        // ListElifBranch -> EMPTY

        struct parse_tree* elsebranch = &parse_tree->children[3];

        if (elsebranch->tag == PARSE_TREE_EPSILON)
        {
            break;
        }

        // ElseBranch -> 'ELSE' '{' ListBlockStatement '}'
        struct parse_tree* sts = &elsebranch->children[2];

        struct scope else_body;
        scope_init(&else_body, UNDEFINED_SCOPE_PARENT);
        else_body.srctok = &module->tokens[elsebranch->children[1].start];

        lower_list_block_statement(&else_body, sts, module);
        stmt->if_stmt->else_block = nalloc(sizeof(struct scope));
        *stmt->if_stmt->else_block = else_body;

        break;
    }
    case RULE_BLOCKSTATEMENT_LOOP_RANGE:
    {
        // BlockStatement -> 'LOOP' 'IDENTIFIER' ':' DataType 'IN'
        //     RangeBoundStart Expression ',' Expression RangeBoundEnd
        //         '{' ListBlockStatement '}'

        struct loop_stmt loop;
        loop_stmt_init(&loop);

        loop.srctok = &module->tokens[parse_tree->children[0].start];
        loop.block->srctok = &module->tokens[parse_tree->children[10].start];

        struct variable_decl var_decl;
        variable_decl_init(&var_decl);
        var_decl.srctok = &module->tokens[parse_tree->children[1].start];

        struct token* id = parse_tree->children[1].terminal;
        lower_identifier(&var_decl.id, id);

        struct parse_tree* data_type = &parse_tree->children[3];
        lower_data_type(&var_decl.dt, data_type, module);

        loop.range.variable_decl = var_decl;

        struct token* start = parse_tree->children[5].children[0].terminal;
        switch (start->tag)
        {
        case TOKEN_OPERATOR_LEFTPARENTHESIS:
        {
            loop.range.lower_clusivity = LOWER_EXCLUSIVE;

            break;
        }
        case TOKEN_OPERATOR_LEFTBRACKET:
        {
            loop.range.lower_clusivity = LOWER_INCLUSIVE;

            break;
        }
        default:
        {
            NPANIC_DFLT_CASE();

            break;
        }
        }

        struct expr lower_bound;
        expr_init(&lower_bound);
        struct parse_tree* lbound = &parse_tree->children[6];
        lower_expression(&lower_bound, lbound, module);

        loop.range.lower_bound = nalloc(sizeof(struct expr));
        *loop.range.lower_bound = lower_bound;

        struct expr upper_bound;
        expr_init(&upper_bound);
        struct parse_tree* ubound = &parse_tree->children[8];
        lower_expression(&upper_bound, ubound, module);

        loop.range.upper_bound = nalloc(sizeof(struct expr));
        *loop.range.upper_bound = upper_bound;

        struct token* end = parse_tree->children[9].children[0].terminal;
        switch (end->tag)
        {
        case TOKEN_OPERATOR_RIGHTPARENTHESIS:
        {
            loop.range.upper_clusivity = UPPER_EXCLUSIVE;

            break;
        }
        case TOKEN_OPERATOR_RIGHTBRACKET:
        {
            loop.range.upper_clusivity = UPPER_INCLUSIVE;

            break;
        }
        default:
        {
            NPANIC_DFLT_CASE();

            break;
        }
        }

        struct parse_tree* stmts = &parse_tree->children[11];
        lower_list_block_statement(loop.block, stmts, module);

        loop.tag = LOOP_RANGE;

        stmt->tag = STMT_LOOP;
        stmt->loop_stmt = nalloc(sizeof(struct loop_stmt));
        *stmt->loop_stmt = loop;

        break;
    }
    case RULE_BLOCKSTATEMENT_LOOP_EXPRESSION:
    {
        // BlockStatement -> 'LOOP' Expression
        //     '{' ListBlockStatement '}'

        struct loop_stmt loop;
        loop_stmt_init(&loop);
        loop.srctok = &module->tokens[parse_tree->children[0].start];
        loop.block->srctok = &module->tokens[parse_tree->children[2].start];

        struct expr expr;
        expr_init(&expr);
        struct parse_tree* expression = &parse_tree->children[1];
        lower_expression(&expr, expression, module);
        loop.conditional.expr = nalloc(sizeof(struct expr));
        *loop.conditional.expr = expr;

        struct parse_tree* stmts = &parse_tree->children[3];
        lower_list_block_statement(loop.block, stmts, module);

        loop.tag = LOOP_CONDITIONAL;

        stmt->tag = STMT_LOOP;
        stmt->loop_stmt = nalloc(sizeof(struct loop_stmt));
        *stmt->loop_stmt = loop;

        break;
    }
    case RULE_BLOCKSTATEMENT_DEFER:
    {
        // BlockStatement -> 'DEFER' '{' ListBlockStatement '}'

        struct scope defscope;
        scope_init(&defscope, UNDEFINED_SCOPE_PARENT);
        defscope.srctok = &module->tokens[parse_tree->children[1].start];

        struct parse_tree* bstm = &parse_tree->children[2];

        lower_list_block_statement(&defscope, bstm, module);

        stmt->tag = STMT_DEFER;
        stmt->defer_stmt.block = nalloc(sizeof(struct scope));
        *stmt->defer_stmt.block = defscope;

        break;
    }
    case RULE_BLOCKSTATEMENT_BREAK:
    {
        // BlockStatement -> 'BREAK'

        stmt->tag = STMT_BREAK;

        break;
    }
    case RULE_BLOCKSTATEMENT_CONTINUE:
    {
        // BlockStatement -> 'CONTINUE'

        stmt->tag = STMT_CONTINUE;

        break;
    }
    case RULE_BLOCKSTATEMENT_RETURN_NONEMPTY:
    {
        // BlockStatement -> 'RETURN' Expression ';'

        struct parse_tree* expression = &parse_tree->children[1];

        struct expr expr;
        expr_init(&expr);

        lower_expression(&expr, expression, module);

        stmt->return_expr = nalloc(sizeof(struct expr));
        *stmt->return_expr = expr;

        stmt->tag = STMT_RETURN;

        break;
    }
    case RULE_BLOCKSTATEMENT_RETURN_EMPTY:
    {
        // BlockStatement -> 'RETURN' ';'

        stmt->return_expr = NULL;
        stmt->tag = STMT_RETURN;

        break;
    }
    case RULE_BLOCKSTATEMENT_EXPRESSION:
    {
        // BlockStatement -> ExpressionPostfix ';'

        struct expr exp;
        expr_init(&exp);
        lower_expression(&exp, &parse_tree->children[0], module);

        stmt->tag = STMT_EXPR;
        stmt->expr = nalloc(sizeof(struct expr));
        *stmt->expr = exp;

        break;
    }
    default:
    {
        NPANIC_DFLT_CASE();

        break;
    }
    }

    // Postconditions.
    nassert(NULL != stmt->srctok);
}

void lower_data_type(
    struct data_type* dt, struct parse_tree* parse_tree, struct module* module)
{
    // DataType -> [MUT] [UNIQUE] [VOLATILE] PRIMITIVE

    dt->qualifiers.is_mut = PARSE_TREE_EPSILON != parse_tree->children[0].tag;
    dt->qualifiers.is_unique =
        PARSE_TREE_EPSILON != parse_tree->children[1].tag;
    dt->qualifiers.is_volatile =
        PARSE_TREE_EPSILON != parse_tree->children[2].tag;

    struct parse_tree* unqualified = &parse_tree->children[3];
    lower_data_type_unqualified(dt, unqualified, module);

    if (PARSE_TREE_EPSILON != parse_tree->children[0].tag)
    {
        dt->srctok = &module->tokens[parse_tree->children[0].start];
    }
    else if (PARSE_TREE_EPSILON != parse_tree->children[1].tag)
    {
        dt->srctok = &module->tokens[parse_tree->children[1].start];
    }
    else if (PARSE_TREE_EPSILON != parse_tree->children[2].tag)
    {
        dt->srctok = &module->tokens[parse_tree->children[2].start];
    }
    else
    {
        dt->srctok = &module->tokens[parse_tree->children[3].start];
    }

    // Postconditions.
    nassert(NULL != dt->srctok);
}

void lower_data_type_unqualified(
    struct data_type* dt, struct parse_tree* parse_tree, struct module* module)
{
    dt->srctok = &module->tokens[parse_tree->children[0].start];
    if (parse_tree->children_length == 1
        && parse_tree->children[0].tag == PARSE_TREE_TERMINAL)
    {
        switch (parse_tree->children[0].terminal->tag)
        {
        case TOKEN_KEYWORD_BOOL:
        {
            // PRIMITIVE -> BOOL
            dt->tag = DT_BOOL;

            break;
        }
        case TOKEN_KEYWORD_F32:
        {
            // PRIMITIVE -> F32
            dt->tag = DT_F32;

            break;
        }
        case TOKEN_KEYWORD_F64:
        {
            // PRIMITIVE -> F64
            dt->tag = DT_F64;

            break;
        }
        case TOKEN_KEYWORD_U:
        {
            // PRIMITIVE -> U
            dt->tag = DT_U;

            break;
        }
        case TOKEN_KEYWORD_U8:
        {
            // PRIMITIVE -> U8
            dt->tag = DT_U8;

            break;
        }
        case TOKEN_KEYWORD_U16:
        {
            // PRIMITIVE -> U16
            dt->tag = DT_U16;

            break;
        }
        case TOKEN_KEYWORD_U32:
        {
            // PRIMITIVE -> U32
            dt->tag = DT_U32;

            break;
        }
        case TOKEN_KEYWORD_U64:
        {
            // PRIMITIVE -> U64
            dt->tag = DT_U64;

            break;
        }
        case TOKEN_KEYWORD_S:
        {
            // PRIMITIVE -> S
            dt->tag = DT_S;

            break;
        }
        case TOKEN_KEYWORD_S8:
        {
            // PRIMITIVE -> S8
            dt->tag = DT_S8;

            break;
        }
        case TOKEN_KEYWORD_S16:
        {
            // PRIMITIVE -> S16
            dt->tag = DT_S16;

            break;
        }
        case TOKEN_KEYWORD_S32:
        {
            // PRIMITIVE -> S32
            dt->tag = DT_S32;

            break;
        }
        case TOKEN_KEYWORD_S64:
        {
            // PRIMITIVE -> S64
            dt->tag = DT_S64;

            break;
        }
        case TOKEN_KEYWORD_ASCII:
        {
            // PRIMITIVE -> ASCII
            dt->tag = DT_ASCII;

            break;
        }
        case TOKEN_KEYWORD_VOID:
        {
            // PRIMITIVE -> VOID
            dt->tag = DT_VOID;

            break;
        }
        case TOKEN_IDENTIFIER:
        {
            // 'Identifier'
            struct identifier id;
            identifier_init(&id);
            lower_identifier(&id, parse_tree->children[0].terminal);

            dt->tag = DT_UNRESOLVED_ID;
            dt->unresolved.id = id;

            break;
        }
        default:
        {
            NPANIC_DFLT_CASE();

            break;
        }
        }
    }
    else if (parse_tree->rule_index == RULE_DATATYPE_PRIMITIVE_FUNCTION)
    {
        // PrimitiveDatatype -> '(' ListDataType ')' '->' DataType

        struct func_sig func_sig;
        func_sig_init(&func_sig);

        if (parse_tree->children[1].tag == PARSE_TREE_EPSILON)
        {
            // ListDataType -> EMPTY
        }
        else
        {
            // ListDataType -> NonemptyListDatatype
            struct parse_tree* root = &parse_tree->children[1].children[0];
            narr_t(struct parse_tree) types =
                narr_alloc(0, sizeof(struct parse_tree));
            while (root->rule_index == RULE_LIST_DATATYPE_NONEMPTY_RECURSE)
            {
                // NonemptyListDatatype -> NonemptyListDatatype ',' DataType
                types = narr_push(types, &root->children[2]);
                root = &root->children[0];
            }
            // NonemptyListDatatype -> DataType
            types = narr_push(types, &root->children[0]);

            for (uint64_t i = 0; i < narr_length(types); ++i)
            {
                uint64_t j = narr_length(types) - i - 1;
                struct data_type x;
                data_type_init(&x);
                lower_data_type(&x, &types[j], module);
                func_sig.param_dts = narr_push(func_sig.param_dts, &x);
            }
            narr_free(types);
        }

        lower_data_type(&func_sig.return_dt, &parse_tree->children[4], module);

        func_sig.return_dt.srctok = &module->tokens[parse_tree->children[4].start];

        dt->tag = DT_FUNCTION;
        dt->func_sig = nalloc(sizeof(typeof(*dt->func_sig)));
        *dt->func_sig = func_sig;
    }
    else if (parse_tree->rule_index == RULE_DATATYPE_PRIMITIVE_POINTER)
    {
        // PrimitiveDatatype -> '^' DataType

        struct data_type inner;
        data_type_init(&inner);
        lower_data_type(&inner, &parse_tree->children[1], module);

        dt->inner = nalloc(sizeof(struct data_type));
        *dt->inner = inner;

        dt->tag = DT_POINTER;
    }
    else if (parse_tree->rule_index == RULE_DATATYPE_PRIMITIVE_ARRAY)
    {
        // PrimitiveDatatype -> '[' 'U' ']' DataType

        struct data_type inner;
        data_type_init(&inner);
        lower_data_type(&inner, &parse_tree->children[3], module);

        dt->inner = nalloc(sizeof(typeof(*dt->inner)));
        *dt->inner = inner;
        dt->tag = DT_ARRAY;
        dt->array.length =
            (size_t)PARSE_N_INTEGER(n_u, parse_tree->children[1].terminal);
    }
    else if (parse_tree->rule_index == RULE_DATATYPE_PRIMITIVE_TYPEOF_EXPORDT)
    {
        // PrimitiveDatatype -> 'typeof' '(' ExpressionOrDatatype ')'

        // ExpressionOrDataType -> 'Identifier'
        if (parse_tree->children[2].rule_index == RULE_EXPRORDT_IDENTIFIER)
        {
            // XXX: The DataType Expressions Saga continues

            struct token* id = parse_tree->children[2].children[0].terminal;
            struct identifier ident;
            identifier_init(&ident);
            lower_identifier(&ident, id);

            struct expr exp;
            expr_init(&exp);
            exp.srctok =
                &module->tokens[parse_tree->children[2].children[0].start];
            exp.tag = EXPR_PRIMARY_IDENTIFIER;
            exp.id = ident;

            dt->tag = DT_UNRESOLVED_TYPEOF_EXPR;
            dt->unresolved.typeof_expr = nalloc(sizeof(struct expr));
            *dt->unresolved.typeof_expr = exp;
        }
        // ExpressionOrDataType -> NonterminalExpressionOrDatatype
        else
        {
            // NonterminalExpressionOrDatatype -> Expression
            if (parse_tree->children[2].children[0].rule_index
                == RULE_EXPRORDT_EXPRESSION)
            {
                // XXX: The DataType Expressions Saga continues

                struct expr exp;
                expr_init(&exp);
                lower_expression(
                    &exp,
                    &parse_tree->children[2].children[0].children[0],
                    module);

                dt->tag = DT_UNRESOLVED_TYPEOF_EXPR;
                dt->unresolved.typeof_expr = nalloc(sizeof(struct expr));
                *dt->unresolved.typeof_expr = exp;
            }
            // NonterminalExpressionOrDatatype -> Datatype
            if (parse_tree->children[2].children[0].rule_index
                == RULE_EXPRORDT_DATATYPE)
            {
                // XXX: The DataType Expressions Saga continues

                struct data_type inner;
                data_type_init(&inner);
                lower_data_type(
                    &inner,
                    &parse_tree->children[2].children[0].children[0],
                    module);

                dt->tag = DT_UNRESOLVED_TYPEOF_DT;
                dt->unresolved.typeof_dt = nalloc(sizeof(struct data_type));
                *dt->unresolved.typeof_dt = inner;
            }
        }
    }
    else
    {
        NPANIC_DFLT_CASE();
    }
}

void lower_expression(
    struct expr* expr, struct parse_tree* parse_tree, struct module* module)
{
    // TODO: See expr::srctok for correct setting of srctok.
    // When displaying error messages for expressions, the operator in question
    // the line and column where we want to point out an error is on the
    // operator for unary, binary, and postfix expressions, and on the first
    // token of the primary expression for primary expressions.
    // This is because (1) you can always find the source tokens of operands
    // by going to that operand and (b) because the application of operators
    // over operands is where the semantic errors take place in the case of
    // error messages.
    // Futher error messages in the compiler expect the behavior described in
    // the ccomment on expr::srctok.
    // This function should be updated to implement that behavior.
    //
    // It might also be worthwhile to just decribe all non-primary expression in
    // terms of a triple (start_location, stop_location, operator_location) so
    // that error messages could be deliver pointing at the operator with
    // highlighting over the span of all operands and the operator.
    expr->srctok = &module->tokens[parse_tree->start];

    switch (parse_tree->rule_index)
    {
    case RULE_EXPRESSION_EXPRESSION1_A:
    {
        // Expression1 -> Expression1 '||' Expression2
        lower_binary_expression(expr, parse_tree, module);
        expr->tag = EXPR_BINARY_LOGICAL_OR;

        break;
    }
    case RULE_EXPRESSION_EXPRESSION2_A:
    {
        // Expression2 -> Expression2 '&&' Expression3
        lower_binary_expression(expr, parse_tree, module);
        expr->tag = EXPR_BINARY_LOGICAL_AND;

        break;
    }
    case RULE_EXPRESSION_EXPRESSION3_A:
    {
        // Expression3 -> Expression3 '|' Expression4
        lower_binary_expression(expr, parse_tree, module);
        expr->tag = EXPR_BINARY_BITWISE_OR;

        break;
    }
    case RULE_EXPRESSION_EXPRESSION4_A:
    {
        // Expression4 -> Expression4 '^' Expression5
        lower_binary_expression(expr, parse_tree, module);
        expr->tag = EXPR_BINARY_BITWISE_XOR;

        break;
    }
    case RULE_EXPRESSION_EXPRESSION5_A:
    {
        // Expression5 -> Expression5 '&' Expression6
        lower_binary_expression(expr, parse_tree, module);
        expr->tag = EXPR_BINARY_BITWISE_AND;

        break;
    }
    case RULE_EXPRESSION_EXPRESSION6_A:
    {
        // Expression6 -> Expression6 '==' Expression7
        lower_binary_expression(expr, parse_tree, module);
        expr->tag = EXPR_BINARY_REL_EQ;

        break;
    }
    case RULE_EXPRESSION_EXPRESSION6_B:
    {
        // Expression6 -> Expression6 '!=' Expression7
        lower_binary_expression(expr, parse_tree, module);
        expr->tag = EXPR_BINARY_REL_NE;

        break;
    }
    case RULE_EXPRESSION_EXPRESSION7_A:
    {
        // Expression7 -> Expression7 '<=' Expression8
        lower_binary_expression(expr, parse_tree, module);
        expr->tag = EXPR_BINARY_REL_LE;

        break;
    }
    case RULE_EXPRESSION_EXPRESSION7_B:
    {
        // Expression7 -> Expression7 '<' Expression8
        lower_binary_expression(expr, parse_tree, module);
        expr->tag = EXPR_BINARY_REL_LT;

        break;
    }
    case RULE_EXPRESSION_EXPRESSION7_C:
    {
        // Expression7 -> Expression7 '>=' Expression8
        lower_binary_expression(expr, parse_tree, module);
        expr->tag = EXPR_BINARY_REL_GE;

        break;
    }
    case RULE_EXPRESSION_EXPRESSION7_D:
    {
        // Expression7 -> Expression7 '>' Expression8
        lower_binary_expression(expr, parse_tree, module);
        expr->tag = EXPR_BINARY_REL_GT;

        break;
    }
    case RULE_EXPRESSION_EXPRESSION8_A:
    {
        // Expression8 -> Expression8 '<<' Expression9
        lower_binary_expression(expr, parse_tree, module);
        expr->tag = EXPR_BINARY_SHIFT_L;

        break;
    }
    case RULE_EXPRESSION_EXPRESSION8_B:
    {
        // Expression8 -> Expression8 '>>' Expression9
        lower_binary_expression(expr, parse_tree, module);
        expr->tag = EXPR_BINARY_SHIFT_R;

        break;
    }
    case RULE_EXPRESSION_EXPRESSION9_A:
    {
        // Expression9 -> Expression9 '+' Expression10
        lower_binary_expression(expr, parse_tree, module);
        expr->tag = EXPR_BINARY_PLUS;

        break;
    }
    case RULE_EXPRESSION_EXPRESSION9_B:
    {
        // Expression9 -> Expression9 '-' Expression10
        lower_binary_expression(expr, parse_tree, module);
        expr->tag = EXPR_BINARY_MINUS;

        break;
    }
    case RULE_EXPRESSION_EXPRESSION9_C:
    {
        // Expression9 -> Expression9 '-^^' Expression10
        lower_binary_expression(expr, parse_tree, module);
        expr->tag = EXPR_BINARY_PTR_DIFF;

        break;
    }
    case RULE_EXPRESSION_EXPRESSION9_D:
    {
        // Expression9 -> Expression9 '-^' Expression10
        lower_binary_expression(expr, parse_tree, module);
        expr->tag = EXPR_BINARY_PTR_MINUS;

        break;
    }
    case RULE_EXPRESSION_EXPRESSION9_E:
    {
        // Expression9 -> Expression9 '+^' Expression10
        lower_binary_expression(expr, parse_tree, module);
        expr->tag = EXPR_BINARY_PTR_PLUS;

        break;
    }
    case RULE_EXPRESSION_EXPRESSION10_A:
    {
        // Expression10 -> Expression10 '*' Expression11
        lower_binary_expression(expr, parse_tree, module);
        expr->tag = EXPR_BINARY_MULT;

        break;
    }
    case RULE_EXPRESSION_EXPRESSION10_B:
    {
        // Expression10 -> Expression10 '/' Expression11
        lower_binary_expression(expr, parse_tree, module);
        expr->tag = EXPR_BINARY_DIV;

        break;
    }
    case RULE_EXPRESSION_EXPRESSION11_A:
    {
        // Expression11 -> Expression11 'as' DataType
        struct expr exp;
        expr_init(&exp);
        lower_expression(&exp, &parse_tree->children[0], module);

        expr->binary.lhs_expr = nalloc(sizeof(struct expr));
        *expr->binary.lhs_expr = exp;

        struct data_type dt;
        data_type_init(&dt);
        lower_data_type(&dt, &parse_tree->children[2], module);

        expr->binary.rhs_dt = nalloc(sizeof(struct data_type));
        *expr->binary.rhs_dt = dt;
        expr->tag = EXPR_BINARY_CAST;

        break;
    }
    case RULE_EXPRESSION_EXPRESSION_UNARY_SIZEOF:
    {
        // Expression12 -> 'sizeof' '(' ExpressionOrDatatype ')'

        // ExpressionOrDataType -> 'Identifier'
        if (parse_tree->children[2].rule_index == RULE_EXPRORDT_IDENTIFIER)
        {
            // XXX: The DataType Expressions Saga continues

            struct token* id = parse_tree->children[2].children[0].terminal;
            struct identifier ident;
            identifier_init(&ident);
            lower_identifier(&ident, id);

            struct expr expr1;
            expr_init(&expr1);
            expr1.srctok =
                &module->tokens[parse_tree->children[2].children[0].start];
            expr1.tag = EXPR_PRIMARY_IDENTIFIER;
            expr1.id = ident;

            expr->unary.rhs_expr = nalloc(sizeof(struct expr));
            *expr->unary.rhs_expr = expr1;
            expr->tag = EXPR_UNARY_SIZEOF_EXPR;
        }
        // ExpressionOrDataType -> NonterminalExpressionOrDatatype
        else
        {
            // NonterminalExpressionOrDatatype -> Expression
            if (parse_tree->children[2].children[0].rule_index
                == RULE_EXPRORDT_EXPRESSION)
            {
                // XXX: The DataType Expressions Saga continues

                struct expr expr1;
                expr_init(&expr1);
                lower_expression(
                    &expr1,
                    &parse_tree->children[2].children[0].children[0],
                    module);

                expr->unary.rhs_expr = nalloc(sizeof(struct expr));
                *expr->unary.rhs_expr = expr1;
                expr->tag = EXPR_UNARY_SIZEOF_EXPR;
            }
            // NonterminalExpressionOrDatatype -> Datatype
            if (parse_tree->children[2].children[0].rule_index
                == RULE_EXPRORDT_DATATYPE)
            {
                // XXX: The DataType Expressions Saga continues

                struct data_type dt;
                data_type_init(&dt);
                lower_data_type(
                    &dt,
                    &parse_tree->children[2].children[0].children[0],
                    module);

                expr->unary.rhs_dt = nalloc(sizeof(struct data_type));
                *expr->unary.rhs_dt = dt;
                expr->tag = EXPR_UNARY_SIZEOF_DT;
            }
        }

        break;
    }
    case RULE_EXPRESSION_EXPRESSION_UNARY_BANG:
    {
        // Expression12 -> '!' Expression12
        lower_unary_expression(expr, parse_tree, module);

        expr->tag = EXPR_UNARY_LOGICAL_NOT;

        break;
    }
    case RULE_EXPRESSION_EXPRESSION_UNARY_TILDE:
    {
        // Expression12 -> '~' Expression12
        lower_unary_expression(expr, parse_tree, module);

        expr->tag = EXPR_UNARY_BITWISE_NOT;

        break;
    }
    case RULE_EXPRESSION_EXPRESSION_UNARY_DASH:
    {
        // Expression12 -> '-' Expression12
        lower_unary_expression(expr, parse_tree, module);

        expr->tag = EXPR_UNARY_MINUS;

        break;
    }
    case RULE_EXPRESSION_EXPRESSION_UNARY_DOLLAR:
    {
        // Expression12 -> '$' Expression12
        lower_unary_expression(expr, parse_tree, module);

        expr->tag = EXPR_UNARY_DECAY;

        break;
    }
    case RULE_EXPRESSION_EXPRESSION_UNARY_AT:
    {
        // Expression12 -> '@' Expression12
        lower_unary_expression(expr, parse_tree, module);

        expr->tag = EXPR_UNARY_DEREF;

        break;
    }
    case RULE_EXPRESSION_EXPRESSION_UNARY_QUESTION:
    {
        // Expression12 -> '?' Expression12
        lower_unary_expression(expr, parse_tree, module);

        expr->tag = EXPR_UNARY_ADDR_OF;

        break;
    }
    case RULE_EXPRESSION_EXPRESSION_UNARY_COUNTOF:
    {
        // Expression12 -> 'countof' '(' ExpressionOrDatatype ')'

        // ExpressionOrDataType -> 'Identifier'
        if (parse_tree->children[2].rule_index == RULE_EXPRORDT_IDENTIFIER)
        {
            // XXX: The DataType Expressions Saga continues

            struct token* id = parse_tree->children[2].children[0].terminal;
            struct identifier ident;
            identifier_init(&ident);
            lower_identifier(&ident, id);

            struct expr expr1;
            expr_init(&expr1);
            expr1.srctok =
                &module->tokens[parse_tree->children[2].children[0].start];
            expr1.tag = EXPR_PRIMARY_IDENTIFIER;
            expr1.id = ident;

            expr->unary.rhs_expr = nalloc(sizeof(struct expr));
            *expr->unary.rhs_expr = expr1;
            expr->tag = EXPR_UNARY_COUNTOF_EXPR;
        }
        // ExpressionOrDataType -> NonterminalExpressionOrDatatype
        else
        {
            // NonterminalExpressionOrDatatype -> Expression
            if (parse_tree->children[2].children[0].rule_index
                == RULE_EXPRORDT_EXPRESSION)
            {
                // XXX: The DataType Expressions Saga continues

                struct expr expr1;
                expr_init(&expr1);
                lower_expression(
                    &expr1,
                    &parse_tree->children[2].children[0].children[0],
                    module);

                expr->unary.rhs_expr = nalloc(sizeof(struct expr));
                *expr->unary.rhs_expr = expr1;
                expr->tag = EXPR_UNARY_COUNTOF_EXPR;
            }
            // NonterminalExpressionOrDatatype -> Datatype
            if (parse_tree->children[2].children[0].rule_index
                == RULE_EXPRORDT_DATATYPE)
            {
                // XXX: The DataType Expressions Saga continues

                struct data_type dt;
                data_type_init(&dt);
                lower_data_type(
                    &dt,
                    &parse_tree->children[2].children[0].children[0],
                    module);

                expr->unary.rhs_dt = nalloc(sizeof(struct data_type));
                *expr->unary.rhs_dt = dt;
                expr->tag = EXPR_UNARY_COUNTOF_DT;
            }
        }

        break;
    }
    case RULE_EXPRESSION_POSTFIX_A:
    {
        // ExpressionPostfix -> 'Identifier' ListPostfix

        struct token* id = parse_tree->children[0].terminal;
        struct identifier ident;
        identifier_init(&ident);
        lower_identifier(&ident, id);
        expr->id = ident;
        expr->tag = EXPR_PRIMARY_IDENTIFIER;

        // ListPostfix -> EMPTY
        if (parse_tree->children[1].tag == PARSE_TREE_EPSILON)
        {
            // do nothing
        }

        // ListPostfix -> NonemptyListPostfix
        else
        {
            struct expr postfixy_expr;
            expr_init(&postfixy_expr);

            struct parse_tree* root = &parse_tree->children[1].children[0];
            struct expr* p = &postfixy_expr;

            // NonemptyListPostfix -> NonemptyListPostfix Postfix
            if (root->rule_index
                == RULE_EXPRESSION_LIST_POSTFIX_NONEMPTY_RECURSE)
            {
                lower_postfix_expression(p, &root->children[1], module);
                root = &root->children[0];

                // NonemptyListPostfix -> NonemptyListPostfix Postfix
                while (root->rule_index
                       == RULE_EXPRESSION_LIST_POSTFIX_NONEMPTY_RECURSE)
                {
                    struct expr lhs;
                    expr_init(&lhs);
                    lower_postfix_expression(&lhs, &root->children[1], module);
                    p->postfix.lhs = nalloc(sizeof(struct expr));
                    *p->postfix.lhs = lhs;
                    p->postfix.lhs->srctok =
                        &module->tokens[root->children[1].start];
                    p = p->postfix.lhs;

                    root = &root->children[0];
                }
                // NonemptyListPostfix -> Postfix
                struct expr lhs;
                expr_init(&lhs);
                lower_postfix_expression(&lhs, &root->children[0], module);
                p->postfix.lhs = nalloc(sizeof(struct expr));
                *p->postfix.lhs = lhs;
                p->postfix.lhs->srctok =
                    &module->tokens[root->children[0].start];
                p = p->postfix.lhs;
            }
            // NonemptyListPostfix -> Postfix
            else
            {
                lower_postfix_expression(p, &root->children[0], module);
            }

            p->postfix.lhs = nalloc(sizeof(struct expr));
            *p->postfix.lhs = *expr;
            p->postfix.lhs->srctok = &module->tokens[parse_tree->start];

            *expr = postfixy_expr;
        }

        break;
    }
    case RULE_EXPRESSION_POSTFIX_B:
    {
        // ExpressionPostfix -> '(' Expression ')' ListPostfix

        struct expr exp;
        expr_init(&exp);
        lower_expression(&exp, &parse_tree->children[1], module);
        expr->tag = EXPR_PRIMARY_PAREN;
        expr->paren = nalloc(sizeof(struct expr));
        *expr->paren = exp;
        expr->srctok = &module->tokens[parse_tree->start];

        // ListPostfix -> EMPTY
        if (parse_tree->children[3].tag == PARSE_TREE_EPSILON)
        {
            // do nothing
        }

        // ListPostfix -> NonemptyListPostfix
        else
        {
            struct expr postfixy_expr;
            expr_init(&postfixy_expr);

            struct parse_tree* root = &parse_tree->children[3].children[0];
            struct expr* p = &postfixy_expr;

            // NonemptyListPostfix -> NonemptyListPostfix Postfix
            if (root->rule_index
                == RULE_EXPRESSION_LIST_POSTFIX_NONEMPTY_RECURSE)
            {
                lower_postfix_expression(p, &root->children[1], module);
                root = &root->children[0];

                // NonemptyListPostfix -> NonemptyListPostfix Postfix
                while (root->rule_index
                       == RULE_EXPRESSION_LIST_POSTFIX_NONEMPTY_RECURSE)
                {
                    struct expr lhs;
                    expr_init(&lhs);
                    lower_postfix_expression(&lhs, &root->children[1], module);
                    p->postfix.lhs = nalloc(sizeof(struct expr));
                    *p->postfix.lhs = lhs;
                    p->postfix.lhs->srctok =
                        &module->tokens[root->children[1].start];
                    p = p->postfix.lhs;

                    root = &root->children[0];
                }
                // NonemptyListPostfix -> Postfix
                struct expr lhs;
                expr_init(&lhs);
                lower_postfix_expression(&lhs, &root->children[0], module);
                p->postfix.lhs = nalloc(sizeof(struct expr));
                *p->postfix.lhs = lhs;
                p->postfix.lhs->srctok =
                    &module->tokens[root->children[0].start];
                p = p->postfix.lhs;
            }
            // NonemptyListPostfix -> Postfix
            else
            {
                lower_postfix_expression(p, &root->children[0], module);
            }

            p->postfix.lhs = nalloc(sizeof(struct expr));
            *p->postfix.lhs = *expr;
            p->postfix.lhs->srctok = &module->tokens[parse_tree->start];

            *expr = postfixy_expr;
        }

        break;
    }
    case RULE_EXPRESSION_LITERAL_ARRAY_A:
    {
        // Expression20 -> '[' ListLiteral ']'
        struct literal literal;
        literal_init(&literal);
        literal.srctok = &module->tokens[parse_tree->children[0].start];

        narr_t(struct literal) array = narr_alloc(0, sizeof(struct literal));
        lower_list_arg(&array, &parse_tree->children[1], module);

        literal.tag = LITERAL_ARRAY;
        literal.array = array;

        expr->tag = EXPR_PRIMARY_LITERAL;
        expr->literal = nalloc(sizeof(struct literal));
        *expr->literal = literal;

        break;
    }
    case RULE_EXPRESSION_LITERAL_ARRAY_B:
    {
        // Expression20 -> '[' Literal ',' '..' LiteralU ']'

        narr_t(struct literal) array = narr_alloc(0, sizeof(struct literal));

        struct literal literal;
        literal_init(&literal);
        literal.srctok = &module->tokens[parse_tree->children[0].start];

        struct literal value;
        literal_init(&value);
        lower_literal(&value, &parse_tree->children[1], module);

        array = narr_push(array, &value);

        struct literal dotdot;
        literal_init(&dotdot);
        dotdot.srctok = &module->tokens[parse_tree->children[3].start];
        dotdot.tag = LITERAL_ARRAY_FILL;

        struct literal iteration_count;
        literal_init(&iteration_count);
        lower_literal(&iteration_count, &parse_tree->children[4], module);
        dotdot.array_fill_length = (size_t)iteration_count.u;
        literal_fini(&iteration_count);

        array = narr_push(array, &dotdot);

        literal.tag = LITERAL_ARRAY;
        literal.array = array;

        expr->tag = EXPR_PRIMARY_LITERAL;
        expr->literal = nalloc(sizeof(struct literal));
        *expr->literal = literal;

        break;
    }
    // NOTE: Fallthrough to lower terminal
    case RULE_EXPRESSION:
        // Expression -> Expression1
    case RULE_EXPRESSION_EXPRESSION1_B:
        // Expression1 -> Expression2
    case RULE_EXPRESSION_EXPRESSION2_B:
        // Expression2 -> Expression3
    case RULE_EXPRESSION_EXPRESSION3_B:
        // Expression3 -> Expression4
    case RULE_EXPRESSION_EXPRESSION4_B:
        // Expression4 -> Expression5
    case RULE_EXPRESSION_EXPRESSION5_B:
        // Expression5 -> Expression6
    case RULE_EXPRESSION_EXPRESSION6_C:
        // Expression6 -> Expression7
    case RULE_EXPRESSION_EXPRESSION7_E:
        // Expression7 -> Expression8
    case RULE_EXPRESSION_EXPRESSION8_C:
        // Expression8 -> Expression9
    case RULE_EXPRESSION_EXPRESSION9_F:
        // Expression9 -> Expression10
    case RULE_EXPRESSION_EXPRESSION10_C:
        // Expression10 -> Expression11
    case RULE_EXPRESSION_EXPRESSION11_B:
        // Expression11 -> Expression12
    case RULE_EXPRESSION_EXPRESSION_UNARY_POSTFIX:
        // Expression12 -> ExpressionPostfix
    case RULE_EXPRESSION_POSTFIX_C:
        // ExpressionPostfix -> ExpressionLiteral
        {
            lower_expression(expr, &parse_tree->children[0], module);

            break;
        }
    // NOTE: Fallthrough to lower literal
    case RULE_EXPRESSION_LITERAL_U:
        // ExpressionLiteral -> 'U'
    case RULE_EXPRESSION_LITERAL_U8:
        // ExpressionLiteral -> 'U8'
    case RULE_EXPRESSION_LITERAL_U16:
        // ExpressionLiteral -> 'U16'
    case RULE_EXPRESSION_LITERAL_U32:
        // ExpressionLiteral -> 'U32'
    case RULE_EXPRESSION_LITERAL_U64:
        // ExpressionLiteral -> 'U64'
    case RULE_EXPRESSION_LITERAL_S:
        // ExpressionLiteral -> 'S'
    case RULE_EXPRESSION_LITERAL_S8:
        // ExpressionLiteral -> 'S8'
    case RULE_EXPRESSION_LITERAL_S16:
        // ExpressionLiteral -> 'S16'
    case RULE_EXPRESSION_LITERAL_S32:
        // ExpressionLiteral -> 'S32'
    case RULE_EXPRESSION_LITERAL_S64:
        // ExpressionLiteral -> 'S64'
    case RULE_EXPRESSION_LITERAL_F32:
        // ExpressionLiteral -> 'F32'
    case RULE_EXPRESSION_LITERAL_F64:
        // ExpressionLiteral -> 'F64'
    case RULE_EXPRESSION_LITERAL_ASCII:
        // ExpressionLiteral -> 'ASCII'
    case RULE_EXPRESSION_LITERAL_ASTRING:
        // ExpressionLiteral -> 'ASTRING'
    case RULE_EXPRESSION_LITERAL_TRUE:
        // ExpressionLiteral -> 'TRUE'
    case RULE_EXPRESSION_LITERAL_FALSE:
        // ExpressionLiteral -> 'FALSE'
    case RULE_EXPRESSION_LITERAL_NULL:
        // ExpressionLiteral -> 'NULL'
        {
            struct literal literal;
            literal_init(&literal);
            lower_literal(&literal, &parse_tree->children[0], module);

            expr->literal = nalloc(sizeof(struct literal));
            *expr->literal = literal;
            expr->tag = EXPR_PRIMARY_LITERAL;

            break;
        }
    default:
    {
        NPANIC_DFLT_CASE();

        break;
    }
    }

    // Postconditions.
    nassert(NULL != expr->srctok);
}

void lower_literal(
    struct literal* literal,
    struct parse_tree* parse_tree,
    struct module* module)
{
    if (parse_tree->tag == PARSE_TREE_NONTERMINAL)
    {
        switch (parse_tree->rule_index)
        {
        case RULE_EXPRESSION_LITERAL_ARRAY_A:
        {
            // Literal -> '[' ListLiteral ']'
            narr_t(struct literal) array =
                narr_alloc(0, sizeof(struct literal));
            lower_list_arg(&array, &parse_tree->children[1], module);
            literal->tag = LITERAL_ARRAY;
            literal->array = array;

            literal->srctok = &module->tokens[parse_tree->start];

            break;
        }
        case RULE_EXPRESSION_LITERAL_ARRAY_B:
        {
            // Literal -> '[' Literal ',' '..' LiteralU ']'

            narr_t(struct literal) array =
                narr_alloc(0, sizeof(struct literal));

            struct literal value;
            literal_init(&value);
            lower_literal(&value, &parse_tree->children[1], module);

            array = narr_push(array, &value);

            struct literal dotdot;
            literal_init(&dotdot);
            dotdot.srctok = &module->tokens[parse_tree->children[3].start];
            dotdot.tag = LITERAL_ARRAY_FILL;

            struct literal iteration_count;
            literal_init(&iteration_count);
            lower_literal(&iteration_count, &parse_tree->children[4], module);
            dotdot.array_fill_length = (size_t)iteration_count.u;
            literal_fini(&iteration_count);

            array = narr_push(array, &dotdot);

            literal->tag = LITERAL_ARRAY;
            literal->array = array;

            literal->srctok = &module->tokens[parse_tree->start];

            break;
        }
        default:
        {
            lower_literal(literal, &parse_tree->children[0], module);

            break;
        }
        }
    }
    else
    {
        switch (parse_tree->terminal->tag)
        {
        case TOKEN_LITERAL_U:
        {
            literal->u = PARSE_N_INTEGER(n_u, parse_tree->terminal);
            literal->tag = LITERAL_U;
            literal->srctok = parse_tree->terminal;

            break;
        }
        case TOKEN_LITERAL_U8:
        {
            literal->u8 = PARSE_N_INTEGER(n_u8, parse_tree->terminal);
            literal->tag = LITERAL_U8;
            literal->srctok = parse_tree->terminal;

            break;
        }
        case TOKEN_LITERAL_U16:
        {
            literal->u16 = PARSE_N_INTEGER(n_u16, parse_tree->terminal);
            literal->tag = LITERAL_U16;
            literal->srctok = parse_tree->terminal;

            break;
        }
        case TOKEN_LITERAL_U32:
        {
            literal->u32 = PARSE_N_INTEGER(n_u32, parse_tree->terminal);
            literal->tag = LITERAL_U32;
            literal->srctok = parse_tree->terminal;

            break;
        }
        case TOKEN_LITERAL_U64:
        {
            literal->u64 = PARSE_N_INTEGER(n_u64, parse_tree->terminal);
            literal->tag = LITERAL_U64;
            literal->srctok = parse_tree->terminal;

            break;
        }
        case TOKEN_LITERAL_S:
        {
            literal->s = PARSE_N_INTEGER(n_s, parse_tree->terminal);
            literal->tag = LITERAL_S;
            literal->srctok = parse_tree->terminal;

            break;
        }
        case TOKEN_LITERAL_S8:
        {
            literal->s8 = PARSE_N_INTEGER(n_s8, parse_tree->terminal);
            literal->tag = LITERAL_S8;
            literal->srctok = parse_tree->terminal;

            break;
        }
        case TOKEN_LITERAL_S16:
        {
            literal->s16 = PARSE_N_INTEGER(n_s16, parse_tree->terminal);
            literal->tag = LITERAL_S16;
            literal->srctok = parse_tree->terminal;

            break;
        }
        case TOKEN_LITERAL_S32:
        {
            literal->s32 = PARSE_N_INTEGER(n_s32, parse_tree->terminal);
            literal->tag = LITERAL_S32;
            literal->srctok = parse_tree->terminal;

            break;
        }
        case TOKEN_LITERAL_S64:
        {
            literal->s64 = PARSE_N_INTEGER(n_s64, parse_tree->terminal);
            literal->tag = LITERAL_S64;
            literal->srctok = parse_tree->terminal;

            break;
        }
        case TOKEN_LITERAL_F32:
        {
            literal->f32.start = parse_tree->terminal->start;
            literal->f32.length = parse_tree->terminal->length;
            literal->tag = LITERAL_F32;
            literal->srctok = parse_tree->terminal;

            break;
        }
        case TOKEN_LITERAL_F64:
        {
            literal->f64.start = parse_tree->terminal->start;
            literal->f64.length = parse_tree->terminal->length;
            literal->tag = LITERAL_F64;
            literal->srctok = parse_tree->terminal;

            break;
        }
        case TOKEN_LITERAL_ASCII:
        {
            n_ascii nch;
            parse_n_ascii_literal(parse_tree->terminal->start, &nch);

            literal->achar = nch;
            literal->tag = LITERAL_ACHAR;
            literal->srctok = parse_tree->terminal;

            break;
        }
        case TOKEN_LITERAL_ASTRING:
        {
            nstr_t nstr_defer_fini s;
            nstr_init(&s);

            parse_n_astr_literal(parse_tree->terminal->start, &s);

            nstr_assign_nstr(&literal->astr, &s);
            literal->tag = LITERAL_ASTR;
            literal->srctok = parse_tree->terminal;

            break;
        }
        case TOKEN_KEYWORD_TRUE:
        {
            literal->boolean = true;
            literal->tag = LITERAL_BOOL;
            literal->srctok = parse_tree->terminal;

            break;
        }
        case TOKEN_KEYWORD_FALSE:
        {
            literal->boolean = false;
            literal->tag = LITERAL_BOOL;
            literal->srctok = parse_tree->terminal;

            break;
        }
        case TOKEN_KEYWORD_NULL:
        {
            literal->tag = LITERAL_NULL;
            literal->srctok = parse_tree->terminal;

            break;
        }
        default:
        {
            NPANIC_DFLT_CASE();

            break;
        }
        }
    }

    // Postconditions.
    nassert(NULL != literal->srctok);
}

void lower_list_struct_member(
    struct struct_def* def,
    struct parse_tree* parse_tree,
    struct module* module)
{
    narr_t(struct parse_tree) values = narr_alloc(0, sizeof(struct parse_tree));

    struct parse_tree* root = parse_tree;

    // ListStructMember -> ListStructMember StructMember
    while (root->rule_index == RULE_LIST_STRUCTMEMBER_RECURSE)
    {
        values = narr_push(values, &root->children[1]);
        root = &root->children[0];
    }
    // ListStructMember -> StructMember
    values = narr_push(values, &root->children[0]);

    for (uint64_t i = 0; i < narr_length(values); ++i)
    {
        uint64_t j = narr_length(values) - i - 1;
        struct parse_tree* exp = &values[j];

        switch (exp->rule_index)
        {
        case RULE_STRUCTMEMBER_VARIABLE:
        {
            // StructMember -> 'LET' 'IDENTIFIER' ':' DataType ';'
            struct member_var member_var;
            member_var_init(&member_var);

            struct token* id = exp->children[1].terminal;
            lower_identifier(&member_var.id, id);
            lower_data_type(&member_var.dt, &exp->children[3], module);

            member_var.srctok = &module->tokens[exp->start];

            def->member_vars = narr_push(def->member_vars, &member_var);

            break;
        }
        case RULE_STRUCTMEMBER_FUNCTION:
        {
            // StructMember -> 'FUNC' 'IDENTIFIER' '=' 'IDENTIFIER' ';'
            struct member_func member_func;
            member_func_init(&member_func);

            struct token* id = exp->children[1].terminal;
            lower_identifier(&member_func.id, id);
            struct token* tid = exp->children[3].terminal;
            lower_identifier(&member_func.target.id, tid);

            member_func.srctok = &module->tokens[exp->start];

            def->member_funcs = narr_push(def->member_funcs, &member_func);

            break;
        }
        default:
        {
            NPANIC_DFLT_CASE();

            break;
        }
        }
    }
    narr_free(values);

    def->srctok = &module->tokens[parse_tree->start];
}

void lower_argwithtype(
    struct function_decl* decl,
    struct parse_tree* parse_tree,
    struct module* module)
{
    // ArgWithType -> 'Identifier' ':' DataType

    struct identifier id;
    identifier_init(&id);
    lower_identifier(&id, parse_tree->children[0].terminal);

    decl->param_ids = narr_push(decl->param_ids, &id);

    struct data_type dt;
    data_type_init(&dt);
    lower_data_type(&dt, &parse_tree->children[2], module);

    decl->dt.func_sig->param_dts = narr_push(decl->dt.func_sig->param_dts, &dt);
}

void lower_list_argwithtype(
    struct function_decl* decl,
    struct parse_tree* parse_tree,
    struct module* module)
{
    // ListArgWithType -> EMPTY
    if (parse_tree->tag == PARSE_TREE_EPSILON)
    {
        return;
    }

    // ListArgWithType -> NonEmptyListArgWithType
    parse_tree = &parse_tree->children[0];

    narr_t(struct parse_tree) values = narr_alloc(0, sizeof(struct parse_tree));
    // NonEmptyListArgWithType -> NonEmptyListArgWithType ',' ArgWithType
    while (parse_tree->rule_index
           == RULE_LIST_ARGUMENTWITHTYPE_NONEMPTY_RECURSE)
    {
        values = narr_push(values, &parse_tree->children[2]);
        parse_tree = &parse_tree->children[0];
    }
    // NonEmptyListArgWithType -> ArgWithType
    values = narr_push(values, &parse_tree->children[0]);

    for (uint64_t i = 0; i < narr_length(values); ++i)
    {
        uint64_t j = narr_length(values) - i - 1;
        lower_argwithtype(decl, &values[j], module);
    }
    narr_free(values);
}

void lower_list_arg(
    narr_t(struct literal) * array,
    struct parse_tree* parse_tree,
    struct module* module)
{
    // ListLiteral -> ListLiteral ',' ExpressionTerminal
    narr_t(struct parse_tree) values = narr_alloc(0, sizeof(struct parse_tree));
    while (parse_tree->rule_index == RULE_LIST_LITERAL_NONEMPTY_RECURSE)
    {
        values = narr_push(values, &parse_tree->children[2]);
        parse_tree = &parse_tree->children[0];
    }
    // ListLiteral -> ExpressionTerminal
    values = narr_push(values, &parse_tree->children[0]);

    for (uint64_t i = 0; i < narr_length(values); ++i)
    {
        uint64_t j = narr_length(values) - i - 1;
        struct literal literal;
        literal_init(&literal);
        lower_literal(&literal, &values[j], module);
        *array = narr_push(*array, &literal);
    }
    narr_free(values);
}

void lower_list_funcarg(
    narr_t(struct expr) * array,
    struct parse_tree* parse_tree,
    struct module* module)
{
    // FunctionArguments -> EMPTY
    if (parse_tree->tag == PARSE_TREE_EPSILON)
    {
        return;
    }

    // FunctionArguments -> ListArgument
    parse_tree = &parse_tree->children[0];

    narr_t(struct parse_tree) values = narr_alloc(0, sizeof(struct parse_tree));
    // ListArgument -> ListArgument ',' Expression
    while (parse_tree->rule_index == RULE_LIST_ARGUMENT_NONEMPTY_RECURSE)
    {
        values = narr_push(values, &parse_tree->children[2]);
        parse_tree = &parse_tree->children[0];
    }
    // ListArgument -> Expression
    values = narr_push(values, &parse_tree->children[0]);

    for (uint64_t i = 0; i < narr_length(values); ++i)
    {
        uint64_t j = narr_length(values) - i - 1;
        struct expr exp;
        expr_init(&exp);
        lower_expression(&exp, &values[j], module);
        *array = narr_push(*array, &exp);
    }
    narr_free(values);
}

void lower_if_branch(
    struct if_stmt* ifstmt,
    struct parse_tree* parse_tree,
    struct module* module)
{
    // IfBranch -> Expression '{' ListBlockStatement '}'
    struct parse_tree* expression = &parse_tree->children[0];

    struct expr expr;
    expr_init(&expr);
    lower_expression(&expr, expression, module);

    ifstmt->if_conditions = narr_push(ifstmt->if_conditions, &expr);

    struct parse_tree* stmts = &parse_tree->children[2];

    struct scope block;
    scope_init(&block, UNDEFINED_SCOPE_PARENT);
    block.srctok = &module->tokens[parse_tree->children[1].start];

    lower_list_block_statement(&block, stmts, module);

    ifstmt->if_blocks = narr_push(ifstmt->if_blocks, &block);
}

void lower_identifier(struct identifier* ident, struct token* id)
{
    ident->start = id->start;
    ident->length = id->length;
    ident->srctok = id;
}

void lower_binary_expression(
    struct expr* expr, struct parse_tree* parse_tree, struct module* module)
{
    struct expr expr1;
    expr_init(&expr1);
    lower_expression(&expr1, &parse_tree->children[0], module);

    expr->binary.lhs_expr = nalloc(sizeof(struct expr));
    *expr->binary.lhs_expr = expr1;

    struct expr expr2;
    expr_init(&expr2);
    lower_expression(&expr2, &parse_tree->children[2], module);

    expr->binary.rhs_expr = nalloc(sizeof(struct expr));
    *expr->binary.rhs_expr = expr2;

    expr->srctok = &module->tokens[parse_tree->start];
}

void lower_unary_expression(
    struct expr* expr, struct parse_tree* parse_tree, struct module* module)
{
    struct expr expr1;
    expr_init(&expr1);
    lower_expression(&expr1, &parse_tree->children[1], module);

    expr->unary.rhs_expr = nalloc(sizeof(struct expr));
    *expr->unary.rhs_expr = expr1;

    // Postconditions.
    nassert(NULL != expr->srctok);
}

void lower_postfix_expression(
    struct expr* expr, struct parse_tree* parse_tree, struct module* module)
{
    expr->srctok = &module->tokens[parse_tree->start];

    switch (parse_tree->rule_index)
    {
    case RULE_EXPRESSION_POSTFIX_BASE_A:
    {
        // ExpressionPostPostfix -> '->' 'Identifier'

        struct identifier id;
        identifier_init(&id);
        lower_identifier(&id, parse_tree->children[1].terminal);

        expr->tag = EXPR_POSTFIX_ACCESS_ARROW;
        expr->postfix.access.id = id;

        break;
    }
    case RULE_EXPRESSION_POSTFIX_BASE_B:
    {
        // ExpressionPostPostfix -> '.' 'Identifier'

        struct identifier id;
        identifier_init(&id);
        lower_identifier(&id, parse_tree->children[1].terminal);

        expr->tag = EXPR_POSTFIX_ACCESS_DOT;
        expr->postfix.access.id = id;

        break;
    }
    case RULE_EXPRESSION_POSTFIX_BASE_C:
    {
        // ExpressionPostPostfix -> '(' FunctionArguments ')'

        narr_t(struct expr) args = narr_alloc(0, sizeof(struct expr));
        lower_list_funcarg(&args, &parse_tree->children[1], module);
        expr->tag = EXPR_POSTFIX_FUNCTION_CALL;
        expr->postfix.args = args;

        break;
    }
    case RULE_EXPRESSION_POSTFIX_BASE_D:
    {
        // ExpressionPostPostfix -> '[' Expression ']'

        struct expr exp;
        expr_init(&exp);
        lower_expression(&exp, &parse_tree->children[1], module);

        expr->tag = EXPR_POSTFIX_SUBSCRIPT;
        expr->postfix.subscript = nalloc(sizeof(struct expr));
        *expr->postfix.subscript = exp;

        break;
    }
    default:
    {
        NPANIC_DFLT_CASE();

        break;
    }
    }

    // Postconditions.
    nassert(NULL != expr->srctok);
}

void lower_variant_def(struct variant_def* this, struct parse_tree* tree,
    struct module* module)
{
    nassert(NONTERMINAL_VARIANTMEMBER_LIST == tree->nonterminal);

    narr_t(struct parse_tree) members =
        narr_alloc(0, sizeof(struct parse_tree));
    struct parse_tree* list = tree;

    // List_VariantMember -> List_VariantMember VariantMember
    // left recursion must get reversed so it reads front to back.
    while(RULE_VARIANTMEMBER_LIST_RECURSE == list->rule_index)
    {
        members = narr_push(members, &list->children[1]);
        list = &list->children[0];
    }
    members = narr_push(members, &list->children[0]);

    // Reverse traversal, because the grammar has left recursive grammar.
    const ssize_t n_members = (ssize_t) narr_length(members);
    for(ssize_t i = n_members - 1; i >= 0; --i)
    {
        switch(members[i].rule_index)
        {
        case RULE_VARIANTMEMBER_VARIABLE:
        {
            // VariantMember -> member_variable
            struct member_var mv;
            member_var_init(&mv);
            lower_member_var(&mv, &members[i], module);
            this->member_vars = narr_push(this->member_vars, &mv);
            break;
        }
        case RULE_VARIANTMEMBER_FUNCTION:
        {
            // VariantMember -> member_function
            struct member_func mf;
            member_func_init(&mf);
            lower_member_func(&mf, &members[i], module);
            this->member_funcs = narr_push(this->member_funcs, &mf);
            break;
        }
        case RULE_VARIANTMEMBER_IFACE:
        {
            // VariantMember -> member_interface
            struct member_iface mi;
            member_iface_init(&mi);
            lower_member_iface(&mi, &members[i], module);
            this->member_ifaces = narr_push(this->member_ifaces, &mi);
            break;
        }
        case RULE_VARIANTMEMBER_SECTION_EMPTY: /* Fallthrough */
        case RULE_VARIANTMEMBER_SECTION:
        {
            // VariantMember -> list_variant_tags '{' struct_definition '}'
            lower_member_sections(this, &members[i], module);
            break;
        }
        default:
        {
            NPANIC_DFLT_CASE();
        }
        }
    }
    narr_free(members);

    this->srctok = &module->tokens[tree->start];
}


void lower_member_var(struct member_var* mv, struct parse_tree* tree,
        struct module* module)
{
    nassert(RULE_VARIANTMEMBER_VARIABLE == tree->rule_index ||
            RULE_STRUCTMEMBER_VARIABLE == tree->rule_index ||
            RULE_VARIANTMEMBER_SECTIONMEMBER_VARIABLE == tree->rule_index);

    // member_variable -> let identifier ':' data_type ';'

    lower_identifier(&mv->id, tree->children[1].terminal);
    lower_data_type(&mv->dt, &tree->children[3], module);

    mv->srctok = &module->tokens[tree->start];
}

void lower_member_func(struct member_func* mf, struct parse_tree* tree,
        struct module* module)
{
    nassert(RULE_VARIANTMEMBER_FUNCTION == tree->rule_index ||
            RULE_STRUCTMEMBER_FUNCTION == tree->rule_index ||
            RULE_VARIANTMEMBER_SECTIONMEMBER_FUNCTION == tree->rule_index);

    // member_function -> func identifier '=' identifier ';'

    lower_identifier(&mf->id, tree->children[1].terminal);
    lower_identifier(&mf->target.id, tree->children[3].terminal);

    mf->srctok = &module->tokens[tree->start];
}

void lower_function_signature(struct func_sig* fs,
        struct parse_tree* tree, struct module* module)
{
    nassert(RULE_FUNCTION_SIGNATURE == tree->rule_index);

    // function_signature -> '(' ListDataType ')' '->' ReturnDataType

    if (tree->children[1].tag == PARSE_TREE_EPSILON)
    {
        // ListDataType -> EMPTY
    }
    else
    {
        // ListDataType -> NonemptyListDatatype
        struct parse_tree* root = &tree->children[1].children[0];
        narr_t(struct parse_tree) types = narr_alloc(0, sizeof(struct parse_tree));
        while(root->rule_index == RULE_LIST_DATATYPE_NONEMPTY_RECURSE)
        {
            // NonemptyListDatatype -> NonemptyListDatatype ',' DataType
            types = narr_push(types, &root->children[2]);
            root = &root->children[0];
        }
        // NonemptyListDatatype -> DataType
        types = narr_push(types, &root->children[0]);

        for(uint64_t i = 0; i < narr_length(types); i += 1)
        {
            uint64_t j = narr_length(types) - i - 1;
            struct data_type x;
            data_type_init(&x);
            lower_data_type(&x, &types[j], module);
            fs->param_dts = narr_push(fs->param_dts, &x);
        }
        narr_free(types);
    }

    // ReturnDataType-> DataType
    lower_data_type(&fs->return_dt, &tree->children[4], module);
}

void lower_member_iface(struct member_iface* mi, struct parse_tree* tree,
        struct module* module)
{
    nassert(RULE_VARIANTMEMBER_IFACE == tree->rule_index);

    // member_iface -> iface identifier ':' function_signature

    lower_identifier(&mi->id, tree->children[1].terminal);
    lower_function_signature(&mi->sig, &tree->children[3], module);

    mi->srctok = &module->tokens[tree->start];
}

void lower_member_sections(struct variant_def* variant, struct parse_tree* tree,
        struct module* module)
{
    nassert(RULE_VARIANTMEMBER_SECTION == tree->rule_index ||
            RULE_VARIANTMEMBER_SECTION_EMPTY == tree->rule_index);
    // member_section -> tag_list '{' '}'
    // member_section -> tag_list '{' section_def '}'

    narr_t(struct parse_tree) tags =
        narr_alloc(0, sizeof(struct parse_tree));
    narr_t(struct member_tag) members =
        narr_alloc(0, sizeof(struct member_tag));
    struct parse_tree* taglist = &tree->children[0];

    // tag_list -> tag_list ',' tag
    // left recursion must be reversed to read left to right.
    nassert(NONTERMINAL_VARIANTMEMBER_TAGLIST == taglist->nonterminal);
    while(RULE_VARIANTMEMBER_TAGLIST_RECURSE == taglist->rule_index)
    {
        tags = narr_push(tags, &taglist->children[2]);
        taglist = &taglist->children[0];
    }
    // tag_list -> tag
    tags = narr_push(tags, &taglist->children[0]);

    // backwards traversal for left recursion
    size_t n_tags = narr_length(tags);
    for(ssize_t i = (ssize_t) n_tags - 1; i >= 0; --i)
    {
        struct parse_tree* tag = &tags[i];
        nassert(NONTERMINAL_VARIANTMEMBER_TAG == tag->nonterminal);
        struct member_tag mt;
        member_tag_init(&mt);
        lower_identifier(&mt.id, tag->children[0].terminal);
        switch(tag->rule_index)
        {
        // tag -> identifier : u8_literal
        case RULE_VARIANTMEMBER_TAG_WITH_U8_VAL:  /* Fallthrough */
        // tag -> identifier : u16_literal
        case RULE_VARIANTMEMBER_TAG_WITH_U16_VAL: /* Fallthrough */
        // tag -> identifier : u32_literal
        case RULE_VARIANTMEMBER_TAG_WITH_U32_VAL: /* Fallthrough */
        // tag -> identifier : u64_literal
        case RULE_VARIANTMEMBER_TAG_WITH_U64_VAL: /* Fallthrough */
        // tag -> identifier : u_literal
        case RULE_VARIANTMEMBER_TAG_WITH_U_VAL:   /* Fallthrough */
        // tag -> identifier : s8_literal
        case RULE_VARIANTMEMBER_TAG_WITH_S8_VAL:  /* Fallthrough */
        // tag -> identifier : s16_literal
        case RULE_VARIANTMEMBER_TAG_WITH_S16_VAL: /* Fallthrough */
        // tag -> identifier : s32_literal
        case RULE_VARIANTMEMBER_TAG_WITH_S32_VAL: /* Fallthrough */
        // tag -> identifier : s64_literal
        case RULE_VARIANTMEMBER_TAG_WITH_S64_VAL: /* Fallthrough */
        // tag -> identifier : s_literal
        case RULE_VARIANTMEMBER_TAG_WITH_S_VAL:   /* Fallthrough */
        {
            lower_literal(&mt.value, &tag->children[2], module);
            break;
        }
        case RULE_VARIANTMEMBER_TAG_NO_VAL:
        {
            // tag -> identifier
            mt.value.tag = LITERAL_UNSET;
            mt.value.srctok = &module->tokens[tag->start];
            break;
        }
        default:
        {
            NPANIC_DFLT_CASE();
        }
        }

        mt.srctok = &module->tokens[tag->start];
        members = narr_push(members, &mt);
    }

    // lower the section
    // section -> '{' section_member_list '}'
    struct struct_def sd;
    struct_def_init(&sd);
    sd.srctok = tree->children[1].terminal;

    if(RULE_VARIANTMEMBER_SECTION == tree->rule_index)
    {
        struct parse_tree* section = &tree->children[2];
        narr_t(struct parse_tree) section_members =
            narr_alloc(0, sizeof(struct parse_tree));
        nassert(RULE_VARIANTMEMBER_SECTIONMEMBER_LIST_RECURSE ==
                section->rule_index ||
                RULE_VARIANTMEMBER_SECTIONMEMBER_LIST == section->rule_index);
        // section_member_list -> section_member_list section_member
        // Left recursive statement will get reversed to read forward.
        while(RULE_VARIANTMEMBER_SECTIONMEMBER_LIST_RECURSE ==
                section->rule_index)
        {
            section_members =
                narr_push(section_members, &section->children[1]);
            section = &section->children[0];
        }
        // section_member_list -> section_member
        section_members = narr_push(section_members, &section->children[0]);

        // Reverse traversal because of left recursion.
        size_t n_section_members = narr_length(section_members);
        for(ssize_t i = (ssize_t) n_section_members - 1; i >= 0; --i)
        {
            struct parse_tree* member = &section_members[i];
            nassert(NONTERMINAL_VARIANTMEMBER_SECTIONMEMBER ==
                    member->nonterminal);
            switch(member->rule_index)
            {
            case RULE_VARIANTMEMBER_SECTIONMEMBER_VARIABLE:
            {
                // section_member -> member_variable
                struct member_var mv;
                member_var_init(&mv);
                lower_member_var(&mv, member, module);
                sd.member_vars = narr_push(sd.member_vars, &mv);
                break;
            }
            case RULE_VARIANTMEMBER_SECTIONMEMBER_FUNCTION:
            {
                // section_member -> member_function
                struct member_func mf;
                member_func_init(&mf);
                lower_member_func(&mf, member, module);
                sd.member_funcs = narr_push(sd.member_funcs, &mf);
                break;
            }
            default:
            {
                NPANIC_DFLT_CASE();
            }
            }
        }
        narr_free(section_members);
    }
    // else if { RULE_VARIANTMEMBER_SECTION_EMPTY }
    // {
    //     // section -> '{' '}'
    // }

    //insert the struct into the variant member_tags
    size_t n_members = narr_length(members);
    for(size_t i = 0; i < n_members; ++i)
    {
        struct_def_init_struct_def(&sd, &members[i].section);

        variant->member_tags = narr_push(variant->member_tags, &members[i]);
    }

    narr_free(tags);
    narr_free(members);

    struct_def_fini(&sd);
}
